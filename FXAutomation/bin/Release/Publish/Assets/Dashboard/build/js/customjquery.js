$(function () {
    
    //#region Case Form Started
    GetCaseFormCaseTypeGrid();
    $("#CaseForm_CaseType_Id").change(function (event) {
        //debugger;
        GetCaseFormCaseTypeGrid();
    });
    function GetCaseFormCaseTypeGrid() {
        var CaseTypeId = $('#CaseForm_CaseType_Id').val();
        if (CaseTypeId != 0) {
            $.ajax({
                url: "/Icore_CaseForm/CaseForm_grid",
                data: { CaseType_ID: CaseTypeId },
                type: "Get",
                dataType: "html",
                success: function (data) {
                    $("#CaseFormPartialView").html('');
                    $("#CaseFormPartialView").html(data);

                    var Case_Type_Idparm = $("#CaseForm_CaseType_Id").val();
                    var Case_Title_Idparm = "#CaseForm_CaseTitle_Id";
                    var RequestURLparm = "/Icore_CaseForm/GetDynamicMenu?stateId=";
                    if (Case_Type_Idparm != null) {
                        CaseTypeFromCaseTitle(
                            Case_Type_Id = Case_Type_Idparm,
                            Case_Title_Id = Case_Title_Idparm,
                            RequestURL = RequestURLparm
                        );
                    }
                }
            });
        }
    }
    GetCaseFormCaseTitleGrid();
    $("#CaseForm_CaseTitle_Id").change(function (event) {
        //debugger;
        GetCaseFormCaseTitleGrid();
    });
    function GetCaseFormCaseTitleGrid() {
        var CaseTitleId = $('#CaseForm_CaseTitle_Id').val();
        if (CaseTitleId != 0) {
            $.ajax({
                url: "/Icore_CaseForm/CaseForm_grid",
                data: { CaseTitle_ID: CaseTitleId },
                type: "Get",
                dataType: "html",
                success: function (data) {
                    $("#CaseFormPartialView").html('');
                    $("#CaseFormPartialView").html(data);
                }
            });
        }
    }
    //#endregion Case Form Ended //

    //#region Case Document Dropdown Dynamic value generated Start
    function CaseTypeFromCaseTitle(Case_Type_Id, Case_Title_Id, RequestURL) {
        //debugger;
        var stateId = Case_Type_Id;
        debugger
        $.ajax
            ({
                url: RequestURL + stateId,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                success: function (result) {
                    $(Case_Title_Id).html("");
                    $(Case_Title_Id).append($('<option></option>').val(0).html('--Select Case Title--'));
                    $.each($.parseJSON(result), function (i, Data) {
                        $(Case_Title_Id).append($('<option></option>').val(Data.CaseTitle_Id).html(Data.CaseTitle_Name))
                    })
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
    $("#Case_Doc_CaseType_Id").change(function () {
        //debugger;
        var Case_Type_Idparam = $("#Case_Doc_CaseType_Id").val();
        var Case_Title_Idparam = "#Case_Doc_CaseTitleId";
        var RequestURLparam = "/Icore_CaseDocument/GetDynamicMenu?stateId=";
        if (Case_Type_Idparam != null) {
            CaseTypeFromCaseTitle(
                Case_Type_Id = Case_Type_Idparam,
                Case_Title_Id = Case_Title_Idparam,
                RequestURL = RequestURLparam
            );
        }
    });
    //#endregion Case Document Dropdown Dynamic value generated End//

    //#region Case Document View Start
    GetRelatedDocumentsGrid();
    $("#Case_Doc_CaseTitleId").change(function (event) {
        //debugger;
        GetRelatedDocumentsGrid();
    });
    function GetRelatedDocumentsGrid() {
        var TitleId = $('#Case_Doc_CaseTitleId').val();
        if (TitleId != 0) {
            $.ajax({
                url: "/Icore_CaseDocument/Case_Document_View",
                data: { Case_Title_ID: TitleId },
                type: "Get",
                dataType: "html",
                success: function (data) {
                    $("#divPartialView").html('');
                    $("#divPartialView").html(data);
                }
            });
        }
    }
    //#endregion Case Document view End//
    
    //#region ICORE REGION Start
    
    GetRegionGrid();
    $("#Reg_CaseTitleId").change(function (event) {
        GetRegionGrid();
    });
    function GetRegionGrid() {
        //debugger;
        var Ajax_Id = $('#Reg_CaseTitleId').val();
        if (Ajax_Id != 0) {
            $.ajax({
                url: "/Icore_Region/reg_grid",
                data: { CaseTitle_ID: Ajax_Id },
                type: "Get",
                dataType: "html",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    $("#divPartialView").html('');
                    $("#divPartialView").html(data);
                }
            });
        }
    }
    //#endregion
});
$(function () {
    SetVisibility();
    $("#CaseForm_DataTypeId").change(function () {
        SetVisibility();
    });
});
function SetVisibility() {
    var TableNames = $('#CaseForm_Field_LookupSettings');
    var datatype = $('#CaseForm_DataTypeId option:selected');
    if (datatype.text() == "DROPDOWNLIST") {
        TableNames.show();
    }
    else {
        TableNames.hide();
    }
}

$(function ()
{
    
    $("#Categ_DeptId").attr("disabled", true);
    $("#CaseType_Id").attr("disabled", true);
    $("#CaseTitle_Id").attr("disabled", true);
    if ($("#CaseFormSubmissionView").length == 1) {
        //#region Dynamic Drop Down And View Case Submission Form Start
        function GetDropDownFields(DEPART_ID, REQ_URL, DROPDOWN_ID, VALUE_ID, VALUE_NAME) {
            var TargetID = DEPART_ID;
            $.ajax
                ({
                    url: REQ_URL + TargetID,
                    type: 'POST',
                    datatype: 'application/json',
                    contentType: 'application/json',
                    success: function (result) {
                        $(DROPDOWN_ID).html("");
                        switch (DROPDOWN_ID) {
                            case '#Categ_DeptId':
                                $(DROPDOWN_ID).append($('<option></option>').val(0).html('--Select Category--'));
                                $("#Categ_DeptId").attr("disabled", false);
                                $("#CaseType_Id").attr("disabled", true);
                                $("#CaseTitle_Id").attr("disabled", true);
                                break;
                            case '#CaseType_Id':
                                $(DROPDOWN_ID).append($('<option></option>').val(0).html('--Select Case Type--'));
                                $("#CaseType_Id").attr("disabled", false);
                                $("#CaseTitle_Id").attr("disabled", true);
                                break;
                            case '#CaseTitle_Id':
                                $(DROPDOWN_ID).append($('<option></option>').val(0).html('--Select Case Title--'));
                                $("#CaseTitle_Id").attr("disabled", false);
                                break;
                            default:
                            // code block
                        }
                        $.each($.parseJSON(result), function (i, Data) {
                            $(DROPDOWN_ID).append($('<option></option>').val(Data[VALUE_ID]).html(Data[VALUE_NAME]))
                        })
                    },
                    error: function () {
                        //alert("Something went wrong..");
                    },
                });
        }
        //#endregion
        //#region Detail of Dynamic Dropdown for Submission Form

        function GetDeptWiseCategories() {
            var DepID = $("#Dept_Id").val();
            var URL = '/Icore_CaseSubmission_Form/GetCategoryMenu?TargetID=';
            var DRP_ID = "#Categ_DeptId";
            //alert('DepartmentId ' + DepID);
            if (DepID != 0) {
                var ID = 'Categ_Id';
                var NAME = 'Categ_Name';
                GetDropDownFields(
                    DEPART_ID = DepID,
                    REQ_URL = URL,
                    DROPDOWN_ID = DRP_ID,
                    VALUE_ID = ID,
                    VALUE_NAME = NAME
                );
            }
        }

        function GetCategoryWiseCaseTypes() {
            var CategoryID = $("#Categ_DeptId").val();
            var URL = '/Icore_CaseSubmission_Form/GetCaseTypeMenu?TargetID=';
            var DRP_ID = "#CaseType_Id";
            //alert('Category ' + CategoryID);
            if (CategoryID != 0) {
                var ID = 'CaseType_Id';
                var NAME = 'CaseType_Name';
                GetDropDownFields(
                    DEPART_ID = CategoryID,
                    REQ_URL = URL,
                    DROPDOWN_ID = DRP_ID,
                    VALUE_ID = ID,
                    VALUE_NAME = NAME
                );
            }
        }

        function GetCaseTypeWiseCaseTitles() {
            var CaseTypeId = $("#CaseType_Id").val();
            var URL = '/Icore_CaseSubmission_Form/GetCaseTitleMenu?TargetID=';
            var DRP_ID = "#CaseTitle_Id";
            //alert('Case Type Id ' + CaseTypeId);
            if (CaseTypeId != 0) {
                var ID = 'CaseTitle_Id';
                var NAME = 'CaseTitle_Name';
                GetDropDownFields(
                    DEPART_ID = CaseTypeId,
                    REQ_URL = URL,
                    DROPDOWN_ID = DRP_ID,
                    VALUE_ID = ID,
                    VALUE_NAME = NAME
                );

                $.ajax({
                    url: "/Icore_CaseSubmission_Form/GetFormForSubmission?CaseType_ID=" + CaseTypeId,
                    type: "Get",
                    dataType: "html",
                    success: function (data) {
                        var FormId = data;
                        $("#CaseForm_PartialView").load("/Icore_CaseSubmission_Form/CaseFormView?FormId=" + FormId);
                        $("#CaseDocument_PartialView").load("/Icore_CaseSubmission_Form/Case_Document_View?CaseType_ID=" + CaseTypeId);
                        $("#CaseSubmittedFormList").load("/Icore_CaseSubmission_Form/SubmittedCaseForm?FormId=" + FormId);
                    }
                });
            }
        }

        function GetCaseTitles() {
            var CaseTitle_ID = $("#CaseTitle_Id").val();
            var URL = '';
            var DRP_ID = "#CaseTitle_Id";
            //alert('Case Type Id ' + CaseTypeId);
            if (CaseTitle_ID != 0) {
                var ID = 'CaseTitle_Id';
                var NAME = 'CaseTitle_Name';
                GetDropDownFields(
                    DEPART_ID = CaseTitle_ID,
                    REQ_URL = URL,
                    DROPDOWN_ID = DRP_ID,
                    VALUE_ID = ID,
                    VALUE_NAME = NAME
                );

                $.ajax({
                    url: "/Icore_CaseSubmission_Form/GetFormForSubmission?CaseTitle_ID=" + CaseTitle_ID,
                    type: "Get",
                    dataType: "html",
                    success: function (data) {
                        var FormId = data;
                        $("#CaseForm_PartialView").load("/Icore_CaseSubmission_Form/CaseFormView?FormId=" + FormId);
                        $("#CaseDocument_PartialView").load("/Icore_CaseSubmission_Form/Case_Document_View?CaseTitle_ID=" + CaseTitle_ID);
                        $("#CaseSubmittedFormList").load("/Icore_CaseSubmission_Form/SubmittedCaseForm?FormId=" + FormId);
                    }
                });
            }
        }

        GetDeptWiseCategories();

        $("#Dept_Id").change(function (event) {
            GetDeptWiseCategories();
        });

        $("#Categ_DeptId").change(function (event) {
            GetCategoryWiseCaseTypes();
        });

        $("#CaseType_Id").change(function (event) {
            GetCaseTypeWiseCaseTitles();
        });
        $("#CaseTitle_Id").change(function (event) {
            GetCaseTitles();
        });
        //#region Submit Button of Case Form Submission
        $("#FormSubmission").click(function (event) {
            var CaseTypeId = $("#CaseType_Id").val();
            if (CaseTypeId != 0)
            {
                $.ajax({
                    url: "/Icore_CaseSubmission_Form/GetFormForSubmission?CaseType_ID=" + CaseTypeId,
                    type: "Get",
                    dataType: "html",
                    success: function (data) {

                        var FormId = data;
                        $.ajax({
                            url: "/Icore_CaseSubmission_Form/GetCaseFiledsData?FormId=" + FormId,
                            type: "Get",
                            dataType: "json",
                            success: function (result) {
                                var listOfAllFields = [];
                                $.each($.parseJSON(result), function (i, ResultedData) {

                                    var FindEachField = {};
                                    FindEachField.SCFD_ID = 0;
                                    FindEachField.SCFD_FORM_ID = FormId;
                                    FindEachField.SCFD_FIELD_ID = ResultedData.CaseForm_Field_Id;
                                    var ElementId = "#ctrlfrmsub" + ResultedData.CaseForm_Field_Name.toString().replace(" ", "").substring(0, 3) + ResultedData.CaseForm_Field_Id;
                                    FindEachField.SCFD_FIELD_VALUE = $(ElementId).val();
                                    listOfAllFields.push(FindEachField);
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "/Icore_CaseSubmission_Form/SubmitCaseFormFields?FormId=" + FormId,
                                    data: JSON.stringify(listOfAllFields),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    beforeSend: function () {
                                        $('#loading').show();
                                    },
                                    complete: function () {
                                        $('#loading').hide();
                                    },
                                    success: function (r) {
                                        if (r != "") {
                                            alert(r);
                                        }
                                        else {
                                            alert("Added Successfully.");
                                            $('#loading').hide();
                                        }

                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
        //#endregion
    }
});
//#region Document Upload 
function GetDocumentFile(DocId) {
    var fileUpload = $("#" + DocId).get(0);
    var files = fileUpload.files;

    var fileData = new FormData();
    fileData.append("Document_Id", DocId);
    for (var i = 0; i < files.length; i++) {
        fileData.append(files[i].name, files[i]);
    }
        $.ajax({
        type: "POST",
        url: "/Icore_CaseSubmission_Form/Upload",
        data: fileData,
        processData: false,
        contentType: false,
        cache: false,
        dataType: "json",
        beforeSend: function () {
            $('#loading').show();
        },
        complete: function () {
            $('#loading').hide();
        },
        success: function (r) {
            if (r != "") {
                alert(r);
            }
            else {
                alert("Added Successfully.");
            }
        }
    });
};
//#endregion
function ViewSubmittedFormClick(SCFM_ID, SCFM_CASE_FORM_ID) {
    $.ajax({
        url: "/Icore_CaseSubmission_Form/ViewSubmittedCaseForm?SCFM_ID=" + SCFM_ID,
        type: "POST",
        data: { SCFM_ID: SCFM_ID },
        success: function (Data) {
            $("#SubmittedCaseFormFields").load("/Icore_CaseSubmission_Form/ViewSubmittedCaseForm?SCFM_ID=" + SCFM_ID);
            $.ajax({
                url: "/Icore_CaseSubmission_Form/ViewSubmittedDocument?SCFM_CASE_FORM_ID=" + SCFM_CASE_FORM_ID,
                type: "POST",
                data: { SCFM_CASE_FORM_ID: SCFM_CASE_FORM_ID },
                success: function (Data) {
                    $("#SubmittedCaseDocument").load("/Icore_CaseSubmission_Form/ViewSubmittedDocument?SCFM_CASE_FORM_ID=" + SCFM_CASE_FORM_ID);
                },
                error: function (error) {
                    alert(error);
                }
            });
        },
        error: function (error) {
            alert(error);
        }
    });
}
function AddCommentToDoc(SCD_DOCUMENT_ID){

var LabelData = "#labeldata" +  SCD_DOCUMENT_ID;
var LabelElementId = $(LabelData).val();
var DocElementId = "#doc" + LabelElementId.replace(" ", "").substring(0, 3) + SCD_DOCUMENT_ID;
var comment = $(DocElementId).val();
    $.ajax({
            url: "/Icore_SubmittedCase/AddDocumentComments",
            type: "POST",
            data: { DocElementId : SCD_DOCUMENT_ID, comment: comment},
            success: function (Data) {
            alert(Data);
            },
            error: function (error) {
                alert(error);
            }
        });
}