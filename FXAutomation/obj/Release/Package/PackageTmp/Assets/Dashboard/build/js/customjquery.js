$(function () {
    //#region Case Form Started
    if ($("#CaseForm_CaseType_Id").val() == 0) {
        GetCaseFormCaseTypeGrid();
        GetCaseFormCaseTitleGrid();
    }

    if ($("#CaseForm_CaseType_Id").val() != 0) {
        GetCaseFormCaseTitleGrid();
    }

    
    $("#CaseForm_CaseType_Id").change(function (event) {
        //debugger;
        GetCaseFormCaseTypeGrid();
    });
    function GetCaseFormCaseTypeGrid() {
        var CaseTypeId = $('#CaseForm_CaseType_Id').val();
        if (CaseTypeId != 0) {
            $.ajax({
                url: "/Icore_CaseForm/CaseForm_grid",
                data: { CaseType_ID: CaseTypeId },
                type: "Get",
                dataType: "html",
                success: function (data) {
                    $("#CaseFormPartialView").html('');
                    //$("#CaseFormPartialView").html(data);

                    var Case_Type_Idparm = $("#CaseForm_CaseType_Id").val();
                    var Case_Title_Idparm = "#CaseForm_CaseTitle_Id";
                    var RequestURLparm = "/Icore_CaseForm/GetDynamicMenu?stateId=";
                    if (Case_Type_Idparm != null) {
                        CaseTypeFromCaseTitle(
                            Case_Type_Id = Case_Type_Idparm,
                            Case_Title_Id = Case_Title_Idparm,
                            RequestURL = RequestURLparm
                        );
                    }
                }
            });
        }
    }
  
    $("#CaseForm_CaseTitle_Id").change(function (event) {
        //debugger;
        GetCaseFormCaseTitleGrid();
    });


    function GetCaseFormCaseTitleGrid() {
        var CaseTitleId = $('#CaseForm_CaseTitle_Id').val();
        if (CaseTitleId != 0) {
            $.ajax({
                url: "/Icore_CaseForm/CaseForm_grid",
                data: { CaseTitle_ID: CaseTitleId },
                type: "Get",
                dataType: "html",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    $("#CaseFormPartialView").html('');
                    $("#CaseFormPartialView").html(data);

                    //#region Case Form Master Authorize/Reject/Delete
                    $("[name='Authorize_CaseForm']").click(function (button) {
                        var CaseForm_Id = $(this).attr("value");
                        $.ajax
                            ({
                                url: '/Icore_CaseForm/AuthorizeCaseForm?CaseForm_Id=' + CaseForm_Id,
                                type: 'POST',
                                datatype: 'application/json',
                                contentType: 'application/json',
                                beforeSend: function () {
                                    $('#loading').show();
                                },
                                complete: function () {
                                    $('#loading').hide();
                                },
                                success: function (result) {
                                    alert(result);
                                    GetCaseFormCaseTitleGrid();
                                },
                                error: function () {
                                    //alert("Something went wrong..");
                                },
                            }); 
                    });
                    $("[name='DeleteFromAuthorize_CaseForm']").click(function (button) {
                        var CaseForm_Id = $(this).attr("value");
                        var check = confirm("Are You Sure your want to delete?");
                        if (check == true) {
                            $.ajax
                                ({
                                    url: '/Icore_CaseForm/DeleteFromAuthorizeCaseForm?CaseForm_Id=' + CaseForm_Id,
                                    type: 'POST',
                                    datatype: 'application/json',
                                    contentType: 'application/json',
                                    beforeSend: function () {
                                        $('#loading').show();
                                    },
                                    complete: function () {
                                        $('#loading').hide();
                                    },
                                    success: function (result) {
                                        alert(result);
                                        GetCaseFormCaseTitleGrid();
                                    },
                                    error: function () {
                                        //alert("Something went wrong..");
                                    },
                                });
                        }
                    });
                    //$("[name='DeleteMasterNDetail_Data']").click(function (button) {
                    //    var CaseForm_Id = $(this).attr("value");
                    //    var check = confirm("Are You Sure your want to delete?");
                    //    if (check == true) {
                    //        $.ajax
                    //            ({
                    //                url: '/Icore_CaseForm/DeleteMasterNDetailData?CaseForm_Id=' + CaseForm_Id,
                    //                type: 'POST',
                    //                datatype: 'application/json',
                    //                contentType: 'application/json',
                    //                beforeSend: function () {
                    //                    $('#loading').show();
                    //                },
                    //                complete: function () {
                    //                    $('#loading').hide();
                    //                },
                    //                success: function (result) {
                    //                    alert(result);
                    //                    GetCaseFormCaseTitleGrid();
                    //                },
                    //                error: function () {

                    //                },
                    //            });
                    //    }
                    //});
                    //#endregion
                }
            });
        }
    }
    //#endregion Case Form Ended //

    //#region Case Document Dropdown Dynamic value generated Start
    function CaseTypeFromCaseTitle(Case_Type_Id, Case_Title_Id, RequestURL) {
        //debugger;
        var stateId = Case_Type_Id;
        //debugger
        $.ajax
            ({
                url: RequestURL + stateId,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    $(Case_Title_Id).html("");
                    $(Case_Title_Id).append($('<option></option>').val(0).html('--Select Case Title--'));
                    $.each($.parseJSON(result), function (i, Data) {
                        $(Case_Title_Id).append($('<option></option>').val(Data.CaseTitle_Id).html(Data.CaseTitle_Name))
                    })
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    }
    $("#Case_Doc_CaseType_Id").change(function () {
        //debugger;
        var Case_Type_Idparam = $("#Case_Doc_CaseType_Id").val();
        var Case_Title_Idparam = "#Case_Doc_CaseTitleId";
        var RequestURLparam = "/Icore_CaseDocument/GetDynamicMenu?stateId=";
        if (Case_Type_Idparam != null) {
            CaseTypeFromCaseTitle(
                Case_Type_Id = Case_Type_Idparam,
                Case_Title_Id = Case_Title_Idparam,
                RequestURL = RequestURLparam
            );
        }
    });
    //#endregion Case Document Dropdown Dynamic value generated End//

    //#region Case Document View Start
    GetRelatedDocumentsGrid();
    $("#Case_Doc_CaseTitleId").change(function (event) {
        //debugger;
        GetRelatedDocumentsGrid();
    });
    function GetRelatedDocumentsGrid() {
        var TitleId = $('#Case_Doc_CaseTitleId').val();
        if (TitleId != 0) {
            $.ajax({
                url: "/Icore_CaseDocument/Case_Document_View",
                data: { Case_Title_ID: TitleId },
                type: "Get",
                dataType: "html",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    $("#DocumentPartialView").html('');
                    $("#DocumentPartialView").html(data);

                    //#region Document Authorize/Reject/Delete
                    $("[name='AuthorizeCaseDocumet']").click(function (button) {
                        var Case_Doc_Id = $(this).attr("value");
                        $.ajax
                            ({
                                url: '/Icore_CaseDocument/AuthorizeCaseDocumet?Case_Doc_Id=' + Case_Doc_Id,
                                type: 'POST',
                                datatype: 'application/json',
                                contentType: 'application/json',
                                beforeSend: function () {
                                    $('#loading').show();
                                },
                                complete: function () {
                                    $('#loading').hide();
                                },
                                success: function (result) {
                                    alert(result);
                                    //location.reload();
                                    var TitleId = $('#Case_Doc_CaseTitleId').val();
                                    if (TitleId != 0) {
                                        $.ajax({
                                            url: "/Icore_CaseDocument/Case_Document_View",
                                            data: { Case_Title_ID: TitleId },
                                            type: "Get",
                                            dataType: "html",
                                            beforeSend: function () {
                                                $('#loading').show();
                                            },
                                            complete: function () {
                                                $('#loading').hide();
                                            },
                                            success: function (data) {
                                                GetRelatedDocumentsGrid();
                                            }
                                        });
                                    }
                                },
                                error: function () {
                                    //alert("Something went wrong..");
                                },
                            });
                    });
                    $("[name='DeleteFromAuthorizeCaseDocumet']").click(function (button) {
                        var Case_Doc_Id = $(this).attr("value");
                        var check = confirm("Are You Sure your want to delete?");
                        if (check == true) {
                            $.ajax
                                ({
                                    url: '/Icore_CaseDocument/DeleteFromAuthorizeCaseDocumet?Case_Doc_Id=' + Case_Doc_Id,
                                    type: 'POST',
                                    datatype: 'application/json',
                                    contentType: 'application/json',
                                    beforeSend: function () {
                                        $('#loading').show();
                                    },
                                    complete: function () {
                                        $('#loading').hide();
                                    },
                                    success: function (result) {
                                        alert(result);
                                        var TitleId = $('#Case_Doc_CaseTitleId').val();
                                        if (TitleId != 0) {
                                            $.ajax({
                                                url: "/Icore_CaseDocument/Case_Document_View",
                                                data: { Case_Title_ID: TitleId },
                                                type: "Get",
                                                dataType: "html",
                                                beforeSend: function () {
                                                    $('#loading').show();
                                                },
                                                complete: function () {
                                                    $('#loading').hide();
                                                },
                                                success: function (data) {
                                                    GetRelatedDocumentsGrid();
                                                }
                                            });
                                        }
                                    },
                                    error: function () {
                                        //alert("Something went wrong..");
                                    },
                                });
                        }
                    });
                    $("[name='DeleteCaseDocument']").click(function (button) {
                        var Case_Doc_Id = $(this).attr("value");
                        var check = confirm("Are You Sure your want to delete?");
                        if (check == true) {
                            $.ajax
                                ({
                                    url: '/Icore_CaseDocument/DeleteCaseDocument?Case_Doc_Id=' + Case_Doc_Id,
                                    type: 'POST',
                                    datatype: 'application/json',
                                    contentType: 'application/json',
                                    beforeSend: function () {
                                        $('#loading').show();
                                    },
                                    complete: function () {
                                        $('#loading').hide();
                                    },
                                    success: function (result) {
                                        alert(result);
                                        //location.reload();
                                        var TitleId = $('#Case_Doc_CaseTitleId').val();
                                        if (TitleId != 0) {
                                            $.ajax({
                                                url: "/Icore_CaseDocument/Case_Document_View",
                                                data: { Case_Title_ID: TitleId },
                                                type: "Get",
                                                dataType: "html",
                                                beforeSend: function () {
                                                    $('#loading').show();
                                                },
                                                complete: function () {
                                                    $('#loading').hide();
                                                },
                                                success: function (data) {
                                                    GetRelatedDocumentsGrid();
                                                }
                                            });
                                        }
                                    },
                                    error: function () {
                                        //alert("Something went wrong..");
                                    },
                                });
                        }
                    });
                    //#endregion
                }
            });
        }
    }
    //#endregion Case Document view End//
    
    //#region ICORE REGION Start
    
    GetRegionGrid();
    $("#Reg_CaseTitleId").change(function (event) {
        GetRegionGrid();
    });
    function GetRegionGrid() {
        //debugger;
        var Ajax_Id = $('#Reg_CaseTitleId').val();
        if (Ajax_Id != 0) {
            $.ajax({
                url: "/Icore_Region/reg_grid",
                data: { CaseTitle_ID: Ajax_Id },
                type: "Get",
                dataType: "html",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    $("#divPartialView").html('');
                    $("#divPartialView").html(data);

                    //#region Region Authorize/Reject
                    $("[name='AuthorizeRegion']").click(function (button) {
                        var Reg_Id = $(this).attr("value");
                        $.ajax
                            ({
                                url: '/Icore_Region/AuthorizeRegion?Reg_Id=' + Reg_Id,
                                type: 'POST',
                                datatype: 'application/json',
                                contentType: 'application/json',
                                beforeSend: function () {
                                    $('#loading').show();
                                },
                                complete: function () {
                                    $('#loading').hide();
                                },
                                success: function (result) {
                                    alert(result);
                                    GetRegionGrid();
                                },
                                error: function () {
                                    //alert("Something went wrong..");
                                },
                            });
                    });
                    $("[name='DeleteFromAuthorizeRegion']").click(function (button) {
                        var Reg_Id = $(this).attr("value");
                        var check = confirm("Are You Sure your want to delete?");
                        if (check == true) {
                            $.ajax
                                ({
                                    url: '/Icore_Region/DeleteFromAuthorizeRegion?Reg_Id=' + Reg_Id,
                                    type: 'POST',
                                    datatype: 'application/json',
                                    contentType: 'application/json',
                                    beforeSend: function () {
                                        $('#loading').show();
                                    },
                                    complete: function () {
                                        $('#loading').hide();
                                    },
                                    success: function (result) {
                                        alert(result);
                                        GetRegionGrid();
                                    },
                                    error: function () {
                                        //alert("Something went wrong..");
                                    },
                                });
                        }
                    });
                    //#endregion
                }
            });
        }
    }
    //#endregion
});
$(function () {
    SetVisibility();
    $("#CaseForm_DataTypeId").change(function () {
        SetVisibility();
    });
});
function SetVisibility() {
    var TableNames = $('#CaseForm_Field_LookupSettings');
    var datatype = $('#CaseForm_DataTypeId option:selected');
    if (datatype.text() == "DROPDOWNLIST") {
        TableNames.show();
    }
    else {
        TableNames.hide();
    }
}

$(function ()
{
    //$("#Categ_DeptId").attr("disabled", true);
    //$("#CaseType_Id").attr("disabled", true);
    //$("#CaseTitle_Id").attr("disabled", true);
    if ($("#CaseFormSubmissionView").length == 1) {
        //#region Dynamic Drop Down And View Case Submission Form Start
        function GetDropDownFields(DEPART_ID, REQ_URL, DROPDOWN_ID, VALUE_ID, VALUE_NAME) {
            var TargetID = DEPART_ID;
            $.ajax
                ({
                    url: REQ_URL + TargetID,
                    type: 'POST',
                    datatype: 'application/json',
                    contentType: 'application/json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (result) {
                        $(DROPDOWN_ID).html("");
                        switch (DROPDOWN_ID) {
                            case '#Categ_DeptId':
                                $(DROPDOWN_ID).append($('<option></option>').val(0).html('--Select Category--'));
                                $("#Categ_DeptId").attr("disabled", false);
                                $("#CaseType_Id").attr("disabled", true);
                                $("#CaseTitle_Id").attr("disabled", true);
                                break;
                            case '#CaseType_Id':
                                $(DROPDOWN_ID).append($('<option></option>').val(0).html('--Select Case Type--'));
                                $("#CaseType_Id").attr("disabled", false);
                                $("#CaseTitle_Id").attr("disabled", true);
                                break;
                            case '#CaseTitle_Id':
                                $(DROPDOWN_ID).append($('<option></option>').val(0).html('--Select Case Title--'));
                                $("#CaseTitle_Id").attr("disabled", false);
                                break;
                            default:
                            // code block
                        }
                        $.each($.parseJSON(result), function (i, Data) {
                            $(DROPDOWN_ID).append($('<option></option>').val(Data[VALUE_ID]).html(Data[VALUE_NAME]))
                        })
                    },
                    error: function () {
                        //alert("Something went wrong..");
                    },
                });
        }
        //#endregion
        //#region Detail of Dynamic Dropdown for Submission Form

        function GetDeptWiseCategories() {
            var DepID = $("#Dept_Id").val();
            var URL = '/Icore_CaseSubmission_Form/GetCategoryMenu?TargetID=';
            var DRP_ID = "#Categ_DeptId";
            //alert('DepartmentId ' + DepID);
            if (DepID != 0) {
                var ID = 'Categ_Id';
                var NAME = 'Categ_Name';
                GetDropDownFields(
                    DEPART_ID = DepID,
                    REQ_URL = URL,
                    DROPDOWN_ID = DRP_ID,
                    VALUE_ID = ID,
                    VALUE_NAME = NAME
                );
            }
        }

        function GetCategoryWiseCaseTypes() {
            var CategoryID = $("#Categ_DeptId").val();
            var URL = '/Icore_CaseSubmission_Form/GetCaseTypeMenu?TargetID=';
            var DRP_ID = "#CaseType_Id";
            //alert('Category ' + CategoryID);
            if (CategoryID != 0) {
                var ID = 'CaseType_Id';
                var NAME = 'CaseType_Name';
                GetDropDownFields(
                    DEPART_ID = CategoryID,
                    REQ_URL = URL,
                    DROPDOWN_ID = DRP_ID,
                    VALUE_ID = ID,
                    VALUE_NAME = NAME
                );
            }
        }

        function GetCaseTypeWiseCaseTitles() {
            var CaseTypeId = $("#CaseType_Id").val();
            var URL = '/Icore_CaseSubmission_Form/GetCaseTitleMenu?TargetID=';
            var DRP_ID = "#CaseTitle_Id";
            //alert('Case Type Id ' + CaseTypeId);
            if (CaseTypeId != 0) {
                var ID = 'CaseTitle_Id';
                var NAME = 'CaseTitle_Name';
                GetDropDownFields(
                    DEPART_ID = CaseTypeId,
                    REQ_URL = URL,
                    DROPDOWN_ID = DRP_ID,
                    VALUE_ID = ID,
                    VALUE_NAME = NAME
                );

                $.ajax({
                    url: "/Icore_CaseSubmission_Form/GetFormForSubmission?CaseType_ID=" + CaseTypeId,
                    type: "Get",
                    dataType: "html",
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (data) {
                        var FormId = data;
                        $("#CaseForm_PartialView").load("/Icore_CaseSubmission_Form/CaseFormView?FormId=" + FormId);
                        $("#CaseDocument_PartialView").load("/Icore_CaseSubmission_Form/Case_Document_View?CaseType_ID=" + CaseTypeId);
                        $("#CaseSubmittedFormList").load("/Icore_CaseSubmission_Form/SubmittedCaseForm?FormId=" + FormId);
                    }
                });
            }
        }

        function GetCaseTitles() {
            var CaseTitle_ID = $("#CaseTitle_Id").val();
            var URL = '';
            var DRP_ID = "#CaseTitle_Id";
            //alert('Case Type Id ' + CaseTypeId);
            if (CaseTitle_ID != 0) {
                var ID = 'CaseTitle_Id';
                var NAME = 'CaseTitle_Name';
                GetDropDownFields(
                    DEPART_ID = CaseTitle_ID,
                    REQ_URL = URL,
                    DROPDOWN_ID = DRP_ID,
                    VALUE_ID = ID,
                    VALUE_NAME = NAME
                );

                $.ajax({
                    url: "/Icore_CaseSubmission_Form/GetFormForSubmission?CaseTitle_ID=" + CaseTitle_ID,
                    type: "Get",
                    dataType: "html",
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (data) {
                        var FormId = data;
                        $("#CaseForm_PartialView").load("/Icore_CaseSubmission_Form/CaseFormView?FormId=" + FormId);
                        $("#CaseDocument_PartialView").load("/Icore_CaseSubmission_Form/Case_Document_View?CaseTitle_ID=" + CaseTitle_ID);
                        $("#CaseSubmittedFormList").load("/Icore_CaseSubmission_Form/SubmittedCaseForm?FormId=" + FormId);
                    }
                });
            }
        }

        GetDeptWiseCategories();

        $("#Dept_Id").change(function (event) {
            GetDeptWiseCategories();
        });

        $("#Categ_DeptId").change(function (event) {
            GetCategoryWiseCaseTypes();
        });

        $("#CaseType_Id").change(function (event) {
            GetCaseTypeWiseCaseTitles();
        });
        $("#CaseTitle_Id").change(function (event) {
            GetCaseTitles();
        });
        //#region Submit Button of Case Form Submission
        $("#FormSubmission").click(function (event) {
            var CaseTypeId = $("#CaseType_Id").val();
            if (CaseTypeId != 0)
            {
                $.ajax({
                    url: "/Icore_CaseSubmission_Form/GetFormForSubmission?CaseType_ID=" + CaseTypeId,
                    type: "Get",
                    dataType: "html",
                    success: function (data) {

                        var FormId = data;
                        $.ajax({
                            url: "/Icore_CaseSubmission_Form/GetCaseFiledsData?FormId=" + FormId,
                            type: "Get",
                            dataType: "json",
                            success: function (result) {
                                var listOfAllFields = [];
                                $.each($.parseJSON(result), function (i, ResultedData) {

                                    var FindEachField = {};
                                    FindEachField.SCFD_ID = 0;
                                    FindEachField.SCFD_FORM_ID = FormId;
                                    FindEachField.SCFD_FIELD_ID = ResultedData.CaseForm_Field_Id;
                                    var ElementId = "#ctrlfrmsub" + ResultedData.CaseForm_Field_Name.toString().replace(" ", "").substring(0, 3) + ResultedData.CaseForm_Field_Id;
                                    FindEachField.SCFD_FIELD_VALUE = $(ElementId).val();
                                    listOfAllFields.push(FindEachField);
                                });
                                $.ajax({
                                    type: "POST",
                                    url: "/Icore_CaseSubmission_Form/SubmitCaseFormFields?FormId=" + FormId,
                                    data: JSON.stringify(listOfAllFields),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    beforeSend: function () {
                                        $('#loading').show();
                                    },
                                    complete: function () {
                                        $('#loading').hide();
                                    },
                                    success: function (r) {
                                        if (r != "") {
                                            alert(r);
                                        }
                                        else {
                                            alert("Added Successfully.");
                                            $('#loading').hide();
                                        }

                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
        //#endregion
    }
});

function AddCommentToDoc(SCD_DOCUMENT_ID) {
    var LabelData = "#labeldata" + SCD_DOCUMENT_ID;
    var LabelElementId = $(LabelData).val();
    var DocElementId = "#doc" + LabelElementId.replace(" ", "").substring(0, 3) + SCD_DOCUMENT_ID;
    var comment = $(DocElementId).val();
    $.ajax({
        url: "/Icore_SubmittedCase/AddDocumentComments",
        type: "POST",
        data: { DocElementId: SCD_DOCUMENT_ID, comment: comment },
        beforeSend: function () {
            $('#loading').show();
        },
        complete: function () {
            $('#loading').hide();
        },
        success: function (Data) {
            alert(Data);
        },
        error: function (error) {
            alert(error);
        }
    });
}
//#region Edit Subitted Form AD
$("[name='EditSubittedFormAD']").click(function (button) {
    var FormID = $(this).attr("value");
    if ($("#EditSubmittedForm").length == 1) {
        $.ajax({
            url: "/Icore_CaseSubmission_Form/GetSubmittedFormFields?FormID=" + FormID,
            type: "Get",
            dataType: "json",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (Result) {
                var frmData = new FormData();
                $.each($.parseJSON(Result), function (i, ResultedData) {
                    var ElementId = "#ctrlfrmsub" + ResultedData.CaseForm_Field_Name.toString().replace(" ", "").substring(0, 3) + ResultedData.SCFD_FIELD_ID;
                    var ElementValue = $(ElementId).val();
                    frmData.append(ElementId, ElementValue);
                    //alert("Element ID " + ElementId)
                    //alert("Element Value " + ElementValue);
                });
                $.ajax({
                    url: "/Icore_CaseSubmission_Form/GetSubmittedFormDocuments?ForeignID=" + FormID,
                    type: "Get",
                    dataType: "json",
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (Result) {
                        $('#SavedDocument').each(function () {
                            $.each($.parseJSON(Result), function (i, ResultedData) {
                                var DocumentId = "#ctrldocsub" + ResultedData.Case_Doc_Name.toString().replace(" ", "").substring(0, 3) + ResultedData.SCD_DOCUMENT_ID;
                                //alert(DocumentId);
                                var fileUpload = $(DocumentId).get(0);
                                var files = fileUpload.files;
                                for (var i = 0; i < files.length; i++) {
                                    frmData.append(DocumentId, files[i]);
                                    //alert(files[i].name);
                                }
                            });
                        });
                        $.ajax({
                            url: "/Icore_CaseSubmission_Form/UpdateSubmittedFormAD?FormId=" + FormID,
                            data: frmData,
                            type: "POST",
                            processData: false,
                            contentType: false,
                            dataType: "json",
                            beforeSend: function () {
                                $('#loading').show();
                            },
                            complete: function () {
                                $('#loading').hide();
                            },
                            success: function (r) {
                                if (r == "") {
                                    alert("Form Updated Successfully.");
                                    window.location.href = '/Icore_SubmittedCase/OpsQueue'
                                }
                                else {
                                    alert(r);
                                    $('#loading').hide();
                                }
                            }
                        });
                    }
                });
            }
        });
    }
});
//#endregion


$(document).on("click", "[name='AddCommentToDocTest']", function (event) {
    var LabelData = $(this).attr("value");
    AddCommentToDoc(LabelData);
});

//#region View Submitted Form AD
$("[name='ViewSubmittedFormClick']").click(function (button) {
    var value = $(this).attr("value").replace('sp_', '');
    var SCFM_ID = value.substr(0, value.indexOf(' '));
    var SCFM_CASE_FORM_ID = value.substr(value.indexOf(' ') + 2);
    $.ajax({
        url: "/Icore_CaseSubmission_Form/ViewSubmittedCaseForm?SCFM_ID=" + SCFM_ID,
        type: "POST",
        data: { SCFM_ID: SCFM_ID },
        beforeSend: function () {
            $('#loading').show();
        },
        complete: function () {
            $('#loading').hide();
        },
        success: function (Data) {
            $("#SubmittedCaseFormFields").load("/Icore_CaseSubmission_Form/ViewSubmittedCaseForm?SCFM_ID=" + SCFM_ID);
            $.ajax({
                url: "/Icore_CaseSubmission_Form/ViewSubmittedDocument?SCFM_ID=" + SCFM_ID,
                type: "POST",
                data: { SCFM_ID: SCFM_ID },
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (Data) {
                    $("#SubmittedCaseDocument").load("/Icore_CaseSubmission_Form/ViewSubmittedDocument?SCFM_ID=" + SCFM_ID);
                },
                error: function (error) {
                    alert(error);
                }
            });
        },
        error: function (error) {
            alert(error);
        }
    });
});
//#endregion


$(document).ready(function() {
    $(".hide-password").hide();
  $(".show-password, .hide-password").on('click', function() {
    var passwordId = $("#SS_HASH").attr("id");
    if ($(this).hasClass('show-password')) {
      $("#" + passwordId).attr("type", "text");
      $(this).parent().find(".show-password").hide();
      $(this).parent().find(".hide-password").show();
    } else {
      $("#" + passwordId).attr("type", "password");
      $(this).parent().find(".hide-password").hide();
      $(this).parent().find(".show-password").show();
    }
  });
});

//#region Open Chat popup
$(document).on("click", "[name='OpenChatAgainstDocument']", function (event) {
    var DocID = $(this).attr("value");
   $("#LoadChatWithClientsHere").load("/Icore_CaseSubmission_Form/ViewChatWithClient?DocElementId=" + DocID);
});
//#endregion

//#region Close Chat with Client Modal
$(document).on("click", "[name='ChatWithClientModel']", function (event) {
    $('#modal-lg').modal('hide');
});
//#endregion

//#region Add Comments to database
$(document).on("blur", "#CommentsData", function (e) {
    var comment = $("#CommentsData").val();
    comment = comment.replace(/[`~!@#%^&*()_|+\=;"<>\{\}\[\]\\\/]/gi, '');
    $("#CommentsData").val(comment);
});
$(document).on("keypress", "#CommentsData", function (e) {
    if ($("#CommentsData").val() == "") {
        var regex = new RegExp("^[a-zA-Z0-9 , . : ? / ( ) $ -]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();
        return false;
    }
    else {
        var regex = new RegExp("^[a-zA-Z0-9 , . : ? / ' ( ) $ -]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();
        return false;
    }
});
$(document).on("keypress", "#SCFM_COMMENT", function (e) {
    if ($("#SCFM_COMMENT").val() == "") {
        var regex = new RegExp("^[a-zA-Z0-9 , . : ? / ( ) $ -]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();
        return false;
    }
    else {
        var regex = new RegExp("^[a-zA-Z0-9 , . : ? / ' ( ) $ -]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();
        return false;
    }
});
$(document).on("click", "[name='GetCommendFromSendButton']", function (event) {
    var Comments = $("#CommentsData").val();
    var DocumentID = $("#DocumentID").val();
     $.ajax({
        url: "/Icore_SubmittedCase/AddDocumentComments",
        type: "POST",
        data: { DocElementId: DocumentID, comment: Comments },
        beforeSend: function () {
            $('#loading').show();
        },
        complete: function () {
            $('#loading').hide();
        },
        success: function (Data) {
            if(Data == "Comment Added Successfully")
            {
                $("#LoadChatWithClientsHere").load("/Icore_CaseSubmission_Form/ViewChatWithClient?DocElementId=" + DocumentID);
                var Comments = $("#CommentsData").val('');
            }
            else
            {
                alert(Data);
            }
        },
        error: function (error) {
            alert(error);
        }
    });
});
//#endregion

//#region Open Chat popup for Master Case
$(document).on("click", "[name='ViewPreviousCommentsOfMaster']", function (event) {
    var CaseNo = $(this).attr("value");
   $("#LoadPreviousMasterCaseComments").load("/Icore_SubmittedCase/LoadPreviousMasterCaseComments?CaseNo=" + CaseNo);
});
//#endregion