﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FXAutomation.Models
{
    public class CommentHistory
    {
        public IEnumerable<ICORE_COMMENT_DETAIL_LOG> CommentLog { get; set; }
        public IEnumerable<ICORE_CASE_FORMS_MASTER> Master { get; set; }
    }
}