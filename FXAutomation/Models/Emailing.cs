﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace FXAutomation.Models
{
    public class Emailing
    {
        #region if Production 
        public static bool Application_Mode = true;
        #endregion

        #region if Developer 
        //public static bool Application_Mode = false;
        #endregion

        #region Ops Emails
        public static string OpsEmail(int ID, int Status_Id, string PreviousQueue, string NewQueue, string Comment)
        {
            string message = "";
            try
            {
                FXDIGEntities context = new FXDIGEntities();
                ICORE_EMAIL_CONFIGRATION CheckEmail = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (CheckEmail != null)
                {
                    ICORE_SUBMIT_CASE_FORM_MASTER Entity = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == ID).FirstOrDefault();
                    if (Entity != null)
                    {
                        #region Get Email CC
                        string CCUser = "";
                        ICORE_CASE_FORMS_MASTER FormDetails = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == Entity.SCFM_CASE_FORM_ID).FirstOrDefault();
                        ICORE_CASE_TITLES Title = context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Id == FormDetails.CaseForm_CaseTitle_Id).FirstOrDefault();
                        ICORE_FUNCTIONS GetFunction = context.ICORE_FUNCTIONS.Where(m => m.F_NAME.Contains(PreviousQueue)).FirstOrDefault();
                        if (GetFunction != null)
                        {
                            int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                            if (ROlEIdsWithCaseTitles.Count() > 0)
                            {
                                List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == GetFunction.F_ID && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                if (checkfunction != null)
                                {
                                    foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                    {
                                        ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                        if (getRoleFromFunction != null)
                                        {
                                            List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                            if (GetRolesDetail.Count() > 0)
                                            {
                                                foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                {
                                                    ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                    if (GetUser != null)
                                                    {
                                                        if (!CCUser.Contains(GetUser.LOG_EMAIL))
                                                            CCUser += GetUser.LOG_EMAIL + ",";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ICORE_FUNCTIONS GetFunctionofNewQueue = context.ICORE_FUNCTIONS.Where(m => m.F_NAME.Contains(NewQueue)).FirstOrDefault();
                        if (GetFunctionofNewQueue != null)
                        {
                            int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                            if (ROlEIdsWithCaseTitles.Count() > 0)
                            {
                                List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == GetFunctionofNewQueue.F_ID && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                if (checkfunction != null)
                                {
                                    foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                    {
                                        ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                        if (getRoleFromFunction != null)
                                        {
                                            List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                            if (GetRolesDetail.Count() > 0)
                                            {
                                                foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                {
                                                    ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                    if (GetUser != null)
                                                    {
                                                        if (!CCUser.Contains(GetUser.LOG_EMAIL))
                                                            CCUser += GetUser.LOG_EMAIL + ",";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (CCUser != "")
                                CCUser = CCUser.Remove(CCUser.Length - 1);
                        }
                        #endregion
                        if (Application_Mode == true)
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                string ToEmail = Entity.SCFM_INSERTED_BY;
                                string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                string HostName = CheckEmail.EC_SEREVER_HOST;
                                int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                NetworkCredentials.UserName = FromEmail;
                                mail.From = new MailAddress(FromEmail);
                                mail.To.Add(new MailAddress(ToEmail));
                                if (CCUser != "")
                                {
                                    string[] CC_Email = CCUser.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                }
                                mail.Subject = "FX Digitalization (Case No : " + Entity.SCFM_CASE_NO + ").";
                                ICORE_SUBMIT_CASE_STATUS StatusToShow = context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID == Status_Id).FirstOrDefault();
                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:40px;text-align:center'>Foreign Exchange Case Submission</p><p style='font-family:Candara;font-size:15px'>Dear Valued Client,</p><p style='font-family:Candara;font-size:15px'>Below is the current status of your case:<br />  </p><p style='font-family:Candara;font-size:15px'>Case No# : <span style='font-family:Arial'><b>" + Entity.SCFM_CASE_NO + "</b></span></p><p style='font-family:Candara;font-size:15px'>Case Status : <b style='font-size:18px'>" + StatusToShow.SCFS_NAME + " -- " + StatusToShow.SCFS_DESCRIPTION + "</b></p><p style='font-family:Candara;font-size:15px'>Submitted Date & Time : <span style='font-family:Arial'><b>" + DateTime.Now.AddHours(5).ToLongDateString() + "</b></span></p><p style='font-family:Candara;font-size:15px'>Comments : <span style='font-family:Arial'><b>" + Comment + "</b></span></p><p style='font-family:Candara;font-size:15px'> <b>This is a system generated email, please do reply all. In case of queries please call <span style='font-family:arial'>111-777-777</span> for respective your service contact. </b></p></div></div></div><div class='col-md-2'></div></div>";
                                mail.IsBodyHtml = true;

                                using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                {
                                    smtp.Credentials = NetworkCredentials;
                                    smtp.Send(mail);
                                }
                                message += "Email sent sucessfully" + Environment.NewLine;
                            }
                        }
                        else
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                string ToEmail = Entity.SCFM_INSERTED_BY;
                                string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                string HostName = CheckEmail.EC_SEREVER_HOST;
                                int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                NetworkCredentials.UserName = FromEmail;
                                NetworkCredentials.Password = "Testuser0592";
                                mail.From = new MailAddress(FromEmail);
                                mail.To.Add(new MailAddress(ToEmail));
                                if (CCUser != "")
                                {
                                    string[] CC_Email = CCUser.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                }
                                mail.Subject = "FX Digitalization (Case No : " + Entity.SCFM_CASE_NO + ").";
                                ICORE_SUBMIT_CASE_STATUS StatusToShow = context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID == Status_Id).FirstOrDefault();
                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:40px;text-align:center'>Foreign Exchange Case Submission</p><p style='font-family:Candara;font-size:15px'>Dear Valued Client,</p><p style='font-family:Candara;font-size:15px'>Below is the current status of your case:<br />  </p><p style='font-family:Candara;font-size:15px'>Case No# : <span style='font-family:Arial'><b>" + Entity.SCFM_CASE_NO + "</b></span></p><p style='font-family:Candara;font-size:15px'>Case Status : <b style='font-size:18px'>" + StatusToShow.SCFS_NAME + " -- " + StatusToShow.SCFS_DESCRIPTION + "</b></p><p style='font-family:Candara;font-size:15px'>Submitted Date & Time : <span style='font-family:Arial'><b>" + DateTime.Now.AddHours(5).ToLongDateString() + "</b></span></p><p style='font-family:Candara;font-size:15px'>Comments : <span style='font-family:Arial'><b>" + Comment + "</b></span></p><p style='font-family:Candara;font-size:15px'> <b>This is a system generated email, please do reply all. In case of queries please call <span style='font-family:arial'>111-777-777</span> for respective your service contact. </b></p></div></div></div><div class='col-md-2'></div></div>";
                                mail.IsBodyHtml = true;

                                using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                {
                                    smtp.Credentials = NetworkCredentials;
                                    smtp.EnableSsl = true;
                                    smtp.Send(mail);
                                }
                                message += "Email sent sucessfully" + Environment.NewLine;
                            }
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For Submitted Form ID : " + ID + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += ex.Message + Environment.NewLine;
            }
            return (message);
        }

        public static string OpsEmailWithoutClient(int ID, int Status_Id, string PreviousQueue, string NewQueue, string Comment)
        {
            string message = "";
            try
            {
                FXDIGEntities context = new FXDIGEntities();
                ICORE_EMAIL_CONFIGRATION CheckEmail = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (CheckEmail != null)
                {
                    ICORE_SUBMIT_CASE_FORM_MASTER Entity = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == ID).FirstOrDefault();
                    if (Entity != null)
                    {
                        #region Get Email CC
                        string CCUser = "";
                        string TOUSER = "";
                        ICORE_CASE_FORMS_MASTER FormDetails = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == Entity.SCFM_CASE_FORM_ID).FirstOrDefault();
                        ICORE_CASE_TITLES Title = context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Id == FormDetails.CaseForm_CaseTitle_Id).FirstOrDefault();
                        ICORE_FUNCTIONS GetFunction = context.ICORE_FUNCTIONS.Where(m => m.F_NAME.Contains(PreviousQueue)).FirstOrDefault();
                        if (GetFunction != null)
                        {
                            int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                            if (ROlEIdsWithCaseTitles.Count() > 0)
                            {
                                List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == GetFunction.F_ID && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                if (checkfunction != null)
                                {
                                    foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                    {
                                        ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                        if (getRoleFromFunction != null)
                                        {
                                            List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                            if (GetRolesDetail.Count() > 0)
                                            {
                                                foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                {
                                                    ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                    if (GetUser != null)
                                                    {
                                                        if (!CCUser.Contains(GetUser.LOG_EMAIL))
                                                            CCUser += GetUser.LOG_EMAIL + ",";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (CCUser != "")
                                CCUser = CCUser.Remove(CCUser.Length - 1);
                        }
                        ICORE_FUNCTIONS GetFunctionofNewQueue = context.ICORE_FUNCTIONS.Where(m => m.F_NAME.Contains(NewQueue)).FirstOrDefault();
                        if (GetFunctionofNewQueue != null)
                        {
                            int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                            if (ROlEIdsWithCaseTitles.Count() > 0)
                            {
                                List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == GetFunctionofNewQueue.F_ID && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                if (checkfunction != null)
                                {
                                    foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                    {
                                        ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                        if (getRoleFromFunction != null)
                                        {
                                            List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                            if (GetRolesDetail.Count() > 0)
                                            {
                                                foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                {
                                                    ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                    if (GetUser != null)
                                                    {
                                                        if (!TOUSER.Contains(GetUser.LOG_EMAIL))
                                                            TOUSER += GetUser.LOG_EMAIL + ",";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (TOUSER != "")
                                TOUSER = TOUSER.Remove(TOUSER.Length - 1);
                        }
                        if (TOUSER == "")
                        {
                            message = "This Case Title : " + Title.CaseTitle_Name + " is not assign to any user on " + NewQueue +  Environment.NewLine;
                            return (message);
                        }
                        #endregion
                        if (Application_Mode == true)
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                string ToEmail = Entity.SCFM_INSERTED_BY;
                                string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                string HostName = CheckEmail.EC_SEREVER_HOST;
                                int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                NetworkCredentials.UserName = FromEmail;
                                mail.From = new MailAddress(FromEmail);
                                //mail.To.Add(new MailAddress(ToEmail));
                                if (TOUSER != "")
                                {
                                    string[] TO_Email = TOUSER.Split(',');
                                    foreach (string MultiEmail in TO_Email)
                                    {
                                        mail.To.Add(MultiEmail);
                                    }
                                }
                                if (CCUser != "")
                                {
                                    string[] CC_Email = CCUser.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                }
                                mail.Subject = "FX Digitalization (Case No : " + Entity.SCFM_CASE_NO + ").";
                                ICORE_SUBMIT_CASE_STATUS StatusToShow = context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID == Status_Id).FirstOrDefault();
                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:40px;text-align:center'>Foreign Exchange Case Submission</p><p style='font-family:Candara;font-size:15px'>Dear Valued Client,</p><p style='font-family:Candara;font-size:15px'>Below is the current status of your case:<br />  </p><p style='font-family:Candara;font-size:15px'>Case No# : <span style='font-family:Arial'><b>" + Entity.SCFM_CASE_NO + "</b></span></p><p style='font-family:Candara;font-size:15px'>Case Status : <b style='font-size:18px'>" + StatusToShow.SCFS_NAME + " -- " + StatusToShow.SCFS_DESCRIPTION + "</b></p><p style='font-family:Candara;font-size:15px'>Submitted Date & Time : <span style='font-family:Arial'><b>" + DateTime.Now.AddHours(5).ToLongDateString() + "</b></span></p><p style='font-family:Candara;font-size:15px'>Comments : <span style='font-family:Arial'><b>" + Comment + "</b></span></p><p style='font-family:Candara;font-size:15px'> <b>This is a system generated email, please do reply all. In case of queries please call <span style='font-family:arial'>111-777-777</span> for respective your service contact. </b></p></div></div></div><div class='col-md-2'></div></div>";
                                mail.IsBodyHtml = true;

                                using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                {
                                    smtp.Credentials = NetworkCredentials;
                                    smtp.Send(mail);
                                }
                                message += "Email sent sucessfully" + Environment.NewLine;
                            }
                        }
                        else
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                string ToEmail = Entity.SCFM_INSERTED_BY;
                                string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                string HostName = CheckEmail.EC_SEREVER_HOST;
                                int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                NetworkCredentials.UserName = FromEmail;
                                NetworkCredentials.Password = "Test@@123";
                                mail.From = new MailAddress(FromEmail);
                                //mail.To.Add(new MailAddress(ToEmail));
                                if (TOUSER != "")
                                {
                                    string[] TO_Email = TOUSER.Split(',');
                                    foreach (string MultiEmail in TO_Email)
                                    {
                                        mail.To.Add(MultiEmail);
                                    }
                                }
                                if (CCUser != "")
                                {
                                    string[] CC_Email = CCUser.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                }
                                mail.Subject = "FX Digitalization (Case No : " + Entity.SCFM_CASE_NO + ").";
                                ICORE_SUBMIT_CASE_STATUS StatusToShow = context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID == Status_Id).FirstOrDefault();
                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:40px;text-align:center'>Foreign Exchange Case Submission</p><p style='font-family:Candara;font-size:15px'>Dear Valued Client,</p><p style='font-family:Candara;font-size:15px'>Below is the current status of your case:<br />  </p><p style='font-family:Candara;font-size:15px'>Case No# : <span style='font-family:Arial'><b>" + Entity.SCFM_CASE_NO + "</b></span></p><p style='font-family:Candara;font-size:15px'>Case Status : <b style='font-size:18px'>" + StatusToShow.SCFS_NAME + " -- " + StatusToShow.SCFS_DESCRIPTION + "</b></p><p style='font-family:Candara;font-size:15px'>Submitted Date & Time : <span style='font-family:Arial'><b>" + DateTime.Now.AddHours(5).ToLongDateString() + "</b></span></p><p style='font-family:Candara;font-size:15px'>Comments : <span style='font-family:Arial'><b>" + Comment + "</b></span></p><p style='font-family:Candara;font-size:15px'> <b>This is a system generated email, please do reply all. In case of queries please call <span style='font-family:arial'>111-777-777</span> for respective your service contact. </b></p></div></div></div><div class='col-md-2'></div></div>";
                                mail.IsBodyHtml = true;

                                using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                {
                                    smtp.Credentials = NetworkCredentials;
                                    smtp.EnableSsl = true;
                                    smtp.Send(mail);
                                }
                                message += "Email sent sucessfully" + Environment.NewLine;
                            }
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For Submitted Form ID : " + ID + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += ex.Message + Environment.NewLine;
            }
            return (message);
        }
        #endregion

        #region Compliance Emails
        public static string ComplianceEmail(int ID, int Status_Id, string PreviousQueue, string NewQueue, string Comment)
        {
            string message = "";
            try
            {
                FXDIGEntities context = new FXDIGEntities();
                ICORE_EMAIL_CONFIGRATION CheckEmail = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (CheckEmail != null)
                {
                    ICORE_SUBMIT_CASE_FORM_MASTER Entity = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == ID).FirstOrDefault();
                    if (Entity != null)
                    {
                        #region Get Email CC
                        string CCUser = "";
                        ICORE_CASE_FORMS_MASTER FormDetails = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == Entity.SCFM_CASE_FORM_ID).FirstOrDefault();
                        ICORE_CASE_TITLES Title = context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Id == FormDetails.CaseForm_CaseTitle_Id).FirstOrDefault();
                        ICORE_FUNCTIONS GetFunction = context.ICORE_FUNCTIONS.Where(m => m.F_NAME.Contains("Ops Queue")).FirstOrDefault();
                        if (GetFunction != null)
                        {
                            int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                            if (ROlEIdsWithCaseTitles.Count() > 0)
                            {
                                List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == GetFunction.F_ID && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                if (checkfunction != null)
                                {
                                    foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                    {
                                        ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                        if (getRoleFromFunction != null)
                                        {
                                            List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                            if (GetRolesDetail.Count() > 0)
                                            {
                                                foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                {
                                                    ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                    if (GetUser != null)
                                                    {
                                                        if (!CCUser.Contains(GetUser.LOG_EMAIL))
                                                            CCUser += GetUser.LOG_EMAIL + ",";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        GetFunction = context.ICORE_FUNCTIONS.Where(m => m.F_NAME.Contains(PreviousQueue)).FirstOrDefault();
                        if (GetFunction != null)
                        {
                            int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                            if (ROlEIdsWithCaseTitles.Count() > 0)
                            {
                                List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == GetFunction.F_ID && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                if (checkfunction != null)
                                {
                                    foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                    {
                                        ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                        if (getRoleFromFunction != null)
                                        {
                                            List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                            if (GetRolesDetail.Count() > 0)
                                            {
                                                foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                {
                                                    ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                    if (GetUser != null)
                                                    {
                                                        if (!CCUser.Contains(GetUser.LOG_EMAIL))
                                                            CCUser += GetUser.LOG_EMAIL + ",";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ICORE_FUNCTIONS GetFunctionofNewQueue = context.ICORE_FUNCTIONS.Where(m => m.F_NAME.Contains(NewQueue)).FirstOrDefault();
                        if (GetFunctionofNewQueue != null)
                        {
                            int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                            if (ROlEIdsWithCaseTitles.Count() > 0)
                            {
                                List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == GetFunctionofNewQueue.F_ID && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                if (checkfunction != null)
                                {
                                    foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                    {
                                        ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                        if (getRoleFromFunction != null)
                                        {
                                            List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                            if (GetRolesDetail.Count() > 0)
                                            {
                                                foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                {
                                                    ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                    if (GetUser != null)
                                                    {
                                                        if (!CCUser.Contains(GetUser.LOG_EMAIL))
                                                            CCUser += GetUser.LOG_EMAIL + ",";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (CCUser != "")
                                CCUser = CCUser.Remove(CCUser.Length - 1);
                        }
                        #endregion
                        if (Application_Mode == true)
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                string LoggedInUser = Entity.SCFM_INSERTED_BY;
                                string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                string HostName = CheckEmail.EC_SEREVER_HOST;
                                int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                NetworkCredentials.UserName = FromEmail;
                                mail.From = new MailAddress(FromEmail);
                                mail.To.Add(LoggedInUser);
                                if (CCUser != "")
                                {
                                    string[] CC_Email = CCUser.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                }
                                mail.Subject = "FX Digitalization (Case No : " + Entity.SCFM_CASE_NO + ").";
                                ICORE_SUBMIT_CASE_STATUS StatusToShow = context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID == Status_Id).FirstOrDefault();
                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:40px;text-align:center'>Foreign Exchange Case Submission</p><p style='font-family:Candara;font-size:15px'>Dear Valued Client,</p><p style='font-family:Candara;font-size:15px'>Below is the current status of your case:<br />  </p><p style='font-family:Candara;font-size:15px'>Case No# : <span style='font-family:Arial'><b>" + Entity.SCFM_CASE_NO + "</b></span></p><p style='font-family:Candara;font-size:15px'>Case Status : <b style='font-size:18px'>" + StatusToShow.SCFS_NAME + " -- " + StatusToShow.SCFS_DESCRIPTION + "</b></p><p style='font-family:Candara;font-size:15px'>Submitted Date & Time : <span style='font-family:Arial'><b>" + DateTime.Now.AddHours(5).ToLongDateString() + "</b></span></p><p style='font-family:Candara;font-size:15px'>Comments : <span style='font-family:Arial'><b>" + Comment + "</b></span></p><p style='font-family:Candara;font-size:15px'> <b>This is a system generated email, please do reply all. In case of queries please call <span style='font-family:arial'>111-777-777</span> for respective your service contact. </b></p></div></div></div><div class='col-md-2'></div></div>";
                                mail.IsBodyHtml = true;

                                using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                {
                                    smtp.Credentials = NetworkCredentials;
                                    smtp.Send(mail);
                                }
                                message += "Email sent sucessfully" + Environment.NewLine;
                            }
                        }
                        else
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                string LoggedInUser = Entity.SCFM_INSERTED_BY;
                                string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                string HostName = CheckEmail.EC_SEREVER_HOST;
                                int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                NetworkCredentials.UserName = FromEmail;
                                NetworkCredentials.Password = "Testuser0592";
                                mail.From = new MailAddress(FromEmail);
                                mail.To.Add(LoggedInUser);
                                if (CCUser != "")
                                {
                                    string[] CC_Email = CCUser.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                }
                                mail.Subject = "FX Digitalization (Case No : " + Entity.SCFM_CASE_NO + ").";
                                ICORE_SUBMIT_CASE_STATUS StatusToShow = context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID == Status_Id).FirstOrDefault();
                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:40px;text-align:center'>Foreign Exchange Case Submission</p><p style='font-family:Candara;font-size:15px'>Dear Valued Client,</p><p style='font-family:Candara;font-size:15px'>Below is the current status of your case:<br />  </p><p style='font-family:Candara;font-size:15px'>Case No# : <span style='font-family:Arial'><b>" + Entity.SCFM_CASE_NO + "</b></span></p><p style='font-family:Candara;font-size:15px'>Case Status : <b style='font-size:18px'>" + StatusToShow.SCFS_NAME + " -- " + StatusToShow.SCFS_DESCRIPTION + "</b></p><p style='font-family:Candara;font-size:15px'>Submitted Date & Time : <span style='font-family:Arial'><b>" + DateTime.Now.AddHours(5).ToLongDateString() + "</b></span></p><p style='font-family:Candara;font-size:15px'>Comments : <span style='font-family:Arial'><b>" + Comment + "</b></span></p><p style='font-family:Candara;font-size:15px'> <b>This is a system generated email, please do reply all. In case of queries please call <span style='font-family:arial'>111-777-777</span> for respective your service contact. </b></p></div></div></div><div class='col-md-2'></div></div>";
                                mail.IsBodyHtml = true;

                                using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                {
                                    smtp.Credentials = NetworkCredentials;
                                    smtp.EnableSsl = true;
                                    smtp.Send(mail);
                                }
                                message += "Email sent sucessfully" + Environment.NewLine;
                            }
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For Submitted Form ID : " + ID + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += ex.Message + Environment.NewLine;
            }
            return (message);
        }
        public static string ComplianceEmailWithoutClient(int ID, int Status_Id, string PreviousQueue, string NewQueue, string Comment)
        {
            string message = "";
            try
            {
                FXDIGEntities context = new FXDIGEntities();
                ICORE_EMAIL_CONFIGRATION CheckEmail = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (CheckEmail != null)
                {
                    ICORE_SUBMIT_CASE_FORM_MASTER Entity = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == ID).FirstOrDefault();
                    if (Entity != null)
                    {
                        #region Get Email CC
                        string CCUser = "";
                        string TOUSER = "";
                        ICORE_CASE_FORMS_MASTER FormDetails = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == Entity.SCFM_CASE_FORM_ID).FirstOrDefault();
                        ICORE_CASE_TITLES Title = context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Id == FormDetails.CaseForm_CaseTitle_Id).FirstOrDefault();
                        ICORE_FUNCTIONS GetFunction = context.ICORE_FUNCTIONS.Where(m => m.F_NAME.Contains("Ops Queue")).FirstOrDefault();
                        if (GetFunction != null)
                        {
                            int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                            if (ROlEIdsWithCaseTitles.Count() > 0)
                            {
                                List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == GetFunction.F_ID && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                if (checkfunction != null)
                                {
                                    foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                    {
                                        ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                        if (getRoleFromFunction != null)
                                        {
                                            List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                            if (GetRolesDetail.Count() > 0)
                                            {
                                                foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                {
                                                    ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                    if (GetUser != null)
                                                    {
                                                        if (!CCUser.Contains(GetUser.LOG_EMAIL))
                                                            CCUser += GetUser.LOG_EMAIL + ",";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        GetFunction = context.ICORE_FUNCTIONS.Where(m => m.F_NAME.Contains(PreviousQueue)).FirstOrDefault();
                        if (GetFunction != null)
                        {
                            int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                            if (ROlEIdsWithCaseTitles.Count() > 0)
                            {
                                List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == GetFunction.F_ID && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                if (checkfunction != null)
                                {
                                    foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                    {
                                        ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                        if (getRoleFromFunction != null)
                                        {
                                            List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                            if (GetRolesDetail.Count() > 0)
                                            {
                                                foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                {
                                                    ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                    if (GetUser != null)
                                                    {
                                                        if (!CCUser.Contains(GetUser.LOG_EMAIL))
                                                            CCUser += GetUser.LOG_EMAIL + ",";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (CCUser != "")
                                CCUser = CCUser.Remove(CCUser.Length - 1);
                        }
                        ICORE_FUNCTIONS GetFunctionofNewQueue = context.ICORE_FUNCTIONS.Where(m => m.F_NAME.Contains(NewQueue)).FirstOrDefault();
                        if (GetFunctionofNewQueue != null)
                        {
                            int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                            if (ROlEIdsWithCaseTitles.Count() > 0)
                            {
                                List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == GetFunctionofNewQueue.F_ID && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                if (checkfunction != null)
                                {
                                    foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                    {
                                        ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                        if (getRoleFromFunction != null)
                                        {
                                            List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                            if (GetRolesDetail.Count() > 0)
                                            {
                                                foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                {
                                                    ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                    if (GetUser != null)
                                                    {
                                                        if (!TOUSER.Contains(GetUser.LOG_EMAIL))
                                                            TOUSER += GetUser.LOG_EMAIL + ",";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (TOUSER != "")
                                TOUSER = TOUSER.Remove(TOUSER.Length - 1);
                        }
                        if (TOUSER == "")
                        {
                            message = "This Case Title : " + Title.CaseTitle_Name + " is not assign to any user on " + NewQueue + Environment.NewLine;
                            return (message);
                        }
                        #endregion
                        if (Application_Mode == true)
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                string HostName = CheckEmail.EC_SEREVER_HOST;
                                int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                NetworkCredentials.UserName = FromEmail;
                                mail.From = new MailAddress(FromEmail);
                                if (TOUSER != "")
                                {
                                    string[] TO_Email = TOUSER.Split(',');
                                    foreach (string MultiEmail in TO_Email)
                                    {
                                        mail.To.Add(MultiEmail);
                                    }
                                }
                                if (CCUser != "")
                                {
                                    string[] CC_Email = CCUser.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                }
                                mail.Subject = "FX Digitalization (Case No : " + Entity.SCFM_CASE_NO + ").";
                                ICORE_SUBMIT_CASE_STATUS StatusToShow = context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID == Status_Id).FirstOrDefault();
                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:40px;text-align:center'>Foreign Exchange Case Submission</p><p style='font-family:Candara;font-size:15px'>Dear Valued Client,</p><p style='font-family:Candara;font-size:15px'>Below is the current status of your case:<br />  </p><p style='font-family:Candara;font-size:15px'>Case No# : <span style='font-family:Arial'><b>" + Entity.SCFM_CASE_NO + "</b></span></p><p style='font-family:Candara;font-size:15px'>Case Status : <b style='font-size:18px'>" + StatusToShow.SCFS_NAME + " -- " + StatusToShow.SCFS_DESCRIPTION + "</b></p><p style='font-family:Candara;font-size:15px'>Submitted Date & Time : <span style='font-family:Arial'><b>" + DateTime.Now.AddHours(5).ToLongDateString() + "</b></span></p><p style='font-family:Candara;font-size:15px'>Comments : <span style='font-family:Arial'><b>" + Comment + "</b></span></p><p style='font-family:Candara;font-size:15px'> <b>This is a system generated email, please do reply all. In case of queries please call <span style='font-family:arial'>111-777-777</span> for respective your service contact. </b></p></div></div></div><div class='col-md-2'></div></div>";
                                mail.IsBodyHtml = true;

                                using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                {
                                    smtp.Credentials = NetworkCredentials;
                                    smtp.Send(mail);
                                }
                                message += "Email sent sucessfully" + Environment.NewLine;
                            }
                        }
                        else
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                string HostName = CheckEmail.EC_SEREVER_HOST;
                                int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                NetworkCredentials.UserName = FromEmail;
                                NetworkCredentials.Password = "Testuser0592";
                                mail.From = new MailAddress(FromEmail);
                                if (TOUSER != "")
                                {
                                    string[] TO_Email = TOUSER.Split(',');
                                    foreach (string MultiEmail in TO_Email)
                                    {
                                        mail.To.Add(MultiEmail);
                                    }
                                }
                                if (CCUser != "")
                                {
                                    string[] CC_Email = CCUser.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                }
                                mail.Subject = "FX Digitalization (Case No : " + Entity.SCFM_CASE_NO + ").";
                                ICORE_SUBMIT_CASE_STATUS StatusToShow = context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID == Status_Id).FirstOrDefault();
                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:40px;text-align:center'>Foreign Exchange Case Submission</p><p style='font-family:Candara;font-size:15px'>Dear Valued Client,</p><p style='font-family:Candara;font-size:15px'>Below is the current status of your case:<br />  </p><p style='font-family:Candara;font-size:15px'>Case No# : <span style='font-family:Arial'><b>" + Entity.SCFM_CASE_NO + "</b></span></p><p style='font-family:Candara;font-size:15px'>Case Status : <b style='font-size:18px'>" + StatusToShow.SCFS_NAME + " -- " + StatusToShow.SCFS_DESCRIPTION + "</b></p><p style='font-family:Candara;font-size:15px'>Submitted Date & Time : <span style='font-family:Arial'><b>" + DateTime.Now.AddHours(5).ToLongDateString() + "</b></span></p><p style='font-family:Candara;font-size:15px'>Comments : <span style='font-family:Arial'><b>" + Comment + "</b></span></p><p style='font-family:Candara;font-size:15px'> <b>This is a system generated email, please do reply all. In case of queries please call <span style='font-family:arial'>111-777-777</span> for respective your service contact. </b></p></div></div></div><div class='col-md-2'></div></div>";
                                mail.IsBodyHtml = true;

                                using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                {
                                    smtp.Credentials = NetworkCredentials;
                                    smtp.EnableSsl = true;
                                    smtp.Send(mail);
                                }
                                message += "Email sent sucessfully" + Environment.NewLine;
                            }
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For Submitted Form ID : " + ID + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += ex.Message + Environment.NewLine;
            }
            return (message);
        }
        #endregion

        #region Regulatory Head
        public static string RegulatoryEmail(int ID, int Status_Id, string PreviousQueue, string NewQueue, string Comment)
        {
            string message = "";
            try
            {
                FXDIGEntities context = new FXDIGEntities();
                ICORE_EMAIL_CONFIGRATION CheckEmail = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (CheckEmail != null)
                {
                    ICORE_SUBMIT_CASE_FORM_MASTER Entity = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == ID).FirstOrDefault();
                    if (Entity != null)
                    {
                        #region Get Email CC
                        string CCUser = "";
                        ICORE_CASE_FORMS_MASTER FormDetails = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == Entity.SCFM_CASE_FORM_ID).FirstOrDefault();
                        ICORE_CASE_TITLES Title = context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Id == FormDetails.CaseForm_CaseTitle_Id).FirstOrDefault();

                        ICORE_FUNCTIONS GetFunction = context.ICORE_FUNCTIONS.Where(m => m.F_NAME.Contains("Ops Queue")).FirstOrDefault();
                        if (GetFunction != null)
                        {
                            int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                            if (ROlEIdsWithCaseTitles.Count() > 0)
                            {
                                List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == GetFunction.F_ID && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                if (checkfunction != null)
                                {
                                    foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                    {
                                        ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                        if (getRoleFromFunction != null)
                                        {
                                            List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                            if (GetRolesDetail.Count() > 0)
                                            {
                                                foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                {
                                                    ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                    if (GetUser != null)
                                                    {
                                                        if (!CCUser.Contains(GetUser.LOG_EMAIL))
                                                            CCUser += GetUser.LOG_EMAIL + ",";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        GetFunction = context.ICORE_FUNCTIONS.Where(m => m.F_NAME.Contains(PreviousQueue)).FirstOrDefault();
                        if (GetFunction != null)
                        {
                            int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                            if (ROlEIdsWithCaseTitles.Count() > 0)
                            {
                                List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == GetFunction.F_ID && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                if (checkfunction != null)
                                {
                                    foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                    {
                                        ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                        if (getRoleFromFunction != null)
                                        {
                                            List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                            if (GetRolesDetail.Count() > 0)
                                            {
                                                foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                {
                                                    ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                    if (GetUser != null)
                                                    {
                                                        if (!CCUser.Contains(GetUser.LOG_EMAIL))
                                                            CCUser += GetUser.LOG_EMAIL + ",";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ICORE_FUNCTIONS GetFunctionofNewQueue = context.ICORE_FUNCTIONS.Where(m => m.F_NAME.Contains(NewQueue)).FirstOrDefault();
                        if (GetFunctionofNewQueue != null)
                        {
                            int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                            if (ROlEIdsWithCaseTitles.Count() > 0)
                            {
                                List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == GetFunctionofNewQueue.F_ID && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                if (checkfunction != null)
                                {
                                    foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                    {
                                        ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                        if (getRoleFromFunction != null)
                                        {
                                            List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                            if (GetRolesDetail.Count() > 0)
                                            {
                                                foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                {
                                                    ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                    if (GetUser != null)
                                                    {
                                                        if (!CCUser.Contains(GetUser.LOG_EMAIL))
                                                            CCUser += GetUser.LOG_EMAIL + ",";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (CCUser != "")
                                CCUser = CCUser.Remove(CCUser.Length - 1);
                        }
                        #endregion
                        if (Application_Mode == true)
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                string LoggedInUser = Entity.SCFM_INSERTED_BY;
                                string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                string HostName = CheckEmail.EC_SEREVER_HOST;
                                int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                NetworkCredentials.UserName = FromEmail;
                                mail.From = new MailAddress(FromEmail);
                                mail.To.Add(LoggedInUser);
                                if (CCUser != "")
                                {
                                    string[] CC_Email = CCUser.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                }
                                mail.Subject = "FX Digitalization (Case No : " + Entity.SCFM_CASE_NO + ").";
                                ICORE_SUBMIT_CASE_STATUS StatusToShow = context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID == Status_Id).FirstOrDefault();
                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:40px;text-align:center'>Foreign Exchange Case Submission</p><p style='font-family:Candara;font-size:15px'>Dear Valued Client,</p><p style='font-family:Candara;font-size:15px'>Below is the current status of your case:<br />  </p><p style='font-family:Candara;font-size:15px'>Case No# : <span style='font-family:Arial'><b>" + Entity.SCFM_CASE_NO + "</b></span></p><p style='font-family:Candara;font-size:15px'>Case Status : <b style='font-size:18px'>" + StatusToShow.SCFS_NAME + " -- " + StatusToShow.SCFS_DESCRIPTION + "</b></p><p style='font-family:Candara;font-size:15px'>Submitted Date & Time : <span style='font-family:Arial'><b>" + DateTime.Now.AddHours(5).ToLongDateString() + "</b></span></p><p style='font-family:Candara;font-size:15px'>Comments : <span style='font-family:Arial'><b>" + Comment + "</b></span></p><p style='font-family:Candara;font-size:15px'> <b>This is a system generated email, please do reply all. In case of queries please call <span style='font-family:arial'>111-777-777</span> for respective your service contact. </b></p></div></div></div><div class='col-md-2'></div></div>";
                                mail.IsBodyHtml = true;

                                using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                {
                                    smtp.Credentials = NetworkCredentials;
                                    smtp.Send(mail);
                                }
                                message += "Email sent sucessfully" + Environment.NewLine;
                            }
                        }
                        else
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                string LoggedInUser = Entity.SCFM_INSERTED_BY;
                                string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                string HostName = CheckEmail.EC_SEREVER_HOST;
                                int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                NetworkCredentials.UserName = FromEmail;
                                NetworkCredentials.Password = "Testuser0592";
                                mail.From = new MailAddress(FromEmail);
                                mail.To.Add(LoggedInUser);
                                if (CCUser != "")
                                {
                                    string[] CC_Email = CCUser.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                }
                                mail.Subject = "FX Digitalization (Case No : " + Entity.SCFM_CASE_NO + ").";
                                ICORE_SUBMIT_CASE_STATUS StatusToShow = context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID == Status_Id).FirstOrDefault();
                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:40px;text-align:center'>Foreign Exchange Case Submission</p><p style='font-family:Candara;font-size:15px'>Dear Valued Client,</p><p style='font-family:Candara;font-size:15px'>Below is the current status of your case:<br />  </p><p style='font-family:Candara;font-size:15px'>Case No# : <span style='font-family:Arial'><b>" + Entity.SCFM_CASE_NO + "</b></span></p><p style='font-family:Candara;font-size:15px'>Case Status : <b style='font-size:18px'>" + StatusToShow.SCFS_NAME + " -- " + StatusToShow.SCFS_DESCRIPTION + "</b></p><p style='font-family:Candara;font-size:15px'>Submitted Date & Time : <span style='font-family:Arial'><b>" + DateTime.Now.AddHours(5).ToLongDateString() + "</b></span></p><p style='font-family:Candara;font-size:15px'>Comments : <span style='font-family:Arial'><b>" + Comment + "</b></span></p><p style='font-family:Candara;font-size:15px'> <b>This is a system generated email, please do reply all. In case of queries please call <span style='font-family:arial'>111-777-777</span> for respective your service contact. </b></p></div></div></div><div class='col-md-2'></div></div>";
                                mail.IsBodyHtml = true;

                                using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                {
                                    smtp.Credentials = NetworkCredentials;
                                    smtp.EnableSsl = true;
                                    smtp.Send(mail);
                                }
                                message += "Email sent sucessfully" + Environment.NewLine;
                            }
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For Submitted Form ID : " + ID + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += ex.Message + Environment.NewLine;
            }
            return (message);
        }
        public static string RegulatoryEmailWithoutClient(int ID, int Status_Id, string PreviousQueue, string NewQueue, string Comment)
        {
            string message = "";
            try
            {
                FXDIGEntities context = new FXDIGEntities();
                ICORE_EMAIL_CONFIGRATION CheckEmail = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true).FirstOrDefault();
                if (CheckEmail != null)
                {
                    ICORE_SUBMIT_CASE_FORM_MASTER Entity = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == ID).FirstOrDefault();
                    if (Entity != null)
                    {
                        #region Get Email CC
                        string CCUser = "";
                        string TOUSER = "";
                        ICORE_CASE_FORMS_MASTER FormDetails = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == Entity.SCFM_CASE_FORM_ID).FirstOrDefault();
                        ICORE_CASE_TITLES Title = context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Id == FormDetails.CaseForm_CaseTitle_Id).FirstOrDefault();
                        ICORE_FUNCTIONS GetFunction = context.ICORE_FUNCTIONS.Where(m => m.F_NAME.Contains("Ops Queue")).FirstOrDefault();
                        if (GetFunction != null)
                        {
                            int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                            if (ROlEIdsWithCaseTitles.Count() > 0)
                            {
                                List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == GetFunction.F_ID && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                if (checkfunction != null)
                                {
                                    foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                    {
                                        ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                        if (getRoleFromFunction != null)
                                        {
                                            List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                            if (GetRolesDetail.Count() > 0)
                                            {
                                                foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                {
                                                    ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                    if (GetUser != null)
                                                    {
                                                        if (!CCUser.Contains(GetUser.LOG_EMAIL))
                                                            CCUser += GetUser.LOG_EMAIL + ",";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        GetFunction = context.ICORE_FUNCTIONS.Where(m => m.F_NAME.Contains(PreviousQueue)).FirstOrDefault();
                        if (GetFunction != null)
                        {
                            int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                            if (ROlEIdsWithCaseTitles.Count() > 0)
                            {
                                List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == GetFunction.F_ID && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                if (checkfunction != null)
                                {
                                    foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                    {
                                        ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                        if (getRoleFromFunction != null)
                                        {
                                            List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                            if (GetRolesDetail.Count() > 0)
                                            {
                                                foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                {
                                                    ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                    if (GetUser != null)
                                                    {
                                                        if (!CCUser.Contains(GetUser.LOG_EMAIL))
                                                            CCUser += GetUser.LOG_EMAIL + ",";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (CCUser != "")
                                CCUser = CCUser.Remove(CCUser.Length - 1);
                        }
                        ICORE_FUNCTIONS GetFunctionofNewQueue = context.ICORE_FUNCTIONS.Where(m => m.F_NAME.Contains(NewQueue)).FirstOrDefault();
                        if (GetFunctionofNewQueue != null)
                        {
                            int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                            if (ROlEIdsWithCaseTitles.Count() > 0)
                            {
                                List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == GetFunctionofNewQueue.F_ID && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                if (checkfunction != null)
                                {
                                    foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                    {
                                        ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                        if (getRoleFromFunction != null)
                                        {
                                            List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                            if (GetRolesDetail.Count() > 0)
                                            {
                                                foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                {
                                                    ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                    if (GetUser != null)
                                                    {
                                                        if (!TOUSER.Contains(GetUser.LOG_EMAIL))
                                                            TOUSER += GetUser.LOG_EMAIL + ",";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (TOUSER != "")
                                TOUSER = TOUSER.Remove(TOUSER.Length - 1);
                        }
                        if (TOUSER == "")
                        {
                            message = "This Case Title : " + Title.CaseTitle_Name + " is not assign to any user on " + NewQueue + Environment.NewLine;
                            return (message);
                        }
                        #endregion
                        if (Application_Mode == true)
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                string HostName = CheckEmail.EC_SEREVER_HOST;
                                int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                NetworkCredentials.UserName = FromEmail;
                                mail.From = new MailAddress(FromEmail);
                                if (TOUSER != "")
                                {
                                    string[] TO_Email = TOUSER.Split(',');
                                    foreach (string MultiEmail in TO_Email)
                                    {
                                        mail.To.Add(MultiEmail);
                                    }
                                }
                                if (CCUser != "")
                                {
                                    string[] CC_Email = CCUser.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                }
                                mail.Subject = "FX Digitalization (Case No : " + Entity.SCFM_CASE_NO + ").";
                                ICORE_SUBMIT_CASE_STATUS StatusToShow = context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID == Status_Id).FirstOrDefault();
                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:40px;text-align:center'>Foreign Exchange Case Submission</p><p style='font-family:Candara;font-size:15px'>Dear Valued Client,</p><p style='font-family:Candara;font-size:15px'>Below is the current status of your case:<br />  </p><p style='font-family:Candara;font-size:15px'>Case No# : <span style='font-family:Arial'><b>" + Entity.SCFM_CASE_NO + "</b></span></p><p style='font-family:Candara;font-size:15px'>Case Status : <b style='font-size:18px'>" + StatusToShow.SCFS_NAME + " -- " + StatusToShow.SCFS_DESCRIPTION + "</b></p><p style='font-family:Candara;font-size:15px'>Submitted Date & Time : <span style='font-family:Arial'><b>" + DateTime.Now.AddHours(5).ToLongDateString() + "</b></span></p><p style='font-family:Candara;font-size:15px'>Comments : <span style='font-family:Arial'><b>" + Comment + "</b></span></p><p style='font-family:Candara;font-size:15px'> <b>This is a system generated email, please do reply all. In case of queries please call <span style='font-family:arial'>111-777-777</span> for respective your service contact. </b></p></div></div></div><div class='col-md-2'></div></div>";
                                mail.IsBodyHtml = true;

                                using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                {
                                    smtp.Credentials = NetworkCredentials;
                                    smtp.Send(mail);
                                }
                                message += "Email sent sucessfully" + Environment.NewLine;
                            }
                        }
                        else
                        {
                            using (MailMessage mail = new MailMessage())
                            {
                                string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                string HostName = CheckEmail.EC_SEREVER_HOST;
                                int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                NetworkCredentials.UserName = FromEmail;
                                NetworkCredentials.Password = "Testuser0592";
                                mail.From = new MailAddress(FromEmail);
                                if (TOUSER != "")
                                {
                                    string[] TO_Email = TOUSER.Split(',');
                                    foreach (string MultiEmail in TO_Email)
                                    {
                                        mail.To.Add(MultiEmail);
                                    }
                                }
                                if (CCUser != "")
                                {
                                    string[] CC_Email = CCUser.Split(',');
                                    foreach (string MultiEmail in CC_Email)
                                    {
                                        mail.CC.Add(MultiEmail);
                                    }
                                }
                                mail.Subject = "FX Digitalization (Case No : " + Entity.SCFM_CASE_NO + ").";
                                ICORE_SUBMIT_CASE_STATUS StatusToShow = context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID == Status_Id).FirstOrDefault();
                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:40px;text-align:center'>Foreign Exchange Case Submission</p><p style='font-family:Candara;font-size:15px'>Dear Valued Client,</p><p style='font-family:Candara;font-size:15px'>Below is the current status of your case:<br />  </p><p style='font-family:Candara;font-size:15px'>Case No# : <span style='font-family:Arial'><b>" + Entity.SCFM_CASE_NO + "</b></span></p><p style='font-family:Candara;font-size:15px'>Case Status : <b style='font-size:18px'>" + StatusToShow.SCFS_NAME + " -- " + StatusToShow.SCFS_DESCRIPTION + "</b></p><p style='font-family:Candara;font-size:15px'>Submitted Date & Time : <span style='font-family:Arial'><b>" + DateTime.Now.AddHours(5).ToLongDateString() + "</b></span></p><p style='font-family:Candara;font-size:15px'>Comments : <span style='font-family:Arial'><b>" + Comment + "</b></span></p><p style='font-family:Candara;font-size:15px'> <b>This is a system generated email, please do reply all. In case of queries please call <span style='font-family:arial'>111-777-777</span> for respective your service contact. </b></p></div></div></div><div class='col-md-2'></div></div>";
                                mail.IsBodyHtml = true;

                                using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                {
                                    smtp.Credentials = NetworkCredentials;
                                    smtp.EnableSsl = true;
                                    smtp.Send(mail);
                                }
                                message += "Email sent sucessfully" + Environment.NewLine;
                            }
                        }
                    }
                    else
                    {
                        message += "Exception : Problem while Fetching Data For Submitted Form ID : " + ID + Environment.NewLine;
                    }
                }
                else
                {
                    message += "No Email Configration found." + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                message += ex.Message + Environment.NewLine;
            }
            return (message);
        }
        #endregion
    }
}