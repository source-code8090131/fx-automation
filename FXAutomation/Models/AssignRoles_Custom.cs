﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FXAutomation.Models
{
    public class AssignRoles_Custom
    {
        public ICORE_LOGIN Entity { get; set; }
        public int?[] SelectedRoles { get; set; }
        public IEnumerable<SelectListItem> RolesList { get; set; }
    }
}