﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace FXAutomation.Models
{
    public class WriteEERS
    {  
        public void WriteERSFeed(string P)
        {
            string message = "";
            try
            {
                string complete_path = P;
                string filename = "EERS 174894.txt";
                string path = complete_path + filename;
                if (!System.IO.Directory.Exists(complete_path))
                {
                    System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(complete_path);
                }
                DateTime creation = File.GetLastWriteTime(path);
                if (creation.Date < DateTime.Now.Date)
                {
                    WriteEERSFile(path);
                }
                else
                {
                    string FileContent = File.ReadAllText(path);
                    if (FileContent == "")
                    {
                        WriteEERSFile(path);
                    }
                }
            }
            catch (Exception ex)
            {
                message = "Exception Occured " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
            }
            finally
            {
                if (message != "")
                {
                    System.Diagnostics.EventLog.WriteEntry("FXDIG", message, System.Diagnostics.EventLogEntryType.Error);
                }
            }
        }


        public void WriteEERSFile(string path)
        {
            File.WriteAllText(path, String.Empty);
            using (System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write))
            {
                int TLRRowCount = 0;
                System.IO.StreamWriter writer = new System.IO.StreamWriter(fs);
                writer.WriteLine("HDR " + DateTime.Now.ToString("yyyy/MM/dd"));
                writer.WriteLine("----------------------------------------------------------------------------------------------");
                using (FXDIGEntities context = new FXDIGEntities())
                {
                    List<ICORE_LOGIN> Users = context.ICORE_LOGIN.Where(m => m.LOG_USER_STATUS == "Active" && m.LOG_USER_ID != "Admin123").ToList();
                    TLRRowCount = Users.Count;
                    foreach (ICORE_LOGIN user in Users)
                    {
                        List<int?> AssignRolesIds = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_USER_ID == user.LOG_USER_ID).Select(m => m.ALR_ROLE_ID).ToList();
                        foreach (int? RoleId in AssignRolesIds)
                        {
                            ICORE_ROLES RoleInfo = context.ICORE_ROLES.Where(m => m.R_ID == RoleId).FirstOrDefault();
                            if (RoleInfo != null)
                            {
                                writer.WriteLine("174894 \t " + user.LOG_USER_NAME + "\t" + "   " + user.LOG_RIST_ID + "\t" + "    " + RoleInfo.R_NAME + "\t" + "    " + RoleInfo.R_DESCRIPTION + "." + user.LOG_USER_ID);
                            }
                        }
                    }
                }
                writer.WriteLine("----------------------------------------------------------------------------------------------");
                writer.WriteLine("TLR " + TLRRowCount);
                writer.Close();
                fs.Close();
            }
        }

    }
}