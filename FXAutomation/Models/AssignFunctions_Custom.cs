﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FXAutomation.Models
{
    public class AssignFunctions_Custom
    {

        public ICORE_ROLES Entity { get; set; }
        public int?[] SelectedFunctions { get; set; }
        public IEnumerable<SelectListItem> FunctionsList { get; set; }
        public ICORE_ASSIGN_CASE_TITLES CaseTitileEntity { get; set; }
        public int?[] SelectedCaseTitles { get; set; }
        public IEnumerable<SelectListItem> CaseTitlesList { get; set; }

    }
}