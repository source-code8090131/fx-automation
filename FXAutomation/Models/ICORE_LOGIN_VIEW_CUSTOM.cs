﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FXAutomation.Models
{
    public class ICORE_LOGIN_VIEW_CUSTOM
    {
        public int LOG_ID { get; set; }
        public string LOG_USER_ID { get; set; }
        public string LOG_HASH { get; set; }
    
        public string DOMAIN_NAME { get; set; }
    }
}