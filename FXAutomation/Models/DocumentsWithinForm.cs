﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FXAutomation.Models
{
    public class DocumentsWithinForm
    {
        public int SCD_ID { get; set; }
        public Nullable<int> SCD_DOCUMENT_ID { get; set; }
        public string SCD_DOCUMENT_VALUE { get; set; }
        public Nullable<System.DateTime> SCD_ENTRY_DATETIME { get; set; }
        public Nullable<System.DateTime> SCD_EDIT_DATETIME { get; set; }
        public string SCD_MAKER_ID { get; set; }
        public string SCD_CHECKER_ID { get; set; }
        public HttpPostedFileBase PostedFile { get; set; }
            
    }
}