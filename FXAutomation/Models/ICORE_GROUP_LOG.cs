//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FXAutomation.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ICORE_GROUP_LOG
    {
        public int GLOG_ID { get; set; }
        public string GLOG_ROLE_NAME { get; set; }
        public string GLOG_DESCRIPTION { get; set; }
        public string GLOG_ASSIGNED_ROLES { get; set; }
        public string GLOG_ADDED_BY { get; set; }
        public Nullable<System.DateTime> GLOG_ADDED_DATETIME { get; set; }
        public string GLOG_ACTIVATED_BY { get; set; }
        public Nullable<System.DateTime> GLOG_ACTIVATED_DATETIME { get; set; }
        public string GLOG_CURRENT_STATUS { get; set; }
        public System.DateTime GLOG_ENTRY_DATETIME { get; set; }
    }
}
