//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FXAutomation.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ICORE_CASE_TITLES_VIEW
    {
        public string CaseType_Code { get; set; }
        public string CaseType_Name { get; set; }
        public string CaseType_Description { get; set; }
        public Nullable<decimal> CaseType_CategoryId { get; set; }
        public string CaseType_MakerId { get; set; }
        public string CaseType_CheckerId { get; set; }
        public Nullable<System.DateTime> CaseType_DataEditDateTime { get; set; }
        public Nullable<System.DateTime> CaseType_DataEntryDateTime { get; set; }
        public Nullable<bool> CaseType_Status { get; set; }
        public Nullable<System.DateTime> CaseTitle_DataEntryDateTime { get; set; }
        public decimal CaseTitle_Id { get; set; }
        public string CaseTitle_Code { get; set; }
        public string CaseTitle_Name { get; set; }
        public string CaseTitle_Description { get; set; }
        public Nullable<bool> CaseTitle_Status { get; set; }
        public Nullable<System.DateTime> CaseTitle_DataEditDateTime { get; set; }
        public string CaseTitle_MakerId { get; set; }
        public Nullable<decimal> CaseTitle_CaseTypeId { get; set; }
        public string CaseTitle_CheckerId { get; set; }
        public Nullable<bool> CaseTitle_IsAuth { get; set; }
    }
}
