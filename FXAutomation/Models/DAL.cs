﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using FXAutomation.Models;

namespace FXAutomation
{
    public class DAL
    {
        #region Encryption Dycrytion
        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        #endregion

        #region Check Requested User Validity
        public static bool CheckRequestedUserValidity(string userID, string UniqueId)
        {
            try
            {
                FXDIGEntities context = new FXDIGEntities();
                bool success = false;
                ICORE_LOGIN_LOG AuthCheck = context.ICORE_LOGIN_LOG.Where(m => m.LL_LOG_USER_ID == userID && m.LL_UNIQUE_ID == UniqueId).OrderByDescending(m => m.LL_ID).FirstOrDefault();
                if (AuthCheck == null)
                {
                    success = false;
                    return success;
                }
                else
                {
                    success = true;
                    return success;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region Check Function Validity
        public static bool CheckFunctionValidity(string CName, string AName, string userID)
        {
            try
            {
                FXDIGEntities context = new FXDIGEntities();
                bool success = false;
                int FunctionId = context.ICORE_FUNCTIONS.Where(m => m.F_CONTROLLER == CName && m.F_ACTION == AName).Select(m => m.F_ID).FirstOrDefault();
                List<ICORE_ASSIGN_LOGIN_RIGHTS> RoleId = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_USER_ID == userID && m.ALR_STATUS == true).ToList();
                foreach (ICORE_ASSIGN_LOGIN_RIGHTS CheckRoles in RoleId)
                {
                    ICORE_ASSIGN_FUNCTIONS CheckFunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == CheckRoles.ALR_ROLE_ID && m.AF_FUNCTION_ID == FunctionId && m.AF_STATUS == true).FirstOrDefault();
                    if (CheckFunction == null)
                    {
                        success = false;
                    }
                    else
                    {
                        success = true;
                        return success;
                    }
                }
                return success;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region Check if form is from same Queue Validity
        public static bool CheckFormQueue(string DocID, string userID)
        {
            try
            {
                FXDIGEntities context = new FXDIGEntities();
                bool success = false;
                int DocId = 0;
                bool IsValid = int.TryParse(DocID, out DocId);
                ICORE_SUBMIT_CASE_DOCUMENT Entity = context.ICORE_SUBMIT_CASE_DOCUMENT.Where(m => m.SCD_ID == DocId).FirstOrDefault();
                if (Entity != null)
                {
                    ICORE_SUBMIT_CASE_FORM_MASTER Master = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == Entity.SCD_FORM_ID).FirstOrDefault();
                    if (Master != null)
                    {
                        string Action = "";
                        if (Master.SCFM_QUEUE == "Ops Queue")
                            Action = "OpsQueue";
                        if (Master.SCFM_QUEUE == "Compliance Queue")
                            Action = "ComplianceQueue";
                        if (Master.SCFM_QUEUE == "Regulatory Head")
                            Action = "RegulatoryHead";
                        int FunctionId = context.ICORE_FUNCTIONS.Where(m => m.F_ACTION == Action).Select(m => m.F_ID).FirstOrDefault();
                        List<ICORE_ASSIGN_LOGIN_RIGHTS> RoleId = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_USER_ID == userID && m.ALR_STATUS == true).ToList();
                        foreach (ICORE_ASSIGN_LOGIN_RIGHTS CheckRoles in RoleId)
                        {
                            ICORE_ASSIGN_FUNCTIONS CheckFunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == CheckRoles.ALR_ROLE_ID && m.AF_FUNCTION_ID == FunctionId && m.AF_STATUS == true).FirstOrDefault();
                            if (CheckFunction == null)
                            {
                                success = false;
                            }
                            else
                            {
                                success = true;
                                return success;
                            }
                        }
                        return success;
                    }
                }
                return success;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool CheckFormQueueForFieldsAndDocuments(decimal? Scfm_id, string userID)
        {
            try
            {
                FXDIGEntities context = new FXDIGEntities();
                bool success = false;
                int SCFM_ID = Convert.ToInt32(Scfm_id);
                ICORE_SUBMIT_CASE_FORM_MASTER Master = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == SCFM_ID).FirstOrDefault();
                if (Master != null)
                {
                    string Action = "";
                    if (Master.SCFM_QUEUE == "Ops Queue")
                        Action = "OpsQueue";
                    if (Master.SCFM_QUEUE == "Compliance Queue")
                        Action = "ComplianceQueue";
                    if (Master.SCFM_QUEUE == "Regulatory Head")
                        Action = "RegulatoryHead";
                    int FunctionId = context.ICORE_FUNCTIONS.Where(m => m.F_ACTION == Action).Select(m => m.F_ID).FirstOrDefault();
                    List<ICORE_ASSIGN_LOGIN_RIGHTS> RoleId = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_USER_ID == userID && m.ALR_STATUS == true).ToList();
                    foreach (ICORE_ASSIGN_LOGIN_RIGHTS CheckRoles in RoleId)
                    {
                        ICORE_ASSIGN_FUNCTIONS CheckFunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == CheckRoles.ALR_ROLE_ID && m.AF_FUNCTION_ID == FunctionId && m.AF_STATUS == true).FirstOrDefault();
                        if (CheckFunction == null)
                        {
                            success = false;
                        }
                        else
                        {
                            success = true;
                            return success;
                        }
                    }
                    return success;
                }
                return success;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region Log Every Exception
        public static string LogException(string controllername, string functionName, string userId, Exception excep)
        {
            try
            {
                string path = System.Web.Hosting.HostingEnvironment.MapPath("~") + "FXDIG_logs\\";
                string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
                bool bool_icorelogs = false; bool bool_icorelogs_rps_logs = false;
                string complete_path = path + filename;
                if (System.IO.Directory.Exists(path))
                {
                    bool_icorelogs = true;
                }
                else
                {
                    System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(path);
                    bool_icorelogs = true;
                }

                if (bool_icorelogs)
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(complete_path, System.IO.FileMode.Append, System.IO.FileAccess.Write))
                    {
                        using (System.IO.StreamWriter writer = new System.IO.StreamWriter(fs))
                        {
                            writer.WriteLine("---------------------------------------------------------------");
                            writer.WriteLine("Time of logging: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                            writer.WriteLine("Class/Form/Report Name: " + controllername);
                            writer.WriteLine("Function Name: " + functionName);
                            writer.WriteLine("User ID: " + userId);
                            if (excep.InnerException != null)
                            {
                                writer.WriteLine("Inner Exception: " + excep.InnerException);
                                if (excep.InnerException.Message != null)
                                    writer.WriteLine("      Inner Exception Message : " + excep.InnerException.Message);

                                if (excep.InnerException.InnerException != null)
                                {
                                    writer.WriteLine("Inner Inner Exception: " + excep.InnerException.InnerException);
                                    if (excep.InnerException.InnerException.Message != null)
                                        writer.WriteLine("      Inner Inner Exception Message : " + excep.InnerException.InnerException.Message);
                                }
                            }
                            writer.WriteLine("Exception Description:");
                            writer.WriteLine(excep.Message);
                            writer.WriteLine("Exception Stack Trace:");
                            writer.WriteLine(excep.StackTrace);
                            writer.WriteLine("--------------------------------------------------------------- ");
                            writer.Close();
                        }
                    }
                }


                return "Some error occured, please contact software administrator.";

            }
            catch (Exception ex)
            {
                string message = "Exception Occured " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                }
                return message;
            }
        }
        #endregion
        #region Log Every Login Response
        public static string LogLoginMessage(string userId, string Domain, string logmessage)
        {
            try
            {
                string adurl = "";
                string path = System.Web.Hosting.HostingEnvironment.MapPath("~") + "FXDIG_logs\\";
                string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
                bool bool_icorelogs = false; bool bool_icorelogs_rps_logs = false;
                string complete_path = path + filename;
                if (System.IO.Directory.Exists(path))
                {
                    bool_icorelogs = true;
                }
                else
                {
                    System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(path);
                    bool_icorelogs = true;
                }

                if (bool_icorelogs)
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(complete_path, System.IO.FileMode.Append, System.IO.FileAccess.Write))
                    {
                        using (System.IO.StreamWriter writer = new System.IO.StreamWriter(fs))
                        {
                            if (Domain == "APAC")
                                adurl = "apac.nsroot.net";
                            else if (Domain == "EUR")
                                adurl = "eur.nsroot.net";
                            writer.WriteLine("---------------------------------------------------------------");
                            writer.WriteLine("Time of logging: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                            writer.WriteLine("User-Id: " + userId + " Domain: " + adurl + " Response: " + logmessage);
                            writer.WriteLine("--------------------------------------------------------------- ");
                            writer.Close();
                        }
                    }
                }


                return "Some error occured, please contact software administrator.";

            }
            catch (Exception ex)
            {
                string message = "Exception Occured " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                }
                return message;
            }
        }
        #endregion

        #region Log Every Exception
        public static string LogLdapRequest(string controllername, string functionName, string userId, dynamic result)
        {
            try
            {
                string path = System.Web.Hosting.HostingEnvironment.MapPath("~") + "FXDIG_logs\\";
                string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
                bool bool_icorelogs = false; bool bool_icorelogs_rps_logs = false;
                string complete_path = path + filename;
                if (System.IO.Directory.Exists(path))
                {
                    bool_icorelogs = true;
                }
                else
                {
                    System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(path);
                    bool_icorelogs = true;
                }

                if (bool_icorelogs)
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(complete_path, System.IO.FileMode.Append, System.IO.FileAccess.Write))
                    {
                        using (System.IO.StreamWriter writer = new System.IO.StreamWriter(fs))
                        {
                            writer.WriteLine("---------------------------------------------------------------");
                            writer.WriteLine("Time of logging: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                            writer.WriteLine("Class/Form/Report Name: " + controllername);
                            writer.WriteLine("Function Name: " + functionName);
                            writer.WriteLine("User ID: " + userId);
                            writer.WriteLine("Ldap Result: " + result);
                            writer.WriteLine("--------------------------------------------------------------- ");
                            writer.Close();
                        }
                    }
                }


                return "Some error occured, please contact software administrator.";

            }
            catch (Exception ex)
            {
                string message = "Exception Occured " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                }
                return message;
            }
        }
        #endregion
    }
}