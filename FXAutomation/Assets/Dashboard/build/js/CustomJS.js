﻿$(function () {
    $("#example1").DataTable({
        "responsive": true, "lengthChange": true, "autoWidth": false, "order": false, "lengthMenu": [[10, 50, 500, 5000, -1], [10, 50, 500, 5000, "All"]]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $("#Reporting").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false, "order": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#Reporting_wrapper .col-md-6:eq(0)');
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    })
});
$('#FormTypeFX').each(function () {
    var CheckFormType = $(this).attr('value');
    if (CheckFormType == "OnEdit") {
        $('#Entity_LOG_USER_ID').attr('readonly', true);
    }
    else {
        $('#Entity_LOG_USER_ID').attr('readonly', false);
    }
});
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});
$(function () {
    bsCustomFileInput.init();
});
    //#region Department Authorize/Reject
    $("[name='AuthorizeDept']").click(function (button) {
        var Dept_Id = $(this).attr("value");
        $.ajax
            ({
                url: '/Icore_Department/AuthorizeDept?DeptID=' + Dept_Id,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    });
    $("[name='DeleteFromAuthorizeDept']").click(function (button) {
        var Dept_Id = $(this).attr("value");
        var check = confirm("Are You Sure your want to delete?");
        if (check == true) {
            $.ajax
                ({
                    url: '/Icore_Department/DeleteFromAuthorizeDept?DeptID=' + Dept_Id,
                    type: 'POST',
                    datatype: 'application/json',
                    contentType: 'application/json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (result) {
                        alert(result);
                        location.reload();
                    },
                    error: function () {
                        //alert("Something went wrong..");
                    },
                });
        }
    });
    //#endregion

    //#region Category Authorize/Reject
    $("[name='AuthorizeCateg']").click(function (button) {
        var Categ_Id = $(this).attr("value");
        $.ajax
            ({
                url: '/Icore_Categories/AuthorizeCateg?CatID=' + Categ_Id,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    });
    $("[name='DeleteFromAuthorizeCateg']").click(function (button) {
        var CatID = $(this).attr("value");
        var check = confirm("Are You Sure your want to delete?");
        if (check == true) {
            $.ajax
                ({
                    url: '/Icore_Categories/DeleteFromAuthorizeCateg?CatID=' + CatID,
                    type: 'POST',
                    datatype: 'application/json',
                    contentType: 'application/json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (result) {
                        alert(result);
                        location.reload();
                    },
                    error: function () {
                        //alert("Something went wrong..");
                    },
                });
        }
    });
    //#endregion
    
    //#region Case Type Authorize/Reject
    $("[name='AuthorizeCaseType']").click(function (button) {
        var CaseType_Id = $(this).attr("value");
        $.ajax
            ({
                url: '/Icore_Casetype/AuthorizeCaseType?CaseType_Id=' + CaseType_Id,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    });
    $("[name='DeleteFromAuthorizeCaseType']").click(function (button) {
        var CaseType_Id = $(this).attr("value");
        var check = confirm("Are You Sure your want to delete?");
        if (check == true) {
            $.ajax
                ({
                    url: '/Icore_Casetype/DeleteFromAuthorizeCaseType?CaseType_Id=' + CaseType_Id,
                    type: 'POST',
                    datatype: 'application/json',
                    contentType: 'application/json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (result) {
                        alert(result);
                        location.reload();
                    },
                    error: function () {
                        //alert("Something went wrong..");
                    },
                });
        }
    });
    //#endregion

    //#region Case Title Authorize/Reject
    $("[name='AuthorizeCaseTitle']").click(function (button) {
        var CaseTitle_Id = $(this).attr("value");
        $.ajax
            ({
                url: '/Icore_Casetitle/AuthorizeCaseTitle?CaseTitle_Id=' + CaseTitle_Id,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    });
    $("[name='DeleteFromAuthorizeCaseTitle']").click(function (button) {
        var CaseTitle_Id = $(this).attr("value");
        var check = confirm("Are You Sure your want to delete?");
        if (check == true) {
            $.ajax
                ({
                    url: '/Icore_Casetitle/DeleteFromAuthorizeCaseTitle?CaseTitle_Id=' + CaseTitle_Id,
                    type: 'POST',
                    datatype: 'application/json',
                    contentType: 'application/json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (result) {
                        alert(result);
                        location.reload();
                    },
                    error: function () {
                        //alert("Something went wrong..");
                    },
                });
        }
    });
    //#endregion

    //#region Case Form Detail Authorize/Reject/Delete
    $("[name='Authorize_CaseFormDetail']").click(function (button) {
        var CaseForm_Field_Id = $(this).attr("value");
        $.ajax
            ({
                url: '/Icore_CaseForm/AuthorizeCaseFormDetail?CaseForm_Field_Id=' + CaseForm_Field_Id,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    });
    $("[name='DeleteFromAuthorizeCaseFormDetail']").click(function (button) {
        var CaseForm_Field_Id = $(this).attr("value");
        var check = confirm("Are You Sure your want to delete?");
        if (check == true) {
            $.ajax
                ({
                    url: '/Icore_CaseForm/DeleteFromAuthorizeCaseFormDetail?CaseForm_Field_Id=' + CaseForm_Field_Id,
                    type: 'POST',
                    datatype: 'application/json',
                    contentType: 'application/json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (result) {
                        alert(result);
                        $('#loading').hide();
                    },
                    error: function () {
                        //alert("Something went wrong..");
                    },
                });
        }
    });
    $("[name='DeleteFormDetailFieldData']").click(function (button) {
        var FieldId = $(this).attr("value");
        var check = confirm("Are You Sure your want to delete?");
        if (check == true) {
            debugger
            $.ajax
                ({
                    url: '/Icore_CaseForm/DeleteFormDetailFieldData?FieldId=' + FieldId,
                    type: 'POST',
                    datatype: 'application/json',
                    contentType: 'application/json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (result) {
                        alert(result);
                        $('#loading').hide();
                    },
                    error: function () {
                        //alert("Something went wrong..");
                    },
                });
        }
    });
    //#endregion

    //#region Group Role Authorize/Reject
    $("[name='AuthroizeRole']").click(function (button) {
        var R_ID = $(this).attr("value");
        $.ajax
            ({
                url: '/Icore_Groups_Roles/AuthroizeRole?R_ID=' + R_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    });
    $("[name='DeleteFromAuthroizeRole']").click(function (button) {
        var R_ID = $(this).attr("value");
        var check = confirm("Are You Sure your want to delete?");
        if (check == true) {
            $.ajax
                ({
                    url: '/Icore_Groups_Roles/DeleteFromAuthroizeRole?R_ID=' + R_ID,
                    type: 'POST',
                    datatype: 'application/json',
                    contentType: 'application/json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (result) {
                        alert(result);
                        location.reload();
                    },
                    error: function () {
                        //alert("Something went wrong..");
                    },
                });
        }
    });
//#endregion

    //#region User Authorize/Reject
    $("[name='AuthroizeUser']").click(function (button) {
        var LOG_ID = $(this).attr("value");
        $.ajax
            ({
                url: '/Icore_User/AuthroizeUser?LOG_ID=' + LOG_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    });
    $("[name='DeleteFromAuthroizeUser']").click(function (button) {
        var LOG_ID = $(this).attr("value");
        var check = confirm("Are You Sure your want to delete?");
        if (check == true) {
            $.ajax
                ({
                    url: '/Icore_User/DeleteFromAuthroizeUser?LOG_ID=' + LOG_ID,
                    type: 'POST',
                    datatype: 'application/json',
                    contentType: 'application/json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (result) {
                        alert(result);
                        location.reload();
                    },
                    error: function () {
                        //alert("Something went wrong..");
                    },
                });
        }
    });
    //#endregion

    //#region Client Portfolio Authorize/Reject
    $("[name='AuthorizeClient']").click(function (button) {
        var CP_ID = $(this).attr("value");
        $.ajax
            ({
                url: '/CustomerProfile/AuthorizeClient?CP_ID=' + CP_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    });
    $("[name='DeleteFromAuthorizeClient']").click(function (button) {
        var CP_ID = $(this).attr("value");
        var check = confirm("Are You Sure your want to delete?");
        if (check == true) {
            $.ajax
                ({
                    url: '/CustomerProfile/DeleteFromAuthorizeClient?CP_ID=' + CP_ID,
                    type: 'POST',
                    datatype: 'application/json',
                    contentType: 'application/json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (result) {
                        alert(result);
                        location.reload();
                    },
                    error: function () {
                        //alert("Something went wrong..");
                    },
                });
        }
    });
    //#endregion

    //#region Email Configration Authorize/Reject/Set As Current
    $("[name='AuthorizeEmailSetup']").click(function (button) {
        var EC_ID = $(this).attr("value");
        $.ajax
            ({
                url: '/Setup/AuthorizeEmailSetup?EC_ID=' + EC_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    });
    $("[name='RejectEmailSetup']").click(function (button) {
        var EC_ID = $(this).attr("value");
        var check = confirm("Are You Sure your want to delete?");
        if (check == true) {
            $.ajax
                ({
                    url: '/Setup/RejectEmailSetup?EC_ID=' + EC_ID,
                    type: 'POST',
                    datatype: 'application/json',
                    contentType: 'application/json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (result) {
                        alert(result);
                        location.reload();
                    },
                    error: function () {
                        //alert("Something went wrong..");
                    },
                });
        }
    });
    $("[name='UpdateEmailSetupStatus']").click(function (button) {
        var EC_ID = $(this).attr("value");
        $.ajax
            ({
                url: '/Setup/UpdateEmailSetupStatus?EC_ID=' + EC_ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    window.location.href = '/Setup/EmailSetup';
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    });
//#endregion


//#region Queue Authorize/Reject
    $("[name='AuthozrizeQueue']").click(function (button) {
        var ID = $(this).attr("value");
        $.ajax
            ({
                url: '/Setup/AuthozrizeQueue?ID=' + ID,
                type: 'POST',
                datatype: 'application/json',
                contentType: 'application/json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (result) {
                    alert(result);
                    location.reload();
                },
                error: function () {
                    //alert("Something went wrong..");
                },
            });
    });
    $("[name='RejectQueue']").click(function (button) {
        var ID = $(this).attr("value");
        var check = confirm("Are You Sure your want to delete?");
        if (check == true) {
            $.ajax
                ({
                    url: '/Setup/RejectQueue?ID=' + ID,
                    type: 'POST',
                    datatype: 'application/json',
                    contentType: 'application/json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (result) {
                        alert(result);
                        location.reload();
                    },
                    error: function () {
                        //alert("Something went wrong..");
                    },
                });
        }
    });
    //#endregion

    $("[name='RequestLogDetail']").click(function (button) {
        var CRL_ID = $(this).attr("value");
        $.ajax({
            url: "/CustomerProfile/RequestLogDetail",
            type: "POST",
            data: { CRL_ID: CRL_ID },
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (Data) {
                $("#ViewRequestLogDetail").load("/CustomerProfile/RequestLogDetail?CRL_ID=" + CRL_ID);
            },
            error: function (error) {
                alert(error);
            }
        });
    });
    