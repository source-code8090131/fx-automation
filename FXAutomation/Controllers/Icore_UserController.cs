﻿using FXAutomation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FXAutomation.Controllers
{
    public class Icore_UserController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: Icore_User
        public ActionResult AddUser(int L = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_User", "AddUser", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    AssignRoles_Custom entity = new AssignRoles_Custom();
                    try
                    {
                        if (L != 0)
                        {
                            string SessionUser = Session["USER_ID"].ToString();
                            entity.Entity = context.ICORE_LOGIN.Where(m => m.LOG_ID == L && m.LOG_USER_ID != SessionUser).FirstOrDefault();
                            if (entity.Entity != null)
                            {
                                List<int?> PreviousRoles = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_LOG_ID == L && m.ALR_STATUS == true).Select(m => m.ALR_ROLE_ID).ToList();
                                List<SelectListItem> items = new SelectList(context.ICORE_ROLES.Where(m => m.R_ID != 1 && m.R_STATUS == "true" && m.R_ISAUTH == true).OrderBy(m => m.R_ID).ToList(), "R_ID", "R_NAME", 0).ToList();
                                foreach (SelectListItem item in items)
                                {
                                    int? CurrentRId = Convert.ToInt32(item.Value);
                                    if (PreviousRoles.Contains(CurrentRId))
                                        item.Selected = true;
                                }
                                entity.RolesList = items;
                                ViewBag.FormTypeFX = "OnEdit";
                                return View(entity);
                            }
                            else
                            {
                                entity.Entity = new ICORE_LOGIN();
                                entity.Entity.LOG_ID = 0;
                                List<SelectListItem> items = new SelectList(context.ICORE_ROLES.Where(m => m.R_ID != 1 && m.R_STATUS == "true" && m.R_ISAUTH == true).OrderBy(m => m.R_ID).ToList(), "R_ID", "R_NAME", 0).ToList();
                                entity.RolesList = items;
                                message = "Exception Occur while fetching your record on User Id = " + L;
                                return View(entity);
                            }
                        }
                        else
                        {
                            entity.Entity = new ICORE_LOGIN();
                            entity.Entity.LOG_ID = 0;
                            List<SelectListItem> items = new SelectList(context.ICORE_ROLES.Where(m => m.R_ID != 1 && m.R_STATUS == "true" && m.R_ISAUTH == true).OrderBy(m => m.R_ID).ToList(), "R_ID", "R_NAME", 0).ToList();
                            entity.RolesList = items;
                            return View(entity);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Icore_User", "AddUser", Session["USER_ID"].ToString(), ex);
                        entity.Entity = new ICORE_LOGIN();
                        entity.Entity.LOG_ID = 0;
                        List<SelectListItem> items = new SelectList(context.ICORE_ROLES.Where(m => m.R_ID != 1 && m.R_STATUS == "true" && m.R_ISAUTH == true).OrderBy(m => m.R_ID).ToList(), "R_ID", "R_NAME", 0).ToList();
                        entity.RolesList = items;
                        return View(entity);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult AddUser(AssignRoles_Custom db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_User", "AddUser", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        ICORE_LOGIN UpdateEntity = new ICORE_LOGIN();
                        ICORE_LOGIN CheckIfExist = new ICORE_LOGIN();
                        if (db_table.Entity.LOG_ID == 0)
                        {
                            CheckIfExist = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID.ToLower() == db_table.Entity.LOG_USER_ID.ToLower()).FirstOrDefault();
                            if (CheckIfExist == null)
                            {
                                UpdateEntity.LOG_ID = Convert.ToInt32(context.ICORE_LOGIN.Max(m => (decimal?)m.LOG_ID)) + 1;
                                UpdateEntity.LOG_DATA_ENTRY_DATETIME = DateTime.Now;
                                UpdateEntity.LOG_USER_ID = db_table.Entity.LOG_USER_ID;
                                UpdateEntity.LOG_USER_NAME = db_table.Entity.LOG_USER_NAME;
                                UpdateEntity.LOG_USER_LAST_NAME = db_table.Entity.LOG_USER_LAST_NAME;
                                UpdateEntity.LOG_USER_GE_ID = db_table.Entity.LOG_USER_GE_ID;
                                UpdateEntity.LOG_EMAIL = db_table.Entity.LOG_EMAIL;
                                //UpdateEntity.LOG_PASSWORD = "12345";
                                UpdateEntity.LOG_RIST_ID = db_table.Entity.LOG_RIST_ID;
                                UpdateEntity.LOG_USER_STATUS = db_table.Entity.LOG_USER_STATUS;
                                UpdateEntity.LOG_MAKER_ID = Session["USER_ID"].ToString();
                                if (UpdateEntity.LOG_MAKER_ID == "Admin123")
                                {
                                    UpdateEntity.LOG_ISAUTH = true;
                                    UpdateEntity.LOG_CHECKER_ID = UpdateEntity.LOG_MAKER_ID;
                                }
                                else
                                {
                                    UpdateEntity.LOG_ISAUTH = false;
                                    UpdateEntity.LOG_CHECKER_ID = null;
                                }
                                context.ICORE_LOGIN.Add(UpdateEntity);
                            }
                            else
                            {
                                message = "A User with the given " + db_table.Entity.LOG_USER_ID + " Already Exist";
                            }
                        }
                        else
                        {
                            UpdateEntity = context.ICORE_LOGIN.Where(m => m.LOG_ID == db_table.Entity.LOG_ID).FirstOrDefault();
                            UpdateEntity.LOG_DATA_EDIT_DATETIME = DateTime.Now;
                            UpdateEntity = context.ICORE_LOGIN.Where(m => m.LOG_ID == db_table.Entity.LOG_ID).FirstOrDefault();
                            UpdateEntity.LOG_DATA_EDIT_DATETIME = DateTime.Now;
                            UpdateEntity.LOG_USER_ID = db_table.Entity.LOG_USER_ID;
                            UpdateEntity.LOG_USER_NAME = db_table.Entity.LOG_USER_NAME;
                            UpdateEntity.LOG_USER_LAST_NAME = db_table.Entity.LOG_USER_LAST_NAME;
                            UpdateEntity.LOG_USER_GE_ID = db_table.Entity.LOG_USER_GE_ID;
                            UpdateEntity.LOG_EMAIL = db_table.Entity.LOG_EMAIL;
                            //UpdateEntity.LOG_PASSWORD = "12345";
                            UpdateEntity.LOG_RIST_ID = db_table.Entity.LOG_RIST_ID;
                            UpdateEntity.LOG_USER_STATUS = db_table.Entity.LOG_USER_STATUS;
                            UpdateEntity.LOG_MAKER_ID = Session["USER_ID"].ToString();
                            if (UpdateEntity.LOG_MAKER_ID == "Admin123")
                            {
                                UpdateEntity.LOG_ISAUTH = true;
                                UpdateEntity.LOG_CHECKER_ID = UpdateEntity.LOG_MAKER_ID;
                            }
                            else
                            {
                                UpdateEntity.LOG_ISAUTH = false;
                                UpdateEntity.LOG_CHECKER_ID = null;
                            }

                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        }
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            string TempNewUser = "";
                            string TempRemoveUser = "";
                            if (db_table.SelectedRoles != null)
                            {
                                #region Get Groups for Log Table

                                foreach (int RoleId in db_table.SelectedRoles)
                                {
                                    ICORE_ASSIGN_LOGIN_RIGHTS RoleTbl = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == RoleId && m.ALR_USER_ID == UpdateEntity.LOG_USER_ID && m.ALR_STATUS == true).FirstOrDefault();
                                    if (RoleTbl == null)
                                    {
                                        ICORE_ROLES GetRoleName = context.ICORE_ROLES.Where(m => m.R_ID == RoleId).FirstOrDefault();
                                        TempNewUser += GetRoleName.R_NAME + ",";
                                    }
                                }
                                #endregion

                                List<ICORE_ASSIGN_LOGIN_RIGHTS> DeActivatedRoles = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_LOG_ID == db_table.Entity.LOG_ID && !db_table.SelectedRoles.Contains(m.ALR_ROLE_ID)).ToList();
                                if (DeActivatedRoles.Count > 0)
                                {
                                    foreach (ICORE_ASSIGN_LOGIN_RIGHTS EachRolesForUpdate in DeActivatedRoles)
                                    {
                                        ICORE_ROLES RoleTbl = context.ICORE_ROLES.Where(m => m.R_ID == EachRolesForUpdate.ALR_ROLE_ID).FirstOrDefault();
                                        TempRemoveUser += RoleTbl.R_NAME + ",";
                                        EachRolesForUpdate.ALR_STATUS = false;
                                        context.Entry(EachRolesForUpdate).State = EntityState.Modified;
                                    }
                                }
                                int AssignRoleId = 0;
                                foreach (int? SelectedRolesId in db_table.SelectedRoles)
                                {
                                    if (SelectedRolesId != null)
                                    {
                                        ICORE_ASSIGN_LOGIN_RIGHTS EachRolesToUpdate = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_LOG_ID == db_table.Entity.LOG_ID && m.ALR_ROLE_ID == SelectedRolesId).FirstOrDefault();
                                        if (EachRolesToUpdate == null)
                                        {
                                            EachRolesToUpdate = new ICORE_ASSIGN_LOGIN_RIGHTS();
                                            if (AssignRoleId == 0)
                                            {
                                                EachRolesToUpdate.ALR_ID = Convert.ToInt32(context.ICORE_ASSIGN_LOGIN_RIGHTS.Max(m => (decimal?)m.ALR_ID)) + 1;
                                                AssignRoleId = EachRolesToUpdate.ALR_ID;
                                            }
                                            else
                                            {
                                                AssignRoleId = (AssignRoleId + 1);
                                                EachRolesToUpdate.ALR_ID = AssignRoleId;
                                            }
                                            EachRolesToUpdate.ALR_ROLE_ID = SelectedRolesId;
                                            EachRolesToUpdate.ALR_USER_ID = db_table.Entity.LOG_USER_ID;
                                            EachRolesToUpdate.ALR_STATUS = true;
                                            EachRolesToUpdate.ALR_MAKER_ID = Session["USER_ID"].ToString();
                                            EachRolesToUpdate.ALR_CHECKER_ID = null;
                                            EachRolesToUpdate.ALR_DATA_ENTRY_DATETIME = DateTime.Now;
                                            EachRolesToUpdate.ALR_LOG_ID = UpdateEntity.LOG_ID;
                                            context.ICORE_ASSIGN_LOGIN_RIGHTS.Add(EachRolesToUpdate);
                                        }
                                        else
                                        {
                                            EachRolesToUpdate.ALR_MAKER_ID = Session["USER_ID"].ToString();
                                            EachRolesToUpdate.ALR_STATUS = true;
                                            EachRolesToUpdate.ALR_DATA_EDIT_DATETIME = DateTime.Now;
                                            context.Entry(EachRolesToUpdate).State = EntityState.Modified;
                                        }
                                    }
                                }
                                RowsAffected = context.SaveChanges();
                                if (RowsAffected > 0)
                                {
                                    if (Session["USER_ID"].ToString() != "Admin123")
                                    {
                                        RowsAffected = 0;
                                        ICORE_USER_WITH_ROLES_LIST_VIEW AssignGroup = context.ICORE_USER_WITH_ROLES_LIST_VIEW.Where(m => m.LOG_ID == UpdateEntity.LOG_ID).FirstOrDefault();
                                        ICORE_LOGIN GetLogin = context.ICORE_LOGIN.Where(m => m.LOG_ID == AssignGroup.LOG_ID).FirstOrDefault();
                                        ICORE_USER_LOG UserLog = new ICORE_USER_LOG();
                                        UserLog.ULOG_ID = Convert.ToInt32(context.ICORE_USER_LOG.Max(m => (decimal?)m.ULOG_ID)) + 1;
                                        UserLog.ULOG_USER_ID = UpdateEntity.LOG_USER_ID;
                                        UserLog.ULOG_EMAIL = UpdateEntity.LOG_EMAIL;
                                        UserLog.ULOG_ASSIGN_GROUP = AssignGroup.ASSIGNED_ROLES;
                                        UserLog.ULOG_ADDED_BY = Session["USER_ID"].ToString();
                                        UserLog.ULOG_ADDED_DATETIME = GetLogin.LOG_DATA_ENTRY_DATETIME;
                                        if (TempNewUser != "" && TempRemoveUser != "")
                                            UserLog.ULOG_STATUS = "New group " + TempNewUser + " assigned & group name " + TempRemoveUser + " is removed for the user : " + UpdateEntity.LOG_USER_ID;
                                        else if (TempNewUser != "" && TempRemoveUser == "")
                                            UserLog.ULOG_STATUS = "New group assigned to the user : " + UpdateEntity.LOG_USER_ID + ", New Assigned group " + TempNewUser;
                                        else if (TempNewUser == "" && TempRemoveUser != "")
                                            UserLog.ULOG_STATUS = "Following group removed for user : " + UpdateEntity.LOG_USER_ID + ", removed group " + TempRemoveUser;
                                        UserLog.ULOG_ENTRY_DATETIME = DateTime.Now;
                                        context.ICORE_USER_LOG.Add(UserLog);
                                        RowsAffected = context.SaveChanges();
                                    }
                                    if (RowsAffected > 0)
                                    {
                                        RowsAffected = 1;
                                        message = "Data Inserted Successfully " + RowsAffected + " Affected";
                                    }
                                }
                            }
                            else
                            {

                                List<ICORE_ASSIGN_LOGIN_RIGHTS> DeActivatedRoles = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_LOG_ID == db_table.Entity.LOG_ID).ToList();
                                if (DeActivatedRoles.Count > 0)
                                {
                                    foreach (ICORE_ASSIGN_LOGIN_RIGHTS EachRolesForUpdate in DeActivatedRoles)
                                    {
                                        ICORE_ROLES RoleTbl = context.ICORE_ROLES.Where(m => m.R_ID == EachRolesForUpdate.ALR_ROLE_ID).FirstOrDefault();
                                        TempRemoveUser += RoleTbl.R_NAME + ",";
                                        EachRolesForUpdate.ALR_STATUS = false;
                                        context.Entry(EachRolesForUpdate).State = EntityState.Modified;
                                    }
                                    RowsAffected = context.SaveChanges();
                                }
                                else
                                    RowsAffected = 1;
                                if (RowsAffected > 0)
                                {
                                    if (Session["USER_ID"].ToString() != "Admin123")
                                    {
                                        RowsAffected = 0;
                                        ICORE_USER_WITH_ROLES_LIST_VIEW AssignGroup = context.ICORE_USER_WITH_ROLES_LIST_VIEW.Where(m => m.LOG_ID == UpdateEntity.LOG_ID).FirstOrDefault();
                                        ICORE_LOGIN GetLogin = context.ICORE_LOGIN.Where(m => m.LOG_ID == AssignGroup.LOG_ID).FirstOrDefault();
                                        ICORE_USER_LOG UserLog = new ICORE_USER_LOG();
                                        UserLog.ULOG_ID = Convert.ToInt32(context.ICORE_USER_LOG.Max(m => (decimal?)m.ULOG_ID)) + 1;
                                        UserLog.ULOG_USER_ID = UpdateEntity.LOG_USER_ID;
                                        UserLog.ULOG_EMAIL = UpdateEntity.LOG_EMAIL;
                                        UserLog.ULOG_ASSIGN_GROUP = AssignGroup.ASSIGNED_ROLES;
                                        UserLog.ULOG_ADDED_BY = Session["USER_ID"].ToString();
                                        UserLog.ULOG_ADDED_DATETIME = GetLogin.LOG_DATA_ENTRY_DATETIME;
                                        UserLog.ULOG_STATUS = "Roles are removed for the user : " + UpdateEntity.LOG_USER_ID;
                                        UserLog.ULOG_ENTRY_DATETIME = DateTime.Now;
                                        context.ICORE_USER_LOG.Add(UserLog);
                                        RowsAffected = context.SaveChanges();
                                    }
                                    if (RowsAffected > 0)
                                    {
                                        RowsAffected = 1;
                                        message = "Data Inserted Successfully " + RowsAffected + " Affected";
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Icore_User", "AddUser", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    AssignRoles_Custom entity = new AssignRoles_Custom();
                    if (entity.RolesList != null)
                    {
                        entity.Entity = new ICORE_LOGIN();
                        entity.Entity.LOG_ID = db_table.Entity.LOG_ID;
                        List<int?> PreviousRoles = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_LOG_ID == db_table.Entity.LOG_ID && m.ALR_STATUS == true).Select(m => m.ALR_ROLE_ID).ToList();
                        List<SelectListItem> items = new SelectList(context.ICORE_ROLES.Where(m => m.R_ID != 1 && m.R_STATUS == "true" && m.R_ISAUTH == true).OrderBy(m => m.R_ID).ToList(), "R_ID", "R_NAME", 0).ToList();
                        foreach (SelectListItem item in items)
                        {
                            int? CurrentRId = Convert.ToInt32(item.Value);
                            if (PreviousRoles.Contains(CurrentRId))
                                item.Selected = true;
                        }
                        entity.RolesList = items;
                        return View(entity);
                    }
                    else
                    {
                        entity.Entity = new ICORE_LOGIN();
                        entity.Entity.LOG_ID = 0;
                        List<SelectListItem> items = new SelectList(context.ICORE_ROLES.Where(m => m.R_ID != 1 && m.R_STATUS == "true" && m.R_ISAUTH == true).OrderBy(m => m.R_ID).ToList(), "R_ID", "R_NAME", 0).ToList();
                        entity.RolesList = items;
                        return View(entity);
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public ActionResult AddUserView()
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "dashboard");
            }
            else
            {
                if (DAL.CheckFunctionValidity("Icore_User", "AddUser", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<ICORE_USER_WITH_ROLES_LIST_VIEW> ViewData = context.ICORE_USER_WITH_ROLES_LIST_VIEW.Where(m => m.LOG_USER_ID != "Admin123" && m.LOG_USER_ID != SessionUser && (m.LOG_MAKER_ID != SessionUser || m.LOG_ISAUTH == true)).OrderByDescending(m => m.LOG_ID).ToList();
                    return PartialView(ViewData);
                }
                else
                    return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthroizeUser(int LOG_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_User", "AddUser", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string message = "";
                        try
                        {
                            ICORE_LOGIN UpdateEntity = context.ICORE_LOGIN.Where(m => m.LOG_ID == LOG_ID).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                if (UpdateEntity.LOG_MAKER_ID != Session["USER_ID"].ToString())
                                {
                                    UpdateEntity.LOG_ISAUTH = true;
                                    UpdateEntity.LOG_CHECKER_ID = Session["USER_ID"].ToString();
                                    UpdateEntity.LOG_DATA_EDIT_DATETIME = DateTime.Now;
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        RowCount = 0;
                                        ICORE_USER_WITH_ROLES_LIST_VIEW AssignGroup = context.ICORE_USER_WITH_ROLES_LIST_VIEW.Where(m => m.LOG_ID == UpdateEntity.LOG_ID).FirstOrDefault();
                                        ICORE_USER_LOG UserLog = new ICORE_USER_LOG();
                                        UserLog.ULOG_ID = Convert.ToInt32(context.ICORE_USER_LOG.Max(m => (decimal?)m.ULOG_ID)) + 1;
                                        UserLog.ULOG_USER_ID = UpdateEntity.LOG_USER_ID;
                                        UserLog.ULOG_EMAIL = UpdateEntity.LOG_EMAIL;
                                        UserLog.ULOG_ASSIGN_GROUP = AssignGroup.ASSIGNED_ROLES;
                                        UserLog.ULOG_ADDED_BY = UpdateEntity.LOG_MAKER_ID;
                                        UserLog.ULOG_ADDED_DATETIME = UpdateEntity.LOG_DATA_ENTRY_DATETIME;
                                        UserLog.ULOG_ACTIVATED_BY = Session["USER_ID"].ToString();
                                        UserLog.ULOG_ACTIVATED_DATETIME = DateTime.Now;
                                        UserLog.ULOG_STATUS = "Activated the user : " + UpdateEntity.LOG_USER_ID;
                                        UserLog.ULOG_ENTRY_DATETIME = DateTime.Now;
                                        context.ICORE_USER_LOG.Add(UserLog);
                                        RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                            message = "User : " + UpdateEntity.LOG_USER_NAME + " is successfully Authorized";
                                    }
                                }
                                else
                                {
                                    message = "Maker cannot authorize the same record.";
                                    return Json(message);
                                }
                            }
                            else
                            {
                                message = "Problem while fetching your record on ID# " + LOG_ID;
                                return Json(message);
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("Icore_User", "AuthorizeUser", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(message);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult DeleteFromAuthroizeUser(int LOG_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_User", "AddUser", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string message = "";
                        try
                        {
                            ICORE_LOGIN UpdateEntity = context.ICORE_LOGIN.Where(m => m.LOG_ID == LOG_ID).FirstOrDefault();

                            #region Save Group Name Before Deleting
                            string GetGroupName = "";
                            ICORE_USER_WITH_ROLES_LIST_VIEW GroupName = context.ICORE_USER_WITH_ROLES_LIST_VIEW.Where(m => m.LOG_USER_ID == UpdateEntity.LOG_USER_ID).FirstOrDefault();
                            if (GroupName != null)
                            {
                                GetGroupName += GroupName.ASSIGNED_ROLES;
                            }
                            #endregion

                            if (UpdateEntity != null && UpdateEntity.LOG_ISAUTH == false)
                            {
                                if (UpdateEntity.LOG_MAKER_ID != Session["USER_ID"].ToString())
                                {
                                    context.ICORE_LOGIN.Remove(UpdateEntity);
                                    int RowCount = +context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        List<ICORE_ASSIGN_LOGIN_RIGHTS> AssignUser = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_USER_ID == UpdateEntity.LOG_USER_ID).ToList();
                                        if (AssignUser.Count > 0)
                                        {
                                            RowCount = 0;
                                            foreach (ICORE_ASSIGN_LOGIN_RIGHTS item in AssignUser)
                                            {
                                                context.ICORE_ASSIGN_LOGIN_RIGHTS.Remove(item);
                                                RowCount = +context.SaveChanges();
                                            }
                                        }
                                        if (RowCount > 0)
                                        {
                                            RowCount = 0;
                                            ICORE_USER_WITH_ROLES_LIST_VIEW AssignGroup = context.ICORE_USER_WITH_ROLES_LIST_VIEW.Where(m => m.LOG_ID == UpdateEntity.LOG_ID).FirstOrDefault();
                                            ICORE_USER_LOG UserLog = new ICORE_USER_LOG();
                                            UserLog.ULOG_ID = Convert.ToInt32(context.ICORE_USER_LOG.Max(m => (decimal?)m.ULOG_ID)) + 1;
                                            UserLog.ULOG_USER_ID = UpdateEntity.LOG_USER_ID;
                                            UserLog.ULOG_EMAIL = UpdateEntity.LOG_EMAIL;
                                            UserLog.ULOG_ASSIGN_GROUP = GetGroupName;
                                            UserLog.ULOG_ADDED_BY = UpdateEntity.LOG_MAKER_ID;
                                            UserLog.ULOG_ADDED_DATETIME = UpdateEntity.LOG_DATA_ENTRY_DATETIME;
                                            UserLog.ULOG_ACTIVATED_BY = Session["USER_ID"].ToString();
                                            UserLog.ULOG_ACTIVATED_DATETIME = DateTime.Now;
                                            UserLog.ULOG_STATUS = "Following user is Rejected/Deleted : " + UpdateEntity.LOG_USER_ID;
                                            UserLog.ULOG_ENTRY_DATETIME = DateTime.Now;
                                            context.ICORE_USER_LOG.Add(UserLog);
                                            RowCount = context.SaveChanges();
                                            if (RowCount > 0)
                                                message = "User : " + UpdateEntity.LOG_USER_NAME + " is Rejected";
                                        }
                                    }
                                }
                                else
                                {
                                    message = "Maker cannot reject the same record.";
                                    return Json(message);
                                }
                            }
                            else
                            {
                                message = "Problem while fetching your record on ID# " + LOG_ID;
                                return Json(message);
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("Icore_User", "DeleteFromAuthorizeUser", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(message);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }

        #region User Report
        public ActionResult UserActivity()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_User", "UserActivity", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Icore_User", "UserActivity", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult UserActivity(DateTime? FromDate, DateTime? EndDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_User", "UserActivity", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && EndDate != null)
                        {
                            var Data = context.UserActivityReport(FromDate, EndDate).ToList();
                            if (Data.Count() > 0)
                            {
                                Data = Data.Where(m => m.ULOG_ADDED_BY != "Admin123" && m.ULOG_ACTIVATED_BY != "Admin123").ToList();
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + FromDate + " End date : " + EndDate;
                            }
                        }
                        else
                        {
                            if (FromDate == null && EndDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (EndDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Icore_User", "UserActivity", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        #endregion
    }
}