﻿using FXAutomation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FXAutomation.Controllers
{
    public class Icore_CaseSubmission_FormController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: Icore_CaseSubmission_Form
        [DllImport(@"urlmon.dll", CharSet = CharSet.Auto)]
        private extern static System.UInt32 FindMimeFromData(System.UInt32 pBC,
        [MarshalAs(UnmanagedType.LPStr)] System.String pwzUrl,
        [MarshalAs(UnmanagedType.LPArray)] byte[] pBuffer,
        System.UInt32 cbSize, [MarshalAs(UnmanagedType.LPStr)] System.String pwzMimeProposed,
        System.UInt32 dwMimeFlags,
        out System.UInt32 ppwzMimeOut,
        System.UInt32 dwReserverd);
        public FileContentResult ViewFile(int ID = 0)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"].ToString();
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "OpsQueue", Session["USER_ID"].ToString()) || DAL.CheckFunctionValidity("Icore_SubmittedCase", "ComplianceQueue", Session["USER_ID"].ToString()) || DAL.CheckFunctionValidity("Icore_SubmittedCase", "RegulatoryHead", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckFormQueue(ID.ToString(), SessionUser))
                    {
                        if (ID != 0)
                        {
                            ICORE_SUBMIT_CASE_DOCUMENT_VIEW GetFile = context.ICORE_SUBMIT_CASE_DOCUMENT_VIEW.Where(m => m.SCD_ID == ID).FirstOrDefault();
                            string FileAddress = GetFile.SCD_DOCUMENT_VALUE;
                            string FileToOpen = Path.GetFileName(FileAddress);
                            string filePath = FileAddress;  //HttpContext.Server.MapPath(@"~/Uploads/" + FileToOpen);
                            if (System.IO.File.Exists(filePath))
                            {
                                byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
                                string mimeType = "application/pdf";
                                string FileNameDisplay = GetFile.Case_Doc_Name + "." + GetFile.Format_extension;
                                Response.AppendHeader("Content-Disposition", "inline; filename=" + FileNameDisplay);
                                return File(fileBytes, mimeType);
                            }
                            else
                            {
                                string FilePath = "";
                                string localPath = "";
                                ICORE_CASE_DOCUMENTS GetDocDetail = context.ICORE_CASE_DOCUMENTS.Where(m => m.Case_Doc_Id == GetFile.SCD_DOCUMENT_ID).FirstOrDefault();
                                if (GetDocDetail != null)
                                {
                                    ICORE_SYSTEM_SETUP SystemData = context.ICORE_SYSTEM_SETUP.Where(m => m.SS_ID == 1).FirstOrDefault();
                                    if (SystemData != null)
                                    {
                                        string FileDomain = SystemData.SS_DOMAIN_FOR_WEB;
                                        if (GetDocDetail.Case_Doc_IsAD == true)
                                            FilePath = FileDomain + "IUploads/" + FileToOpen;
                                        else
                                            FilePath = FileDomain + "Uploads/" + FileToOpen;
                                        if (System.IO.File.Exists(FilePath))
                                        {
                                            localPath = new Uri(FilePath).LocalPath;
                                        }
                                        else
                                        {
                                            FileDomain = SystemData.SS_DOMAIN_FOR_CLIENT;
                                            if (GetDocDetail.Case_Doc_IsAD == true)
                                                FilePath = FileDomain + "IUploads/" + FileToOpen;
                                            else
                                                FilePath = FileDomain + "Uploads/" + FileToOpen;
                                            localPath = new Uri(FilePath).LocalPath;
                                        }
                                        // Check Here if System Have Permissions To View File on Network
                                    }
                                    else
                                    {
                                        string message = "";
                                        message = "No data found in the System Setup.";
                                        return CreateLogFile(message);
                                    }
                                    try
                                    {
                                        System.IO.File.OpenRead(localPath);
                                    }
                                    catch (Exception ex)
                                    {
                                        string message = "";
                                        message = "File : " + FileToOpen + " Exception : " + ex.Message;
                                        return CreateLogFile(message);
                                    }
                                    if (System.IO.File.Exists(localPath))
                                    {
                                        byte[] fileBytes = System.IO.File.ReadAllBytes(localPath);
                                        string mimeType = "application/pdf";
                                        string FileNameDisplay = GetFile.Case_Doc_Name + "." + GetFile.Format_extension;
                                        Response.AppendHeader("Content-Disposition", "inline; filename=" + FileNameDisplay);
                                        return File(fileBytes, mimeType);
                                    }
                                    else
                                    {
                                        string message = "";
                                        message = "File : " + FileToOpen + " not found in the given path." + Environment.NewLine + "please check permissions.";
                                        return CreateLogFile(message);
                                    }
                                }
                            }
                        }
                        else
                        {
                            string message = "";
                            message = "Error while fecting document at ID : " + ID;
                            return CreateLogFile(message);
                        }
                    }
                    else
                    {
                        string message = "";
                        message = "Unauthorized User, unable to process the request.";
                        return CreateLogFile(message);
                    }
                }
                else
                    return null;
            }
            return null;
        }
        public FileContentResult CreateLogFile(string Message)
        {
            var byteArray = System.Text.Encoding.ASCII.GetBytes(Message);
            var stream = new System.IO.MemoryStream(byteArray);
            var logfile = new FileContentResult(stream.ToArray(), "text/plain");
            return logfile;
        }
        public PartialViewResult ViewSubmittedCaseForm(decimal? SCFM_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "OpsQueue", Session["USER_ID"].ToString()) || DAL.CheckFunctionValidity("Icore_SubmittedCase", "ComplianceQueue", Session["USER_ID"].ToString()) || DAL.CheckFunctionValidity("Icore_SubmittedCase", "RegulatoryHead", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckFormQueueForFieldsAndDocuments(SCFM_ID, Session["USER_ID"].ToString()))
                    {
                        List<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW> data = new List<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW>();
                        if (SCFM_ID != 0)
                        {
                            data = context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW.Where(m => m.SCFD_SCFM_ID == SCFM_ID).OrderBy(m => m.CaseForm_Data_ORDER_BY).ToList();
                        }
                        return PartialView(data);
                    }
                    else
                        return PartialView(null);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView();
            }            
        }
        public PartialViewResult ViewSubmittedDocument(decimal? SCFM_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "OpsQueue", Session["USER_ID"].ToString()) || DAL.CheckFunctionValidity("Icore_SubmittedCase", "ComplianceQueue", Session["USER_ID"].ToString()) || DAL.CheckFunctionValidity("Icore_SubmittedCase", "RegulatoryHead", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckFormQueueForFieldsAndDocuments(SCFM_ID,Session["USER_ID"].ToString()))
                    {
                        List<ICORE_SUBMIT_CASE_DOCUMENT_VIEW> ViewData = new List<ICORE_SUBMIT_CASE_DOCUMENT_VIEW>();
                        if (SCFM_ID != null)
                        {
                            ViewData = context.ICORE_SUBMIT_CASE_DOCUMENT_VIEW.Where(m => m.SCD_FORM_ID == SCFM_ID).OrderBy(m => m.CASE_Doc_Data_ORDER_BY).ToList();
                        }
                        return PartialView(ViewData);
                    }
                    else
                        return PartialView(null);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView();
            }
        }

        public PartialViewResult ViewChatWithClient(string DocElementId)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "OpsQueue", Session["USER_ID"].ToString()) || DAL.CheckFunctionValidity("Icore_SubmittedCase", "ComplianceQueue", Session["USER_ID"].ToString()) || DAL.CheckFunctionValidity("Icore_SubmittedCase", "RegulatoryHead", Session["USER_ID"].ToString()))
                {
                    int DocId = 0;
                    bool IsValid = int.TryParse(DocElementId, out DocId);
                    ICORE_SUBMIT_CASE_DOCUMENT_VIEW GetDocumentDetail = new ICORE_SUBMIT_CASE_DOCUMENT_VIEW();
                    if (DocElementId != null)
                    {
                        GetDocumentDetail = context.ICORE_SUBMIT_CASE_DOCUMENT_VIEW.Where(m => m.SCD_ID == DocId).FirstOrDefault();
                        if (GetDocumentDetail != null)
                        {
                            ICORE_SUBMIT_CASE_FORM_MASTER CaseFormMaster = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == GetDocumentDetail.SCD_FORM_ID).FirstOrDefault();
                            if (CaseFormMaster != null)
                            {
                                List<ICORE_COMMENT_DETAIL_LOG> ViewData = context.ICORE_COMMENT_DETAIL_LOG.Where(m => m.CDL_CASE_NO == CaseFormMaster.SCFM_CASE_NO && m.CDL_DOCUMENT_NAME == GetDocumentDetail.Case_Doc_Name).OrderByDescending(m => m.CDL_ID).ToList();
                                ViewBag.DocumentID = GetDocumentDetail.SCD_ID;
                                return PartialView(ViewData);
                            }
                            else
                                return PartialView("Problem while fetching data");
                        }
                        else
                            return PartialView("Problem while fetching data");
                    }
                    else
                        return PartialView("Problem while fetching data");
                }
                else
                {
                    return PartialView(null);
                }
                return PartialView(ViewData);
            }
            else
            {
                return PartialView();
            }
        }

        #region Edit Submitted Form From Client
        public ActionResult UpdateSubmittedFormAD(int Form_Id)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "OpsQueue", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        EditSubmittedFormAD Entity = new EditSubmittedFormAD();
                        Entity.Master = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == Form_Id).FirstOrDefault();
                        Entity.FieldList = context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW.Where(m => m.SCFD_SCFM_ID == Form_Id).ToList();
                        Entity.DocumentList = context.ICORE_SUBMIT_CASE_DOCUMENT_VIEW.Where(m => m.SCD_FORM_ID == Form_Id).ToList();
                        if (Entity != null)
                        {
                            return View(Entity);
                        }
                        else
                        {
                            message = "Error while fetching records on selected Id";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Icore_CaseSubmission_Form", "UpdateSubmittedFormAD", Session["USER_ID"].ToString(), ex); ;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult UpdateSubmittedFormAD(decimal? FormId)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "OpsQueue", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        ICORE_SUBMIT_CASE_FORM_MASTER DraftMaster = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == FormId).FirstOrDefault();

                        ICORE_CASE_FORMS_MASTER MasterEntity = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == DraftMaster.SCFM_CASE_FORM_ID).FirstOrDefault();
                        List<ICORE_CASE_FORMS_DETAILS> DetailEntity = context.ICORE_CASE_FORMS_DETAILS.Where(m => m.CaseForm_FormId == DraftMaster.SCFM_CASE_FORM_ID).ToList();
                        foreach (ICORE_CASE_FORMS_DETAILS item in DetailEntity)
                        {
                            var ElementId = "#ctrlfrmsub" + item.CaseForm_Field_Name.ToString().Replace(" ", "").Substring(0, 3) + item.CaseForm_Field_Id;
                            string FieldsData = Request.Form[ElementId];
                            if (FieldsData != "" || FieldsData != null)
                            {
                                if (item.CaseForm_Field_IsRequired && item.CaseForm_Filed_IsAD == true)
                                {
                                    if (FieldsData == "" || FieldsData == null)
                                    {
                                        message = "Invalid Data Provided in Field : " + item.CaseForm_Field_Name;
                                        return Json(message);
                                    }
                                    if (item.CaseForm_DataTypeId != 6)
                                    {
                                        if (FieldsData.Length < item.CaseForm_Data_MIN_LENGTH)
                                        {
                                            message = "The value you provided does not reach the minimum length limit for " + item.CaseForm_Field_Name + " field : minimum length limit is : " + item.CaseForm_Data_MIN_LENGTH;
                                            return Json(message);
                                        }
                                        if (FieldsData.Length > item.CaseForm_Data_MAX_LENGTH)
                                        {
                                            message = "Maximum length limit exceeds for " + item.CaseForm_Field_Name + " field maximum length limit is : " + item.CaseForm_Data_MAX_LENGTH;
                                            return Json(message);
                                        }
                                    }
                                }
                            }
                        }
                        List<ICORE_CASE_DOCUMENTS> DocumentEntity = context.ICORE_CASE_DOCUMENTS.Where(m => m.Case_Doc_CaseTitleId == MasterEntity.CaseForm_CaseTitle_Id).ToList();
                        foreach (ICORE_CASE_DOCUMENTS DocItem in DocumentEntity)
                        {
                            ICORE_CASE_DOCUMENTS_FORMAT DocumentFormat = context.ICORE_CASE_DOCUMENTS_FORMAT.Where(m => m.Format_id == DocItem.Case_Doc_Format_id).FirstOrDefault();
                            if (DocItem.Case_Doc_IsAD == true)
                            {
                                var DocumentId = "#ctrldocsub" + DocItem.Case_Doc_Name.ToString().Replace(" ", "").Substring(0, 3) + DocItem.Case_Doc_Id;
                                HttpPostedFileBase DocumentData = Request.Files[DocumentId];
                                if (DocItem.Case_Doc_IsRequired && DocItem.Case_Doc_IsAD == true)
                                {
                                    if (DocumentData != null)
                                    {
                                        string FileExtension = Path.GetExtension(DocumentData.FileName);
                                        string FileExten = FileExtension.Replace(".", "").ToString();
                                        string Filename = Path.GetFileNameWithoutExtension(DocumentData.FileName);
                                        byte[] document = new byte[DocumentData.ContentLength];
                                        DocumentData.InputStream.Read(document, 0, DocumentData.ContentLength);
                                        System.UInt32 mimetype;
                                        FindMimeFromData(0, null, document, 256, null, 0, out mimetype, 0);
                                        System.IntPtr mimeTypePtr = new IntPtr(mimetype);
                                        string mime = Marshal.PtrToStringUni(mimeTypePtr);
                                        Marshal.FreeCoTaskMem(mimeTypePtr);
                                        if (FileExten.ToLower() == "pdf")
                                        {
                                            if (mime != "application/pdf")
                                            {
                                                message = "Invalid file : " + Filename + Environment.NewLine + "Invalid PDF file provided.";
                                                return Json(message);
                                            }
                                        }
                                        if (DocumentData.FileName != "" || DocumentData.FileName != null)
                                        {
                                            string FileName = Path.GetFileNameWithoutExtension(DocumentData.FileName);
                                            if (FileName != DocItem.Case_Doc_Name)
                                            {
                                                string RequiredExten = Path.GetExtension(DocumentData.FileName);
                                                message = "Invalid document name" + Environment.NewLine + "Required document name is " + DocItem.Case_Doc_Name + RequiredExten;
                                                return Json(message);
                                            }
                                            if (DocumentData.FileName == null)
                                            {
                                                message = "Invalid File Provided in Document : " + DocItem.Case_Doc_Name;
                                                return Json(message);
                                            }
                                            if (DocumentFormat.Format_extension.ToLower() == FileExten.ToLower())
                                            {
                                                if (DocumentData.ContentLength > (5 * 1024 * 1024))
                                                {
                                                    message = "Maximum file size is 5mb." + Environment.NewLine + "Selected file " + DocumentData.FileName + " is greater than 5mb";
                                                    return Json(message);
                                                }
                                            }
                                            else
                                            {
                                                message = "Invalid format Provided for the file " + DocumentData.FileName + Environment.NewLine + " Required format is " + DocumentFormat.Format_Name;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        message = DocItem.Case_Doc_Name + " is Required ";
                                        return Json(message);
                                    }
                                }
                                else
                                {
                                    if (DocumentData != null && DocItem.Case_Doc_IsAD == true)
                                    {
                                        string FileExtension = Path.GetExtension(DocumentData.FileName);
                                        string FileExten = FileExtension.Replace(".", "").ToString();
                                        if (DocumentFormat.Format_extension.ToLower() == FileExten.ToLower())
                                        {
                                            if (DocumentData.ContentLength > (5 * 1024 * 1024))
                                            {
                                                message = "Maximum file size is 5mb." + Environment.NewLine + "Selected file " + DocumentData.FileName + " is greater than 5mb";
                                                return Json(message);
                                            }
                                        }
                                        else
                                        {
                                            message = "Invalid format Provided for the file " + DocumentData.FileName + Environment.NewLine + " Required format is " + DocumentFormat.Format_Name;
                                        }
                                    }
                                }
                            }
                        }

                        List<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW> DetailData = context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW.Where(m => m.SCFD_SCFM_ID == FormId).ToList();
                        int RowCount = 0;
                        if (DetailData.Count > 0)
                        {
                            foreach (ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW item in DetailData)
                            {
                                ICORE_SUBMIT_CASE_FORM_DETAIL UpdateEntity = context.ICORE_SUBMIT_CASE_FORM_DETAIL.Where(m => m.SCFD_ID == item.SCFD_ID).FirstOrDefault();
                                if (item.CaseForm_Filed_IsAD == true)
                                {
                                    if (UpdateEntity != null)
                                    {
                                        var ElementId = "#ctrlfrmsub" + item.CaseForm_Field_Name.ToString().Replace(" ", "").Substring(0, 3) + item.SCFD_FIELD_ID;
                                        string FieldsData = Request.Form[ElementId];
                                        string GetFieldID = ElementId.Remove(0, 14);
                                        int FieldID = Convert.ToInt32(GetFieldID);
                                        UpdateEntity.SCFD_FIELD_VALUE = FieldsData;
                                        UpdateEntity.SCFD_EDIT_DATETIME = DateTime.Now;
                                    }
                                    else
                                    {
                                        message = "Error While fetching previous records";
                                    }
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                    RowCount += context.SaveChanges();
                                }
                            }
                            if (RowCount > 0)
                            {
                                RowCount = 0;
                                List<ICORE_SUBMIT_CASE_DOCUMENT_VIEW> ListOfDocument = context.ICORE_SUBMIT_CASE_DOCUMENT_VIEW.Where(m => m.SCD_FORM_ID == FormId).ToList();
                                if (ListOfDocument.Count > 0)
                                {
                                    foreach (ICORE_SUBMIT_CASE_DOCUMENT_VIEW item in ListOfDocument)
                                    {
                                        ICORE_SUBMIT_CASE_DOCUMENT UpdateDocument = context.ICORE_SUBMIT_CASE_DOCUMENT.Where(m => m.SCD_ID == item.SCD_ID).FirstOrDefault();
                                        if (item.Case_Doc_IsAD == true)
                                        {
                                            if (UpdateDocument != null)
                                            {
                                                var DocumentId = "#ctrldocsub" + item.Case_Doc_Name.ToString().Replace(" ", "").Substring(0, 3) + item.SCD_DOCUMENT_ID;
                                                HttpPostedFileBase DocumentData = Request.Files[DocumentId];
                                                if (DocumentData != null && item.Case_Doc_IsAD == true)
                                                {
                                                    string GetDocID = DocumentId.Remove(0, 14);
                                                    int DocId = Convert.ToInt32(GetDocID);
                                                    string newFileName = "";
                                                    string path = HttpContext.Server.MapPath(@"~/IUploads/");
                                                    bool exists = System.IO.Directory.Exists(path);
                                                    if (!exists)
                                                        System.IO.Directory.CreateDirectory(path);
                                                    string extension = Path.GetExtension(DocumentData.FileName);
                                                    newFileName = "C-" + String.Format("{0:000000}", MasterEntity.CaseForm_Id) + "-D-" + String.Format("{0:000000}", DocId) + "-" + DateTime.Now.ToString("ddMMyyyyhhmmss") + extension;
                                                    string filePath = Path.Combine(path, newFileName);
                                                    if (System.IO.File.Exists(UpdateDocument.SCD_DOCUMENT_VALUE))
                                                    {
                                                        System.IO.File.Delete(UpdateDocument.SCD_DOCUMENT_VALUE);
                                                    }
                                                    else
                                                    {
                                                        ICORE_SYSTEM_SETUP SystemData = context.ICORE_SYSTEM_SETUP.Where(m => m.SS_ID == 1).FirstOrDefault();
                                                        if (SystemData != null)
                                                        {
                                                            string FileDomain = SystemData.SS_DOMAIN_FOR_WEB;
                                                            string FileToOpen = Path.GetFileName(UpdateDocument.SCD_DOCUMENT_VALUE);
                                                            string FilePath = FileDomain + "IUploads/" + FileToOpen;
                                                            string localPath = new Uri(FilePath).LocalPath;
                                                            // Check Here if System Have Permissions To View File on Network
                                                            if (System.IO.File.Exists(localPath))
                                                            {
                                                                System.IO.File.Delete(localPath);
                                                            }
                                                            else
                                                            {
                                                                message = "No data ddound in the System Setup.";
                                                                return Json(message);
                                                            }
                                                        }

                                                    }
                                                    UpdateDocument.SCD_DOCUMENT_VALUE = filePath;
                                                    UpdateDocument.SCD_EDIT_DATETIME = DateTime.Now;
                                                    DocumentData.SaveAs(filePath);
                                                    context.Entry(UpdateDocument).State = System.Data.Entity.EntityState.Modified;
                                                    RowCount = context.SaveChanges();
                                                }
                                                else
                                                {
                                                    if (item.Case_Doc_IsAD == true)
                                                    {
                                                        string GetDocID = DocumentId.Remove(0, 14);
                                                        int DocId = Convert.ToInt32(GetDocID);
                                                        UpdateDocument.SCD_DOCUMENT_VALUE = null;
                                                        UpdateDocument.SCD_EDIT_DATETIME = DateTime.Now;
                                                        context.Entry(UpdateDocument).State = System.Data.Entity.EntityState.Modified;
                                                        RowCount = context.SaveChanges();
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                message = "Error While fetching previous records";
                                            }
                                        }
                                    }
                                    if (RowCount > 0)
                                    {
                                        message = "";
                                    }
                                }
                                else
                                {
                                    message = "Error While fetching previous records";
                                }
                            }
                            else
                            {
                                message = "Exception Occur " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Icore_CaseSubmission_Form", "UpdateSubmittedFormAD", Session["USER_ID"].ToString(), ex); ;
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return Json(message);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        public JsonResult GetSubmittedFormFields(int FormID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "OpsQueue", Session["USER_ID"].ToString()))
                {
                    List<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW> DetailEntity = new List<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW>();
                    int FormFieldsID = Convert.ToInt32(FormID);
                    DetailEntity = (context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW.Where(x => x.SCFD_SCFM_ID == FormFieldsID)).ToList();
                    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    string Result = javaScriptSerializer.Serialize(DetailEntity);
                    return Json(Result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        public JsonResult GetSubmittedFormDocuments(int ForeignID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "OpsQueue", Session["USER_ID"].ToString()))
                {
                    List<ICORE_SUBMIT_CASE_DOCUMENT_VIEW> DetailEntity = new List<ICORE_SUBMIT_CASE_DOCUMENT_VIEW>();
                    int FormFieldsID = Convert.ToInt32(ForeignID);
                    DetailEntity = (context.ICORE_SUBMIT_CASE_DOCUMENT_VIEW.Where(x => x.SCD_FORM_ID == FormFieldsID)).ToList();
                    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    string Result = javaScriptSerializer.Serialize(DetailEntity);
                    return Json(Result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }

        #endregion


        #region Not In Work
        //public PartialViewResult SubmittedCaseForm(decimal? FormId)
        //{
        //    List<ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW> MasterData = new List<ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW>();
        //    if (FormId != 0)
        //    {
        //        MasterData = context.ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW.Where(m => m.SCFM_CASE_FORM_ID == FormId).ToList();
        //    }
        //    return PartialView(MasterData);
        //}
        //public PartialViewResult Case_Document_View(decimal? CaseType_ID, decimal? CaseTitle_ID)
        //{
        //    List<ICORE_CASE_DOCUMENTS_VIEW> data = new List<ICORE_CASE_DOCUMENTS_VIEW>();
        //    if (CaseTitle_ID != null)
        //    {
        //        data = context.ICORE_CASE_DOCUMENTS_VIEW.Where(model => model.Case_Doc_CaseTitleId == CaseTitle_ID).ToList();
        //    }
        //    else if (CaseType_ID != null)
        //    {
        //        data = context.ICORE_CASE_DOCUMENTS_VIEW.Where(model => model.Case_Doc_CaseType_Id == CaseType_ID).ToList();
        //    }
        //    return PartialView(data);
        //}
        //public ActionResult Submission_Form()
        //{
        //    string message = "";
        //    try
        //    {
        //        if (Session["USER_ID"] != null)
        //        {
        //            List<SelectListItem> Department = new SelectList(context.ICORE_DEPARTMENTS.Where(m => m.Dept_Status == true).ToList(), "Dept_Id", "Dept_Name", 0).ToList();
        //            Department.Insert(0, (new SelectListItem { Text = "--Select Department--", Value = "0" }));
        //            ViewBag.Dept_Id = Department;
        //            List<SelectListItem> Category = new SelectList(context.ICORE_CATEGORIES.Where(m => m.Categ_Status == true).ToList(), "Categ_Id", "Categ_Name", 0).ToList();
        //            Category.Insert(0, (new SelectListItem { Text = "--Select Category--", Value = "0" }));
        //            ViewBag.Categ_DeptId = Category;
        //            List<SelectListItem> items = new SelectList(context.ICORE_CASE_TYPES.Where(m => m.CaseType_Status == true).ToList(), "CaseType_Id", "CaseType_Name", 0).ToList();
        //            items.Insert(0, (new SelectListItem { Text = "--Select Case Type--", Value = "0" }));
        //            ViewBag.CaseType_Id = items;
        //            List<SelectListItem> item = new SelectList(context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Status == true).ToList(), "CaseTitle_Id", "CaseTitle_Name", 0).ToList();
        //            item.Insert(0, (new SelectListItem { Text = "--Select Case Title--", Value = "0" }));
        //            ViewBag.CaseTitle_Id = item;
        //            return View();
        //        }
        //        else
        //        {
        //            return RedirectToAction("Index", "Login");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        message = ex.Message;
        //    }
        //    finally
        //    {
        //        ViewBag.message = message;
        //    }
        //    return View();
        //}
        //[HttpPost]
        //public ActionResult Submission_Form(ICORE_CASE_SUBMISSION_FORM_VIEW dbtable_view)
        //{
        //    string message = "";
        //    try
        //    {
        //        if (Session["USER_ID"] != null)
        //        {
        //            List<SelectListItem> Department = new SelectList(context.ICORE_DEPARTMENTS.Where(m => m.Dept_Status == true).ToList(), "Dept_Id", "Dept_Name", 0).ToList();
        //            Department.Insert(0, (new SelectListItem { Text = "--Select Department--", Value = "0" }));
        //            ViewBag.Dept_Id = Department;
        //            List<SelectListItem> Category = new SelectList(context.ICORE_CATEGORIES.Where(m => m.Categ_Status == true).ToList(), "Categ_Id", "Categ_Name", 0).ToList();
        //            Category.Insert(0, (new SelectListItem { Text = "--Select Category--", Value = "0" }));
        //            ViewBag.Categ_DeptId = Category;
        //            List<SelectListItem> items = new SelectList(context.ICORE_CASE_TYPES.Where(m => m.CaseType_Status == true).ToList(), "CaseType_Id", "CaseType_Name", 0).ToList();
        //            items.Insert(0, (new SelectListItem { Text = "--Select Case Type--", Value = "0" }));
        //            ViewBag.CaseType_Id = items;
        //            List<SelectListItem> item = new SelectList(context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Status == true).ToList(), "CaseTitle_Id", "CaseTitle_Name", 0).ToList();
        //            item.Insert(0, (new SelectListItem { Text = "--Select Case Title--", Value = "0" }));
        //            ViewBag.CaseTitle_Id = item;
        //            return View();
        //        }
        //        else
        //        {
        //            return RedirectToAction("Index", "Login");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        message = ex.Message;
        //    }
        //    finally
        //    {
        //        ViewBag.message = message;
        //    }
        //    return View();
        //}
        //public ActionResult CaseFormView(decimal? FormId)
        //{
        //    return PartialView(context.ICORE_CASE_FORMS_DETAILS_VIEW.Where(m => m.CaseForm_FormId == FormId).ToList());
        //}
        //public ActionResult SubmitCaseFormFields(List<ICORE_SUBMIT_CASE_FORM_DETAIL> SubmitData, decimal? FormId)
        //{
        //    if (Session["USER_ID"] != null)
        //    {
        //        string message = "";
        //        try
        //        {
        //            ICORE_SUBMIT_CASE_FORM_MASTER MasterEntity = new ICORE_SUBMIT_CASE_FORM_MASTER();
        //            int MasterCaseForm_ID = 0;
        //            if (MasterEntity.SCFM_ID == 0)
        //            {
        //                MasterCaseForm_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_FORM_MASTER.Max(m => (decimal?)m.SCFM_ID)) + 1;
        //                MasterEntity.SCFM_ID = MasterCaseForm_ID;
        //                MasterEntity.SCFM_CASE_FORM_ID = Convert.ToInt32(FormId);
        //                MasterEntity.SCFM_CASE_NO = GenerateCaseNumber();
        //                MasterEntity.SCFM_STATUS = "New";
        //                MasterEntity.SCFM_STATUS_DATE = DateTime.Now;
        //                MasterEntity.SCFM_ENTRY_DATETIME = DateTime.Now;
        //                MasterEntity.SCFM_MAKER_ID = Session["USER_ID"].ToString();
        //                MasterEntity.SCFM_CHECKER_ID = "0";

        //                context.ICORE_SUBMIT_CASE_FORM_MASTER.Add(MasterEntity);
        //                int MasterRowCount = context.SaveChanges();
        //                if (MasterRowCount > 0)
        //                {
        //                    message = "";
        //                }
        //                else
        //                {
        //                    message = "Exception Occur " + Environment.NewLine + "Please Contact to Administrator";
        //                }
        //            }

        //            ICORE_SUBMIT_CASE_FORM_DETAIL DataEntity = new ICORE_SUBMIT_CASE_FORM_DETAIL();
        //            int RowCount = 0;
        //            foreach (ICORE_SUBMIT_CASE_FORM_DETAIL item in SubmitData)
        //            {
        //                if (item.SCFD_ID == 0)
        //                {
        //                    DataEntity = new ICORE_SUBMIT_CASE_FORM_DETAIL();
        //                    DataEntity.SCFD_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_FORM_DETAIL.Max(m => (decimal?)m.SCFD_ID)) + 1;
        //                    DataEntity.SCFD_SCFM_ID = MasterCaseForm_ID;
        //                    DataEntity.SCFD_FORM_ID = item.SCFD_FORM_ID;
        //                    DataEntity.SCFD_FIELD_ID = item.SCFD_FIELD_ID;
        //                    DataEntity.SCFD_FIELD_VALUE = item.SCFD_FIELD_VALUE;
        //                    DataEntity.SCFD_ENTRY_DATETIME = DateTime.Now;
        //                    DataEntity.SCFD_MAKER_ID = Session["USER_ID"].ToString();
        //                    DataEntity.SCFD_CHECKER_ID = "0";
        //                }
        //                context.ICORE_SUBMIT_CASE_FORM_DETAIL.Add(DataEntity);
        //                RowCount += context.SaveChanges();
        //            }
        //            if (RowCount > 0)
        //            {
        //                message = "";
        //            }
        //            else
        //            {
        //                message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            message = ex.Message;
        //        }
        //        return Json(message);
        //    }
        //    else
        //    {
        //        return RedirectToAction("Index", "Login");
        //    }
        //}
        //public string GenerateCaseNumber()
        //{
        //    string CaseNo = "";
        //    int MAXID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_FORM_MASTER.Max(m => (decimal?)m.SCFM_ID)) + 1;
        //    CaseNo = "CBN-FEOD-" + String.Format("{0:00000}", MAXID) + "-" + DateTime.Now.ToString("ddMMyyyy");
        //    return CaseNo;
        //}
        //public ActionResult GetCategoryMenu(string TargetID)
        //{
        //    string message = "";
        //    try
        //    {
        //        List<ICORE_CATEGORIES> drp_menu = new List<ICORE_CATEGORIES>();
        //        int CatID = Convert.ToInt32(TargetID);
        //        drp_menu = (context.ICORE_CATEGORIES.Where(x => x.Categ_DeptId == CatID)).ToList();
        //        JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
        //        string result = javaScriptSerializer.Serialize(drp_menu);
        //        return Json(result, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        message = ex.Message;
        //    }
        //    finally
        //    {
        //        ViewBag.message = message;
        //    }
        //    return null;
        //}
        //public ActionResult GetCaseTypeMenu(string TargetID)
        //{
        //    string message = "";
        //    try
        //    {
        //        List<ICORE_CASE_TYPES> drp_menu = new List<ICORE_CASE_TYPES>();
        //        int CatID = Convert.ToInt32(TargetID);
        //        drp_menu = (context.ICORE_CASE_TYPES.Where(x => x.CaseType_CategoryId == CatID)).ToList();
        //        JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
        //        string result = javaScriptSerializer.Serialize(drp_menu);
        //        return Json(result, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        message = ex.Message;
        //    }
        //    finally
        //    {
        //        ViewBag.message = message;
        //    }
        //    return null;
        //}
        //public ActionResult GetCaseTitleMenu(string TargetID)
        //{
        //    string message = "";
        //    try
        //    {
        //        List<ICORE_CASE_TITLES> drp_menu = new List<ICORE_CASE_TITLES>();
        //        int CatID = Convert.ToInt32(TargetID);
        //        drp_menu = (context.ICORE_CASE_TITLES.Where(x => x.CaseTitle_CaseTypeId == CatID)).ToList();
        //        JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
        //        string result = javaScriptSerializer.Serialize(drp_menu);
        //        return Json(result, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        message = ex.Message;
        //    }
        //    finally
        //    {
        //        ViewBag.message = message;
        //    }
        //    return null;
        //}
        //public ActionResult GetCaseDocumentData(decimal? CaseType_ID, decimal? CaseTitle_ID)
        //{
        //    List<ICORE_CASE_DOCUMENTS> FieldsName = new List<ICORE_CASE_DOCUMENTS>();
        //    if (CaseType_ID != 0)
        //    {
        //        int CatID = Convert.ToInt32(CaseType_ID);
        //        FieldsName = (context.ICORE_CASE_DOCUMENTS.Where(x => x.Case_Doc_CaseType_Id == CatID)).ToList();
        //    }
        //    else if (CaseTitle_ID != 0)
        //    {
        //        int CatID = Convert.ToInt32(CaseType_ID);
        //        FieldsName = (context.ICORE_CASE_DOCUMENTS.Where(x => x.Case_Doc_CaseType_Id == CatID)).ToList();
        //    }
        //    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
        //    string result = javaScriptSerializer.Serialize(FieldsName);
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
        //public int GetFormForSubmission(decimal? CaseType_ID, decimal? CaseTitle_ID)
        //{
        //    string message = "";
        //    try
        //    {
        //        if (CaseTitle_ID != null && CaseTitle_ID != 0)
        //        {
        //            ICORE_CASE_FORMS_MASTER formmaster = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_CaseTitle_Id == CaseTitle_ID).OrderByDescending(m => m.CaseForm_Id).FirstOrDefault();
        //            if (formmaster != null)
        //            {
        //                ICORE_CASE_DOCUMENTS_VIEW data;
        //                data = context.ICORE_CASE_DOCUMENTS_VIEW.Where(model => model.Case_Doc_CaseTitleId == CaseTitle_ID).FirstOrDefault();
        //                return Convert.ToInt32(formmaster.CaseForm_Id);
        //            }
        //            else
        //            {
        //                return 0;
        //            }
        //        }
        //        else if (CaseType_ID != null && CaseType_ID != 0)
        //        {
        //            ICORE_CASE_FORMS_MASTER formmaster = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_CaseType_Id == CaseType_ID).OrderByDescending(m => m.CaseForm_Id).FirstOrDefault();
        //            if (formmaster != null)
        //            {
        //                ICORE_CASE_DOCUMENTS_VIEW data;
        //                data = context.ICORE_CASE_DOCUMENTS_VIEW.Where(model => model.Case_Doc_CaseType_Id == CaseType_ID).FirstOrDefault();
        //                return Convert.ToInt32(formmaster.CaseForm_Id);
        //            }
        //            else
        //                return 0;
        //        }
        //        else
        //        {
        //            return 0;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        message = ex.Message;
        //    }
        //    return 0;
        //}
        //public ActionResult GetCaseFiledsData(decimal? FormId)
        //{
        //    List<ICORE_CASE_FORMS_DETAILS> FieldsName = new List<ICORE_CASE_FORMS_DETAILS>();
        //    int CatID = Convert.ToInt32(FormId);
        //    FieldsName = (context.ICORE_CASE_FORMS_DETAILS.Where(x => x.CaseForm_FormId == CatID)).ToList();
        //    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
        //    string result = javaScriptSerializer.Serialize(FieldsName);
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //    //return PartialView(context.ICORE_CASE_FORMS_DETAILS_VIEW.Where(m => m.CaseForm_FormId == FormId).ToList());
        //}
        #endregion
    }
}