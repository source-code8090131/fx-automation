﻿using FXAutomation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FXAutomation.Controllers
{
    public class SetupController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: Setup
        #region Email Configration
        public ActionResult EmailSetup(int E = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (E != 0)
                        {
                            ICORE_EMAIL_CONFIGRATION edit = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_ID == E).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + E;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "EmailSetup", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult EmailSetup(ICORE_EMAIL_CONFIGRATION db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        ICORE_EMAIL_CONFIGRATION UpdateEntity = new ICORE_EMAIL_CONFIGRATION();

                        ICORE_EMAIL_CONFIGRATION CheckIfExist = new ICORE_EMAIL_CONFIGRATION();
                        if (db_table.EC_ID == 0)
                        {
                            UpdateEntity.EC_ID = Convert.ToInt32(context.ICORE_EMAIL_CONFIGRATION.Max(m => (decimal?)m.EC_ID)) + 1;
                            UpdateEntity.EC_ENTRY_DATETIME = DateTime.Now;
                            CheckIfExist = null;
                        }
                        else
                        {
                            UpdateEntity = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_ID == db_table.EC_ID).FirstOrDefault();
                            UpdateEntity.EC_EDIT_DATETIME = DateTime.Now;
                            CheckIfExist = UpdateEntity;
                        }
                        UpdateEntity.EC_SEREVER_HOST = db_table.EC_SEREVER_HOST;
                        UpdateEntity.EC_CREDENTIAL_ID = db_table.EC_CREDENTIAL_ID;
                        UpdateEntity.EC_EMAIL_PORT = db_table.EC_EMAIL_PORT;
                        //UpdateEntity.EC_CC_EMAIL = db_table.EC_CC_EMAIL;
                        UpdateEntity.EC_STATUS = false;
                        UpdateEntity.EC_MAKER_ID = Session["USER_ID"].ToString();
                        if (UpdateEntity.EC_MAKER_ID == "Admin123")
                        {
                            UpdateEntity.EC_CHECKER_ID = UpdateEntity.EC_MAKER_ID;
                            UpdateEntity.EC_IS_AUTH = true;
                        }
                        else
                        {
                            UpdateEntity.EC_CHECKER_ID = null;
                            UpdateEntity.EC_IS_AUTH = false;
                        }
                        if (CheckIfExist == null)
                            context.ICORE_EMAIL_CONFIGRATION.Add(UpdateEntity);
                        else
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                        int RowsAffected = context.SaveChanges();
                        if (RowsAffected > 0)
                        {
                            message = "Data Inserted Successfully " + RowsAffected + " Affected";
                            ModelState.Clear();
                        }
                        else
                        {
                            message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "EmailSetup", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [ChildActionOnly]
        public ActionResult ManageEmailSetupList()
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "dashboard");
            }
            else
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<ICORE_EMAIL_CONFIGRATION> Data = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_MAKER_ID != SessionUser || m.EC_IS_AUTH == true).ToList();
                    return PartialView(Data);
                }
                else
                    return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeEmailSetup(int EC_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string message = "";
                        try
                        {
                            ICORE_EMAIL_CONFIGRATION UpdateEntity = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_ID == EC_ID).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                if (UpdateEntity.EC_MAKER_ID != Session["USER_ID"].ToString())
                                {
                                    UpdateEntity.EC_IS_AUTH = true;
                                    UpdateEntity.EC_CHECKER_ID = Session["USER_ID"].ToString();
                                    UpdateEntity.EC_EDIT_DATETIME = DateTime.Now;
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        message = "Setup Email : " + UpdateEntity.EC_CREDENTIAL_ID + " is successfully Authorized";
                                    }
                                }
                                else
                                {
                                    message = "Maker cannot authorize the same record.";
                                    return Json(message);
                                }
                            }
                            else
                            {
                                message = "Problem while fetching your record on ID# " + EC_ID;
                                return Json(message);
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("Setup", "AuthorizeEmailSetup", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(message);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult RejectEmailSetup(int EC_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string message = "";
                        try
                        {
                            ICORE_EMAIL_CONFIGRATION UpdateEntity = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_ID == EC_ID).FirstOrDefault();
                            if (UpdateEntity != null && UpdateEntity.EC_IS_AUTH == false)
                            {
                                if (UpdateEntity.EC_MAKER_ID != Session["USER_ID"].ToString())
                                {
                                    context.ICORE_EMAIL_CONFIGRATION.Remove(UpdateEntity);
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        message = "Setup Email : " + UpdateEntity.EC_CREDENTIAL_ID + " is rejected successfully";
                                    }
                                }
                                else
                                {
                                    message = "Maker cannot reject the same record.";
                                    return Json(message);
                                }
                            }
                            else
                            {
                                message = "Problem while fetching your record on ID# " + EC_ID;
                                return Json(message);
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("Setup", "RejectEmail", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(message);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult UpdateEmailSetupStatus(int EC_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "EmailSetup", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string message = "";
                        try
                        {
                            int RowCount = 0;
                            ICORE_EMAIL_CONFIGRATION EntityToUpdate = new ICORE_EMAIL_CONFIGRATION();
                            List<ICORE_EMAIL_CONFIGRATION> DeactiveStatus = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_ID != EC_ID).ToList();
                            if (DeactiveStatus.Count > 0)
                            {
                                ICORE_EMAIL_CONFIGRATION CheckEntityStatus = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_ID == EC_ID).FirstOrDefault();
                                if (CheckEntityStatus.EC_IS_AUTH == true)
                                {
                                    foreach (ICORE_EMAIL_CONFIGRATION item in DeactiveStatus)
                                    {
                                        ICORE_EMAIL_CONFIGRATION GetListOfEmails = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_ID == item.EC_ID).FirstOrDefault();
                                        if (GetListOfEmails != null)
                                        {
                                            EntityToUpdate = GetListOfEmails;
                                            EntityToUpdate.EC_STATUS = false;
                                            context.Entry(EntityToUpdate).State = EntityState.Modified;
                                            RowCount += context.SaveChanges();
                                        }
                                    }
                                    if (RowCount > 0)
                                    {
                                        ICORE_EMAIL_CONFIGRATION CheckEntity = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_ID == EC_ID).FirstOrDefault();
                                        if (CheckEntity != null)
                                        {
                                            RowCount = 0;
                                            EntityToUpdate = new ICORE_EMAIL_CONFIGRATION();
                                            EntityToUpdate = CheckEntity;
                                            EntityToUpdate.EC_STATUS = true;
                                            context.Entry(EntityToUpdate).State = EntityState.Modified;
                                            RowCount = context.SaveChanges();
                                            if (RowCount > 0)
                                                message = "Current email server is changed to : " + CheckEntity.EC_CREDENTIAL_ID;
                                        }

                                    }
                                }
                                else
                                {
                                    message = "Record need to be authorized first.";
                                    return Json(message);
                                }
                            }
                            else
                            {
                                message = "Problem while fetching your record on ID# " + EC_ID;
                                return Json(message);
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("Setup", "UpdateEmailSetupStatus", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(message);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        #endregion

        #region Update Queue
        public ActionResult Queue(int Q = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "Queue", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (Q != 0)
                        {
                            ICORE_CASE_QUEUE edit = context.ICORE_CASE_QUEUE.Where(m => m.CQ_ID == Q).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + Q;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "Queue", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }

            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Queue(ICORE_CASE_QUEUE db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "Queue", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        ICORE_CASE_QUEUE UpdateEntity = context.ICORE_CASE_QUEUE.Where(m => m.CQ_ID == db_table.CQ_ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            UpdateEntity.CQ_NAME = db_table.CQ_NAME;
                            //UpdateEntity.CQ_TO_EMAIL = db_table.CQ_TO_EMAIL;
                            UpdateEntity.CQ_STATSUS = true;
                            UpdateEntity.CQ_MAKER_ID = Session["USER_ID"].ToString();
                            if (UpdateEntity.CQ_MAKER_ID == "Admin123")
                            {
                                UpdateEntity.CQ_ISAUTH = true;
                                UpdateEntity.CQ_CHECKER_ID = UpdateEntity.CQ_MAKER_ID;
                            }
                            else
                            {
                                UpdateEntity.CQ_ISAUTH = false;
                                UpdateEntity.CQ_CHECKER_ID = null;
                            }
                            UpdateEntity.CQ_EDIT_DATETIME = DateTime.Now;
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            int RowCount = context.SaveChanges();
                            if (RowCount > 0)
                            {
                                message = UpdateEntity.CQ_NAME + " updated sucessfully";
                                ModelState.Clear();
                            }
                            else
                            {
                                message = "Problem while updating record " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                        else
                        {
                            message = "Select Queue to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "Queue", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [ChildActionOnly]
        public ActionResult QueuesList()
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "dashboard");
            }
            else
            {
                if (DAL.CheckFunctionValidity("Setup", "Queue", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<ICORE_CASE_QUEUE> Data = context.ICORE_CASE_QUEUE.Where(m => m.CQ_MAKER_ID != SessionUser || m.CQ_ISAUTH == true).ToList();
                    return PartialView(Data);
                }
                else
                    return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthozrizeQueue(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "Queue", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        ICORE_CASE_QUEUE UpdateEntity = context.ICORE_CASE_QUEUE.Where(m => m.CQ_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            if (UpdateEntity.CQ_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                UpdateEntity.CQ_ISAUTH = true;
                                UpdateEntity.CQ_CHECKER_ID = Session["USER_ID"].ToString();
                                UpdateEntity.CQ_EDIT_DATETIME = DateTime.Now;
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = UpdateEntity.CQ_NAME + " is successfully Authorized";
                                }
                            }
                            else
                            {
                                message = "Maker cannot authorize the same record.";
                                return Json(message);
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "AuthorizeQueue", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid Request");
                }
            }
            else
            {
                return Json("Invalid Request");
            }
        }
        [HttpPost]
        public JsonResult RejectQueue(int ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Setup", "Queue", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        ICORE_CASE_QUEUE UpdateEntity = context.ICORE_CASE_QUEUE.Where(m => m.CQ_ID == ID).FirstOrDefault();
                        if (UpdateEntity != null && UpdateEntity.CQ_ISAUTH == false)
                        {
                            if (UpdateEntity.CQ_MAKER_ID != Session["USER_ID"].ToString())
                            {
                                context.ICORE_CASE_QUEUE.Remove(UpdateEntity);
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = UpdateEntity.CQ_NAME + " is rejected successfully";
                                }
                            }
                            else
                            {
                                message = "Maker cannot reject the same record.";
                                return Json(message);
                            }
                        }
                        else
                        {
                            message = "Problem while fetching your record on ID# " + ID;
                            return Json(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "RejectQueue", Session["USER_ID"].ToString(), ex);
                    }
                    return Json(message);
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        #endregion

        #region System Setup
        public ActionResult SystemSetup(int S = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "SystemSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (S != 0)
                        {
                            ICORE_SYSTEM_SETUP edit = context.ICORE_SYSTEM_SETUP.Where(m => m.SS_ID == S).FirstOrDefault();
                            if (edit != null)
                            {
                                //edit.SS_HASH = DAL.Decrypt(edit.SS_HASH);
                                return View(edit);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + S;
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "SystemSetup", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult SystemSetup(ICORE_SYSTEM_SETUP Entity)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Setup", "SystemSetup", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    int RowCount = 0;
                    try
                    {
                        if (Entity != null)
                        {
                            ICORE_SYSTEM_SETUP Update = context.ICORE_SYSTEM_SETUP.Where(m => m.SS_ID == Entity.SS_ID).FirstOrDefault();
                            if (Update != null)
                            {
                                //Update.SS_HASH = DAL.Encrypt(Entity.SS_HASH);
                                Update.SS_DOMAIN_FOR_WEB = Entity.SS_DOMAIN_FOR_WEB;
                                Update.SS_DOMAIN_FOR_CLIENT = Entity.SS_DOMAIN_FOR_CLIENT;
                                Update.SS_EPF_PORTAL_FRAME_SOURCE = Entity.SS_EPF_PORTAL_FRAME_SOURCE;
                                Update.SS_EPF_PORTAL_URI = Entity.SS_EPF_PORTAL_URI;
                                Update.SS_URL_FOR_HOME_REDIRECT = Entity.SS_URL_FOR_HOME_REDIRECT;
                                Update.SS_PROXY_SIGNIN_URL = Entity.SS_PROXY_SIGNIN_URL;
                                Update.SS_IS_AUTH = true;
                                Update.SS_ENTRY_DATETIME = DateTime.Now;
                                Update.SS_MAKER_ID = Session["USER_ID"].ToString();
                                context.Entry(Update).State = EntityState.Modified;
                                RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    ModelState.Clear();
                                    message = "The system setup record has been successfully updated.";
                                }
                                return View();
                            }
                            else
                            {
                                message = "Select record to continue.";
                                return View();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Setup", "SystemSetup", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [ChildActionOnly]
        public ActionResult SystemSetupList()
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "dashboard");
            }
            else
            {
                if (DAL.CheckFunctionValidity("Setup", "SystemSetup", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<ICORE_SYSTEM_SETUP> Data = context.ICORE_SYSTEM_SETUP.ToList();
                    return PartialView(Data);
                }
                else
                    return PartialView(null);
            }
        }
        #endregion
    }
}