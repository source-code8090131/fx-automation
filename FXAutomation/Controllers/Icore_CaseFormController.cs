﻿using FXAutomation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FXAutomation.Controllers
{
    public class Icore_CaseFormController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: Icore_CaseForm
        #region Case Form Master
        public ActionResult CaseForm(string form_ID, string field_ID)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_CaseForm", "CaseForm", Session["USER_ID"].ToString()))
                {
                    int FormId = 0;
                    if (form_ID != null)
                        FormId = Convert.ToInt32(DAL.Decrypt(form_ID));

                    int FieldId = 0;
                    if (field_ID != null)
                        FieldId = Convert.ToInt32(DAL.Decrypt(field_ID));
                    string messeage = "";
                    ICORE_CASE_FORMS_DETAILS_VIEW modeldata = new ICORE_CASE_FORMS_DETAILS_VIEW();
                    try
                    {
                        if (FormId != 0)
                        {
                            List<ICORE_CASE_FORMS_DETAILS_VIEW> allfields = context.ICORE_CASE_FORMS_DETAILS_VIEW.Where(m => m.CaseForm_FormId == FormId).ToList();
                            if (allfields.Count > 0)
                            {
                                if (FieldId != 0)
                                {
                                    modeldata = allfields.Where(m => m.CaseForm_Field_Id == FieldId).FirstOrDefault();
                                }
                                else
                                {
                                    modeldata = allfields.FirstOrDefault();
                                    modeldata.CaseForm_Field_Id = 0;
                                    modeldata.CaseForm_Field_IsRequired = false;
                                    modeldata.CaseForm_Field_Name = "";
                                    modeldata.CaseForm_Field_Status = null;
                                    modeldata.CaseForm_Data_MIN_LENGTH = 0;
                                    modeldata.CaseForm_Data_MAX_LENGTH = 0;
                                    modeldata.CaseForm_Data_ORDER_BY = 0;
                                    modeldata.CaseForm_Field_Description = "";
                                }
                            }
                            else
                            {
                                ICORE_CASE_FORMS_MASTER master = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == FormId).FirstOrDefault();
                                modeldata.CaseForm_FormId = master.CaseForm_Id;
                                modeldata.CaseForm_Name = master.CaseForm_Name;
                                modeldata.CaseForm_Status = master.CaseForm_Status;
                                modeldata.CaseForm_Description = master.CaseForm_Description;
                                modeldata.CaseForm_CaseType_Id = master.CaseForm_CaseType_Id;
                                modeldata.CaseForm_CaseTitle_Id = master.CaseForm_CaseTitle_Id;
                            }
                        }
                        else
                        {
                            modeldata.CaseForm_FormId = 0; //Convert.ToInt32(context.ICORE_CASE_FORMS_DETAILS_VIEW.Max(m => (decimal?)m.CaseForm_FormId)) + 1;
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_CaseForm", "CaseForm", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.messeage = messeage;
                        ViewBag.CaseForm_DataTypeId = new SelectList(context.ICORE_CASE_FORMS_DATA_TYPES.ToList(), "DataTypeId", "DataType_Name");

                        List<SelectListItem> items = new SelectList(context.ICORE_CASE_TYPES.Where(m => m.CaseType_Status == true && m.CaseType_IsAuth == true).ToList(), "CaseType_Id", "CaseType_Name", 0).ToList();
                        items.Insert(0, (new SelectListItem { Text = "--Select Case Type--", Value = "0" }));
                        ViewBag.CaseForm_CaseType_Id = items;
                        List<SelectListItem> item = new SelectList(context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Status == true && m.CaseTitle_IsAuth == true).ToList(), "CaseTitle_Id", "CaseTitle_Name", 0).ToList();
                        item.Insert(0, (new SelectListItem { Text = "--Select Case Title--", Value = "0" }));
                        ViewBag.CaseForm_CaseTitle_Id = item;
                        ViewBag.messeage = messeage;
                    }
                    return View(modeldata);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult CaseForm(ICORE_CASE_FORMS_DETAILS_VIEW dbtable_view)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_CaseForm", "CaseForm", Session["USER_ID"].ToString()))
				{
                    ICORE_CASE_FORMS_DETAILS_VIEW modeldata = new ICORE_CASE_FORMS_DETAILS_VIEW();
                    string messeage = "";
                    try
                    {
                        if (dbtable_view.CaseForm_CaseTitle_Id != 0 && dbtable_view.CaseForm_CaseType_Id != 0)
                        {
                            if (dbtable_view.CaseForm_FormId != 0 && dbtable_view.CaseForm_Status == false)
                            {
                                ICORE_SUBMIT_CASE_FORM_MASTER getsubmittedform = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_CASE_FORM_ID == dbtable_view.CaseForm_FormId).FirstOrDefault();
                                if (getsubmittedform != null)
                                {
                                    messeage = "Form : " + dbtable_view.CaseForm_Name + " cannot be marked as deactive.";
                                    return View(modeldata);
                                }
                            }
                            ICORE_CASE_FORMS_MASTER MasterCheck = new ICORE_CASE_FORMS_MASTER();
                            if (dbtable_view.CaseForm_FormId != 0)
                                MasterCheck = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_CaseTitle_Id == dbtable_view.CaseForm_CaseTitle_Id && m.CaseForm_Id != dbtable_view.CaseForm_FormId).FirstOrDefault();
                            else
                                MasterCheck = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_CaseTitle_Id == dbtable_view.CaseForm_CaseTitle_Id).FirstOrDefault();
                            if (MasterCheck == null)
                            {
                                // Master tabel fields start  
                                ICORE_CASE_FORMS_MASTER insert_master_data = new ICORE_CASE_FORMS_MASTER();

                                ICORE_CASE_FORMS_MASTER CheckIfExistMaster = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == dbtable_view.CaseForm_FormId).AsNoTracking().FirstOrDefault();

                                if (CheckIfExistMaster == null)
                                {
                                    insert_master_data.CaseForm_Id = Convert.ToInt32(context.ICORE_CASE_FORMS_MASTER.Max(m => (decimal?)m.CaseForm_Id)) + 1;
                                    insert_master_data.CaseForm_DataEntryDateTime = DateTime.Now;
                                }
                                else
                                {
                                    insert_master_data.CaseForm_Id = CheckIfExistMaster.CaseForm_Id;
                                    insert_master_data.CaseForm_DataEditDateTime = DateTime.Now;
                                }
                                insert_master_data.CaseForm_Name = dbtable_view.CaseForm_Name;
                                insert_master_data.CaseForm_Description = dbtable_view.CaseForm_Description;
                                insert_master_data.CaseForm_Status = dbtable_view.CaseForm_Status;
                                insert_master_data.CaseForm_MakerId = Session["USER_ID"].ToString();
                                if (insert_master_data.CaseForm_MakerId == "Admin123" && insert_master_data.CaseForm_Status != false)
                                {
                                    insert_master_data.CaseForm_CheckerId = insert_master_data.CaseForm_MakerId;
                                    insert_master_data.CaseForm_IsAuth = true;
                                }
                                else
                                {
                                    insert_master_data.CaseForm_CheckerId = null;
                                    insert_master_data.CaseForm_IsAuth = false;
                                }
                                insert_master_data.CaseForm_CaseTitle_Id = dbtable_view.CaseForm_CaseTitle_Id;
                                insert_master_data.CaseForm_CaseType_Id = dbtable_view.CaseForm_CaseType_Id;
                                if (CheckIfExistMaster == null)
                                    context.ICORE_CASE_FORMS_MASTER.Add(insert_master_data);
                                else
                                    context.Entry(insert_master_data).State = EntityState.Modified;
                                // Master tabel fields end
                                int RowsAffected = context.SaveChanges();
                                if (RowsAffected > 0)
                                {
                                    if (dbtable_view.CaseForm_Status == false)
                                    {
                                        ICORE_CASE_TYPES CaseType = context.ICORE_CASE_TYPES.Where(m => m.CaseType_Id == dbtable_view.CaseForm_CaseType_Id).FirstOrDefault();
                                        ICORE_CASE_TITLES CaseTitle = context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Id == dbtable_view.CaseForm_CaseTitle_Id).FirstOrDefault();
                                        ICORE_DEACTIVE_REJECT_LOG log = new ICORE_DEACTIVE_REJECT_LOG();
                                        log.DRL_ID = Convert.ToInt32(context.ICORE_DEACTIVE_REJECT_LOG.Max(m => (decimal?)m.DRL_ID)) + 1;
                                        log.DRL_FORM_NAME = dbtable_view.CaseForm_Name;
                                        log.DRL_CASE_TYPE = CaseType.CaseType_Name;
                                        log.DRL_CASE_TITLE = CaseTitle.CaseTitle_Name;
                                        log.DRL_FORM_STATUS = "Deactive";
                                        log.DRL_DEACTIVE_BY = Session["USER_ID"].ToString();
                                        log.DRL_ENTRY_DATETIME = DateTime.Now;
                                        context.ICORE_DEACTIVE_REJECT_LOG.Add(log);
                                        RowsAffected = context.SaveChanges();
                                        if (RowsAffected > 0)
                                        {
                                            messeage = "Data Inserted Successfully " + RowsAffected + " Affected";
                                            ModelState.Clear();
                                        }
                                        else
                                            messeage = "Problem occured please contact to administrator.";
                                    }
                                    else
                                    {
                                        messeage = "Data Inserted Successfully " + RowsAffected + " Affected";
                                        ModelState.Clear();
                                    }
                                }
                                else
                                {
                                    messeage = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                                }
                            }
                            else
                            {
                                messeage = "A Form Already Exist on the Selected Case Title";
                            }
                        }
                        else
                        {
                            messeage = "Please select Valid Case Type and Case Title";
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_CaseForm", "CaseForm", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.messeage = messeage;
                        ViewBag.CaseForm_DataTypeId = new SelectList(context.ICORE_CASE_FORMS_DATA_TYPES.ToList(), "DataTypeId", "DataType_Name");

                        List<SelectListItem> items = new SelectList(context.ICORE_CASE_TYPES.Where(m => m.CaseType_Status == true && m.CaseType_IsAuth == true).ToList(), "CaseType_Id", "CaseType_Name", 0).ToList();
                        items.Insert(0, (new SelectListItem { Text = "--Select Case Type--", Value = "0" }));
                        ViewBag.CaseForm_CaseType_Id = items;
                        List<SelectListItem> item = new SelectList(context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Status == true && m.CaseTitle_IsAuth == true).ToList(), "CaseTitle_Id", "CaseTitle_Name", 0).ToList();
                        item.Insert(0, (new SelectListItem { Text = "--Select Case Title--", Value = "0" }));
                        ViewBag.CaseForm_CaseTitle_Id = item;
                        ViewBag.messeage = messeage;
                    }
                    return View(modeldata);
                }
				else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public JsonResult AuthorizeCaseForm(int CaseForm_Id)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_CaseForm", "CaseForm", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        try
                        {
                            ICORE_CASE_FORMS_MASTER UpdateEntity = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == CaseForm_Id).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                if (UpdateEntity.CaseForm_MakerId != Session["USER_ID"].ToString())
                                {
                                    UpdateEntity.CaseForm_IsAuth = true;
                                    UpdateEntity.CaseForm_CheckerId = Session["USER_ID"].ToString();
                                    UpdateEntity.CaseForm_DataEditDateTime = DateTime.Now;
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        if (UpdateEntity != null && UpdateEntity.CaseForm_Status == false)
                                        {
                                            ICORE_DEACTIVE_REJECT_LOG log = context.ICORE_DEACTIVE_REJECT_LOG.Where(m => m.DRL_FORM_NAME == UpdateEntity.CaseForm_Name).OrderByDescending(m => m.DRL_ID).FirstOrDefault();
                                            if (log != null)
                                            {
                                                log.DRL_AUTHORIZED_BY = Session["USER_ID"].ToString();
                                                log.DRL_EDIT_DATETIME = DateTime.Now;
                                                context.Entry(log).State = EntityState.Modified;
                                                RowCount = context.SaveChanges();
                                                if (RowCount > 0)
                                                    messeage = "Case Form : " + UpdateEntity.CaseForm_Name + " is successfully Authorized";
                                            }
                                            else
                                                messeage = "Case Form : " + UpdateEntity.CaseForm_Name + " is successfully Authorized";
                                        }
                                        else
                                            messeage = "Case Form : " + UpdateEntity.CaseForm_Name + " is successfully Authorized";
                                    }
                                }
                                else
                                    messeage = "Sorry, Maker cannot authorize the same record.";
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + CaseForm_Id;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("Icore_CaseForm", "AuthorizeCaseForm", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult DeleteFromAuthorizeCaseForm(int CaseForm_Id)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_CaseForm", "CaseForm", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        try
                        {
                            ICORE_CASE_FORMS_MASTER UpdateEntity = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == CaseForm_Id).FirstOrDefault();
                            if (UpdateEntity != null && UpdateEntity.CaseForm_IsAuth == false)
                            {
                                if (UpdateEntity.CaseForm_MakerId != Session["USER_ID"].ToString())
                                {
                                    ICORE_SUBMIT_CASE_FORM_MASTER getsubmittedform = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_CASE_FORM_ID == UpdateEntity.CaseForm_Id).FirstOrDefault();
                                    if (getsubmittedform != null)
                                    {
                                        messeage = "Form : " + UpdateEntity.CaseForm_Name + " cannot be marked as deactive.";
                                        return Json(messeage);
                                    }
                                    if (UpdateEntity.CaseForm_Status == false)
                                    {
                                        context.ICORE_CASE_FORMS_MASTER.Remove(UpdateEntity);
                                        int RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                        {
                                            ICORE_DEACTIVE_REJECT_LOG log = context.ICORE_DEACTIVE_REJECT_LOG.Where(m => m.DRL_FORM_NAME == UpdateEntity.CaseForm_Name).OrderByDescending(m => m.DRL_ID).FirstOrDefault();
                                            if (log != null)
                                            {
                                                log.DRL_AUTHORIZED_BY = Session["USER_ID"].ToString();
                                                log.DRL_EDIT_DATETIME = DateTime.Now;
                                                context.Entry(log).State = EntityState.Modified;
                                                RowCount = context.SaveChanges();
                                                if (RowCount > 0)
                                                    messeage = "Case Form : " + UpdateEntity.CaseForm_Name + " is successfully deactive";
                                            }
                                            else
                                                messeage = "Case Form : " + UpdateEntity.CaseForm_Name + " is successfully deactive";
                                        }
                                    }
                                    else
                                    {
                                        UpdateEntity.CaseForm_Status = false;
                                        UpdateEntity.CaseForm_MakerId = Session["USER_ID"].ToString();
                                        UpdateEntity.CaseForm_DataEditDateTime = DateTime.Now;
                                        context.Entry(UpdateEntity).State = EntityState.Modified;
                                        int RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                        {
                                            if (UpdateEntity != null && UpdateEntity.CaseForm_Status == false)
                                            {
                                                ICORE_DEACTIVE_REJECT_LOG log = context.ICORE_DEACTIVE_REJECT_LOG.Where(m => m.DRL_FORM_NAME == UpdateEntity.CaseForm_Name).OrderByDescending(m => m.DRL_ID).FirstOrDefault();
                                                if (log != null)
                                                {
                                                    log.DRL_AUTHORIZED_BY = Session["USER_ID"].ToString();
                                                    log.DRL_EDIT_DATETIME = DateTime.Now;
                                                    context.Entry(log).State = EntityState.Modified;
                                                    RowCount = context.SaveChanges();
                                                    if (RowCount > 0)
                                                        messeage = "Case Form : " + UpdateEntity.CaseForm_Name + " is marked as deactive";
                                                }
                                                else if (log == null)
                                                {
                                                    ICORE_CASE_TYPES CaseType = context.ICORE_CASE_TYPES.Where(m => m.CaseType_Id == UpdateEntity.CaseForm_CaseType_Id).FirstOrDefault();
                                                    ICORE_CASE_TITLES CaseTitle = context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Id == UpdateEntity.CaseForm_CaseTitle_Id).FirstOrDefault();
                                                    ICORE_DEACTIVE_REJECT_LOG logs = new ICORE_DEACTIVE_REJECT_LOG();
                                                    logs.DRL_ID = Convert.ToInt32(context.ICORE_DEACTIVE_REJECT_LOG.Max(m => (decimal?)m.DRL_ID)) + 1;
                                                    logs.DRL_FORM_NAME = UpdateEntity.CaseForm_Name;
                                                    logs.DRL_CASE_TYPE = CaseType.CaseType_Name;
                                                    logs.DRL_CASE_TITLE = CaseTitle.CaseTitle_Name;
                                                    logs.DRL_FORM_STATUS = "Deactive";
                                                    logs.DRL_DEACTIVE_BY = Session["USER_ID"].ToString();
                                                    logs.DRL_ENTRY_DATETIME = DateTime.Now;
                                                    context.ICORE_DEACTIVE_REJECT_LOG.Add(logs);
                                                    RowCount = context.SaveChanges();
                                                    if (RowCount > 0)
                                                        messeage = "Case Form : " + UpdateEntity.CaseForm_Name + " is marked as deactive";
                                                }
                                                else
                                                    messeage = "Case Form : " + UpdateEntity.CaseForm_Name + " is marked as deactive";
                                            }
                                            else
                                                messeage = "Case Form : " + UpdateEntity.CaseForm_Name + " is marked as deactive";
                                        }
                                    }

                                }
                                else
                                    messeage = "Sorry, Maker cannot reject the same record.";
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + CaseForm_Id;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("Icore_CaseForm", "DeleteFromAuthorizeCaseForm", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        public ActionResult CaseForm_grid(decimal? CaseType_ID, decimal? CaseTitle_ID)
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "dashboard");
            }
            else
            {
                if (DAL.CheckFunctionValidity("Icore_CaseForm", "CaseForm", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<ICORE_CASE_FORMS_MASTER> data;
                    if (CaseTitle_ID != null && CaseTitle_ID != 0)
                    {
                        data = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_CaseTitle_Id == CaseTitle_ID && (m.CaseForm_IsAuth == true || m.CaseForm_MakerId != SessionUser)).OrderByDescending(m => m.CaseForm_Id).ToList();
                        data.Where(m => m.CaseForm_MakerId != SessionUser || m.CaseForm_IsAuth == true);
                    }
                    else if (CaseType_ID != null && CaseType_ID != 0)
                    {
                        data = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_CaseType_Id == CaseType_ID && (m.CaseForm_IsAuth == true || m.CaseForm_MakerId != SessionUser)).OrderByDescending(m => m.CaseForm_Id).ToList();
                        data.Where(m => m.CaseForm_MakerId != SessionUser || m.CaseForm_IsAuth == true);
                    }
                    else
                    {
                        data = new List<ICORE_CASE_FORMS_MASTER>();
                    }
                    return PartialView(data);
                }
                else
                    return PartialView(null);
            }
        }
        //[HttpPost]
        //public JsonResult DeleteMasterNDetailData(int CaseForm_Id)
        //{
        //    if (Session["USER_ID"] != null && Request.IsAjaxRequest())
        //    {
        //        if (DAL.CheckFunctionValidity("Icore_CaseForm", "CaseForm", Session["USER_ID"].ToString()))
        //        {
        //            string message = "";
        //            int RowCount = 0;
        //            try
        //            {
        //                ICORE_CASE_FORMS_MASTER MasterEntity = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == CaseForm_Id).FirstOrDefault();
        //                if (MasterEntity != null)
        //                {
        //                    context.ICORE_CASE_FORMS_MASTER.Remove(MasterEntity);
        //                    RowCount = +context.SaveChanges();
        //                    if (RowCount > 0)
        //                    {
        //                        List<ICORE_CASE_FORMS_DETAILS> DetailEntity = context.ICORE_CASE_FORMS_DETAILS.Where(m => m.CaseForm_FormId == CaseForm_Id).ToList();
        //                        if (DetailEntity.Count > 0)
        //                        {
        //                            RowCount = 0;
        //                            foreach (ICORE_CASE_FORMS_DETAILS DetailData in DetailEntity)
        //                            {
        //                                context.ICORE_CASE_FORMS_DETAILS.Remove(DetailData);
        //                                RowCount += context.SaveChanges();
        //                            }
        //                        }
        //                    }
        //                    if (RowCount > 0)
        //                    {
        //                        message = "Form : " + MasterEntity.CaseForm_Name + " is deleted sucessfully";
        //                        return Json(message);
        //                    }
        //                }
        //                else
        //                {
        //                    message = "Error While Fetching Form";
        //                    return Json(message);
        //                }
        //                return Json(message);
        //            }
        //            catch (Exception ex)
        //            {
        //                message = DAL.LogException("Icore_CaseForm", "DeleteMasterNDetailData", Session["USER_ID"].ToString(), ex);
        //            }
        //            return Json(message);
        //        }
        //        else
        //        {
        //            return Json("Invalid request");
        //        }
        //    }
        //    else
        //        return Json("Invalid request");
        //}
        #endregion

        #region Case Form Details
        public ActionResult CaseForm_Fields(string form_ID, string field_ID)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_CaseForm", "CaseForm", Session["USER_ID"].ToString()))
                {
                    int FormId = 0;
                    if (form_ID != null)
                        FormId = Convert.ToInt32(DAL.Decrypt(form_ID));

                    int FieldId = 0;
                    if (field_ID != null)
                        FieldId = Convert.ToInt32(DAL.Decrypt(field_ID));
                    string messeage = "";
                    ICORE_CASE_FORMS_DETAILS_VIEW modeldata = new ICORE_CASE_FORMS_DETAILS_VIEW();
                    try
                    {
                        if (FormId != 0)
                        {
                            List<ICORE_CASE_FORMS_DETAILS_VIEW> allfields = context.ICORE_CASE_FORMS_DETAILS_VIEW.Where(m => m.CaseForm_FormId == FormId).ToList();
                            if (allfields.Count > 0)
                            {
                                if (FieldId != 0)
                                {
                                    modeldata = allfields.Where(m => m.CaseForm_Field_Id == FieldId).FirstOrDefault();
                                }
                                else
                                {
                                    modeldata = allfields.FirstOrDefault();
                                    modeldata.CaseForm_Field_Id = 0;
                                    modeldata.CaseForm_Field_IsRequired = false;
                                    modeldata.CaseForm_Field_Name = "";
                                    modeldata.CaseForm_Field_Status = null;
                                    modeldata.CaseForm_Data_MIN_LENGTH = null;
                                    modeldata.CaseForm_Data_MAX_LENGTH = null;
                                    modeldata.CaseForm_Data_ORDER_BY = null;
                                    modeldata.CaseForm_Filed_IsAD = false;
                                    modeldata.CaseForm_Field_Description = "";
                                }
                            }
                            else
                            {
                                ICORE_CASE_FORMS_MASTER master = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == FormId).FirstOrDefault();
                                modeldata.CaseForm_FormId = master.CaseForm_Id;
                                modeldata.CaseForm_Name = master.CaseForm_Name;
                            }
                        }
                        else
                        {
                            ICORE_CASE_FORMS_MASTER master = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == FormId).FirstOrDefault();
                            modeldata.CaseForm_FormId = master.CaseForm_Id;
                            modeldata.CaseForm_Name = master.CaseForm_Name;
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_CaseForm", "CaseForm_Fields", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.messeage = messeage;
                        ViewBag.CaseForm_DataTypeId = new SelectList(context.ICORE_CASE_FORMS_DATA_TYPES.ToList(), "DataTypeId", "DataType_Name");
                        ViewBag.messeage = messeage;
                    }
                    return View(modeldata);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult CaseForm_Fields(ICORE_CASE_FORMS_DETAILS_VIEW dbtable_view)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_CaseForm", "CaseForm", Session["USER_ID"].ToString()))
                {
                    ICORE_CASE_FORMS_DETAILS_VIEW modeldata = new ICORE_CASE_FORMS_DETAILS_VIEW();
                    string messeage = "";
                    try
                    {
                        ICORE_CASE_FORMS_DETAILS insert_detail_data = new ICORE_CASE_FORMS_DETAILS();

                        ICORE_CASE_FORMS_DETAILS CheckIfExistDetail = new ICORE_CASE_FORMS_DETAILS();
                        ICORE_CASE_FORMS_DETAILS CheckDupliacte = new ICORE_CASE_FORMS_DETAILS();
                        if (dbtable_view.CaseForm_Field_Id == 0)
                        {
                            CheckDupliacte = context.ICORE_CASE_FORMS_DETAILS.Where(m => m.CaseForm_FormId == dbtable_view.CaseForm_FormId && (m.CaseForm_Data_ORDER_BY == dbtable_view.CaseForm_Data_ORDER_BY || m.CaseForm_Field_Name == dbtable_view.CaseForm_Field_Name)).FirstOrDefault();
                            if (CheckDupliacte != null)
                            {
                                messeage = "Field Already Exist With the Same name or Order No";
                            }
                            else
                            {
                                insert_detail_data.CaseForm_Field_Id = Convert.ToInt32(context.ICORE_CASE_FORMS_DETAILS.Max(m => (decimal?)m.CaseForm_Field_Id)) + 1;
                                insert_detail_data.CaseForm_Field_DataEntryDateTime = DateTime.Now;
                                CheckIfExistDetail = null;
                            }
                        }
                        else
                        {
                            CheckDupliacte = context.ICORE_CASE_FORMS_DETAILS.Where(m => m.CaseForm_Field_Id != dbtable_view.CaseForm_Field_Id && m.CaseForm_FormId == dbtable_view.CaseForm_FormId && (m.CaseForm_Data_ORDER_BY == dbtable_view.CaseForm_Data_ORDER_BY || m.CaseForm_Field_Name == dbtable_view.CaseForm_Field_Name)).FirstOrDefault();
                            if (CheckDupliacte != null)
                            {
                                messeage = "Field Already Exist With the Same name or Order No";
                            }
                            else
                            {
                                insert_detail_data = context.ICORE_CASE_FORMS_DETAILS.Where(m => m.CaseForm_Field_Id == dbtable_view.CaseForm_Field_Id).AsNoTracking().FirstOrDefault();
                                insert_detail_data.CaseForm_Field_Id = (int)dbtable_view.CaseForm_Field_Id;
                                insert_detail_data.CaseForm_Field_DataEditDateTime = DateTime.Now;
                                CheckIfExistDetail = insert_detail_data;
                            }
                        }
                        if (CheckDupliacte == null)
                        {
                            insert_detail_data.CaseForm_DataTypeId = (int)dbtable_view.CaseForm_DataTypeId;
                            insert_detail_data.CaseForm_FormId = dbtable_view.CaseForm_FormId;
                            insert_detail_data.CaseForm_Field_Name = dbtable_view.CaseForm_Field_Name;
                            insert_detail_data.CaseForm_Field_Description = dbtable_view.CaseForm_Field_Description;
                            insert_detail_data.CaseForm_Data_MIN_LENGTH = dbtable_view.CaseForm_Data_MIN_LENGTH;
                            insert_detail_data.CaseForm_Data_MAX_LENGTH = dbtable_view.CaseForm_Data_MAX_LENGTH;
                            insert_detail_data.CaseForm_Data_ORDER_BY = dbtable_view.CaseForm_Data_ORDER_BY;
                            insert_detail_data.CaseForm_Field_Status = dbtable_view.CaseForm_Field_Status;
                            insert_detail_data.CaseForm_Field_MakerId = Session["USER_ID"].ToString();
                            if (insert_detail_data.CaseForm_Field_MakerId == "Admin123")
                            {
                                insert_detail_data.CaseForm_Field_CheckerId = insert_detail_data.CaseForm_Field_MakerId;
                                insert_detail_data.CaseForm_Field_IsAuth = true;
                            }
                            else
                            {
                                insert_detail_data.CaseForm_Field_CheckerId = null;
                                insert_detail_data.CaseForm_Field_IsAuth = false;
                            }
                            insert_detail_data.CaseForm_Field_IsRequired = dbtable_view.CaseForm_Field_IsRequired;
                            insert_detail_data.CaseForm_Filed_IsAD = dbtable_view.CaseForm_Filed_IsAD;

                            if (dbtable_view.CaseForm_DataTypeId == 6)
                            {
                                if (dbtable_view.Lookup_Value != null)
                                {
                                    ICORE_FIELDS_LOOKUP_DATA insert_lookup_data = new ICORE_FIELDS_LOOKUP_DATA();
                                    ICORE_FIELDS_LOOKUP_DATA CheckIfExistLookup = context.ICORE_FIELDS_LOOKUP_DATA.Where(m => m.Lookup_Field_Id == dbtable_view.CaseForm_Field_Id).AsNoTracking().FirstOrDefault();

                                    if (CheckIfExistLookup == null)
                                        insert_lookup_data.Lookup_Data_Id = Convert.ToInt32(context.ICORE_FIELDS_LOOKUP_DATA.Max(m => (int?)m.Lookup_Data_Id)) + 1;
                                    else
                                        insert_lookup_data.Lookup_Data_Id = dbtable_view.Lookup_Data_Id;

                                    insert_lookup_data.Lookup_Field_Id = (int)insert_detail_data.CaseForm_Field_Id;
                                    insert_lookup_data.Lookup_Text = dbtable_view.Lookup_Value;
                                    insert_lookup_data.Lookup_Value = dbtable_view.Lookup_Value;

                                    if (CheckIfExistLookup == null)
                                        context.ICORE_FIELDS_LOOKUP_DATA.Add(insert_lookup_data);
                                    else
                                        context.Entry(insert_lookup_data).State = EntityState.Modified;
                                }
                                else
                                {
                                    messeage = "Dropdown value and text is required !!";
                                    return View(modeldata);
                                }
                            }
                            if (CheckIfExistDetail == null)
                                context.ICORE_CASE_FORMS_DETAILS.Add(insert_detail_data);
                            else
                                context.Entry(insert_detail_data).State = EntityState.Modified;
                            dbtable_view.CaseForm_Field_Id = insert_detail_data.CaseForm_Field_Id;

                            int RowsAffected = context.SaveChanges();
                            if (RowsAffected > 0)
                            {
                                messeage = "Data Inserted Successfully " + RowsAffected + " Affected";
                            }
                            else
                            {
                                messeage = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_CaseForm", "CaseForm_Fields", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.messeage = messeage;
                        ViewBag.CaseForm_DataTypeId = new SelectList(context.ICORE_CASE_FORMS_DATA_TYPES.ToList(), "DataTypeId", "DataType_Name");
                        if (dbtable_view.CaseForm_Field_Id == 0)
                            modeldata = context.ICORE_CASE_FORMS_DETAILS_VIEW.Where(m => m.CaseForm_FormId == dbtable_view.CaseForm_FormId).FirstOrDefault();
                        else
                            modeldata = context.ICORE_CASE_FORMS_DETAILS_VIEW.Where(m => m.CaseForm_FormId == dbtable_view.CaseForm_FormId && m.CaseForm_Field_Id == dbtable_view.CaseForm_Field_Id).FirstOrDefault();

                    }
                    return View(modeldata);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public ActionResult CaseFormView(decimal? FormId)
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "dashboard");
            }
            else
            {
                if (DAL.CheckFunctionValidity("Icore_CaseForm", "CaseForm", Session["USER_ID"].ToString()))
                {
                    string SesssionUser = Session["USER_ID"].ToString();
                    List<ICORE_CASE_FORMS_DETAILS_VIEW> ViewData = context.ICORE_CASE_FORMS_DETAILS_VIEW.Where(m => m.CaseForm_FormId == FormId).OrderBy(m => m.CaseForm_Data_ORDER_BY).ToList();
                    return PartialView(ViewData);
                }
                else
                    return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeCaseFormDetail(int CaseForm_Field_Id)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_CaseForm", "CaseForm", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        try
                        {
                            ICORE_CASE_FORMS_DETAILS UpdateEntity = context.ICORE_CASE_FORMS_DETAILS.Where(m => m.CaseForm_Field_Id == CaseForm_Field_Id).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                if (UpdateEntity.CaseForm_Field_MakerId != Session["USER_ID"].ToString())
                                {
                                    UpdateEntity.CaseForm_Field_IsAuth = true;
                                    UpdateEntity.CaseForm_Field_CheckerId = Session["USER_ID"].ToString();
                                    UpdateEntity.CaseForm_Field_DataEditDateTime = DateTime.Now;
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        messeage = "Form Field : " + UpdateEntity.CaseForm_Field_Name + " is successfully Authorized";
                                    }
                                }
                                else
                                {
                                    messeage = "Maker cannot authorize the same record.";
                                    return Json(messeage);
                                }
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + CaseForm_Field_Id;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("Icore_CaseForm", "AuthorizeCaseFormDetail", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult DeleteFromAuthorizeCaseFormDetail(int CaseForm_Field_Id)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_CaseForm", "CaseForm", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        int RowCount = 0;
                        try
                        {
                            ICORE_CASE_FORMS_DETAILS UpdateEntity = context.ICORE_CASE_FORMS_DETAILS.Where(m => m.CaseForm_Field_Id == CaseForm_Field_Id).FirstOrDefault();
                            if (UpdateEntity != null && UpdateEntity.CaseForm_Field_IsAuth == false)
                            {
                                if (UpdateEntity.CaseForm_Field_MakerId != Session["USER_ID"].ToString())
                                {
                                    context.ICORE_CASE_FORMS_DETAILS.Remove(UpdateEntity);
                                    RowCount = +context.SaveChanges();
                                    ICORE_FIELDS_LOOKUP_DATA DrpData = context.ICORE_FIELDS_LOOKUP_DATA.Where(m => m.Lookup_Field_Id == CaseForm_Field_Id).FirstOrDefault();
                                    if (DrpData != null)
                                    {
                                        RowCount = 0;
                                        context.ICORE_FIELDS_LOOKUP_DATA.Remove(DrpData);
                                        RowCount += context.SaveChanges();
                                    }
                                    if (RowCount > 0)
                                    {
                                        messeage = "Form Field : " + UpdateEntity.CaseForm_Field_Name + " is Rejected";
                                    }
                                }
                                else
                                {
                                    messeage = "Maker cannot reject the same record.";
                                    return Json(messeage);
                                }
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + CaseForm_Field_Id;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("Icore_CaseForm", "DeleteFromAuthorizeCaseFormDetail", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult DeleteFormDetailFieldData(int FieldId)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_CaseForm", "CaseForm", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string message = "";
                        int RowCount = 0;
                        try
                        {
                            ICORE_CASE_FORMS_DETAILS DetailEntity = context.ICORE_CASE_FORMS_DETAILS.Where(m => m.CaseForm_Field_Id == FieldId).FirstOrDefault();
                            if (DetailEntity != null)
                            {
                                context.ICORE_CASE_FORMS_DETAILS.Remove(DetailEntity);
                                RowCount = +context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    ICORE_FIELDS_LOOKUP_DATA DrpData = context.ICORE_FIELDS_LOOKUP_DATA.Where(m => m.Lookup_Field_Id == DetailEntity.CaseForm_Field_Id).FirstOrDefault();
                                    if (DrpData != null)
                                    {
                                        RowCount = 0;
                                        context.ICORE_FIELDS_LOOKUP_DATA.Remove(DrpData);
                                        RowCount += context.SaveChanges();
                                    }
                                    if (RowCount > 0)
                                    {
                                        message = "Successfully deleted Field : " + DetailEntity.CaseForm_Field_Name;
                                        return Json(message);
                                    }
                                }
                            }
                            else
                            {
                                message = "Error: Unable to Find Record on the Provided Id ";
                                return Json(message);
                            }
                            return Json(message);
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("Icore_CaseForm", "DeleteFormDetailFieldData", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(message);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        #endregion
        [HttpPost]
        public JsonResult GetDynamicMenu(string stateId)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_CaseForm", "CaseForm", Session["USER_ID"].ToString()))
                {
                    List<ICORE_CASE_TITLES> drp_menu = new List<ICORE_CASE_TITLES>();
                    int stateiD = Convert.ToInt32(stateId);

                    drp_menu = (context.ICORE_CASE_TITLES.Where(x => x.CaseTitle_CaseTypeId == stateiD && x.CaseTitle_Status == true && x.CaseTitle_IsAuth == true)).ToList();

                    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    string result = javaScriptSerializer.Serialize(drp_menu);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
    }
}