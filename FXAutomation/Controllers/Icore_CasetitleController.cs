﻿using FXAutomation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FXAutomation.Controllers
{
    public class Icore_CasetitleController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: Icore_Casetitle
        public ActionResult Casetitle(string title_id)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_Casetitle", "Casetitle", Session["USER_ID"].ToString()))
                {
                    int ID = 0;
                    if (title_id != null)
                        ID = Convert.ToInt32(DAL.Decrypt(title_id));
                    string messeage = "";
                    try
                    {
                        if (ID != 0)
                        {
                            ICORE_CASE_TITLES edit = context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Id == ID).FirstOrDefault();
                            if (edit != null)
                                return View(edit);
                            else
                            {
                                messeage = "Problem while getting your id " + ID;
                                return View();
                            }
                        }
                        else
                            return View();
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_Casetitle", "Casetitle", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        List<SelectListItem> CaseType = new SelectList(context.ICORE_CASE_TYPES.Where(m => m.CaseType_Status == true && m.CaseType_IsAuth == true).ToList(), "CaseType_Id", "CaseType_Name").ToList();
                        CaseType.Insert(0, (new SelectListItem { Text = "--Select Case Type--", Value = "0" }));
                        ViewBag.CaseTitle_CaseTypeId = CaseType;
                        ViewBag.messeage = messeage;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }                
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult Casetitle(ICORE_CASE_TITLES dbtable)
        {
            string messeage = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_Casetitle", "Casetitle", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        if (dbtable.CaseTitle_Id != 0)
                        {
                            ICORE_CASE_TITLES update_casetitle = context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Id == dbtable.CaseTitle_Id).FirstOrDefault();
                            if (update_casetitle != null)
                            {
                                update_casetitle.CaseTitle_Code = dbtable.CaseTitle_Code;
                                update_casetitle.CaseTitle_Name = dbtable.CaseTitle_Name;
                                update_casetitle.CaseTitle_Description = dbtable.CaseTitle_Description;
                                update_casetitle.CaseTitle_Status = dbtable.CaseTitle_Status;
                                update_casetitle.CaseTitle_DataEditDateTime = DateTime.Now;
                                update_casetitle.CaseTitle_MakerId = Session["USER_ID"].ToString();
                                if (update_casetitle.CaseTitle_MakerId == "Admin123")
                                {
                                    update_casetitle.CaseTitle_CheckerId = update_casetitle.CaseTitle_MakerId;
                                    update_casetitle.CaseTitle_IsAuth = true;
                                }
                                else
                                {
                                    update_casetitle.CaseTitle_CheckerId = null;
                                    update_casetitle.CaseTitle_IsAuth = false;
                                }
                                update_casetitle.CaseTitle_CaseTypeId = dbtable.CaseTitle_CaseTypeId;

                                context.Entry(update_casetitle).State = EntityState.Modified;
                            }
                            else
                            {
                                messeage = "Problem occur while getting previous record";
                            }
                        }
                        else
                        {
                            ICORE_CASE_TITLES check_casetitle = context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Name == dbtable.CaseTitle_Name || m.CaseTitle_Code == dbtable.CaseTitle_Code).FirstOrDefault();
                            if (check_casetitle == null)
                            {
                                ICORE_CASE_TITLES insert_casetitle = new ICORE_CASE_TITLES();
                                insert_casetitle.CaseTitle_Id = Convert.ToInt32(context.ICORE_CASE_TITLES.Max(m => (decimal?)m.CaseTitle_Id)) + 1;
                                insert_casetitle.CaseTitle_Code = dbtable.CaseTitle_Code;
                                insert_casetitle.CaseTitle_Name = dbtable.CaseTitle_Name;
                                insert_casetitle.CaseTitle_Description = dbtable.CaseTitle_Description;
                                insert_casetitle.CaseTitle_Status = dbtable.CaseTitle_Status;
                                insert_casetitle.CaseTitle_DataEntryDateTime = DateTime.Now;
                                insert_casetitle.CaseTitle_MakerId = Session["USER_ID"].ToString();
                                if (insert_casetitle.CaseTitle_MakerId == "Admin123")
                                {
                                    insert_casetitle.CaseTitle_CheckerId = insert_casetitle.CaseTitle_MakerId;
                                    insert_casetitle.CaseTitle_IsAuth = true;
                                }
                                else
                                {
                                    insert_casetitle.CaseTitle_CheckerId = null;
                                    insert_casetitle.CaseTitle_IsAuth = false;
                                }
                                insert_casetitle.CaseTitle_CaseTypeId = dbtable.CaseTitle_CaseTypeId;

                                context.ICORE_CASE_TITLES.Add(insert_casetitle);

                            }
                            else
                            {
                                if (check_casetitle.CaseTitle_Name == dbtable.CaseTitle_Name)
                                    messeage = "Sorry Case Title Name " + dbtable.CaseTitle_Name + " Alredy Exist";
                                else if (check_casetitle.CaseTitle_Code == dbtable.CaseTitle_Code)
                                    messeage = "Sorry Case Title Name " + dbtable.CaseTitle_Code + " Alredy Exist";
                            }
                        }
                        if (messeage == "")
                        {
                            int overall_msg = context.SaveChanges();
                            if (overall_msg > 0)
                            {
                                messeage = "Data Saved Sucessfully " + overall_msg + "  Row affected";
                                ModelState.Clear();
                            }
                            else
                            {
                                messeage = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_Casetitle", "Casetitle", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        List<SelectListItem> CaseType = new SelectList(context.ICORE_CASE_TYPES.Where(m => m.CaseType_Status == true && m.CaseType_IsAuth == true).ToList(), "CaseType_Id", "CaseType_Name").ToList();
                        CaseType.Insert(0, (new SelectListItem { Text = "--Select Case Type--", Value = "0" }));
                        ViewBag.CaseTitle_CaseTypeId = CaseType;
                        ViewBag.messeage = messeage;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public ActionResult Casetitle_grid()
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "dashboard");
            }
            else
            {
                if (DAL.CheckFunctionValidity("Icore_Casetitle", "Casetitle", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<ICORE_CASE_TITLES_VIEW> ViewData = context.ICORE_CASE_TITLES_VIEW.Where(m => m.CaseTitle_IsAuth == true || m.CaseTitle_MakerId != SessionUser).OrderByDescending(m => m.CaseTitle_Id).ToList();
                    return PartialView(ViewData);
                }
                else
                    return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeCaseTitle(int CaseTitle_Id)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_Casetitle", "Casetitle", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        try
                        {
                            ICORE_CASE_TITLES UpdateEntity = context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Id == CaseTitle_Id).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                if (UpdateEntity.CaseTitle_MakerId != Session["USER_ID"].ToString())
                                {
                                    UpdateEntity.CaseTitle_IsAuth = true;
                                    UpdateEntity.CaseTitle_CheckerId = Session["USER_ID"].ToString();
                                    UpdateEntity.CaseTitle_DataEditDateTime = DateTime.Now;
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        messeage = "Case Title : " + UpdateEntity.CaseTitle_Name + " is successfully Authorized";
                                    }
                                }
                                else
                                {
                                    messeage = "Maker cannot authorize the same record.";
                                    return Json(messeage);
                                }
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + CaseTitle_Id;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("Icore_Casetitle", "AuthorizeCaseTitle", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult DeleteFromAuthorizeCaseTitle(int CaseTitle_Id)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_Casetitle", "Casetitle", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        try
                        {
                            ICORE_CASE_TITLES UpdateEntity = context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Id == CaseTitle_Id).FirstOrDefault();
                            if (UpdateEntity != null && UpdateEntity.CaseTitle_IsAuth == false)
                            {
                                if (UpdateEntity.CaseTitle_MakerId != Session["USER_ID"].ToString())
                                {
                                    context.ICORE_CASE_TITLES.Remove(UpdateEntity);
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        messeage = "Case Title : " + UpdateEntity.CaseTitle_Name + " is Rejected";
                                    }
                                }
                                else
                                {
                                    messeage = "Maker cannot reject the same record.";
                                    return Json(messeage);
                                }
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + CaseTitle_Id;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("Icore_Casetitle", "DeleteFromAuthorizeCaseTitle", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
    }
}