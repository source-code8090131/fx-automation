﻿using FXAutomation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace FXAutomation.Controllers
{
    public class Icore_SubmittedCaseController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: Icore_SubmittedCase
        #region Ops Queue
        public ActionResult OpsQueue(int FID = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "OpsQueue", Session["USER_ID"].ToString()))
                {
                    string messeage = "";
                    try
                    {
                        if (FID != 0)
                        {
                            ICORE_SUBMIT_CASE_FORM_MASTER edit = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == FID).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                messeage = "Problem while getting your id " + FID;
                                return View();
                            }
                        }
                        else
                        {
                            ViewBag.messeage = messeage;
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_SubmittedCase", "OpsQueue", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        List<SelectListItem> Status = new SelectList(context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID != 10 && m.SCFS_ID != 11 && m.SCFS_ID != 12 && m.SCFS_ID != 14 && m.SCFS_ID != 15 && m.SCFS_ID != 16).Select(f => new {
                            f.SCFS_ID,
                            SCFS_NAME = f.SCFS_NAME + " -- " + f.SCFS_DESCRIPTION
                        }).ToList(), "SCFS_ID", "SCFS_NAME", 0).ToList();
                        Status.Insert(0, (new SelectListItem { Text = "--Select Status--", Value = "0" }));
                        ViewBag.SCFM_STATUS = Status;
                        List<SelectListItem> Queue = new SelectList(context.ICORE_CASE_QUEUE.Where(m => m.CQ_STATSUS == true).ToList(), "CQ_NAME", "CQ_NAME").ToList();
                        Queue.Insert(0, (new SelectListItem { Text = "--Select Queue--", Value = "0" }));
                        ViewBag.SCFM_QUEUE = Queue;
                        List<SelectListItem> Region = new SelectList(context.ICORE_REGIONS.Where(m => m.Reg_IsAuth == true && m.Reg_Status == true).ToList(), "Reg_Name", "Reg_Name").ToList();
                        Region.Insert(0, (new SelectListItem { Text = "--Select Region--", Value = "0" }));
                        ViewBag.SCFM_REGION = Region;
                        ViewBag.messeage = messeage;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult OpsQueue(ICORE_SUBMIT_CASE_FORM_MASTER db_table)
        {
            string messeage = "";
            string PreviousQueue = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "OpsQueue", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        if (db_table.SCFM_ID != 0 && db_table.SCFM_STATUS != "0")
                        {
                            if (db_table.SCFM_REGION != "0")
                            {
                                ICORE_SUBMIT_CASE_FORM_MASTER UpdateEntity = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == db_table.SCFM_ID).FirstOrDefault();
                                if (UpdateEntity != null)
                                {
                                    PreviousQueue = UpdateEntity.SCFM_QUEUE;
                                    DateTime CurrentDateTime = DateTime.Now;
                                    CurrentDateTime = CurrentDateTime.AddHours(5);
                                    UpdateEntity.SCFM_EDIT_BY = Session["USER_ID"].ToString();
                                    UpdateEntity.SCFM_COMMENT = db_table.SCFM_COMMENT;
                                    if (db_table.SCFM_STATUS == "9")
                                        UpdateEntity.SCFM_QUEUE = "Compliance Queue";
                                    else if (db_table.SCFM_STATUS == "13")
                                        UpdateEntity.SCFM_QUEUE = "Regulatory Head";
                                    else
                                        UpdateEntity.SCFM_QUEUE = db_table.SCFM_QUEUE;
                                    if (UpdateEntity.SCFM_STATUS != db_table.SCFM_STATUS)
                                    {
                                        if (db_table.SCFM_STATUS == "9" || db_table.SCFM_STATUS == "13")
                                            messeage += Emailing.OpsEmailWithoutClient(UpdateEntity.SCFM_ID, Convert.ToInt32(db_table.SCFM_STATUS), PreviousQueue, UpdateEntity.SCFM_QUEUE, db_table.SCFM_COMMENT);
                                        else
                                            messeage += Emailing.OpsEmail(UpdateEntity.SCFM_ID, Convert.ToInt32(db_table.SCFM_STATUS), PreviousQueue, UpdateEntity.SCFM_QUEUE, db_table.SCFM_COMMENT);
                                    }
                                    CaseLogs(db_table.SCFM_STATUS, db_table.SCFM_CASE_NO, CurrentDateTime,db_table.SCFM_REGION,UpdateEntity.SCFM_INSERTED_BY);
                                    UpdateEntity.SCFM_EDIT_DATETIME = CurrentDateTime;
                                    UpdateEntity.SCFM_REGION = db_table.SCFM_REGION;
                                    UpdateEntity.SCFM_STATUS = db_table.SCFM_STATUS;
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                }
                                else
                                {
                                    messeage = "Problem occur while getting previous record";
                                    return View();
                                }

                                int overall_msg = context.SaveChanges();
                                if (overall_msg > 0)
                                {
                                    if (db_table.SCFM_COMMENT != null || db_table.SCFM_COMMENT != "")
                                        AddCommentToMasterLog(db_table.SCFM_CASE_NO, db_table.SCFM_COMMENT);

                                    messeage += "Data Updated Sucessfully " + overall_msg + "  Row affected" +
                                    Environment.NewLine + " Case No : " + UpdateEntity.SCFM_CASE_NO + " is currently in " + UpdateEntity.SCFM_QUEUE;
                                    ModelState.Clear();
                                }
                                else
                                {
                                    messeage = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                                    return View();
                                }
                            }
                            else
                            {
                                messeage = "Select region to continue";
                                return View();
                            }
                        }
                        else
                        {
                            messeage = "Please Select Status" + Environment.NewLine;
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_SubmittedCase", "OpsQueue", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        List<SelectListItem> Status = new SelectList(context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID != 10 && m.SCFS_ID != 11 && m.SCFS_ID != 12 && m.SCFS_ID != 14 && m.SCFS_ID != 15 && m.SCFS_ID != 16).Select(f => new
                        {
                            f.SCFS_ID,
                            SCFS_NAME = f.SCFS_NAME + " -- " + f.SCFS_DESCRIPTION
                        }).ToList(), "SCFS_ID", "SCFS_NAME", 0).ToList();
                        Status.Insert(0, (new SelectListItem { Text = "--Select Status--", Value = "0" }));
                        ViewBag.SCFM_STATUS = Status;
                        List<SelectListItem> Queue = new SelectList(context.ICORE_CASE_QUEUE.Where(m => m.CQ_STATSUS == true).ToList(), "CQ_NAME", "CQ_NAME").ToList();
                        Queue.Insert(0, (new SelectListItem { Text = "--Select Queue--", Value = "0" }));
                        ViewBag.SCFM_QUEUE = Queue;
                        List<SelectListItem> Region = new SelectList(context.ICORE_REGIONS.Where(m => m.Reg_IsAuth == true && m.Reg_Status == true).ToList(), "Reg_Name", "Reg_Name").ToList();
                        Region.Insert(0, (new SelectListItem { Text = "--Select Region--", Value = "0" }));
                        ViewBag.SCFM_REGION = Region;
                        ViewBag.messeage = messeage;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        private void AddCommentToMasterLog(string CASE_NO, string COMMENT)
        {
            ICORE_SUBMIT_CASE_COMMENTS log = new ICORE_SUBMIT_CASE_COMMENTS();
            log.SCC_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_COMMENTS.Max(m => (int?)m.SCC_ID)) + 1;
            log.SCC_CASE_NO = CASE_NO;
            log.SCC_COMMENT = COMMENT;
            log.SCC_ADDED_BY = Session["USER_ID"].ToString();
            log.SCC_ADDED_DATE_TIME = DateTime.Now;
            log.SCC_IS_AD = true;
            context.ICORE_SUBMIT_CASE_COMMENTS.Add(log);
            context.SaveChanges();
        }

        private void CaseLogs(string Status, string CaseNo,DateTime StatusDate, string CaseRegion, string ClientEmail)
        {
            ICORE_OPS_LOG GetPrevLog = context.ICORE_OPS_LOG.Where(m => m.OL_CASE_NUMBER.Contains(CaseNo)).OrderByDescending(m => m.OL_ID).FirstOrDefault();
            if (GetPrevLog != null)
            {
                int Days = 0;
                string Ops = "";string Compliance = ""; string Regulatory = "";
                string UserRole = "";
                string SessionUser = Session["USER_ID"].ToString();
                string GetEventNo = GetPrevLog.OL_CASE_NUMBER.Split('-').Last();
                string CaseNumber = GetPrevLog.OL_CASE_NUMBER.Remove(GetPrevLog.OL_CASE_NUMBER.LastIndexOf('-'));
                int EventNo = Convert.ToInt32(GetEventNo) + 1;
                ICORE_OPS_LOG CaseLog = new ICORE_OPS_LOG();
                CaseLog.OL_ID = Convert.ToInt32(context.ICORE_OPS_LOG.Max(m => (int?)m.OL_ID)) + 1;
                CaseLog.OL_CASE_NUMBER = CaseNumber + "-" + EventNo;
                CaseLog.OL_SUBMITTED_DATE = GetPrevLog.OL_SUBMITTED_DATE;
                CaseLog.OL_DEPARTMENT = GetPrevLog.OL_DEPARTMENT;
                CaseLog.OL_CATEGORY = GetPrevLog.OL_CATEGORY;
                CaseLog.OL_CASE_TYPE = GetPrevLog.OL_CASE_TYPE;
                CaseLog.OL_CASE_TITLE = GetPrevLog.OL_CASE_TITLE;
                CaseLog.OL_CASE_TYPE = GetPrevLog.OL_CASE_TYPE;
                CaseLog.OL_CASE_FORM = GetPrevLog.OL_CASE_FORM;
                CaseLog.OL_REGION = CaseRegion;
                CaseLog.OL_STATUS_DATE = StatusDate;
                if (GetPrevLog.OL_STATUS_DATE != null && CaseLog.OL_STATUS_DATE != null)
                {
                    DateTime Prev = (DateTime)GetPrevLog.OL_STATUS_DATE;
                    DateTime Current = (DateTime)CaseLog.OL_STATUS_DATE;
                    int NoOfDays = (Current - Prev).Days;
                    Days = NoOfDays;
                    ICORE_OPS_LOG Update = GetPrevLog;
                    Update.OL_AGEING = NoOfDays;
                    context.Entry(Update).State = EntityState.Modified;
                }
                CaseLog.OL_AGEING = 0;
                //Get Status Name from Status ID
                int SubmitStatus = Convert.ToInt32(Status);
                ICORE_SUBMIT_CASE_STATUS Case_Status = context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID == SubmitStatus).FirstOrDefault();
                ///End
                CaseLog.OL_CASE_STATUS = Case_Status.SCFS_NAME;
                CaseLog.OL_LOG_STATUS = Case_Status.SCFS_DESCRIPTION;
                CaseLog.OL_CUSTOMER = GetPrevLog.OL_CUSTOMER;
                CaseLog.OL_AMOUNT = GetPrevLog.OL_AMOUNT;
                CaseLog.OL_CURRENCY = GetPrevLog.OL_CURRENCY;
                CaseLog.OL_USER_ID = GetPrevLog.OL_USER_ID;
                CaseLog.OL_M_FORM_NUMBER = GetPrevLog.OL_M_FORM_NUMBER;
                CaseLog.OL_CUSTOMER_EMAIL = ClientEmail;
                CaseLog.OL_UPDATE_BY = Session["USER_ID"].ToString();
                ICORE_USER_WITH_ROLES_LIST_VIEW GetUserRole = context.ICORE_USER_WITH_ROLES_LIST_VIEW.Where(m => m.LOG_USER_ID == SessionUser).FirstOrDefault();
                if (GetUserRole != null)
                {
                    string[] UserRoles = GetUserRole.ASSIGNED_ROLES.Split(',');
                    foreach (string roles in UserRoles)
                    {
                        List<ICORE_ROLE_WITH_FUNCTIONS_LIST_VIEW> GetAllFunction = context.ICORE_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => roles.Contains(m.R_NAME)).ToList();
                        if (GetAllFunction.Count > 0)
                        {
                            foreach (ICORE_ROLE_WITH_FUNCTIONS_LIST_VIEW items in GetAllFunction)
                            {
                                string[] Userfunction = items.ASSIGNED_FUNCTION.Split(',');
                                foreach (string function in Userfunction)
                                {
                                    if (function.Contains("Ops Queue"))
                                        Ops = function + ",";
                                    if (function.Contains("Compliance Queue"))
                                        Compliance = function + ",";
                                    if (function.Contains("Regulatory Head"))
                                        Regulatory = function + ",";
                                }
                            }
                        }
                    }
                    UserRole = Ops + Compliance + Regulatory;
                    UserRole = UserRole.Substring(0, UserRole.Length - 1) + ".";
                    CaseLog.OL_USER_ROLE = UserRole;
                }
                else
                    CaseLog.OL_USER_ROLE = "";
                CaseLog.OL_ENTRY_DATETIME = DateTime.Now;
                context.ICORE_OPS_LOG.Add(CaseLog);
                context.SaveChanges();
            }
        }
        [ChildActionOnly]
        public ActionResult OpsQueueView()
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "dashboard");
            }
            else
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "OpsQueue", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW> ViewData = new List<ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW>();
                    if (SessionUser == "Admin123")
                    {
                        ViewData = context.ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW.Where(m => m.SCFM_QUEUE == "Ops Queue").OrderByDescending(m => m.SCFM_ID).ToList();
                    }
                    else
                    {
                        List<int?> AssignedRoles = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_USER_ID == SessionUser && m.ALR_STATUS == true).Select(m => m.ALR_ROLE_ID).ToList();
                        AssignedRoles = context.ICORE_ASSIGN_FUNCTIONS.Where(m => AssignedRoles.Contains(m.AF_ROLE_ID) && m.AF_STATUS == true && m.AF_FUNCTION_ID == 12).Select(m => m.AF_ROLE_ID).Distinct().ToList();
                        AssignedRoles = context.ICORE_ROLES.Where(m => AssignedRoles.Contains(m.R_ID) && m.R_ISAUTH == true && m.R_STATUS == "true").Select(m => (int?)m.R_ID).Distinct().ToList();
                        List<int?> AssignedCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => AssignedRoles.Contains(m.ACT_ROLE_ID) && m.ACT_STATUS == true).Select(m => m.ACT_TITLE_ID).Distinct().ToList();
                        List<decimal> FomIdsToDisplay = context.ICORE_CASE_FORMS_MASTER.Where(m => AssignedCaseTitles.Contains((int)m.CaseForm_CaseTitle_Id)).Select(m => m.CaseForm_Id).ToList();
                        ViewData = context.ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW.Where(m => m.SCFM_QUEUE == "Ops Queue" && FomIdsToDisplay.Contains((decimal)m.SCFM_CASE_FORM_ID)).OrderByDescending(m => m.SCFM_ID).ToList();
                    }
                    return PartialView(ViewData);
                }
                else
                    return PartialView(null);
            }
        }
        #endregion
        
        #region Compliance Queue
        public ActionResult ComplianceQueue(int FID = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "ComplianceQueue", Session["USER_ID"].ToString()))
                {
                    string messeage = "";
                    try
                    {
                        if (FID != 0)
                        {
                            ICORE_SUBMIT_CASE_FORM_MASTER edit = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == FID).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                messeage = "Problem while getting your id " + FID;
                                return View();
                            }
                        }
                        else
                        {
                            ViewBag.messeage = messeage;
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_SubmittedCase", "ComplianceQueue", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        List<SelectListItem> Status = new SelectList(context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID != 9 && m.SCFS_ID != 13 && m.SCFS_ID != 14 && m.SCFS_ID != 15 && m.SCFS_ID != 16).Select(f => new {
                            f.SCFS_ID,
                            SCFS_NAME = f.SCFS_NAME + " -- " + f.SCFS_DESCRIPTION
                        }).ToList(), "SCFS_ID", "SCFS_NAME", 0).ToList();
                        Status.Insert(0, (new SelectListItem { Text = "--Select Status--", Value = "0" }));
                        ViewBag.SCFM_STATUS = Status;
                        List<SelectListItem> Queue = new SelectList(context.ICORE_CASE_QUEUE.Where(m => m.CQ_STATSUS == true).ToList(), "CQ_NAME", "CQ_NAME").ToList();
                        Queue.Insert(0, (new SelectListItem { Text = "--Select Queue--", Value = "0" }));
                        ViewBag.SCFM_QUEUE = Queue;
                        ViewBag.messeage = messeage;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult ComplianceQueue(ICORE_SUBMIT_CASE_FORM_MASTER db_table)
        {
            string messeage = "";
            string PreviousQueue = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "ComplianceQueue", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        if (db_table.SCFM_ID != 0 && db_table.SCFM_STATUS != "0")
                        {
                            ICORE_SUBMIT_CASE_FORM_MASTER UpdateEntity = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == db_table.SCFM_ID).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                PreviousQueue = UpdateEntity.SCFM_QUEUE;
                                DateTime CurrentDateTime = DateTime.Now;
                                CurrentDateTime = CurrentDateTime.AddHours(5);
                                UpdateEntity.SCFM_EDIT_BY = Session["USER_ID"].ToString();
                                UpdateEntity.SCFM_COMMENT = db_table.SCFM_COMMENT;
                                if (db_table.SCFM_STATUS == "10" || db_table.SCFM_STATUS == "11" || db_table.SCFM_STATUS == "12")
                                    UpdateEntity.SCFM_QUEUE = "Ops Queue";
                                else
                                    UpdateEntity.SCFM_QUEUE = db_table.SCFM_QUEUE;
                                if (UpdateEntity.SCFM_STATUS != db_table.SCFM_STATUS)
                                {
                                    if (db_table.SCFM_STATUS == "10" || db_table.SCFM_STATUS == "11" || db_table.SCFM_STATUS == "12")
                                        messeage += Emailing.ComplianceEmailWithoutClient(UpdateEntity.SCFM_ID, Convert.ToInt32(db_table.SCFM_STATUS), PreviousQueue, UpdateEntity.SCFM_QUEUE, db_table.SCFM_COMMENT);
                                    else
                                        messeage += Emailing.ComplianceEmail(UpdateEntity.SCFM_ID, Convert.ToInt32(db_table.SCFM_STATUS), PreviousQueue, UpdateEntity.SCFM_QUEUE, db_table.SCFM_COMMENT);
                                }
                                CaseLogs(db_table.SCFM_STATUS, db_table.SCFM_CASE_NO, CurrentDateTime, UpdateEntity.SCFM_REGION, UpdateEntity.SCFM_INSERTED_BY);
                                UpdateEntity.SCFM_STATUS = db_table.SCFM_STATUS;
                                UpdateEntity.SCFM_EDIT_DATETIME = DateTime.Now;
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                            }
                            else
                            {
                                messeage = "Problem occur while getting previous record" + Environment.NewLine;
                                return View();
                            }

                            int overall_msg = context.SaveChanges();
                            if (overall_msg > 0)
                            {
                                if (db_table.SCFM_COMMENT != null || db_table.SCFM_COMMENT != "")
                                    AddCommentToMasterLog(db_table.SCFM_CASE_NO, db_table.SCFM_COMMENT);

                                messeage += "Data Updated Sucessfully " + overall_msg + "  Row affected" +
                                    Environment.NewLine + " Case No : " + UpdateEntity.SCFM_CASE_NO + " is currently in " + UpdateEntity.SCFM_QUEUE;
                                ModelState.Clear();
                            }
                            else
                            {
                                messeage = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                                return View();
                            }
                        }
                        else
                        {
                            messeage = "Please Select Status" + Environment.NewLine;
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_SubmittedCase", "ComplianceQueue", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        List<SelectListItem> Status = new SelectList(context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID != 9 && m.SCFS_ID != 13 && m.SCFS_ID != 14 && m.SCFS_ID != 15 && m.SCFS_ID != 16).Select(f => new {
                            f.SCFS_ID,
                            SCFS_NAME = f.SCFS_NAME + " -- " + f.SCFS_DESCRIPTION
                        }).ToList(), "SCFS_ID", "SCFS_NAME", 0).ToList();
                        Status.Insert(0, (new SelectListItem { Text = "--Select Status--", Value = "0" }));
                        ViewBag.SCFM_STATUS = Status;
                        List<SelectListItem> Queue = new SelectList(context.ICORE_CASE_QUEUE.Where(m => m.CQ_STATSUS == true).ToList(), "CQ_NAME", "CQ_NAME").ToList();
                        Queue.Insert(0, (new SelectListItem { Text = "--Select Queue--", Value = "0" }));
                        ViewBag.SCFM_QUEUE = Queue;
                        ViewBag.messeage = messeage;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public ActionResult ComplianceQueueView()
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "dashboard");
            }
            else
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "ComplianceQueue", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW> ViewData = new List<ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW>();
                    if (SessionUser == "Admin123")
                    {
                        ViewData = context.ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW.Where(m => m.SCFM_QUEUE == "Compliance Queue").OrderByDescending(m => m.SCFM_ID).ToList();
                    }
                    else
                    {
                        List<int?> AssignedRoles = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_USER_ID == SessionUser && m.ALR_STATUS == true).Select(m => m.ALR_ROLE_ID).ToList();
                        AssignedRoles = context.ICORE_ASSIGN_FUNCTIONS.Where(m => AssignedRoles.Contains(m.AF_ROLE_ID) && m.AF_STATUS == true && m.AF_FUNCTION_ID == 14).Select(m => m.AF_ROLE_ID).Distinct().ToList();
                        AssignedRoles = context.ICORE_ROLES.Where(m => AssignedRoles.Contains(m.R_ID) && m.R_ISAUTH == true && m.R_STATUS == "true").Select(m => (int?)m.R_ID).Distinct().ToList();
                        List<int?> AssignedCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => AssignedRoles.Contains(m.ACT_ROLE_ID) && m.ACT_STATUS == true).Select(m => m.ACT_TITLE_ID).Distinct().ToList();
                        List<decimal> FomIdsToDisplay = context.ICORE_CASE_FORMS_MASTER.Where(m => AssignedCaseTitles.Contains((int)m.CaseForm_CaseTitle_Id)).Select(m => m.CaseForm_Id).ToList();
                        ViewData = context.ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW.Where(m => m.SCFM_QUEUE == "Compliance Queue" && FomIdsToDisplay.Contains((decimal)m.SCFM_CASE_FORM_ID)).OrderByDescending(m => m.SCFM_ID).ToList();
                    }
                    return PartialView(ViewData);
                }
                else
                    return PartialView(null);
            }
        }
        #endregion

        #region Regulatory Head
        public ActionResult RegulatoryHead(int FID = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "RegulatoryHead", Session["USER_ID"].ToString()))
                {
                    string messeage = "";
                    try
                    {
                        if (FID != 0)
                        {
                            ICORE_SUBMIT_CASE_FORM_MASTER edit = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == FID).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                messeage = "Problem while getting your id " + FID;
                                return View();
                            }
                        }
                        else
                        {
                            ViewBag.messeage = messeage;
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_SubmittedCase", "RegulatoryHead", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        List<SelectListItem> Status = new SelectList(context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID != 9 && m.SCFS_ID != 10 && m.SCFS_ID != 11 && m.SCFS_ID != 12 && m.SCFS_ID != 13).Select(f => new {
                            f.SCFS_ID,
                            SCFS_NAME = f.SCFS_NAME + " -- " + f.SCFS_DESCRIPTION
                        }).ToList(), "SCFS_ID", "SCFS_NAME", 0).ToList();
                        Status.Insert(0, (new SelectListItem { Text = "--Select Status--", Value = "0" }));
                        ViewBag.SCFM_STATUS = Status;
                        List<SelectListItem> Queue = new SelectList(context.ICORE_CASE_QUEUE.Where(m => m.CQ_STATSUS == true).ToList(), "CQ_NAME", "CQ_NAME").ToList();
                        Queue.Insert(0, (new SelectListItem { Text = "--Select Queue--", Value = "0" }));
                        ViewBag.SCFM_QUEUE = Queue;
                        ViewBag.messeage = messeage;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult RegulatoryHead(ICORE_SUBMIT_CASE_FORM_MASTER db_table)
        {
            string messeage = "";
            string PreviousQueue = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "RegulatoryHead", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        if (db_table.SCFM_ID != 0 && db_table.SCFM_STATUS != "0")
                        {
                            ICORE_SUBMIT_CASE_FORM_MASTER UpdateEntity = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == db_table.SCFM_ID).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                PreviousQueue = UpdateEntity.SCFM_QUEUE;
                                DateTime CurrentDateTime = DateTime.Now;
                                CurrentDateTime = CurrentDateTime.AddHours(5);
                                UpdateEntity.SCFM_EDIT_BY = Session["USER_ID"].ToString();
                                UpdateEntity.SCFM_COMMENT = db_table.SCFM_COMMENT;
                                if (db_table.SCFM_STATUS == "14")
                                    UpdateEntity.SCFM_QUEUE = "Compliance Queue";
                                else if (db_table.SCFM_STATUS == "15" || db_table.SCFM_STATUS == "16")
                                    UpdateEntity.SCFM_QUEUE = "Ops Queue";
                                else
                                    UpdateEntity.SCFM_QUEUE = db_table.SCFM_QUEUE;
                                if (UpdateEntity.SCFM_STATUS != db_table.SCFM_STATUS)
                                {
                                    if (db_table.SCFM_STATUS == "14" || db_table.SCFM_STATUS == "15" || db_table.SCFM_STATUS == "16")
                                        messeage += Emailing.RegulatoryEmailWithoutClient(UpdateEntity.SCFM_ID, Convert.ToInt32(db_table.SCFM_STATUS), PreviousQueue, UpdateEntity.SCFM_QUEUE, db_table.SCFM_COMMENT);
                                    else
                                        messeage += Emailing.RegulatoryEmail(UpdateEntity.SCFM_ID, Convert.ToInt32(db_table.SCFM_STATUS), PreviousQueue, UpdateEntity.SCFM_QUEUE, db_table.SCFM_COMMENT);
                                }
                                CaseLogs(db_table.SCFM_STATUS, db_table.SCFM_CASE_NO, CurrentDateTime, UpdateEntity.SCFM_REGION, UpdateEntity.SCFM_INSERTED_BY);
                                UpdateEntity.SCFM_STATUS = db_table.SCFM_STATUS;
                                UpdateEntity.SCFM_EDIT_DATETIME = DateTime.Now;
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                            }
                            else
                            {
                                messeage = "Problem occur while getting previous record" + Environment.NewLine;
                                return View();
                            }

                            int overall_msg = context.SaveChanges();
                            if (overall_msg > 0)
                            {
                                if (db_table.SCFM_COMMENT != null || db_table.SCFM_COMMENT != "")
                                    AddCommentToMasterLog(db_table.SCFM_CASE_NO, db_table.SCFM_COMMENT);

                                messeage += "Data Updated Sucessfully " + overall_msg + "  Row affected" +
                                    Environment.NewLine + " Case No : " + UpdateEntity.SCFM_CASE_NO + " is currently in " + UpdateEntity.SCFM_QUEUE;
                                ModelState.Clear();
                            }
                            else
                            {
                                messeage = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                                return View();
                            }
                        }
                        else
                        {
                            messeage = "Please Select Status" + Environment.NewLine;
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_SubmittedCase", "RegulatoryHead", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        List<SelectListItem> Status = new SelectList(context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID != 9 && m.SCFS_ID != 10 && m.SCFS_ID != 11 && m.SCFS_ID != 12 && m.SCFS_ID != 13).Select(f => new {
                            f.SCFS_ID,
                            SCFS_NAME = f.SCFS_NAME + " -- " + f.SCFS_DESCRIPTION
                        }).ToList(), "SCFS_ID", "SCFS_NAME", 0).ToList();
                        Status.Insert(0, (new SelectListItem { Text = "--Select Status--", Value = "0" }));
                        ViewBag.SCFM_STATUS = Status;
                        List<SelectListItem> Queue = new SelectList(context.ICORE_CASE_QUEUE.Where(m => m.CQ_STATSUS == true).ToList(), "CQ_NAME", "CQ_NAME").ToList();
                        Queue.Insert(0, (new SelectListItem { Text = "--Select Queue--", Value = "0" }));
                        ViewBag.SCFM_QUEUE = Queue;
                        ViewBag.messeage = messeage;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public ActionResult RegulatoryHeadView()
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "dashboard");
            }
            else
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "RegulatoryHead", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW> ViewData = new List<ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW>();
                    if (SessionUser == "Admin123")
                    {
                        ViewData = context.ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW.Where(m => m.SCFM_QUEUE == "Regulatory Head").OrderByDescending(m => m.SCFM_ID).ToList();
                    }
                    else
                    {
                        List<int?> AssignedRoles = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_USER_ID == SessionUser && m.ALR_STATUS == true).Select(m => m.ALR_ROLE_ID).ToList();
                        AssignedRoles = context.ICORE_ASSIGN_FUNCTIONS.Where(m => AssignedRoles.Contains(m.AF_ROLE_ID) && m.AF_STATUS == true && m.AF_FUNCTION_ID == 19).Select(m => m.AF_ROLE_ID).Distinct().ToList();
                        AssignedRoles = context.ICORE_ROLES.Where(m => AssignedRoles.Contains(m.R_ID) && m.R_ISAUTH == true && m.R_STATUS == "true").Select(m => (int?)m.R_ID).Distinct().ToList();
                        List<int?> AssignedCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => AssignedRoles.Contains(m.ACT_ROLE_ID) && m.ACT_STATUS == true).Select(m => m.ACT_TITLE_ID).Distinct().ToList();
                        List<decimal> FomIdsToDisplay = context.ICORE_CASE_FORMS_MASTER.Where(m => AssignedCaseTitles.Contains((int)m.CaseForm_CaseTitle_Id)).Select(m => m.CaseForm_Id).ToList();
                        ViewData = context.ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW.Where(m => m.SCFM_QUEUE == "Regulatory Head" && FomIdsToDisplay.Contains((decimal)m.SCFM_CASE_FORM_ID)).OrderByDescending(m => m.SCFM_ID).ToList();
                    }
                    return PartialView(ViewData);
                }
                else
                    return PartialView(null);
            }
        }
        #endregion

        #region Queue Cases Report
        public ActionResult QueueCases()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "QueueCases", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Icore_SubmittedCase", "QueueCases", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult QueueCases(DateTime? FromDate, DateTime? EndDate, string CaseNumber)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "QueueCases", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && EndDate != null)
                        {
                            var Data = context.QueueCasesReport(FromDate, EndDate, CaseNumber).OrderByDescending(m => m.OL_STATUS_DATE).ToList();
                            if (Data.Count() > 0)
                            {
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + FromDate + " End date : " + EndDate;
                            }
                        }
                        else
                        {
                            if (FromDate == null && EndDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (EndDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Icore_SubmittedCase", "QueueCases", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        #endregion

        [HttpPost]
        public ActionResult AddDocumentComments(string DocElementId, string comment)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "OpsQueue", Session["USER_ID"].ToString()) || DAL.CheckFunctionValidity("Icore_SubmittedCase", "ComplianceQueue", Session["USER_ID"].ToString()) || DAL.CheckFunctionValidity("Icore_SubmittedCase", "RegulatoryHead", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string SessionUser = Session["USER_ID"].ToString();
                        if (DAL.CheckFormQueue(DocElementId, SessionUser))
                        {
                            string message = "";
                            int DocId = 0;
                            bool IsValid = int.TryParse(DocElementId, out DocId);
                            if (IsValid)
                            {
                                if (comment != null && comment != "" && comment.Length <= 999)
                                {
                                    comment = RemoveSpecialCharacters(comment);
                                    ICORE_SUBMIT_CASE_DOCUMENT UpdateEntity = context.ICORE_SUBMIT_CASE_DOCUMENT.Where(m => m.SCD_ID == DocId).FirstOrDefault();
                                    if (UpdateEntity != null)
                                    {
                                        UpdateEntity.SCD_COMMENT = comment;
                                        context.Entry(UpdateEntity).State = EntityState.Modified;
                                    }
                                    else
                                        return Json("Problem while updating Documents");

                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        RowCount = 0;
                                        #region Add Comments To Comments Log
                                        ICORE_COMMENT_DETAIL_LOG LogComment = new ICORE_COMMENT_DETAIL_LOG();
                                        ICORE_CASE_DOCUMENTS Doc = context.ICORE_CASE_DOCUMENTS.Where(m => m.Case_Doc_Id == UpdateEntity.SCD_DOCUMENT_ID).FirstOrDefault();
                                        ICORE_SUBMIT_CASE_FORM_MASTER Master = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == UpdateEntity.SCD_FORM_ID).FirstOrDefault();
                                        if (Master != null)
                                        {
                                            LogComment.CDL_ID = Convert.ToInt32(context.ICORE_COMMENT_DETAIL_LOG.Max(m => (int?)m.CDL_ID)) + 1;
                                            LogComment.CDL_FORM_ID = Master.SCFM_CASE_FORM_ID;
                                            LogComment.CDL_CASE_NO = Master.SCFM_CASE_NO;
                                            LogComment.CDL_CLIENT_NAME = Master.SCFM_INSERTED_BY;
                                            LogComment.CDL_DOCUMENT_NAME = Doc.Case_Doc_Name;
                                            LogComment.CDL_COMMENTS = comment;
                                            LogComment.CDL_ADDED_BY = Session["USER_ID"].ToString();
                                            LogComment.CDL_ENTRY_DATETIME = DateTime.Now;
                                            LogComment.CDL_IS_AD = true;
                                            context.ICORE_COMMENT_DETAIL_LOG.Add(LogComment);
                                            RowCount = context.SaveChanges();
                                            if (RowCount > 0)
                                            {
                                                message = "Comment Added Successfully";
                                                return Json(message);
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                else
                                {
                                    if (comment == "" || comment == null)
                                        message = "Enter comment for the document";
                                    else
                                        message = "The maximum length for comment is 999";
                                    return Json(message);
                                }
                            }
                            else
                                message = "Invalid Document id Provided";
                            return Json(message);
                        }
                        else
                            return Json("Invalid Result");
                    }
                    else
                        return Json("Invalid request");
                }
                else
                    return Json("Invalid Result");
            }
            else
                return Json("Invalid request");
        }
        public static string RemoveSpecialCharacters(string str)
        {
            return System.Text.RegularExpressions.Regex.Replace(str, "[^a-zA-Z0-9,.:?/'()$-]+", "", System.Text.RegularExpressions.RegexOptions.Compiled);
        }
        public PartialViewResult LoadPreviousMasterCaseComments(string CaseNo)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "OpsQueue", Session["USER_ID"].ToString()) || DAL.CheckFunctionValidity("Icore_SubmittedCase", "ComplianceQueue", Session["USER_ID"].ToString()) || DAL.CheckFunctionValidity("Icore_SubmittedCase", "RegulatoryHead", Session["USER_ID"].ToString()))
                {
                    List<ICORE_SUBMIT_CASE_COMMENTS> ViewData = context.ICORE_SUBMIT_CASE_COMMENTS.Where(m => m.SCC_CASE_NO == CaseNo).OrderByDescending(m => m.SCC_ID).ToList();
                    return PartialView(ViewData);
                }
                else
                {
                    return PartialView(null);
                }
            }
            else
            {
                return PartialView();
            }
        }

        #region Documeent Histroy Report 
        public ActionResult CommentsHistory()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "CommentsHistory", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                    CommentHistory Entity = new CommentHistory();
                    Entity.Master = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_IsAuth == true && m.CaseForm_Status == true).ToList();
                    Entity.CommentLog = new List<ICORE_COMMENT_DETAIL_LOG>();
                    return View(Entity);
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Icore_SubmittedCase", "CommentsHistory", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult CommentsHistory(int Master = 0, string CaseNumber = null)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_SubmittedCase", "CommentsHistory", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        ICORE_CASE_FORMS_MASTER master = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == Master).FirstOrDefault();
                        CommentHistory Entity = new CommentHistory();
                        Entity.Master = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_IsAuth == true && m.CaseForm_Status == true).ToList();
                        Entity.CommentLog = new List<ICORE_COMMENT_DETAIL_LOG>();
                        if (Master != 0 && CaseNumber != "")
                            Entity.CommentLog = context.ICORE_COMMENT_DETAIL_LOG.Where(m => m.CDL_FORM_ID == Master && m.CDL_CASE_NO == CaseNumber).ToList().OrderBy(m => m.CDL_DOCUMENT_NAME);
                        else if (Master == 0 && CaseNumber != "")
                            Entity.CommentLog = context.ICORE_COMMENT_DETAIL_LOG.Where(m => m.CDL_CASE_NO == CaseNumber).OrderBy(m => m.CDL_DOCUMENT_NAME);
                        else if (Master != 0 && CaseNumber == "")
                            Entity.CommentLog = context.ICORE_COMMENT_DETAIL_LOG.Where(m => m.CDL_FORM_ID == Master).OrderBy(m => m.CDL_DOCUMENT_NAME);
                        else
                        {
                            if (Master == 0 && CaseNumber == "")
                            {
                                message = "Select Form Name or enter Case Number to continue";
                                return View(Entity);
                            }
                        }
                        if (Entity.CommentLog.Count() > 0)
                            return View(Entity);
                        else
                        {
                            message = "No data found";
                            return View(Entity);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Icore_SubmittedCase", "CommentsHistory", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        #endregion
    }
}