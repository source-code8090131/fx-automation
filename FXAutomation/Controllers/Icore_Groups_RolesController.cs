﻿using FXAutomation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FXAutomation.Controllers
{
    public class Icore_Groups_RolesController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: Icore_Groups_Roles
        public ActionResult Index(int R = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_Groups_Roles", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (R != 0)
                        {
                            AssignFunctions_Custom entity = new AssignFunctions_Custom();
                            entity.Entity = context.ICORE_ROLES.Where(m => m.R_ID == R).FirstOrDefault();
                            if (entity.Entity != null)
                            {
                                List<int?> PreviousFunctionIds = context.ICORE_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == R && m.AF_STATUS == true).Select(m => m.AF_FUNCTION_ID).ToList();
                                List<SelectListItem> items = new SelectList(context.ICORE_FUNCTIONS.OrderBy(m => m.F_PARENT_ID).ToList(), "F_ID", "F_NAME", 0).ToList();
                                List<int?> PreviousCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_ROLE_ID == entity.Entity.R_ID && m.ACT_STATUS == true).Select(m => m.ACT_TITLE_ID).ToList();
                                List<SelectListItem> CaseTitleItem = new SelectList(context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Status == true && m.CaseTitle_IsAuth == true).OrderBy(m => m.CaseTitle_Id).Select(f => new {
                                    f.CaseTitle_Id,
                                    CaseTitle_Name = f.CaseTitle_Name + " -- " + f.CaseTitle_Code
                                }).ToList(), "CaseTitle_Id", "CaseTitle_Name", 0).ToList();
                                foreach (SelectListItem item in items)
                                {
                                    int? CurrentFId = Convert.ToInt32(item.Value);
                                    if (PreviousFunctionIds.Contains(CurrentFId))
                                        item.Selected = true;
                                }
                                foreach (SelectListItem item in CaseTitleItem)
                                {
                                    int? CurrentTId = Convert.ToInt32(item.Value);
                                    if (PreviousCaseTitles.Contains(CurrentTId))
                                        item.Selected = true;
                                }
                                entity.FunctionsList = items;
                                entity.CaseTitlesList = CaseTitleItem;
                                return View(entity);
                            }
                            else
                            {
                                message = "Exception Occur while fetching your record on User Id = " + R;
                                return View();
                            }
                        }
                        else
                        {
                            AssignFunctions_Custom entity = new AssignFunctions_Custom();
                            entity.Entity = new ICORE_ROLES();
                            entity.Entity.R_ID = 0;
                            List<SelectListItem> items = new SelectList(context.ICORE_FUNCTIONS.OrderBy(m => m.F_PARENT_ID).ToList(), "F_ID", "F_NAME", 0).ToList();
                            entity.FunctionsList = items;
                            entity.CaseTitileEntity = new ICORE_ASSIGN_CASE_TITLES();
                            entity.CaseTitileEntity.ACT_ID = 0;
                            List<SelectListItem> CaseTitleItem = new SelectList(context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Status == true && m.CaseTitle_IsAuth == true).OrderBy(m => m.CaseTitle_Id).Select(f => new {
                                f.CaseTitle_Id,
                                CaseTitle_Name = f.CaseTitle_Name + " -- " + f.CaseTitle_Code
                            }).ToList(), "CaseTitle_Id", "CaseTitle_Name", 0).ToList();
                            entity.CaseTitlesList = CaseTitleItem;
                            return View(entity);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Icore_Groups_Roles", "Index", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Index(AssignFunctions_Custom db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_Groups_Roles", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        ICORE_ROLES MasterEntityToUpdate = new ICORE_ROLES();
                        if (db_table.SelectedFunctions.Count() > 0)
                        {
                            bool QueSelected = false;
                            List<int?> list = new List<int?>() { 12, 14, 19 };
                            foreach (int function in db_table.SelectedFunctions)
                            {
                                if (list.Contains(function))
                                {
                                    QueSelected = true;
                                }
                            }
                            if (QueSelected == false)
                            {
                                if (db_table.Entity.R_ID != 0)
                                {
                                    MasterEntityToUpdate = context.ICORE_ROLES.Where(m => m.R_ID == db_table.Entity.R_ID).FirstOrDefault();
                                    MasterEntityToUpdate.R_DATA_EDIT_DATETIME = DateTime.Now;
                                    MasterEntityToUpdate.R_NAME = db_table.Entity.R_NAME;
                                    MasterEntityToUpdate.R_DESCRIPTION = db_table.Entity.R_DESCRIPTION;
                                    MasterEntityToUpdate.R_MAKER_ID = Session["USER_ID"].ToString();
                                    if (MasterEntityToUpdate.R_MAKER_ID == "Admin123")
                                    {
                                        MasterEntityToUpdate.R_CHECKER_ID = MasterEntityToUpdate.R_MAKER_ID;
                                        MasterEntityToUpdate.R_ISAUTH = true;
                                    }
                                    else
                                    {
                                        MasterEntityToUpdate.R_CHECKER_ID = null;
                                        MasterEntityToUpdate.R_ISAUTH = false;
                                    }
                                    MasterEntityToUpdate.R_STATUS = db_table.Entity.R_STATUS;
                                    context.Entry(MasterEntityToUpdate).State = EntityState.Modified;
                                }
                                else
                                {
                                    MasterEntityToUpdate = context.ICORE_ROLES.Where(m => m.R_NAME.ToLower() == db_table.Entity.R_NAME.ToLower()).FirstOrDefault();
                                    if (MasterEntityToUpdate == null)
                                    {
                                        MasterEntityToUpdate = new ICORE_ROLES();
                                        var RoleID = 0;
                                        RoleID = Convert.ToInt32(context.ICORE_ROLES.Max(m => (decimal?)m.R_ID));
                                        MasterEntityToUpdate.R_ID = RoleID + 1;
                                        MasterEntityToUpdate.R_DATA_ENTRY_DATETIME = DateTime.Now;
                                        MasterEntityToUpdate.R_NAME = db_table.Entity.R_NAME;
                                        MasterEntityToUpdate.R_DESCRIPTION = db_table.Entity.R_DESCRIPTION;
                                        MasterEntityToUpdate.R_MAKER_ID = Session["USER_ID"].ToString();
                                        if (MasterEntityToUpdate.R_MAKER_ID == "Admin123")
                                        {
                                            MasterEntityToUpdate.R_CHECKER_ID = MasterEntityToUpdate.R_MAKER_ID;
                                            MasterEntityToUpdate.R_ISAUTH = true;
                                        }
                                        else
                                        {
                                            MasterEntityToUpdate.R_CHECKER_ID = null;
                                            MasterEntityToUpdate.R_ISAUTH = false;
                                        }
                                        MasterEntityToUpdate.R_STATUS = db_table.Entity.R_STATUS;
                                        context.ICORE_ROLES.Add(MasterEntityToUpdate);
                                    }
                                    else
                                    {
                                        message = "A Role with the given name " + db_table.Entity.R_NAME + " Already Exist";
                                    }
                                }
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {

                                    #region Get Old Function
                                    List<ICORE_ASSIGN_FUNCTIONS> OldFunctions = context.ICORE_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == MasterEntityToUpdate.R_ID && m.AF_STATUS == true).ToList();

                                    string TempNewFunction = "";
                                    string TempRemoveFunction = "";
                                    foreach (int functionid in db_table.SelectedFunctions)
                                    {
                                        ICORE_ASSIGN_FUNCTIONS Function = context.ICORE_ASSIGN_FUNCTIONS.Where(m => m.AF_FUNCTION_ID == functionid && m.AF_ROLE_ID == MasterEntityToUpdate.R_ID && m.AF_STATUS == true).FirstOrDefault();
                                        if (Function == null)
                                        {
                                            ICORE_FUNCTIONS FunctionTbl = context.ICORE_FUNCTIONS.Where(m => m.F_ID == functionid).FirstOrDefault();
                                            TempNewFunction += FunctionTbl.F_NAME + ",";
                                        }
                                    }

                                    #endregion

                                    List<ICORE_ASSIGN_FUNCTIONS> DeActivatedFunctions = context.ICORE_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == db_table.Entity.R_ID && !db_table.SelectedFunctions.Contains(m.AF_FUNCTION_ID)).ToList();
                                    if (DeActivatedFunctions.Count > 0)
                                    {
                                        foreach (ICORE_ASSIGN_FUNCTIONS EachFunctionForUpdate in DeActivatedFunctions)
                                        {
                                            ICORE_FUNCTIONS FunctionTbl = context.ICORE_FUNCTIONS.Where(m => m.F_ID == EachFunctionForUpdate.AF_FUNCTION_ID).FirstOrDefault();
                                            TempRemoveFunction += FunctionTbl.F_NAME + ",";
                                            EachFunctionForUpdate.AF_STATUS = false;
                                            context.Entry(EachFunctionForUpdate).State = EntityState.Modified;
                                        }
                                    }
                                    int AssignFunId = 0;
                                    foreach (int? SelectedFunctionId in db_table.SelectedFunctions)
                                    {
                                        if (SelectedFunctionId != null)
                                        {
                                            ICORE_ASSIGN_FUNCTIONS EachFunctionToUpdate = context.ICORE_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == db_table.Entity.R_ID && m.AF_FUNCTION_ID == SelectedFunctionId).FirstOrDefault();
                                            if (EachFunctionToUpdate == null)
                                            {
                                                EachFunctionToUpdate = new ICORE_ASSIGN_FUNCTIONS();
                                                if (AssignFunId == 0)
                                                {
                                                    EachFunctionToUpdate.AF_ID = Convert.ToInt32(context.ICORE_ASSIGN_FUNCTIONS.Max(m => (decimal?)m.AF_ID)) + 1;
                                                    AssignFunId = EachFunctionToUpdate.AF_ID;
                                                }
                                                else
                                                {
                                                    AssignFunId = (AssignFunId + 1);
                                                    EachFunctionToUpdate.AF_ID = AssignFunId;
                                                }
                                                EachFunctionToUpdate.AF_FUNCTION_ID = SelectedFunctionId;
                                                EachFunctionToUpdate.AF_ROLE_ID = MasterEntityToUpdate.R_ID;
                                                EachFunctionToUpdate.AF_MAKER_ID = Session["USER_ID"].ToString();
                                                EachFunctionToUpdate.AF_STATUS = true;
                                                EachFunctionToUpdate.AF_DATA_ENTRY_DATETIME = DateTime.Now;
                                                context.ICORE_ASSIGN_FUNCTIONS.Add(EachFunctionToUpdate);
                                            }
                                            else
                                            {
                                                EachFunctionToUpdate.AF_MAKER_ID = Session["USER_ID"].ToString();
                                                EachFunctionToUpdate.AF_STATUS = true;
                                                EachFunctionToUpdate.AF_DATA_ENTRY_DATETIME = DateTime.Now;
                                                context.Entry(EachFunctionToUpdate).State = EntityState.Modified;
                                            }
                                        }
                                    }
                                    RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        if (db_table.SelectedCaseTitles != null)
                                        {
                                            RowCount = 0;
                                            List<ICORE_ASSIGN_CASE_TITLES> DeActivatedCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_ROLE_ID == db_table.Entity.R_ID && !db_table.SelectedCaseTitles.Contains(m.ACT_TITLE_ID)).ToList();
                                            if (DeActivatedCaseTitles.Count > 0)
                                            {
                                                foreach (ICORE_ASSIGN_CASE_TITLES EachCTForUpdate in DeActivatedCaseTitles)
                                                {
                                                    EachCTForUpdate.ACT_STATUS = false;
                                                    context.Entry(EachCTForUpdate).State = EntityState.Modified;
                                                }
                                            }
                                            int AssignTitleId = 0;
                                            foreach (int? SelectedTitleId in db_table.SelectedCaseTitles)
                                            {
                                                if (SelectedTitleId != null)
                                                {
                                                    ICORE_ASSIGN_CASE_TITLES EachTitleToUpdate = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_ROLE_ID == db_table.Entity.R_ID && m.ACT_TITLE_ID == SelectedTitleId).FirstOrDefault();
                                                    if (EachTitleToUpdate == null)
                                                    {
                                                        EachTitleToUpdate = new ICORE_ASSIGN_CASE_TITLES();
                                                        if (AssignTitleId == 0)
                                                        {
                                                            EachTitleToUpdate.ACT_ID = Convert.ToInt32(context.ICORE_ASSIGN_CASE_TITLES.Max(m => (decimal?)m.ACT_ID)) + 1;
                                                            AssignTitleId = EachTitleToUpdate.ACT_ID;
                                                        }
                                                        else
                                                        {
                                                            AssignTitleId = (AssignTitleId + 1);
                                                            EachTitleToUpdate.ACT_ID = AssignTitleId;
                                                        }
                                                        EachTitleToUpdate.ACT_TITLE_ID = SelectedTitleId;
                                                        EachTitleToUpdate.ACT_ROLE_ID = db_table.Entity.R_ID;
                                                        EachTitleToUpdate.ACT_STATUS = true;
                                                        EachTitleToUpdate.ACT_MAKER_ID = Session["USER_ID"].ToString();
                                                        EachTitleToUpdate.ACT_CHECKER_ID = null;
                                                        EachTitleToUpdate.ACT_ENTRY_DATETIME = DateTime.Now;
                                                        context.ICORE_ASSIGN_CASE_TITLES.Add(EachTitleToUpdate);
                                                    }
                                                    else
                                                    {
                                                        EachTitleToUpdate.ACT_MAKER_ID = Session["USER_ID"].ToString();
                                                        EachTitleToUpdate.ACT_STATUS = true;
                                                        EachTitleToUpdate.ACT_EDIT_DATETIME = DateTime.Now;
                                                        context.Entry(EachTitleToUpdate).State = EntityState.Modified;
                                                    }
                                                }
                                            }
                                            RowCount += context.SaveChanges();
                                        }
                                        else
                                        {
                                            RowCount = 0;
                                            List<ICORE_ASSIGN_CASE_TITLES> DeActivatedCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_ROLE_ID == db_table.Entity.R_ID && m.ACT_STATUS == true).ToList();
                                            if (DeActivatedCaseTitles.Count > 0)
                                            {
                                                foreach (ICORE_ASSIGN_CASE_TITLES EachCTForUpdate in DeActivatedCaseTitles)
                                                {
                                                    EachCTForUpdate.ACT_STATUS = false;
                                                    context.Entry(EachCTForUpdate).State = EntityState.Modified;
                                                }
                                            }
                                            RowCount += context.SaveChanges();
                                        }
                                        if (RowCount > 0)
                                        {
                                            #region Group Role Log
                                            if (Session["USER_ID"].ToString() != "Admin123")
                                            {
                                                ICORE_ROLE_WITH_FUNCTIONS_LIST_VIEW AssignFunction = context.ICORE_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.R_ID == MasterEntityToUpdate.R_ID).FirstOrDefault();
                                                ICORE_ROLES GetRoleDetail = context.ICORE_ROLES.Where(m => m.R_ID == AssignFunction.R_ID).FirstOrDefault();
                                                ICORE_GROUP_LOG GroupLog = new ICORE_GROUP_LOG();
                                                GroupLog.GLOG_ID = Convert.ToInt32(context.ICORE_GROUP_LOG.Max(m => (decimal?)m.GLOG_ID)) + 1;
                                                GroupLog.GLOG_ROLE_NAME = MasterEntityToUpdate.R_NAME;
                                                GroupLog.GLOG_DESCRIPTION = MasterEntityToUpdate.R_DESCRIPTION;
                                                GroupLog.GLOG_ASSIGNED_ROLES = AssignFunction.ASSIGNED_FUNCTION;
                                                GroupLog.GLOG_ADDED_BY = Session["USER_ID"].ToString();
                                                GroupLog.GLOG_ADDED_DATETIME = GetRoleDetail.R_DATA_ENTRY_DATETIME;
                                                if (TempNewFunction != "" && TempRemoveFunction != "")
                                                    GroupLog.GLOG_CURRENT_STATUS = "New function " + TempNewFunction + " assigned & function name " + TempRemoveFunction + " is removed from the system : " + MasterEntityToUpdate.R_NAME;
                                                else if (TempNewFunction != "" && TempRemoveFunction == "")
                                                    GroupLog.GLOG_CURRENT_STATUS = "New functions assigned to the Role : " + MasterEntityToUpdate.R_NAME + ", New Assigned functions " + TempNewFunction;
                                                else if (TempNewFunction == "" && TempRemoveFunction != "")
                                                    GroupLog.GLOG_CURRENT_STATUS = "Following Functions Removed From Role " + MasterEntityToUpdate.R_NAME + ", Removed Functions " + TempRemoveFunction;
                                                GroupLog.GLOG_ACTIVATED_BY = null;
                                                GroupLog.GLOG_ACTIVATED_DATETIME = null;
                                                GroupLog.GLOG_ENTRY_DATETIME = DateTime.Now;
                                                context.ICORE_GROUP_LOG.Add(GroupLog);
                                                RowCount = context.SaveChanges();
                                            }
                                            if (RowCount > 0)
                                            {
                                                message = "Data Inserted Successfully " + RowCount + " Affected";
                                            }
                                            #endregion
                                        }
                                    }
                                }
                            }
                            else if (QueSelected == true)
                            {
                                if (db_table.SelectedCaseTitles != null)
                                {
                                    if (db_table.Entity.R_ID != 0)
                                    {
                                        MasterEntityToUpdate = context.ICORE_ROLES.Where(m => m.R_ID == db_table.Entity.R_ID).FirstOrDefault();
                                        MasterEntityToUpdate.R_DATA_EDIT_DATETIME = DateTime.Now;
                                        MasterEntityToUpdate.R_NAME = db_table.Entity.R_NAME;
                                        MasterEntityToUpdate.R_DESCRIPTION = db_table.Entity.R_DESCRIPTION;
                                        MasterEntityToUpdate.R_MAKER_ID = Session["USER_ID"].ToString();
                                        MasterEntityToUpdate.R_CHECKER_ID = null;
                                        if (Session["USER_ID"].ToString() == "Admin123")
                                            MasterEntityToUpdate.R_ISAUTH = true;
                                        else
                                            MasterEntityToUpdate.R_ISAUTH = false;
                                        MasterEntityToUpdate.R_STATUS = db_table.Entity.R_STATUS;
                                        context.Entry(MasterEntityToUpdate).State = EntityState.Modified;
                                    }
                                    else
                                    {
                                        MasterEntityToUpdate = context.ICORE_ROLES.Where(m => m.R_NAME.ToLower() == db_table.Entity.R_NAME.ToLower()).FirstOrDefault();
                                        if (MasterEntityToUpdate == null)
                                        {
                                            MasterEntityToUpdate = new ICORE_ROLES();
                                            var RoleID = 0;
                                            RoleID = Convert.ToInt32(context.ICORE_ROLES.Max(m => (decimal?)m.R_ID));
                                            MasterEntityToUpdate.R_ID = RoleID + 1;
                                            MasterEntityToUpdate.R_DATA_ENTRY_DATETIME = DateTime.Now;
                                            MasterEntityToUpdate.R_NAME = db_table.Entity.R_NAME;
                                            MasterEntityToUpdate.R_DESCRIPTION = db_table.Entity.R_DESCRIPTION;
                                            MasterEntityToUpdate.R_MAKER_ID = Session["USER_ID"].ToString();
                                            MasterEntityToUpdate.R_CHECKER_ID = null;
                                            if (Session["USER_ID"].ToString() == "Admin123")
                                                MasterEntityToUpdate.R_ISAUTH = true;
                                            else
                                                MasterEntityToUpdate.R_ISAUTH = false;
                                            MasterEntityToUpdate.R_STATUS = db_table.Entity.R_STATUS;
                                            context.ICORE_ROLES.Add(MasterEntityToUpdate);
                                        }
                                        else
                                        {
                                            message = "A Role with the given name " + db_table.Entity.R_NAME + " Already Exist";
                                        }
                                    }
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {

                                        #region Get Old Function
                                        List<ICORE_ASSIGN_FUNCTIONS> OldFunctions = context.ICORE_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == MasterEntityToUpdate.R_ID && m.AF_STATUS == true).ToList();

                                        string TempNewFunction = "";
                                        string TempRemoveFunction = "";
                                        foreach (int functionid in db_table.SelectedFunctions)
                                        {
                                            ICORE_ASSIGN_FUNCTIONS Function = context.ICORE_ASSIGN_FUNCTIONS.Where(m => m.AF_FUNCTION_ID == functionid && m.AF_ROLE_ID == MasterEntityToUpdate.R_ID && m.AF_STATUS == true).FirstOrDefault();
                                            if (Function == null)
                                            {
                                                ICORE_FUNCTIONS FunctionTbl = context.ICORE_FUNCTIONS.Where(m => m.F_ID == functionid).FirstOrDefault();
                                                TempNewFunction += FunctionTbl.F_NAME + ",";
                                            }
                                        }

                                        #endregion

                                        List<ICORE_ASSIGN_FUNCTIONS> DeActivatedFunctions = context.ICORE_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == db_table.Entity.R_ID && !db_table.SelectedFunctions.Contains(m.AF_FUNCTION_ID)).ToList();
                                        if (DeActivatedFunctions.Count > 0)
                                        {
                                            foreach (ICORE_ASSIGN_FUNCTIONS EachFunctionForUpdate in DeActivatedFunctions)
                                            {
                                                ICORE_FUNCTIONS FunctionTbl = context.ICORE_FUNCTIONS.Where(m => m.F_ID == EachFunctionForUpdate.AF_FUNCTION_ID).FirstOrDefault();
                                                TempRemoveFunction += FunctionTbl.F_NAME + ",";
                                                EachFunctionForUpdate.AF_STATUS = false;
                                                context.Entry(EachFunctionForUpdate).State = EntityState.Modified;
                                            }
                                        }
                                        int AssignFunId = 0;
                                        foreach (int? SelectedFunctionId in db_table.SelectedFunctions)
                                        {
                                            if (SelectedFunctionId != null)
                                            {
                                                ICORE_ASSIGN_FUNCTIONS EachFunctionToUpdate = context.ICORE_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == db_table.Entity.R_ID && m.AF_FUNCTION_ID == SelectedFunctionId).FirstOrDefault();
                                                if (EachFunctionToUpdate == null)
                                                {
                                                    EachFunctionToUpdate = new ICORE_ASSIGN_FUNCTIONS();
                                                    if (AssignFunId == 0)
                                                    {
                                                        EachFunctionToUpdate.AF_ID = Convert.ToInt32(context.ICORE_ASSIGN_FUNCTIONS.Max(m => (decimal?)m.AF_ID)) + 1;
                                                        AssignFunId = EachFunctionToUpdate.AF_ID;
                                                    }
                                                    else
                                                    {
                                                        AssignFunId = (AssignFunId + 1);
                                                        EachFunctionToUpdate.AF_ID = AssignFunId;
                                                    }
                                                    EachFunctionToUpdate.AF_FUNCTION_ID = SelectedFunctionId;
                                                    EachFunctionToUpdate.AF_ROLE_ID = MasterEntityToUpdate.R_ID;
                                                    EachFunctionToUpdate.AF_MAKER_ID = Session["USER_ID"].ToString();
                                                    EachFunctionToUpdate.AF_STATUS = true;
                                                    EachFunctionToUpdate.AF_DATA_ENTRY_DATETIME = DateTime.Now;
                                                    context.ICORE_ASSIGN_FUNCTIONS.Add(EachFunctionToUpdate);
                                                }
                                                else
                                                {
                                                    EachFunctionToUpdate.AF_MAKER_ID = Session["USER_ID"].ToString();
                                                    EachFunctionToUpdate.AF_STATUS = true;
                                                    EachFunctionToUpdate.AF_DATA_ENTRY_DATETIME = DateTime.Now;
                                                    context.Entry(EachFunctionToUpdate).State = EntityState.Modified;
                                                }
                                            }
                                        }
                                        RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                        {
                                            RowCount = 0;
                                            List<ICORE_ASSIGN_CASE_TITLES> DeActivatedCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_ROLE_ID == db_table.Entity.R_ID && !db_table.SelectedCaseTitles.Contains(m.ACT_TITLE_ID)).ToList();
                                            if (DeActivatedCaseTitles.Count > 0)
                                            {
                                                foreach (ICORE_ASSIGN_CASE_TITLES EachCTForUpdate in DeActivatedCaseTitles)
                                                {
                                                    EachCTForUpdate.ACT_STATUS = false;
                                                    context.Entry(EachCTForUpdate).State = EntityState.Modified;
                                                }
                                            }
                                            int AssignTitleId = 0;
                                            foreach (int? SelectedTitleId in db_table.SelectedCaseTitles)
                                            {
                                                if (SelectedTitleId != null)
                                                {
                                                    ICORE_ASSIGN_CASE_TITLES EachTitleToUpdate = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_ROLE_ID == db_table.Entity.R_ID && m.ACT_TITLE_ID == SelectedTitleId).FirstOrDefault();
                                                    if (EachTitleToUpdate == null)
                                                    {
                                                        EachTitleToUpdate = new ICORE_ASSIGN_CASE_TITLES();
                                                        if (AssignTitleId == 0)
                                                        {
                                                            EachTitleToUpdate.ACT_ID = Convert.ToInt32(context.ICORE_ASSIGN_CASE_TITLES.Max(m => (decimal?)m.ACT_ID)) + 1;
                                                            AssignTitleId = EachTitleToUpdate.ACT_ID;
                                                        }
                                                        else
                                                        {
                                                            AssignTitleId = (AssignTitleId + 1);
                                                            EachTitleToUpdate.ACT_ID = AssignTitleId;
                                                        }
                                                        EachTitleToUpdate.ACT_TITLE_ID = SelectedTitleId;
                                                        EachTitleToUpdate.ACT_ROLE_ID = db_table.Entity.R_ID;
                                                        EachTitleToUpdate.ACT_STATUS = true;
                                                        EachTitleToUpdate.ACT_MAKER_ID = Session["USER_ID"].ToString();
                                                        EachTitleToUpdate.ACT_CHECKER_ID = null;
                                                        EachTitleToUpdate.ACT_ENTRY_DATETIME = DateTime.Now;
                                                        context.ICORE_ASSIGN_CASE_TITLES.Add(EachTitleToUpdate);
                                                    }
                                                    else
                                                    {
                                                        EachTitleToUpdate.ACT_MAKER_ID = Session["USER_ID"].ToString();
                                                        EachTitleToUpdate.ACT_STATUS = true;
                                                        EachTitleToUpdate.ACT_EDIT_DATETIME = DateTime.Now;
                                                        context.Entry(EachTitleToUpdate).State = EntityState.Modified;
                                                    }
                                                }
                                            }
                                            RowCount = context.SaveChanges();
                                            if (RowCount > 0)
                                            {
                                                #region Group Role Log
                                                if (Session["USER_ID"].ToString() != "Admin123")
                                                {
                                                    ICORE_ROLE_WITH_FUNCTIONS_LIST_VIEW AssignFunction = context.ICORE_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.R_ID == MasterEntityToUpdate.R_ID).FirstOrDefault();
                                                    ICORE_ROLES GetRoleDetail = context.ICORE_ROLES.Where(m => m.R_ID == AssignFunction.R_ID).FirstOrDefault();
                                                    ICORE_GROUP_LOG GroupLog = new ICORE_GROUP_LOG();
                                                    GroupLog.GLOG_ID = Convert.ToInt32(context.ICORE_GROUP_LOG.Max(m => (decimal?)m.GLOG_ID)) + 1;
                                                    GroupLog.GLOG_ROLE_NAME = MasterEntityToUpdate.R_NAME;
                                                    GroupLog.GLOG_DESCRIPTION = MasterEntityToUpdate.R_DESCRIPTION;
                                                    GroupLog.GLOG_ASSIGNED_ROLES = AssignFunction.ASSIGNED_FUNCTION;
                                                    GroupLog.GLOG_ADDED_BY = Session["USER_ID"].ToString();
                                                    GroupLog.GLOG_ADDED_DATETIME = GetRoleDetail.R_DATA_ENTRY_DATETIME;
                                                    if (TempNewFunction != "" && TempRemoveFunction != "")
                                                        GroupLog.GLOG_CURRENT_STATUS = "New function " + TempNewFunction + " assigned & function name " + TempRemoveFunction + " is removed from the system : " + MasterEntityToUpdate.R_NAME;
                                                    else if (TempNewFunction != "" && TempRemoveFunction == "")
                                                        GroupLog.GLOG_CURRENT_STATUS = "New functions assigned to the Role : " + MasterEntityToUpdate.R_NAME + ", New Assigned functions " + TempNewFunction;
                                                    else if (TempNewFunction == "" && TempRemoveFunction != "")
                                                        GroupLog.GLOG_CURRENT_STATUS = "Following Functions Removed From Role " + MasterEntityToUpdate.R_NAME + ", Removed Functions " + TempRemoveFunction;
                                                    GroupLog.GLOG_ACTIVATED_BY = null;
                                                    GroupLog.GLOG_ACTIVATED_DATETIME = null;
                                                    GroupLog.GLOG_ENTRY_DATETIME = DateTime.Now;
                                                    context.ICORE_GROUP_LOG.Add(GroupLog);
                                                    RowCount = context.SaveChanges();
                                                }
                                                if (RowCount > 0)
                                                {
                                                    message = "Data Inserted Successfully " + RowCount + " Affected";
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                }
                                else
                                    message = "Case Title is required in the following function(OPS Queue, Compliance Queue, Regularity Queue)";
                            }
                            else
                            {
                                message = "Selected function doesnot any contain functions like (OPS Queue, Compliance Queue, Regularity Queue)";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Icore_Groups_Roles", "Index", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    AssignFunctions_Custom entity = new AssignFunctions_Custom();
                    if (entity.SelectedCaseTitles != null)
                    {
                        entity.Entity = new ICORE_ROLES();
                        entity.Entity.R_ID = db_table.Entity.R_ID;
                        List<int?> PreviousFunctionIds = context.ICORE_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == db_table.Entity.R_ID).Select(m => m.AF_FUNCTION_ID).ToList();
                        List<SelectListItem> items = new SelectList(context.ICORE_FUNCTIONS.OrderBy(m => m.F_PARENT_ID).ToList(), "F_ID", "F_NAME", 0).ToList();
                        List<int?> PreviousCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_ROLE_ID == db_table.Entity.R_ID && m.ACT_STATUS == true).Select(m => m.ACT_TITLE_ID).ToList();
                        List<SelectListItem> CaseTitleItem = new SelectList(context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Status == true && m.CaseTitle_IsAuth == true).OrderBy(m => m.CaseTitle_Id).Select(f => new
                        {
                            f.CaseTitle_Id,
                            CaseTitle_Name = f.CaseTitle_Name + " -- " + f.CaseTitle_Code
                        }).ToList(), "CaseTitle_Id", "CaseTitle_Name", 0).ToList();
                        foreach (SelectListItem item in items)
                        {
                            int? CurrentFId = Convert.ToInt32(item.Value);
                            if (PreviousFunctionIds.Contains(CurrentFId))
                                item.Selected = true;
                        }
                        foreach (SelectListItem item in CaseTitleItem)
                        {
                            int? CurrentId = Convert.ToInt32(item.Value);
                            if (PreviousCaseTitles.Contains(CurrentId))
                                item.Selected = true;
                        }
                        entity.FunctionsList = items;
                        entity.CaseTitlesList = CaseTitleItem;
                        return View(entity);
                    }
                    else
                    {
                        entity.Entity = new ICORE_ROLES();
                        entity.Entity.R_ID = 0;
                        List<SelectListItem> items = new SelectList(context.ICORE_FUNCTIONS.OrderBy(m => m.F_PARENT_ID).ToList(), "F_ID", "F_NAME", 0).ToList();
                        entity.FunctionsList = items;
                        entity.CaseTitileEntity = new ICORE_ASSIGN_CASE_TITLES();
                        entity.CaseTitileEntity.ACT_ID = 0;
                        List<SelectListItem> CaseTitleItem = new SelectList(context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Status == true && m.CaseTitle_IsAuth == true).OrderBy(m => m.CaseTitle_Id).Select(f => new {
                            f.CaseTitle_Id,
                            CaseTitle_Name = f.CaseTitle_Name + " -- " + f.CaseTitle_Code
                        }).ToList(), "CaseTitle_Id", "CaseTitle_Name", 0).ToList();
                        entity.CaseTitlesList = CaseTitleItem;
                        return View(entity);
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public ActionResult Grid_View()
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "dashboard");
            }
            else
            {
                if (DAL.CheckFunctionValidity("Icore_Groups_Roles", "Index", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<ICORE_ROLE_WITH_FUNCTIONS_LIST_VIEW> ViewData = context.ICORE_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.R_ID != 1 &&  m.R_ISAUTH == true || m.R_MAKER_ID != SessionUser).OrderByDescending(m => m.R_ID).ToList();
                    return PartialView(ViewData);
                }
                else
                    return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthroizeRole(int R_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_Groups_Roles", "Index", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string message = "";
                        try
                        {
                            ICORE_ROLES UpdateEntity = context.ICORE_ROLES.Where(m => m.R_ID == R_ID).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                if (UpdateEntity.R_MAKER_ID != Session["USER_ID"].ToString())
                                {
                                    UpdateEntity.R_ISAUTH = true;
                                    UpdateEntity.R_CHECKER_ID = Session["USER_ID"].ToString();
                                    UpdateEntity.R_DATA_EDIT_DATETIME = DateTime.Now;
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        RowCount = 0;
                                        #region Group Role Log
                                        ICORE_ROLES MasterEntityToUpdate = context.ICORE_ROLES.Where(m => m.R_ID == R_ID).FirstOrDefault();
                                        ICORE_ROLE_WITH_FUNCTIONS_LIST_VIEW AssignFunction = context.ICORE_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.R_ID == MasterEntityToUpdate.R_ID).FirstOrDefault();
                                        ICORE_GROUP_LOG GroupLog = new ICORE_GROUP_LOG();
                                        GroupLog.GLOG_ID = Convert.ToInt32(context.ICORE_GROUP_LOG.Max(m => (decimal?)m.GLOG_ID)) + 1;
                                        GroupLog.GLOG_ROLE_NAME = MasterEntityToUpdate.R_NAME;
                                        GroupLog.GLOG_DESCRIPTION = MasterEntityToUpdate.R_DESCRIPTION;
                                        GroupLog.GLOG_ASSIGNED_ROLES = AssignFunction.ASSIGNED_FUNCTION;
                                        GroupLog.GLOG_ADDED_BY = UpdateEntity.R_MAKER_ID;
                                        GroupLog.GLOG_ADDED_DATETIME = UpdateEntity.R_DATA_ENTRY_DATETIME;
                                        GroupLog.GLOG_ACTIVATED_BY = Session["USER_ID"].ToString();
                                        GroupLog.GLOG_ACTIVATED_DATETIME = DateTime.Now;
                                        GroupLog.GLOG_ENTRY_DATETIME = DateTime.Now;
                                        GroupLog.GLOG_CURRENT_STATUS = " Activated the Role : " + MasterEntityToUpdate.R_NAME;
                                        context.ICORE_GROUP_LOG.Add(GroupLog);
                                        RowCount = context.SaveChanges();
                                        if (RowCount > 0)
                                        {
                                            message = "Group Role : " + UpdateEntity.R_NAME + " is successfully Authorized";
                                        }
                                        #endregion
                                    }
                                }
                                else
                                {
                                    message = "Maker cannot authorize the same record.";
                                    return Json(message);
                                }
                            }
                            else
                            {
                                message = "Problem while fetching your record on ID# " + R_ID;
                                return Json(message);
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("Icore_Groups_Roles", "AuthorizeRole", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(message);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult DeleteFromAuthroizeRole(int R_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_Groups_Roles", "Index", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string message = "";
                        try
                        {
                            ICORE_ROLES UpdateEntity = context.ICORE_ROLES.Where(m => m.R_ID == R_ID).FirstOrDefault();
                            #region Save Temp Function Name Before Deleting
                            string GetAllFunction = "";
                            ICORE_ROLE_WITH_FUNCTIONS_LIST_VIEW AllFunction = context.ICORE_ROLE_WITH_FUNCTIONS_LIST_VIEW.Where(m => m.R_ID == R_ID).FirstOrDefault();
                            if (AllFunction != null)
                            {
                                GetAllFunction += AllFunction.ASSIGNED_FUNCTION;
                            }
                            #endregion
                            if (UpdateEntity != null && UpdateEntity.R_ISAUTH == false)
                            {
                                if (UpdateEntity.R_MAKER_ID != Session["USER_ID"].ToString())
                                {
                                    context.ICORE_ROLES.Remove(UpdateEntity);
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        List<ICORE_ASSIGN_FUNCTIONS> AssignGroup = context.ICORE_ASSIGN_FUNCTIONS.Where(m => m.AF_ROLE_ID == UpdateEntity.R_ID).ToList();
                                        if (AssignGroup.Count > 0)
                                        {
                                            RowCount = 0;
                                            foreach (ICORE_ASSIGN_FUNCTIONS item in AssignGroup)
                                            {
                                                context.ICORE_ASSIGN_FUNCTIONS.Remove(item);
                                                RowCount = context.SaveChanges();
                                            }
                                        }
                                        if (RowCount > 0)
                                        {
                                            ICORE_GROUP_LOG GroupLog = new ICORE_GROUP_LOG();
                                            GroupLog.GLOG_ID = Convert.ToInt32(context.ICORE_GROUP_LOG.Max(m => (decimal?)m.GLOG_ID)) + 1;
                                            GroupLog.GLOG_ROLE_NAME = UpdateEntity.R_NAME;
                                            GroupLog.GLOG_DESCRIPTION = UpdateEntity.R_DESCRIPTION;
                                            GroupLog.GLOG_ASSIGNED_ROLES = GetAllFunction;
                                            GroupLog.GLOG_ADDED_BY = UpdateEntity.R_MAKER_ID;
                                            GroupLog.GLOG_ADDED_DATETIME = UpdateEntity.R_DATA_ENTRY_DATETIME;
                                            GroupLog.GLOG_CURRENT_STATUS = "Following Role Rejected/Deleted : " + UpdateEntity.R_NAME;
                                            GroupLog.GLOG_ACTIVATED_BY = Session["USER_ID"].ToString();
                                            GroupLog.GLOG_ACTIVATED_DATETIME = DateTime.Now;
                                            GroupLog.GLOG_ENTRY_DATETIME = DateTime.Now;
                                            context.ICORE_GROUP_LOG.Add(GroupLog);
                                            RowCount = context.SaveChanges();
                                            if (RowCount > 0)
                                                message = "Group Role : " + UpdateEntity.R_NAME + " is Rejected";
                                        }
                                    }
                                }
                                else
                                {
                                    message = "Maker cannot reject the same record.";
                                    return Json(message);
                                }
                            }
                            else
                            {
                                message = "Problem while fetching your record on ID# " + R_ID;
                                return Json(message);
                            }
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("Icore_Groups_Roles", "DeleteFromAuthorizeRole", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(message);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }

        #region Group Report
        public ActionResult GroupActivity()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_Groups_Roles", "GroupActivity", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Icore_Groups_Roles", "GroupActivity", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        [HttpPost]
        public ActionResult GroupActivity(DateTime? FromDate, DateTime? EndDate)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_Groups_Roles", "GroupActivity", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (FromDate != null && EndDate != null)
                        {
                            var Data = context.GroupActivityReport(FromDate, EndDate).ToList();
                            if (Data.Count() > 0)
                            {
                                Data = Data.Where(m => m.GLOG_ACTIVATED_BY != "Admin123" && m.GLOG_ADDED_BY != "Admin123").ToList();
                                return View(Data);
                            }
                            else
                            {
                                message = "No Data found in the system : On selected Start date : " + FromDate + " End date : " + EndDate;
                            }
                        }
                        else
                        {
                            if (FromDate == null && EndDate == null)
                                message = "Select start date and end date to continue";
                            else if (FromDate == null)
                                message = "Select start date to continue";
                            else if (EndDate == null)
                                message = "Select end date to continue";
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("Icore_Groups_Roles", "GroupActivity", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        #endregion
    }
}