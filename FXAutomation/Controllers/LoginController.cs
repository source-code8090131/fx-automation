﻿using FXAutomation.Models;
using PSW.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace FXAutomation.Controllers
{
    public class LoginController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: Login
        public string GetIP()
        {
            string Str = "";
            Str = System.Net.Dns.GetHostName();
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(Str);
            IPAddress[] addr = ipEntry.AddressList;
            return addr[addr.Length - 1].ToString();
        }
        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Index()
        {
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            return View();
        }
        [HttpPost]
        public ActionResult Index(ICORE_LOGIN_VIEW_CUSTOM db_table)
        {
            string message = "";
            try
            {
                //FX-00001 Invalid email
                //FX-00002 Invalid password
                //FX-00003 Unauthorized user
                //FX-00004 Authentication Failed from the Domain
                //FX-00005 Login Was Successfull but No Rights Assign to the User
                //FX-00006 Invalid Domain Seleted For Authentication.
                //FX-00007 Exception & Inner Exception.
                Session["IP"] = GetIP();
                ICORE_LOGIN login = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == db_table.LOG_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                if (login != null)
                {
                    ICORE_LOGIN_VIEW loginview = context.ICORE_LOGIN_VIEW.Where(m => m.LOG_USER_ID == db_table.LOG_USER_ID).FirstOrDefault();

                    Session["IP"] = GetIP();
                    if (loginview != null)
                    {
                        if (!loginview.LOG_USER_ID.ToLower().Contains("admin123") && !loginview.LOG_USER_ID.ToLower().Contains("adminc"))
                        {
                            string adPath = "";
                            string adurl = "";
                            if (db_table.DOMAIN_NAME == "APAC")
                            {
                                adPath = "LDAP://apac.nsroot.net"; //Fully-qualified Domain Name
                                adurl = "apac.nsroot.net";
                            }
                            else if(db_table.DOMAIN_NAME == "EUR")
                            {
                                adPath = "LDAP://eur.nsroot.net"; //Fully-qualified Domain Name
                                adurl = "eur.nsroot.net";
                            }
                            else if (db_table.DOMAIN_NAME == "NAM")
                            {
                                adPath = "LDAP://nam.nsroot.net"; //Fully-qualified Domain Name
                                adurl = "nam.nsroot.net";
                            }
                            else
                            {
                                message = "Unsuccessful login. Please contact the support desk.";
                                //DAL.LogLoginMessage(db_table.LOG_USER_ID, db_table.DOMAIN_NAME, "Select the domain to continue.");
                                return View();
                            }
                            LdapAuthentication adAuth = new LdapAuthentication(adPath);
                            bool CheckAuth = adAuth.IsAuthenticated(adurl, db_table.LOG_USER_ID, db_table.LOG_HASH);
                            if (CheckAuth == true)
                            {
                                bool isCookiePersistent = User.Identity.IsAuthenticated;
                                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                                    (1, db_table.LOG_USER_ID, DateTime.Now, DateTime.Now.AddMinutes(60), isCookiePersistent, null);
                                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                                HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                                if (true == isCookiePersistent)
                                    authCookie.Expires = authTicket.Expiration;
                                Response.Cookies.Add(authCookie);
                                LogUserDetails(loginview.LOG_USER_ID);
                                Session["USER_ID"] = loginview.LOG_USER_ID;
                                Session["USER_NAME"] = loginview.LOG_USER_NAME;
                                Session["USER_TYPE"] = loginview.R_NAME;
                                return RedirectToAction("Index", "dashboard");
                            }
                            else
                            {
                                message = "Unsuccessful login. Please contact the support desk.";
                                //DAL.LogLoginMessage(db_table.LOG_USER_ID, db_table.DOMAIN_NAME, "Authentication Failed from the Domain.");
                            }
                        }
                        else
                        {
                            ICORE_SYSTEM_SETUP SystemData = context.ICORE_SYSTEM_SETUP.Where(m => m.SS_ID == 1).FirstOrDefault();
                            if (login.LOG_USER_ID == db_table.LOG_USER_ID && db_table.LOG_HASH == DAL.Decrypt(SystemData.SS_HASH) && login.LOG_ISAUTH == true)
                            {
                                bool isCookiePersistent = User.Identity.IsAuthenticated;
                                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                                       (1, db_table.LOG_USER_ID, DateTime.Now, DateTime.Now.AddMinutes(60), isCookiePersistent, null);
                                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                                HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                                if (true == isCookiePersistent)
                                    authCookie.Expires = authTicket.Expiration;
                                Response.Cookies.Add(authCookie);
                                LogUserDetails(loginview.LOG_USER_ID);
                                Session["USER_ID"] = loginview.LOG_USER_ID;
                                Session["USER_NAME"] = loginview.LOG_USER_NAME;
                                Session["USER_TYPE"] = loginview.R_NAME;
                                return RedirectToAction("Index", "dashboard");
                            }
                            else
                            {
                                if (db_table.LOG_HASH != DAL.Decrypt(SystemData.SS_HASH))
                                {
                                    message = "Unsuccessful login. Please contact the support desk.";
                                    //DAL.LogLoginMessage(db_table.LOG_USER_ID, db_table.DOMAIN_NAME, "Invalid Password Provided for the User " + db_table.LOG_USER_ID);
                                }
                                else if (login.LOG_USER_ID != db_table.LOG_USER_ID)
                                {
                                    message = "Unsuccessful login. Please contact the support desk.";
                                    //DAL.LogLoginMessage(db_table.LOG_USER_ID, db_table.DOMAIN_NAME, "Invalid User ID provided '" + db_table.LOG_USER_ID + "'");
                                }
                                else if (login.LOG_ISAUTH != true)
                                {
                                    message = "Unsuccessful login. Please contact the support desk.";
                                    //DAL.LogLoginMessage(db_table.LOG_USER_ID, db_table.DOMAIN_NAME, "Unauthorize User : " + db_table.LOG_USER_ID + " is unauthorize");
                                }
                                return View();
                            }
                        }
                    }
                    else
                    {
                        message = "Unsuccessful login. Please contact the support desk.";
                        //DAL.LogLoginMessage(db_table.LOG_USER_ID, db_table.DOMAIN_NAME, "Login Was Successfull but No Rights Assign to the User.");
                        return View();
                    }
                }
                else
                {
                    //DAL.LogLoginMessage(db_table.LOG_USER_ID, db_table.DOMAIN_NAME, "Invalid User Id.");
                    message = "Unsuccessful login. Please contact the support desk.";
                    return View();
                }
            }
            catch (Exception ex)
            {
                message = DAL.LogException("Login", "Index", db_table.LOG_USER_ID, ex);
                //message += "Exception Occured " + ex.Message + Environment.NewLine;
                message = "Unsuccessful login: Please contact the support desk.";
                if (ex.InnerException != null)
                {
                    //message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    message += "Unsuccessful login: Please contact the support desk.";
                    if (ex.InnerException.InnerException != null)
                    {
                        //message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                        message += "Unsuccessful login: Please contact the support desk.";
                    }
                }
            }
            finally {
                ViewBag.message = message;
                ModelState.Clear();
            }
            return View();
        }

        private void LogUserDetails(string lOG_USER_ID)
        {
            ICORE_LOGIN_LOG log = new ICORE_LOGIN_LOG();
            log.LL_ID = Convert.ToInt32(context.ICORE_LOGIN_LOG.Max(m => (decimal?)m.LL_ID)) + 1;
            log.LL_LOG_USER_ID = lOG_USER_ID;
            log.LL_DATE = DateTime.Now;
            log.LL_UNIQUE_ID = Guid.NewGuid().ToString();
            context.ICORE_LOGIN_LOG.Add(log);
            context.SaveChanges();

            Session["UNIQUE_ID"] = log.LL_UNIQUE_ID;
        }

        public ActionResult Logout()
        {
            if (Session["USER_ID"] != null)
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();
            }
            FormsAuthentication.SignOut();
            Session.Clear();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ExpiresAbsolute = DateTime.UtcNow.AddDays(-1d);
            Response.Expires = -1500;
            Response.CacheControl = "no-Cache";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            return RedirectToAction("Index", "Login");
        }
    }
}