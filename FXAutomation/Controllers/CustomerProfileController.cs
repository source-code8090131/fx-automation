﻿using FXAutomation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FXAutomation.Controllers
{
    public class CustomerProfileController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: CustomerProfile

        #region Client Portfolio
        public ActionResult Index(int CP = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("CustomerProfile", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (CP != 0)
                        {
                            ICORE_CLIENT_PORTFOLIO edit = context.ICORE_CLIENT_PORTFOLIO.Where(m => m.CP_ID == CP).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                message = "Problem while getting your id " + CP;
                                return View();
                            }
                        }
                        else
                        {
                            ViewBag.message = message;
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("CustomerProfile", "Index", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult Index(ICORE_CLIENT_PORTFOLIO db_table)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("CustomerProfile", "Index", Session["USER_ID"].ToString()))
                {
                    string message = "";
                    try
                    {
                        if (db_table.CP_ID != 0)
                        {
                            ICORE_CLIENT_PORTFOLIO check_dep = context.ICORE_CLIENT_PORTFOLIO.Where(m => m.CP_ID != db_table.CP_ID && m.CP_PARTY_ID == db_table.CP_PARTY_ID && m.CP_EMAIL == db_table.CP_EMAIL).FirstOrDefault();
                            if (check_dep == null)
                            {
                                ICORE_CLIENT_PORTFOLIO UpdateEntity = context.ICORE_CLIENT_PORTFOLIO.Where(m => m.CP_ID == db_table.CP_ID).FirstOrDefault();
                                if (UpdateEntity != null)
                                {
                                    UpdateEntity.CP_NAME = db_table.CP_NAME;
                                    UpdateEntity.CP_EMAIL = db_table.CP_EMAIL;
                                    UpdateEntity.CP_CONTACT = db_table.CP_CONTACT;
                                    UpdateEntity.CP_IBAN = db_table.CP_IBAN;
                                    UpdateEntity.CP_NTN = db_table.CP_NTN;
                                    UpdateEntity.CP_CONTACT = db_table.CP_CONTACT;
                                    UpdateEntity.CP_STATUS = db_table.CP_STATUS;
                                    UpdateEntity.CP_ADDRESS = db_table.CP_ADDRESS;
                                    UpdateEntity.CP_PARTY_ID = db_table.CP_PARTY_ID;
                                    UpdateEntity.CP_COMPANY_NAME = db_table.CP_COMPANY_NAME;
                                    UpdateEntity.CP_BRIEF_PROFILE = db_table.CP_BRIEF_PROFILE;
                                    UpdateEntity.CP_EDIT_DATETIME = DateTime.Now;
                                    UpdateEntity.CP_MAKER_ID = Session["USER_ID"].ToString();
                                    if (UpdateEntity.CP_MAKER_ID == "Admin123")
                                    {
                                        UpdateEntity.CP_CHECKER_ID = UpdateEntity.CP_MAKER_ID;
                                        UpdateEntity.CP_ISAUTH = true;
                                    }
                                    else
                                    {
                                        UpdateEntity.CP_CHECKER_ID = null;
                                        UpdateEntity.CP_ISAUTH = false;
                                    }
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                }
                                else
                                {
                                    message = "Problem occur while getting previous record";
                                }
                            }
                            else
                            {
                                message = "Email Address : " + db_table.CP_EMAIL + " and IBAN No # " + db_table.CP_IBAN + " alredy exist";
                            }
                        }
                        else
                        {
                            ICORE_CLIENT_PORTFOLIO check_dep = context.ICORE_CLIENT_PORTFOLIO.Where(m => m.CP_PARTY_ID == db_table.CP_PARTY_ID && m.CP_EMAIL == db_table.CP_EMAIL).FirstOrDefault();
                            if (check_dep == null)
                            {
                                ICORE_CLIENT_PORTFOLIO Insert_Entity = new ICORE_CLIENT_PORTFOLIO();
                                Insert_Entity.CP_ID = Convert.ToInt32(context.ICORE_CLIENT_PORTFOLIO.Max(m => (decimal?)m.CP_ID)) + 1;
                                Insert_Entity.CP_NAME = db_table.CP_NAME;
                                Insert_Entity.CP_EMAIL = db_table.CP_EMAIL;
                                Insert_Entity.CP_CONTACT = db_table.CP_CONTACT;
                                Insert_Entity.CP_IBAN = db_table.CP_IBAN;
                                Insert_Entity.CP_NTN = db_table.CP_NTN;
                                Insert_Entity.CP_CONTACT = db_table.CP_CONTACT;
                                Insert_Entity.CP_ADDRESS = db_table.CP_ADDRESS;
                                Insert_Entity.CP_PARTY_ID = db_table.CP_PARTY_ID;
                                Insert_Entity.CP_COMPANY_NAME = db_table.CP_COMPANY_NAME;
                                Insert_Entity.CP_BRIEF_PROFILE = db_table.CP_BRIEF_PROFILE;
                                Insert_Entity.CP_STATUS = db_table.CP_STATUS;
                                Insert_Entity.CP_ENTRY_DATETIME = DateTime.Now;
                                Insert_Entity.CP_MAKER_ID = Session["USER_ID"].ToString();
                                if (Insert_Entity.CP_MAKER_ID == "Admin123")
                                {
                                    Insert_Entity.CP_CHECKER_ID = Insert_Entity.CP_MAKER_ID;
                                    Insert_Entity.CP_ISAUTH = true;
                                }
                                else
                                {
                                    Insert_Entity.CP_CHECKER_ID = null;
                                    Insert_Entity.CP_ISAUTH = false;
                                }
                                context.ICORE_CLIENT_PORTFOLIO.Add(Insert_Entity);
                            }
                            else
                            {
                                message = "Email Address : " + db_table.CP_EMAIL + " and IBAN No # " + db_table.CP_IBAN + " alredy exist";
                            }

                        }

                        if (message == "")
                        {
                            int overall_msg = context.SaveChanges();
                            if (overall_msg > 0)
                            {
                                message = "Data Saved Sucessfully " + overall_msg + "  Row affected";
                                ModelState.Clear();
                            }
                            else
                            {
                                message = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = DAL.LogException("CustomerProfile", "Index", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.message = message;
                    }
                    return View(db_table);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public ActionResult ClientView()
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "dashboard");
            }
            else
            {
                if (DAL.CheckFunctionValidity("CustomerProfile", "Index", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<ICORE_CLIENT_PORTFOLIO> ViewData = context.ICORE_CLIENT_PORTFOLIO.Where(m => m.CP_ISAUTH == true || m.CP_MAKER_ID != SessionUser).OrderByDescending(m => m.CP_ID).ToList();
                    return PartialView(ViewData);
                }
                else
                    return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeClient(int CP_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("CustomerProfile", "Index", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        try
                        {
                            ICORE_CLIENT_PORTFOLIO UpdateEntity = context.ICORE_CLIENT_PORTFOLIO.Where(m => m.CP_ID == CP_ID).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                if (UpdateEntity.CP_MAKER_ID != Session["USER_ID"].ToString())
                                {
                                    UpdateEntity.CP_ISAUTH = true;
                                    UpdateEntity.CP_CHECKER_ID = Session["USER_ID"].ToString();
                                    UpdateEntity.CP_EDIT_DATETIME = DateTime.Now;
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        messeage = "Client : " + UpdateEntity.CP_NAME + " is successfully Authorized";
                                    }
                                }
                                else
                                    messeage = "Sorry, Maker cannot authorize the same record.";
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + CP_ID;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("CustomerProfile", "AuthorizeClient", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult DeleteFromAuthorizeClient(int CP_ID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("CustomerProfile", "Index", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        try
                        {
                            ICORE_CLIENT_PORTFOLIO UpdateEntity = context.ICORE_CLIENT_PORTFOLIO.Where(m => m.CP_ID == CP_ID).FirstOrDefault();
                            if (UpdateEntity != null && UpdateEntity.CP_ISAUTH == false)
                            {
                                if (UpdateEntity.CP_MAKER_ID != Session["USER_ID"].ToString())
                                {
                                    context.ICORE_CLIENT_PORTFOLIO.Remove(UpdateEntity);
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        messeage = "Client : " + UpdateEntity.CP_NAME + " is Rejected";
                                    }
                                }
                                else
                                    messeage = "Sorry, Maker cannot reject the same record.";
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + CP_ID;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("CustomerProfile", "DeleteFromAuthorizeClient", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        #endregion

        #region Client Request Log
        [ChildActionOnly]
        public PartialViewResult CustomerPortalLogs()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("CustomerProfile", "Index", Session["USER_ID"].ToString()))
                {
                    List<ICORE_CLIENT_REQUESTS_LOG> DataList = context.ICORE_CLIENT_REQUESTS_LOG.OrderByDescending(m => m.CRL_ID).Take(1000).ToList();
                    return PartialView(DataList);
                }
                else
                    return PartialView(null);
            }
            else
            {
                return PartialView(null);
            }
        }
        public PartialViewResult RequestLogDetail(int CRL_ID = 0)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("CustomerProfile", "Index", Session["USER_ID"].ToString()))
                {
                    List<ICORE_CLIENT_REQUESTS_LOG> DataList = context.ICORE_CLIENT_REQUESTS_LOG.Where(m => m.CRL_ID == CRL_ID).ToList();
                    return PartialView(DataList);
                }
                else
                {
                    return PartialView();
                }
            }
            else
            {
                return PartialView();
            }
        }
        #endregion
    }
}