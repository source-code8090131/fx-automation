﻿using FXAutomation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FXAutomation.Controllers
{
    public class Icore_RegionController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: Icore_Region
        public ActionResult Regions(string region_id)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_Region", "Regions", Session["USER_ID"].ToString()))
                {
                    int reg_id = 0;
                    if (region_id != null)
                        reg_id = Convert.ToInt32(DAL.Decrypt(region_id));
                    string messeage = "";
                    try
                    {
                        if (reg_id != 0)
                        {
                            ICORE_REGIONS_VIEW region_edit = context.ICORE_REGIONS_VIEW.Where(m => m.Reg_Id == reg_id).FirstOrDefault();
                            if (region_edit != null)
                            {
                                return View(region_edit);
                            }
                            else
                            {
                                messeage = "Problem while getting your id " + reg_id;
                                return View();
                            }
                        }
                        else
                        {
                            ViewBag.messeage = messeage;
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_Region", "Regions", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        List<SelectListItem> items = new SelectList(context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Status == true && m.CaseTitle_IsAuth == true).ToList(), "CaseTitle_Id", "CaseTitle_Name", 0).ToList();
                        items.Insert(0, (new SelectListItem { Text = "--Select Case Title--", Value = "Null" }));
                        ViewBag.Reg_CaseTitleId = items;
                        ViewBag.messeage = messeage;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult Regions(ICORE_REGIONS_VIEW dbtable_region)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_Region", "Regions", Session["USER_ID"].ToString()))
                {
                    string messeage = "";
                    try
                    {
                        if (dbtable_region.Reg_Id != 0)
                        {
                            ICORE_REGIONS update_region = context.ICORE_REGIONS.Where(m => m.Reg_Id == dbtable_region.Reg_Id).FirstOrDefault();
                            if (update_region != null)
                            {

                                update_region.Reg_Code = dbtable_region.Reg_Code;
                                update_region.Reg_Name = dbtable_region.Reg_Name;
                                update_region.Reg_Status = dbtable_region.Reg_Status;
                                update_region.Reg_CaseTitleId = dbtable_region.Reg_CaseTitleId;
                                update_region.Reg_DataEditDateTime = DateTime.Now;
                                update_region.Reg_MakerId = Session["USER_ID"].ToString();
                                if (update_region.Reg_MakerId == "Admin123")
                                {
                                    update_region.Reg_CheckerId = update_region.Reg_MakerId;
                                    update_region.Reg_IsAuth = true;
                                }
                                else
                                {
                                    update_region.Reg_CheckerId = null;
                                    update_region.Reg_IsAuth = false;
                                }
                                context.Entry(update_region).State = EntityState.Modified;
                            }
                            else
                            {
                                messeage = "Problem occur while getting previous record";
                            }
                        }
                        else
                        {
                            ICORE_REGIONS check_region = context.ICORE_REGIONS.Where(m => m.Reg_Name == dbtable_region.Reg_Name || m.Reg_Code == dbtable_region.Reg_Code).FirstOrDefault();
                            if (check_region == null)
                            {
                                ICORE_REGIONS insert_region = new ICORE_REGIONS();
                                insert_region.Reg_Id = Convert.ToInt32(context.ICORE_REGIONS.Max(m => (decimal?)m.Reg_Id)) + 1;
                                insert_region.Reg_Code = dbtable_region.Reg_Code;
                                insert_region.Reg_Name = dbtable_region.Reg_Name;
                                insert_region.Reg_Status = dbtable_region.Reg_Status;
                                insert_region.Reg_CaseTitleId = dbtable_region.Reg_CaseTitleId;
                                insert_region.Reg_DataEntryDateTime = DateTime.Now;
                                insert_region.Reg_MakerId = Session["USER_ID"].ToString();
                                if (insert_region.Reg_MakerId == "Admin123")
                                {
                                    insert_region.Reg_CheckerId = insert_region.Reg_MakerId;
                                    insert_region.Reg_IsAuth = true;
                                }
                                else
                                {
                                    insert_region.Reg_CheckerId = null;
                                    insert_region.Reg_IsAuth = false;
                                }
                                context.ICORE_REGIONS.Add(insert_region);

                            }
                            else
                            {
                                if (check_region.Reg_Name == dbtable_region.Reg_Name)
                                    messeage = "Region Name " + dbtable_region.Reg_Name + " Alredy Exist";
                                else if (check_region.Reg_Code == dbtable_region.Reg_Code)
                                    messeage = "Region Code " + dbtable_region.Reg_Code + " Alredy Exist";
                            }
                        }
                        if (messeage == "")
                        {
                            int overall_msg = context.SaveChanges();
                            if (overall_msg > 0)
                            {
                                messeage = "Data Saved Sucessfully " + overall_msg + "  Row affected";
                                ModelState.Clear();
                            }
                            else
                            {
                                messeage = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_Region", "Regions", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.messeage = messeage;
                        List<SelectListItem> items = new SelectList(context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Status == true && m.CaseTitle_IsAuth == true).ToList(), "CaseTitle_Id", "CaseTitle_Name", 0).ToList();
                        items.Insert(0, (new SelectListItem { Text = "--Select Case Title--", Value = "Null" }));
                        ViewBag.Reg_CaseTitleId = items;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        public ActionResult reg_grid(decimal? CaseTitle_ID)
        {
            if (Request.IsAjaxRequest())
            {
                if (Session["USER_ID"] == null)
                {
                    return RedirectToAction("Index", "dashboard");
                }
                else
                {
                    if (DAL.CheckFunctionValidity("Icore_Region", "Regions", Session["USER_ID"].ToString()))
                    {
                        string SessionUser = Session["USER_ID"].ToString();
                        List<ICORE_REGIONS_VIEW> data;
                        if (CaseTitle_ID == 0)
                            data = context.ICORE_REGIONS_VIEW.OrderByDescending(m => m.Reg_Id).ToList();
                        else
                        {
                            data = context.ICORE_REGIONS_VIEW.Where(model => model.Reg_CaseTitleId == CaseTitle_ID).OrderByDescending(m => m.Reg_Id).ToList();
                            data.Where(m => m.Reg_IsAuth == true && m.Reg_MakerId != SessionUser).ToList();
                        }
                        return PartialView(data);
                    }
                    else
                        return PartialView(null);
                }
            }
            else
                return PartialView();
        }
        [HttpPost]
        public JsonResult AuthorizeRegion(int Reg_Id)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_Region", "Regions", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        try
                        {
                            ICORE_REGIONS UpdateEntity = context.ICORE_REGIONS.Where(m => m.Reg_Id == Reg_Id).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                if (UpdateEntity.Reg_MakerId != Session["USER_ID"].ToString())
                                {
                                    UpdateEntity.Reg_IsAuth = true;
                                    UpdateEntity.Reg_CheckerId = Session["USER_ID"].ToString();
                                    UpdateEntity.Reg_DataEditDateTime = DateTime.Now;
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        messeage = "Region : " + UpdateEntity.Reg_Name + " is successfully Authorized";
                                    }
                                }
                                else
                                {
                                    messeage = "Maker cannot authorize the same record.";
                                    return Json(messeage);
                                }
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + Reg_Id;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("Icore_Region", "AuthorizeRegion", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult DeleteFromAuthorizeRegion(int Reg_Id)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_Region", "Regions", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        try
                        {
                            ICORE_REGIONS UpdateEntity = context.ICORE_REGIONS.Where(m => m.Reg_Id == Reg_Id).FirstOrDefault();
                            if (UpdateEntity != null && UpdateEntity.Reg_IsAuth == false)
                            {
                                if (UpdateEntity.Reg_MakerId != Session["USER_ID"].ToString())
                                {
                                    context.ICORE_REGIONS.Remove(UpdateEntity);
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        messeage = "Region " + UpdateEntity.Reg_Name + " is Rejected";
                                    }
                                }
                                else
                                {
                                    messeage = "Maker cannot reject the same record.";
                                    return Json(messeage);
                                }
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + Reg_Id;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("Icore_Region", "DeleteFromAuthorizeRegion", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
    }
}