﻿using FXAutomation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FXAutomation.Controllers
{
    public class Icore_DepartmentController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: Department
        public ActionResult Department(string Depid)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_Department", "Department", Session["USER_ID"].ToString()))
                {
                    int ID = 0;
                    if (Depid != null)
                        ID = Convert.ToInt32(DAL.Decrypt(Depid));
                    string messeage = "";
                    try
                    {
                        if (ID != 0)
                        {
                            ICORE_DEPARTMENTS edit = context.ICORE_DEPARTMENTS.Where(m => m.Dept_Id == ID).FirstOrDefault();
                            if (edit != null)
                            {
                                return View(edit);
                            }
                            else
                            {
                                messeage = "Problem while getting your id " + ID;
                                return View();
                            }
                        }
                        else
                        {
                            ViewBag.messeage = messeage;
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_Department", "Department", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.Categ_DeptId = new SelectList(context.ICORE_DEPARTMENTS.Where(m => m.Dept_Status == true).ToList(), "Dept_Id", "Dept_Name");
                        ViewBag.messeage = messeage;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult Department(ICORE_DEPARTMENTS dep_table)
        {
            string messeage = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_Department", "Department", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        if (dep_table.Dept_Id != 0)
                        {
                            ICORE_DEPARTMENTS update_dep = context.ICORE_DEPARTMENTS.Where(m => m.Dept_Id == dep_table.Dept_Id).FirstOrDefault();
                            if (update_dep != null)
                            {
                                update_dep.Dept_Code = dep_table.Dept_Code;
                                update_dep.Dept_Name = dep_table.Dept_Name;
                                update_dep.Dept_Status = dep_table.Dept_Status;
                                update_dep.Dept_DataEditDateTime = DateTime.Now;
                                update_dep.Dept_MakerId = Session["USER_ID"].ToString();
                                if (update_dep.Dept_MakerId == "Admin123")
                                {
                                    update_dep.Dept_CheckerId = update_dep.Dept_MakerId;
                                    update_dep.Dept_IsAuth = true;
                                }
                                else
                                {
                                    update_dep.Dept_CheckerId = null;
                                    update_dep.Dept_IsAuth = false;
                                }
                                context.Entry(update_dep).State = EntityState.Modified;
                            }
                            else
                            {
                                messeage = "Problem occur while getting previous record";
                            }
                        }
                        else
                        {
                            ICORE_DEPARTMENTS check_dep = context.ICORE_DEPARTMENTS.Where(m => m.Dept_Name == dep_table.Dept_Name || m.Dept_Code == dep_table.Dept_Code).FirstOrDefault();
                            if (check_dep == null)
                            {
                                ICORE_DEPARTMENTS insert_dep = new ICORE_DEPARTMENTS();
                                insert_dep.Dept_Id = Convert.ToInt32(context.ICORE_DEPARTMENTS.Max(m => (decimal?)m.Dept_Id)) + 1;
                                insert_dep.Dept_Code = dep_table.Dept_Code;
                                insert_dep.Dept_Name = dep_table.Dept_Name;
                                insert_dep.Dept_Status = dep_table.Dept_Status;
                                insert_dep.Dept_DataEntryDateTime = DateTime.Now;
                                insert_dep.Dept_MakerId = Session["USER_ID"].ToString();
                                if (insert_dep.Dept_MakerId == "Admin123")
                                {
                                    insert_dep.Dept_CheckerId = insert_dep.Dept_MakerId;
                                    insert_dep.Dept_IsAuth = true;
                                }
                                else
                                {
                                    insert_dep.Dept_CheckerId = null;
                                    insert_dep.Dept_IsAuth = false;
                                }
                                context.ICORE_DEPARTMENTS.Add(insert_dep);
                            }
                            else
                            {
                                if (check_dep.Dept_Name == dep_table.Dept_Name)
                                    messeage = "Sorry Department Name " + dep_table.Dept_Name + " Alredy Exist";
                                else if (check_dep.Dept_Code == dep_table.Dept_Code)
                                    messeage = "Sorry Department Code " + dep_table.Dept_Code + " Already Exists";
                            }

                        }

                        if (messeage == "")
                        {
                            int overall_msg = context.SaveChanges();
                            if (overall_msg > 0)
                            {
                                messeage = "Data Saved Sucessfully " + overall_msg + "  Row affected";
                                ModelState.Clear();
                            }
                            else
                            {
                                messeage = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_Department", "Department", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.messeage = messeage;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public JsonResult AuthorizeDept(int DeptID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_Department", "Department", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        try
                        {
                            ICORE_DEPARTMENTS UpdateEntity = context.ICORE_DEPARTMENTS.Where(m => m.Dept_Id == DeptID).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                if (UpdateEntity.Dept_MakerId != Session["USER_ID"].ToString())
                                {
                                    UpdateEntity.Dept_IsAuth = true;
                                    UpdateEntity.Dept_CheckerId = Session["USER_ID"].ToString();
                                    UpdateEntity.Dept_DataEditDateTime = DateTime.Now;
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        messeage = "Department : " + UpdateEntity.Dept_Name + " is successfully Authorized";
                                    }
                                }
                                else
                                {
                                    messeage = "Maker cannot authorize the same record.";
                                    return Json(messeage);
                                }
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + DeptID;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("Icore_Department", "AuthorizeDept", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                return Json("Invalid request");    
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult DeleteFromAuthorizeDept(int DeptID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_Department", "Department", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        try
                        {
                            ICORE_DEPARTMENTS UpdateEntity = context.ICORE_DEPARTMENTS.Where(m => m.Dept_Id == DeptID).FirstOrDefault();
                            if (UpdateEntity != null && UpdateEntity.Dept_IsAuth == false)
                            {
                                if (UpdateEntity.Dept_MakerId != Session["USER_ID"].ToString())
                                {
                                    context.ICORE_DEPARTMENTS.Remove(UpdateEntity);
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        messeage = "Department : " + UpdateEntity.Dept_Name + " is Rejected";
                                    }
                                }
                                else
                                {
                                    messeage = "Maker cannot reject the same record.";
                                    return Json(messeage);
                                }
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + DeptID;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("Icore_Department", "DeleteFormAuthorizeDept", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                    return Json("Invalid request");
            }
            else
                return Json("Invalid request");
        }
        [ChildActionOnly]
        public ActionResult Dep_Grid()
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "dashboard");
            }
            else
            {
                if (DAL.CheckFunctionValidity("Icore_Department", "Department", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<ICORE_DEPARTMENTS> ViewData = context.ICORE_DEPARTMENTS.OrderByDescending(m => m.Dept_Id).Where(m => m.Dept_IsAuth == true || m.Dept_MakerId != SessionUser).OrderByDescending(m => m.Dept_Id).ToList();
                    return PartialView(ViewData);
                }
                else
                    return PartialView(null);
            }
        }
    }
}