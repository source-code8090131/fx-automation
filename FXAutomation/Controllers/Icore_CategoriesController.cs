﻿using FXAutomation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FXAutomation.Controllers
{
    public class Icore_CategoriesController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: Icore_Categories
        public ActionResult Categories(string cat_ID)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_Categories", "Categories", Session["USER_ID"].ToString()))
                {
                    int ID = 0;
                    if (cat_ID != null)
                        ID = Convert.ToInt32(DAL.Decrypt(cat_ID));
                    string messeage = "";
                    try
                    {
                        if (ID != 0)
                        {
                            ICORE_CATEGORIES edit = context.ICORE_CATEGORIES.Where(m => m.Categ_Id == ID).FirstOrDefault();
                            if (edit != null)
                                return View(edit);
                            else
                            {
                                messeage = "Problem while getting your id " + ID;
                                return View();
                            }
                        }
                        else
                            return View();
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_Categories", "Categories", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        List<SelectListItem> DepartmentName = new SelectList(context.ICORE_DEPARTMENTS.Where(m => m.Dept_Status == true && m.Dept_IsAuth == true).ToList(), "Dept_Id", "Dept_Name", 0).ToList();
                        DepartmentName.Insert(0, (new SelectListItem { Text = "--Select Department--", Value = "0" }));
                        ViewBag.Categ_DeptId = DepartmentName;
                        ViewBag.messeage = messeage;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult Categories(ICORE_CATEGORIES dbtable)
        {
            string messeage = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_Categories", "Categories", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        if (dbtable.Categ_Id != 0)
                        {
                            ICORE_CATEGORIES update_cate = context.ICORE_CATEGORIES.Where(m => m.Categ_Id == dbtable.Categ_Id).FirstOrDefault();
                            if (update_cate != null)
                            {
                                update_cate.Categ_Code = dbtable.Categ_Code;
                                update_cate.Categ_Name = dbtable.Categ_Name;
                                update_cate.Categ_Description = dbtable.Categ_Description;
                                update_cate.Categ_Status = dbtable.Categ_Status;
                                update_cate.Categ_DataEditDateTime = DateTime.Now;
                                update_cate.Categ_DeptId = dbtable.Categ_DeptId;
                                update_cate.Categ_MakerId = Session["USER_ID"].ToString();
                                if (update_cate.Categ_MakerId == "Admin123")
                                {
                                    update_cate.Categ_CheckerId = update_cate.Categ_MakerId;
                                    update_cate.Categ_IsAuth = true;
                                }
                                else
                                {
                                    update_cate.Categ_CheckerId = null;
                                    update_cate.Categ_IsAuth = false;
                                }
                                context.Entry(update_cate).State = EntityState.Modified;
                            }
                            else
                            {
                                messeage = "Problem occur while getting previous record";
                            }
                        }
                        else
                        {
                            ICORE_CATEGORIES check_cate = context.ICORE_CATEGORIES.Where(m => m.Categ_Name == dbtable.Categ_Name || m.Categ_Code == dbtable.Categ_Code).FirstOrDefault();
                            if (check_cate == null)
                            {
                                ICORE_CATEGORIES insert_cate = new ICORE_CATEGORIES();
                                insert_cate.Categ_Id = Convert.ToInt32(context.ICORE_CATEGORIES.Max(m => (int?)m.Categ_Id)) + 1;
                                insert_cate.Categ_Name = dbtable.Categ_Name;
                                insert_cate.Categ_Code = dbtable.Categ_Code;
                                insert_cate.Categ_Description = dbtable.Categ_Description;
                                insert_cate.Categ_Status = dbtable.Categ_Status;
                                insert_cate.Categ_DataEntryDateTime = DateTime.Now;
                                insert_cate.Categ_MakerId = Session["USER_ID"].ToString();
                                if (insert_cate.Categ_MakerId == "Admin123")
                                {
                                    insert_cate.Categ_CheckerId = insert_cate.Categ_MakerId;
                                    insert_cate.Categ_IsAuth = true;
                                }
                                else
                                {
                                    insert_cate.Categ_CheckerId = null;
                                    insert_cate.Categ_IsAuth = false;
                                }
                                insert_cate.Categ_DeptId = dbtable.Categ_DeptId;

                                context.ICORE_CATEGORIES.Add(insert_cate);

                            }
                            else
                            {
                                if (check_cate.Categ_Name == dbtable.Categ_Name)
                                    messeage = "Sorry Category Name " + dbtable.Categ_Name + " Alredy Exist";
                                else if (check_cate.Categ_Code == dbtable.Categ_Code)
                                    messeage = "Sorry Category Code " + dbtable.Categ_Code + " Already Exists";
                            }
                        }
                        if (messeage == "")
                        {
                            int overall_msg = context.SaveChanges();
                            if (overall_msg > 0)
                            {
                                messeage = "Data Saved Sucessfully " + overall_msg + " Row affected";
                                ModelState.Clear();
                            }
                            else
                            {
                                messeage = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_Categories", "Categories", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        List<SelectListItem> DepartmentName = new SelectList(context.ICORE_DEPARTMENTS.Where(m => m.Dept_Status == true && m.Dept_IsAuth == true).ToList(), "Dept_Id", "Dept_Name", 0).ToList();
                        DepartmentName.Insert(0, (new SelectListItem { Text = "--Select Department--", Value = "0" }));
                        ViewBag.Categ_DeptId = DepartmentName;
                        ViewBag.messeage = messeage;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public ActionResult Cate_Grid()
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "dashboard");
            }
            else
            {
                if (DAL.CheckFunctionValidity("Icore_Categories", "Categories", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<ICORE_CATEGORIES_VIEW> ViewData = context.ICORE_CATEGORIES_VIEW.Where(m => m.Categ_IsAuth == true || m.Categ_MakerId != SessionUser).OrderByDescending(m => m.Categ_Id).ToList();
                    return PartialView(ViewData);
                }
                else
                    return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeCateg(int CatID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_Categories", "Categories", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        try
                        {
                            ICORE_CATEGORIES UpdateEntity = context.ICORE_CATEGORIES.Where(m => m.Categ_Id == CatID).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                if (UpdateEntity.Categ_MakerId != Session["USER_ID"].ToString())
                                {
                                    UpdateEntity.Categ_IsAuth = true;
                                    UpdateEntity.Categ_CheckerId = Session["USER_ID"].ToString();
                                    UpdateEntity.Categ_DataEditDateTime = DateTime.Now;
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        messeage = "Category : " + UpdateEntity.Categ_Name + " is successfully Authorized";
                                    }
                                }
                                else
                                {
                                    messeage = "Maker cannot authorize the same record.";
                                    return Json(messeage);
                                }
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + CatID;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("Icore_Categories", "Authorize", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult DeleteFromAuthorizeCateg(int CatID)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_Categories", "Categories", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        try
                        {
                            ICORE_CATEGORIES UpdateEntity = context.ICORE_CATEGORIES.Where(m => m.Categ_Id == CatID).FirstOrDefault();
                            if (UpdateEntity != null && UpdateEntity.Categ_IsAuth == false)
                            {
                                if (UpdateEntity.Categ_MakerId != Session["USER_ID"].ToString())
                                {
                                    context.ICORE_CATEGORIES.Remove(UpdateEntity);
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        messeage = "Category : " + UpdateEntity.Categ_Name + " is Rejected";
                                    }
                                }
                                else
                                {
                                    messeage = "Maker cannot reject the same record.";
                                    return Json(messeage);
                                }
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + CatID;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("Icore_Categories", "DeleteFromAuthorize", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
    }
}