﻿using FXAutomation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FXAutomation.Controllers
{
    public class Icore_CaseDocumentController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: Icore_CaseDocument
        public ActionResult CaseDocuments(string docID)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_CaseDocument", "CaseDocuments", Session["USER_ID"].ToString()))
                {
                    int doc_id = 0;
                    if (docID != null)
                        doc_id = Convert.ToInt32(DAL.Decrypt(docID));
                    string messeage = "";
                    try
                    {
                        if (doc_id != 0)
                        {
                            ICORE_CASE_DOCUMENTS_VIEW checkID = context.ICORE_CASE_DOCUMENTS_VIEW.Where(m => m.Case_Doc_Id == doc_id).FirstOrDefault();
                            if (checkID != null)
                            {
                                return View(checkID);
                            }
                            else
                            {
                                messeage = "Problem while getting your ID";
                            }
                        }
                        else
                        {
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_CaseDocument", "CaseDocuments", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.Case_Doc_Format_id = new SelectList(context.ICORE_CASE_DOCUMENTS_FORMAT.ToList(), "Format_id", "Format_extension");
                        List<SelectListItem> item = new SelectList(context.ICORE_CASE_TYPES.Where(m => m.CaseType_Status == true && m.CaseType_IsAuth == true).ToList(), "CaseType_Id", "CaseType_Name", 0).ToList();
                        item.Insert(0, (new SelectListItem { Text = "--Select Case Type--", Value = "0" }));
                        ViewBag.Case_Doc_CaseType_Id = item;
                        List<SelectListItem> items = new SelectList(context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Status == true && m.CaseTitle_IsAuth == true).ToList(), "CaseTitle_Id", "CaseTitle_Name", 0).ToList();
                        items.Insert(0, (new SelectListItem { Text = "--Select Case Title--", Value = "0" }));
                        ViewBag.Case_Doc_CaseTitleId = items;
                        ViewBag.messeage = messeage;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult CaseDocuments(ICORE_CASE_DOCUMENTS_VIEW dbtable_case_doc)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_CaseDocument", "CaseDocuments", Session["USER_ID"].ToString()))
                {
                    string messeage = "";
                    try
                    {
                        if (dbtable_case_doc.Case_Doc_CaseType_Id != 0 && dbtable_case_doc.Case_Doc_CaseTitleId != 0)
                        {
                            if (dbtable_case_doc.Case_Doc_Id != 0)
                            {
                                ICORE_CASE_DOCUMENTS CheckDuplicate = context.ICORE_CASE_DOCUMENTS.Where(m => m.Case_Doc_CaseTitleId == dbtable_case_doc.Case_Doc_CaseTitleId && m.Case_Doc_Id != dbtable_case_doc.Case_Doc_Id && (m.Case_Doc_Name == dbtable_case_doc.Case_Doc_Name || m.CASE_Doc_Data_ORDER_BY == dbtable_case_doc.CASE_Doc_Data_ORDER_BY)).FirstOrDefault();
                                if (CheckDuplicate == null)
                                {
                                    ICORE_CASE_DOCUMENTS dbupdate_doc = context.ICORE_CASE_DOCUMENTS.Where(m => m.Case_Doc_Id == dbtable_case_doc.Case_Doc_Id).FirstOrDefault();
                                    if (dbupdate_doc != null)
                                    {
                                        dbupdate_doc.Case_Doc_Id = (decimal)dbtable_case_doc.Case_Doc_Id;
                                        dbupdate_doc.Case_Doc_Name = dbtable_case_doc.Case_Doc_Name;
                                        dbupdate_doc.Case_Doc_Description = dbtable_case_doc.Case_Doc_Description;
                                        dbupdate_doc.Case_Doc_Status = dbtable_case_doc.Case_Doc_Status;
                                        dbupdate_doc.Case_Doc_Data_DataEditDateTime = DateTime.Now;
                                        dbupdate_doc.Case_Doc_Data_MakerId = Session["USER_ID"].ToString();
                                        if (dbupdate_doc.Case_Doc_Data_MakerId == "Admin123")
                                        {
                                            dbupdate_doc.Case_Doc_Data_CheckerId = dbupdate_doc.Case_Doc_Data_MakerId;
                                            dbupdate_doc.Case_Doc_IsAuth = true;
                                        }
                                        else
                                        {
                                            dbupdate_doc.Case_Doc_Data_CheckerId = null;
                                            dbupdate_doc.Case_Doc_IsAuth = false;
                                        }
                                        dbupdate_doc.Case_Doc_IsRequired = dbtable_case_doc.Case_Doc_IsRequired;
                                        dbupdate_doc.CASE_Doc_Data_ORDER_BY = dbtable_case_doc.CASE_Doc_Data_ORDER_BY;
                                        dbupdate_doc.Case_Doc_CaseType_Id = dbtable_case_doc.Case_Doc_CaseType_Id;
                                        dbupdate_doc.Case_Doc_CaseTitleId = (decimal)dbtable_case_doc.Case_Doc_CaseTitleId;
                                        dbupdate_doc.Case_Doc_Format_id = (int)dbtable_case_doc.Case_Doc_Format_id;
                                        dbupdate_doc.Case_Doc_IsAD = dbtable_case_doc.Case_Doc_IsAD;

                                        context.Entry(dbupdate_doc).State = EntityState.Modified;
                                    }
                                    else
                                    {
                                        messeage = "No Data Found On The Given Key";
                                    }
                                }
                                else
                                {
                                    if (CheckDuplicate.Case_Doc_Name == dbtable_case_doc.Case_Doc_Name)
                                        messeage = "A Field With Name " + CheckDuplicate.Case_Doc_Name + " Alredy Exist on Document Name " + CheckDuplicate.Case_Doc_Name;
                                    if (CheckDuplicate.CASE_Doc_Data_ORDER_BY == dbtable_case_doc.CASE_Doc_Data_ORDER_BY)
                                        messeage = "A Field With Name " + CheckDuplicate.Case_Doc_Name + " Alredy Exist On Order No " + CheckDuplicate.CASE_Doc_Data_ORDER_BY;
                                }
                            }
                            else
                            {
                                ICORE_CASE_DOCUMENTS CheckIfExist = context.ICORE_CASE_DOCUMENTS.Where(m => m.Case_Doc_Name.ToLower() == dbtable_case_doc.Case_Doc_Name.ToLower() && m.Case_Doc_CaseTitleId == dbtable_case_doc.Case_Doc_CaseTitleId && m.CASE_Doc_Data_ORDER_BY == dbtable_case_doc.CASE_Doc_Data_ORDER_BY).FirstOrDefault();
                                if (CheckIfExist == null)
                                {
                                    ICORE_CASE_DOCUMENTS dbinsert_doc = new ICORE_CASE_DOCUMENTS();
                                    dbinsert_doc.Case_Doc_Id = Convert.ToInt32(context.ICORE_CASE_DOCUMENTS.Max(m => (decimal?)m.Case_Doc_Id)) + 1;
                                    dbinsert_doc.Case_Doc_Name = dbtable_case_doc.Case_Doc_Name;
                                    dbinsert_doc.Case_Doc_Description = dbtable_case_doc.Case_Doc_Description;
                                    dbinsert_doc.Case_Doc_Status = dbtable_case_doc.Case_Doc_Status;
                                    dbinsert_doc.Case_Doc_Data_DataEntryDateTime = DateTime.Now;
                                    dbinsert_doc.Case_Doc_Data_MakerId = Session["USER_ID"].ToString();
                                    if (dbinsert_doc.Case_Doc_Data_MakerId == "Admin123")
                                    {
                                        dbinsert_doc.Case_Doc_Data_CheckerId = dbinsert_doc.Case_Doc_Data_MakerId;
                                        dbinsert_doc.Case_Doc_IsAuth = true;
                                    }
                                    else
                                    {
                                        dbinsert_doc.Case_Doc_Data_CheckerId = null;
                                        dbinsert_doc.Case_Doc_IsAuth = false;
                                    }
                                    dbinsert_doc.Case_Doc_IsRequired = dbtable_case_doc.Case_Doc_IsRequired;
                                    dbinsert_doc.CASE_Doc_Data_ORDER_BY = dbtable_case_doc.CASE_Doc_Data_ORDER_BY;
                                    dbinsert_doc.Case_Doc_CaseType_Id = dbtable_case_doc.Case_Doc_CaseType_Id;
                                    dbinsert_doc.Case_Doc_CaseTitleId = (decimal)dbtable_case_doc.Case_Doc_CaseTitleId;
                                    dbinsert_doc.Case_Doc_Format_id = (int)dbtable_case_doc.Case_Doc_Format_id;
                                    dbinsert_doc.Case_Doc_IsAD = dbtable_case_doc.Case_Doc_IsAD;

                                    context.ICORE_CASE_DOCUMENTS.Add(dbinsert_doc);
                                }
                                else
                                {
                                    if (CheckIfExist.Case_Doc_Name == dbtable_case_doc.Case_Doc_Name)
                                        messeage = "A Field With Name " + CheckIfExist.Case_Doc_Name + " Alredy Exist on Document Name " + CheckIfExist.Case_Doc_Name;
                                    if (CheckIfExist.CASE_Doc_Data_ORDER_BY == dbtable_case_doc.CASE_Doc_Data_ORDER_BY)
                                        messeage = "A Field With Name " + CheckIfExist.Case_Doc_Name + " Alredy Exist On Order No " + CheckIfExist.CASE_Doc_Data_ORDER_BY;

                                }
                            }
                            if (messeage == "")
                            {
                                int overall_msg = context.SaveChanges();
                                if (overall_msg > 0)
                                {
                                    messeage = "Data Saved Sucessfully " + overall_msg + " Row affected";
                                    ModelState.Clear();
                                }
                            }
                        }
                        else
                        {
                            messeage = "Please select Valid Case Type and Case Title";
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_CaseDocument", "CaseDocuments", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        ViewBag.Case_Doc_Format_id = new SelectList(context.ICORE_CASE_DOCUMENTS_FORMAT.ToList(), "Format_id", "Format_extension");
                        List<SelectListItem> item = new SelectList(context.ICORE_CASE_TYPES.Where(m => m.CaseType_Status == true && m.CaseType_IsAuth == true).ToList(), "CaseType_Id", "CaseType_Name", 0).ToList();
                        item.Insert(0, (new SelectListItem { Text = "--Select Case Type--", Value = "0" }));
                        ViewBag.Case_Doc_CaseType_Id = item;
                        List<SelectListItem> items = new SelectList(context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Status == true && m.CaseTitle_IsAuth == true).ToList(), "CaseTitle_Id", "CaseTitle_Name", 0).ToList();
                        items.Insert(0, (new SelectListItem { Text = "--Select Case Title--", Value = "0" }));
                        ViewBag.Case_Doc_CaseTitleId = items;
                        ViewBag.messeage = messeage;
                    }
                    return View(dbtable_case_doc);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        public ActionResult Case_Document_View(decimal? Case_Title_ID)
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "dashboard");
            }
            else
            {
                if (DAL.CheckFunctionValidity("Icore_CaseDocument", "CaseDocuments", Session["USER_ID"].ToString()))
                {
                    List<ICORE_CASE_DOCUMENTS_VIEW> data;
                    if (Case_Title_ID != null && Case_Title_ID != 0)
                    {
                        data = context.ICORE_CASE_DOCUMENTS_VIEW.Where(m => m.Case_Doc_CaseTitleId == Case_Title_ID).OrderBy(m => m.CASE_Doc_Data_ORDER_BY).ToList();
                    }
                    else
                    {
                        data = new List<ICORE_CASE_DOCUMENTS_VIEW>();
                    }
                    return PartialView(data);
                }
                else
                    return PartialView(null);
            }
        }
        public JsonResult GetDynamicMenu(string stateId)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_CaseDocument", "CaseDocuments", Session["USER_ID"].ToString()))
                {
                    List<ICORE_CASE_TITLES> lstcity = new List<ICORE_CASE_TITLES>();
                    int stateiD = Convert.ToInt32(stateId);

                    lstcity = (context.ICORE_CASE_TITLES.Where(x => x.CaseTitle_CaseTypeId == stateiD && x.CaseTitle_Status == true && x.CaseTitle_IsAuth == true)).ToList();

                    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    string result = javaScriptSerializer.Serialize(lstcity);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult AuthorizeCaseDocumet(int Case_Doc_Id)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_CaseDocument", "CaseDocuments", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        try
                        {
                            ICORE_CASE_DOCUMENTS UpdateEntity = context.ICORE_CASE_DOCUMENTS.Where(m => m.Case_Doc_Id == Case_Doc_Id).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                if (UpdateEntity.Case_Doc_Data_MakerId != Session["USER_ID"].ToString())
                                {
                                    UpdateEntity.Case_Doc_IsAuth = true;
                                    UpdateEntity.Case_Doc_Data_CheckerId = Session["USER_ID"].ToString();
                                    UpdateEntity.Case_Doc_Data_DataEditDateTime = DateTime.Now;
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        messeage = "Document : " + UpdateEntity.Case_Doc_Name + " is successfully Authorized";
                                    }
                                }
                                else
                                    messeage = "Sorry, Maker cannot authorize the same record.";
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + Case_Doc_Id;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("Icore_CaseDocument", "AuthorizeCaseDocumet", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult DeleteFromAuthorizeCaseDocumet(int Case_Doc_Id)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_CaseDocument", "CaseDocuments", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        try
                        {
                            ICORE_CASE_DOCUMENTS UpdateEntity = context.ICORE_CASE_DOCUMENTS.Where(m => m.Case_Doc_Id == Case_Doc_Id).FirstOrDefault();
                            if (UpdateEntity != null && UpdateEntity.Case_Doc_IsAuth == false)
                            {
                                if (UpdateEntity.Case_Doc_Data_MakerId != Session["USER_ID"].ToString())
                                {
                                    context.ICORE_CASE_DOCUMENTS.Remove(UpdateEntity);
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        messeage = "Document : " + UpdateEntity.Case_Doc_Name + " is Rejected";
                                    }
                                }
                                else
                                    messeage = "Sorry, Maker cannot reject the same record.";
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + Case_Doc_Id;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("Icore_CaseDocument", "DeleteFromAuthorizeCaseDocumet", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult DeleteCaseDocument(int Case_Doc_Id)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_CaseDocument", "CaseDocuments", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string message = "";
                        int RowCount = 0;
                        try
                        {
                            ICORE_CASE_DOCUMENTS DetailEntity = context.ICORE_CASE_DOCUMENTS.Where(m => m.Case_Doc_Id == Case_Doc_Id).FirstOrDefault();
                            if (DetailEntity != null)
                            {
                                context.ICORE_CASE_DOCUMENTS.Remove(DetailEntity);
                                RowCount += context.SaveChanges();
                            }
                            else
                            {
                                message = "Error: Unable to Find Record on the Provided Id ";
                            }
                            if (RowCount > 0)
                            {
                                message = "Successfully deleted form : " + DetailEntity.Case_Doc_Name;
                            }

                            return Json(message);
                        }
                        catch (Exception ex)
                        {
                            message = DAL.LogException("Icore_CaseDocument", "DeleteCaseDocument", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(message);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
    }
}