﻿using FXAutomation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FXAutomation.Controllers
{
    public class Icore_CasetypeController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: Icore_Casetype
        public ActionResult Casetype(string type_id)
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_Casetype", "Casetype", Session["USER_ID"].ToString()))
                {
                    int ID = 0;
                    if (type_id != null)
                        ID = Convert.ToInt32(DAL.Decrypt(type_id));
                    string messeage = "";
                    try
                    {
                        if (ID != 0)
                        {
                            ICORE_CASE_TYPES edit = context.ICORE_CASE_TYPES.Where(m => m.CaseType_Id == ID).FirstOrDefault();
                            if (edit != null)
                                return View(edit);
                            else
                            {
                                messeage = "Problem while getting your id " + ID;
                                return View();
                            }
                        }
                        else
                            return View();
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_Casetype", "Casetype", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        List<SelectListItem> CategoryList = new SelectList(context.ICORE_CATEGORIES.Where(m => m.Categ_Status == true && m.Categ_IsAuth == true).ToList(), "Categ_Id", "Categ_Name").ToList();
                        CategoryList.Insert(0, (new SelectListItem { Text = "--Select Category--", Value = "0" }));
                        ViewBag.CaseType_CategoryId = CategoryList;
                        ViewBag.messeage = messeage;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }                
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [HttpPost]
        public ActionResult Casetype(ICORE_CASE_TYPES dbtable)
        {
            string messeage = "";
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("Icore_Casetype", "Casetype", Session["USER_ID"].ToString()))
                {
                    try
                    {
                        if (dbtable.CaseType_Id != 0)
                        {
                            ICORE_CASE_TYPES update_casetype = context.ICORE_CASE_TYPES.Where(m => m.CaseType_Id == dbtable.CaseType_Id).FirstOrDefault();
                            if (update_casetype != null)
                            {
                                update_casetype.CaseType_Code = dbtable.CaseType_Code;
                                update_casetype.CaseType_Name = dbtable.CaseType_Name;
                                update_casetype.CaseType_Description = dbtable.CaseType_Description;
                                update_casetype.CaseType_Status = dbtable.CaseType_Status;
                                update_casetype.CaseType_DataEditDateTime = DateTime.Now;
                                update_casetype.CaseType_MakerId = Session["USER_ID"].ToString();
                                if (update_casetype.CaseType_MakerId == "Admin123")
                                {
                                    update_casetype.CaseType_CheckerId = update_casetype.CaseType_MakerId;
                                    update_casetype.CaseType_IsAuth = true;
                                }
                                else
                                {
                                    update_casetype.CaseType_CheckerId = null;
                                    update_casetype.CaseType_IsAuth = false;
                                }
                                update_casetype.CaseType_CategoryId = dbtable.CaseType_CategoryId;

                                context.Entry(update_casetype).State = EntityState.Modified;
                            }
                            else
                            {
                                messeage = "Problem occur while getting previous record";
                            }
                        }
                        else
                        {
                            ICORE_CASE_TYPES check_casetype = context.ICORE_CASE_TYPES.Where(m => m.CaseType_Name == dbtable.CaseType_Name || m.CaseType_Code == dbtable.CaseType_Code).FirstOrDefault();
                            if (check_casetype == null)
                            {
                                ICORE_CASE_TYPES insert_casetype = new ICORE_CASE_TYPES();
                                insert_casetype.CaseType_Id = Convert.ToInt32(context.ICORE_CASE_TYPES.Max(m => (decimal?)m.CaseType_Id)) + 1;
                                insert_casetype.CaseType_Code = dbtable.CaseType_Code;
                                insert_casetype.CaseType_Name = dbtable.CaseType_Name;
                                insert_casetype.CaseType_Description = dbtable.CaseType_Description;
                                insert_casetype.CaseType_Status = dbtable.CaseType_Status;
                                insert_casetype.CaseType_DataEntryDateTime = DateTime.Now;
                                insert_casetype.CaseType_MakerId = Session["USER_ID"].ToString();
                                if (insert_casetype.CaseType_MakerId == "Admin123")
                                {
                                    insert_casetype.CaseType_CheckerId = insert_casetype.CaseType_MakerId;
                                    insert_casetype.CaseType_IsAuth = true;
                                }
                                else
                                {
                                    insert_casetype.CaseType_CheckerId = null;
                                    insert_casetype.CaseType_IsAuth = false;
                                }
                                insert_casetype.CaseType_CategoryId = dbtable.CaseType_CategoryId;

                                context.ICORE_CASE_TYPES.Add(insert_casetype);

                            }
                            else
                            {
                                if (check_casetype.CaseType_Name == dbtable.CaseType_Name)
                                { messeage = "The Case type Name " + dbtable.CaseType_Name + " Already Exists"; }
                                else if (check_casetype.CaseType_Code == dbtable.CaseType_Code)
                                { messeage = "The Case type Code " + dbtable.CaseType_Code + " Already Exists"; }
                            }

                        }
                        if (messeage == "")
                        {
                            int overall_msg = context.SaveChanges();
                            if (overall_msg > 0)
                            {
                                messeage = "Data Saved Sucessfully " + overall_msg + "  Row affected";
                                ModelState.Clear();
                            }
                            else
                            {
                                messeage = "Problem While Inserting data " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        messeage = DAL.LogException("Icore_Casetype", "Casetype", Session["USER_ID"].ToString(), ex);
                    }
                    finally
                    {
                        List<SelectListItem> CategoryList = new SelectList(context.ICORE_CATEGORIES.Where(m => m.Categ_Status == true && m.Categ_IsAuth == true).ToList(), "Categ_Id", "Categ_Name").ToList();
                        CategoryList.Insert(0, (new SelectListItem { Text = "--Select Category--", Value = "0" }));
                        ViewBag.CaseType_CategoryId = CategoryList;
                        ViewBag.messeage = messeage;
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public ActionResult Casetype_grid()
        {
            if (Session["USER_ID"] == null)
            {
                return RedirectToAction("Index", "dashboard");
            }
            else
            {
                if (DAL.CheckFunctionValidity("Icore_Casetype", "Casetype", Session["USER_ID"].ToString()))
                {
                    string SessionUser = Session["USER_ID"].ToString();
                    List<ICORE_CASE_TYPES_VIEW> ViewData = context.ICORE_CASE_TYPES_VIEW.Where(m => m.CaseType_IsAuth == true || m.CaseType_MakerId != SessionUser).OrderByDescending(m => m.CaseType_Id).ToList();
                    return PartialView(ViewData);
                }
                else
                    return PartialView(null);
            }
        }
        [HttpPost]
        public JsonResult AuthorizeCaseType(int CaseType_Id)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_Casetype", "Casetype", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        try
                        {
                            ICORE_CASE_TYPES UpdateEntity = context.ICORE_CASE_TYPES.Where(m => m.CaseType_Id == CaseType_Id).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                if (UpdateEntity.CaseType_MakerId != Session["USER_ID"].ToString())
                                {
                                    UpdateEntity.CaseType_IsAuth = true;
                                    UpdateEntity.CaseType_CheckerId = Session["USER_ID"].ToString();
                                    UpdateEntity.CaseType_DataEditDateTime = DateTime.Now;
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        messeage = "Case Type : " + UpdateEntity.CaseType_Name + " is successfully Authorized";
                                    }
                                }
                                else
                                {
                                    messeage = "Maker cannot authorize the same record.";
                                    return Json(messeage);
                                }
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + CaseType_Id;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("Icore_Casetype", "AuthorizeCaseType", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
        [HttpPost]
        public JsonResult DeleteFromAuthorizeCaseType(int CaseType_Id)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                if (DAL.CheckFunctionValidity("Icore_Casetype", "Casetype", Session["USER_ID"].ToString()))
                {
                    if (DAL.CheckRequestedUserValidity(Session["USER_ID"].ToString(), Session["UNIQUE_ID"].ToString()))
                    {
                        string messeage = "";
                        try
                        {
                            ICORE_CASE_TYPES UpdateEntity = context.ICORE_CASE_TYPES.Where(m => m.CaseType_Id == CaseType_Id).FirstOrDefault();
                            if (UpdateEntity != null && UpdateEntity.CaseType_IsAuth == false)
                            {
                                if (UpdateEntity.CaseType_MakerId != Session["USER_ID"].ToString())
                                {
                                    context.ICORE_CASE_TYPES.Remove(UpdateEntity);
                                    int RowCount = context.SaveChanges();
                                    if (RowCount > 0)
                                    {
                                        messeage = "Case Type :" + UpdateEntity.CaseType_Name + " is Rejected";
                                    }
                                }
                                else
                                {
                                    messeage = "Maker cannot reject the same record.";
                                    return Json(messeage);
                                }
                            }
                            else
                            {
                                messeage = "Problem while fetching your record on ID# " + CaseType_Id;
                                return Json(messeage);
                            }
                        }
                        catch (Exception ex)
                        {
                            messeage = DAL.LogException("Icore_Casetype", "DeleteFromAuthorizeCaseType", Session["USER_ID"].ToString(), ex);
                        }
                        return Json(messeage);
                    }
                    else
                        return Json("Invalid request");
                }
                else
                {
                    return Json("Invalid request");
                }
            }
            else
                return Json("Invalid request");
        }
    }
}