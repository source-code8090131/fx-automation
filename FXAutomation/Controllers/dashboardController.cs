﻿using FXAutomation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FXAutomation.Controllers
{
    public class dashboardController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: dashboard
        public ActionResult Index()
        {
            if (Session["USER_ID"] != null)
            {
                if (DAL.CheckFunctionValidity("dashboard", "Index", Session["USER_ID"].ToString()))
                {
                    string UserID = Session["USER_ID"].ToString();
                    int?[] RoleIds = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_USER_ID == UserID && m.ALR_STATUS == true).Select(m => m.ALR_ROLE_ID).ToArray();
                    RoleIds = context.ICORE_ROLES.Where(m => RoleIds.Contains(m.R_ID) && m.R_STATUS == "true" && m.R_ISAUTH == true).Select(m => (int?)m.R_ID).ToArray();
                    int?[] FunctionIds = context.ICORE_ASSIGN_FUNCTIONS.Where(m => RoleIds.Contains(m.AF_ROLE_ID) && m.AF_STATUS == true).Select(m => m.AF_FUNCTION_ID).Distinct().ToArray();
                    List<ICORE_FUNCTIONS> menu = context.ICORE_FUNCTIONS.Where(m => FunctionIds.Contains(m.F_ID)).Distinct().ToList();
                    return PartialView(menu);
                }
                else
                {
                    return RedirectToAction("UnAuthorizedUrl", "dashboard");
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        [ChildActionOnly]
        public PartialViewResult Menu()
        {
            if (Session["USER_ID"] != null)
            {
                string UserID = Session["USER_ID"].ToString();
                int?[] RoleIds = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_USER_ID == UserID && m.ALR_STATUS == true).Select(m => m.ALR_ROLE_ID).ToArray();
                RoleIds = context.ICORE_ROLES.Where(m => RoleIds.Contains(m.R_ID) && m.R_STATUS == "true" && m.R_ISAUTH == true).Select(m => (int?)m.R_ID).ToArray();
                int?[] FunctionIds = context.ICORE_ASSIGN_FUNCTIONS.Where(m => RoleIds.Contains(m.AF_ROLE_ID) && m.AF_STATUS == true).Select(m => m.AF_FUNCTION_ID).Distinct().ToArray();
                List<ICORE_FUNCTIONS> menu = context.ICORE_FUNCTIONS.Where(m => FunctionIds.Contains(m.F_ID)).Distinct().ToList();
                return PartialView(menu);
            }
            else
            {
                return PartialView();
            }
        }
        [ChildActionOnly]
        public ActionResult UnAuthorizedUrl()
        {
            return View();
        }
    }
}