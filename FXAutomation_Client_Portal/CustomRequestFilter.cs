﻿//#define DEV
#define PROD
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FXAutomation_Client_Portal
{
    public class CustomRequestFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
#if DEV
            filterContext.HttpContext.Session["USER_ID"] = "laraib.shah.g69@gmail.com";
            filterContext.HttpContext.Session["USER_NAME"] = "Laraib Shah";
            filterContext.HttpContext.Session["USER_PARTY_ID"] = "456789";
            base.OnActionExecuting(filterContext);
            return;
#endif

#if PROD
            try
            {
                System.Collections.Specialized.NameValueCollection headers = filterContext.HttpContext.Request.Headers;
                string userid = "";
                string sigtime = "";
                string sig = "";
                string sigkey = "";
                if (headers["x-citiportal-LoginID"] != null)
                    userid = headers["x-citiportal-LoginID"].ToString();
                else
                {
                    filterContext.HttpContext.Session["ERRORH"] = "Insuficient Headers Or Invalid Request";
                    filterContext.HttpContext.Session["ERRORM"] = "Please try to Login Again from citidirect portal, " + Environment.NewLine + "Details : No header exist in the request for LoginID";
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "fxdig",
                        action = "Error"
                    }));
                    Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
                    base.OnActionExecuting(filterContext);
                    return;
                }
                if (headers["x-citiportal-sigkey"] != null)
                    sigkey = headers["x-citiportal-sigkey"].ToString();
                else
                {
                    filterContext.HttpContext.Session["ERRORH"] = "Insuficient Headers Or Invalid Request";
                    filterContext.HttpContext.Session["ERRORM"] = "Please try to Login Again from citidirect portal, " + Environment.NewLine + "Details : No header exist in the request for sig key";
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "fxdig",
                        action = "Error"
                    }));
                    Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
                    base.OnActionExecuting(filterContext);
                    return;
                }
                if (headers["x-citiportal-authtime"] != null)
                    sigtime = headers["x-citiportal-authtime"].ToString();
                else
                {
                    filterContext.HttpContext.Session["ERRORH"] = "Insuficient Headers Or Invalid Request";
                    filterContext.HttpContext.Session["ERRORM"] = "Please try to Login Again from citidirect portal, " + Environment.NewLine + "Details : No header exist in the request for sig time";
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "fxdig",
                        action = "Error"
                    }));
                    Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
                    base.OnActionExecuting(filterContext);
                    return;
                }
                if (headers["x-citiportal-sig"] != null)
                    sig = headers["x-citiportal-sig"].ToString();
                else
                {
                    filterContext.HttpContext.Session["ERRORH"] = "Insuficient Headers Or Invalid Request";
                    filterContext.HttpContext.Session["ERRORM"] = "Please try to Login Again from citidirect portal, " + Environment.NewLine + "Details : No header exist in the request for signature";
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "fxdig",
                        action = "Error"
                    }));
                    Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
                    base.OnActionExecuting(filterContext);
                    return;
                }

                ProxyAuthentication proxy = new ProxyAuthentication();
                if (proxy.VerifyData(userid, sig, sigkey, sigtime))
                {
                    if (headers["x-citiportal-emailaddr"] != null)
                    {
                        string email = headers["x-citiportal-emailaddr"].ToString();
                        if (headers["x-citiportal-clientpartyid"] != null)
                        {
                            string partyId = headers["x-citiportal-clientpartyid"].ToString();
                            Models.FXDIGEntities context = new Models.FXDIGEntities();
                            Models.ICORE_CLIENT_PORTFOLIO CheckPartyID = context.ICORE_CLIENT_PORTFOLIO.Where(m => m.CP_PARTY_ID == partyId).FirstOrDefault();
                            if (CheckPartyID != null)
                            {
                                Models.ICORE_CLIENT_PORTFOLIO CheckClientEmail = context.ICORE_CLIENT_PORTFOLIO.Where(m => m.CP_EMAIL == email && m.CP_PARTY_ID == CheckPartyID.CP_PARTY_ID).FirstOrDefault();
                                if (CheckClientEmail != null)
                                {
                                    filterContext.HttpContext.Session["USER_ID"] = CheckClientEmail.CP_EMAIL;
                                    filterContext.HttpContext.Session["USER_NAME"] = CheckClientEmail.CP_NAME;
                                    filterContext.HttpContext.Session["USER_PARTY_ID"] = CheckClientEmail.CP_PARTY_ID;
                                    Log(filterContext, "Success");
                                }
                                else
                                {
                                    Models.ICORE_CLIENT_PORTFOLIO UpdateEntity = new Models.ICORE_CLIENT_PORTFOLIO();
                                    UpdateEntity = CheckPartyID;
                                    UpdateEntity.CP_EMAIL = email;
                                    UpdateEntity.CP_NAME = GetClientNameFromEmail(email);
                                    UpdateEntity.CP_EDIT_DATETIME = DateTime.Now;
                                    context.Entry(UpdateEntity).State = System.Data.Entity.EntityState.Modified;
                                    int CheckResult = context.SaveChanges();
                                    if (CheckResult > 0)
                                    {
                                        filterContext.HttpContext.Session["USER_ID"] = UpdateEntity.CP_EMAIL;
                                        filterContext.HttpContext.Session["USER_NAME"] = UpdateEntity.CP_NAME;
                                        filterContext.HttpContext.Session["USER_PARTY_ID"] = UpdateEntity.CP_PARTY_ID;
                                        Log(filterContext, "Success");
                                    }
                                    else
                                    {
                                        filterContext.HttpContext.Session["ERRORH"] = "Exception Occured";
                                        filterContext.HttpContext.Session["ERRORM"] = "Please try to Login Again from citidirect portal, " + Environment.NewLine + "Details :  Unable to update client email";
                                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                                        {
                                            controller = "fxdig",
                                            action = "Error"
                                        }));
                                        Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
                                        base.OnActionExecuting(filterContext);
                                        return;
                                    }
                                }
                            }
                            else
                            {
                                Models.ICORE_CLIENT_PORTFOLIO Entity = new Models.ICORE_CLIENT_PORTFOLIO();
                                Entity.CP_ID = Convert.ToInt32(context.ICORE_CLIENT_PORTFOLIO.Max(m => m.CP_ID)) + 1;
                                Entity.CP_NAME = GetClientNameFromEmail(email);
                                Entity.CP_EMAIL = email;
                                Entity.CP_PARTY_ID = partyId;
                                Entity.CP_MAKER_ID = "FXDIG";
                                Entity.CP_ISAUTH = true;
                                Entity.CP_STATUS = "Active";
                                Entity.CP_ENTRY_DATETIME = DateTime.Now;
                                context.ICORE_CLIENT_PORTFOLIO.Add(Entity);
                                int CheckResult = context.SaveChanges();
                                if (CheckResult > 0)
                                {
                                    filterContext.HttpContext.Session["USER_ID"] = Entity.CP_EMAIL;
                                    filterContext.HttpContext.Session["USER_NAME"] = Entity.CP_NAME;
                                    filterContext.HttpContext.Session["USER_PARTY_ID"] = Entity.CP_PARTY_ID;
                                    Log(filterContext, "Success");
                                }
                                else
                                {
                                    filterContext.HttpContext.Session["ERRORH"] = "Exception Occured";
                                    filterContext.HttpContext.Session["ERRORM"] = "Please try to Login Again from citidirect portal, " + Environment.NewLine + "Details : Unable to add client on Party ID : " + partyId;
                                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                                    {
                                        controller = "fxdig",
                                        action = "Error"
                                    }));
                                    Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
                                    base.OnActionExecuting(filterContext);
                                    return;
                                }
                            }
                        }
                        else
                        {
                            filterContext.HttpContext.Session["ERRORH"] = "Insuficient Headers Or Invalid Request";
                            filterContext.HttpContext.Session["ERRORM"] = "Please try to Login Again from citidirect portal, " + Environment.NewLine + "Details : No header exist in the request for partyId";
                            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                            {
                                controller = "fxdig",
                                action = "Error"
                            }));
                            Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
                            base.OnActionExecuting(filterContext);
                            return;
                        }
                    }
                    else
                    {
                        filterContext.HttpContext.Session["ERRORH"] = "Insuficient Headers Or Invalid Request";
                        filterContext.HttpContext.Session["ERRORM"] = "Please try to Login Again from citidirect portal, " + Environment.NewLine + "Details : No header exist in the request for email";
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                        {
                            controller = "fxdig",
                            action = "Error"
                        }));
                        Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
                        base.OnActionExecuting(filterContext);
                        return;
                    }
                }
                else
                {
                    filterContext.HttpContext.Session["ERRORH"] = "Unable to Verify Signature";
                    filterContext.HttpContext.Session["ERRORM"] = "Please try to Login Again from citidirect portal, " + Environment.NewLine + "Details : The Proxy Authentication Method VerifySignedData returned false";
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "fxdig",
                        action = "Error"
                    }));
                    Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
                    base.OnActionExecuting(filterContext);
                    return;
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                filterContext.HttpContext.Session["ERRORH"] = "Exception Occured";
                filterContext.HttpContext.Session["ERRORM"] = message;
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "fxdig",
                    action = "Error"
                }));
                Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
            }
#endif
        }

        public string GetClientNameFromEmail(string Email = "")
        {
            string Client_Name = "";
            string UserName = (Email).Split('@')[0];
            Client_Name = Regex.Replace(UserName, @"[^a-zA-Z]+", " ");
            return Client_Name;
        }

        public void Log(ActionExecutingContext requestcontext, string message)
        {
            Models.FXDIGEntities cont = new Models.FXDIGEntities();
            Models.ICORE_CLIENT_REQUESTS_LOG log = new Models.ICORE_CLIENT_REQUESTS_LOG();
            log.CRL_DATETIME = DateTime.Now;
            string RequestRaw = "Request : " + Environment.NewLine;
            RequestRaw += "Request Method : " + requestcontext.HttpContext.Request.HttpMethod + ", RequestUri : " + requestcontext.HttpContext.Request.Url.ToString() + Environment.NewLine;
            string headers = "";
            foreach (string key in requestcontext.HttpContext.Request.Headers)
                headers += key + "=" + requestcontext.HttpContext.Request.Headers[key] + Environment.NewLine;
            RequestRaw += "Request Headers : { " + Environment.NewLine + headers + " }" + Environment.NewLine;
            string Resheaders = "";
            foreach (string key in requestcontext.HttpContext.Response.Headers)
                Resheaders += key + "=" + requestcontext.HttpContext.Response.Headers[key] + Environment.NewLine;
            RequestRaw += "Response Headers : { " + Environment.NewLine + Resheaders + " }" + Environment.NewLine;
            log.CRL_REQUEST_RECIEVED = RequestRaw;
            log.CRL_SYSTEM_RESPONSE = message;
            if (message == "Success")
                log.CRL_STATUS = true;
            else
                log.CRL_STATUS = false;
            cont.ICORE_CLIENT_REQUESTS_LOG.Add(log);
            cont.SaveChanges();
        }


    }
}