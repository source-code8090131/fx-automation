﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FXAutomation_Client_Portal.Models
{
    public class EditSubmittedForm
    {
        public ICORE_SUBMIT_CASE_FORM_MASTER Master { get; set; }
        public IEnumerable<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW> FieldList { get; set; }
        public IEnumerable<ICORE_SUBMIT_CASE_DOCUMENT_VIEW> DocumentList { get; set; }
    }
}