﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FXAutomation_Client_Portal.Models
{
    public class EditSavedForm
    {
        public ICORE_SUBMIT_CASE_FORM_MASTER_DRAFT Master { get; set; }
        public IEnumerable<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_DRAFT_VIEW> FieldList { get; set; }
        public IEnumerable<ICORE_SUBMIT_CASE_DOCUMENT_DRAFT_VIEW> DocumentList { get; set; }
    }
}