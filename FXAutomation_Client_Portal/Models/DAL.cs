﻿//#define DEV
#define prod

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net;
using System.Security.Cryptography;
using System.Net.Http;
using System.Net.Http.Headers;

namespace FXAutomation_Client_Portal.Models
{
    public class DAL
    {


        public static string message = "";
        public static dynamic LogCurrentRequest;

        #region Encrypt N Decrypt
        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        #endregion

         #region GCARVALIDATION

        /// With Certificate Path Provided
        public static HttpResponseMessage SendRequestGCAR_Validation_WithCertFilePath(string CLIENT_ID, string IBAN_ACCTNO, string UNIQUE_REF, string URL, string CERT_FILE_PATH, string PassHash)
        {
            message = "";
            try
            {
                #if prod
                 string BASE_URL = URL;
                #endif

                #if DEV
                 string BASE_URL = "http://localhost/CITI_API/api/dealers/citi/edi/";
                #endif
                GCARValidation RequestData = new GCARValidation();
                RequestData.CLNT_ID = CLIENT_ID;
                RequestData.IBAN_ACCT_NO = IBAN_ACCTNO;
                RequestData.UNIQUE_REF = UNIQUE_REF;
                if (RequestData != null)
                {
                    using (HttpClientHandler clientHandler = new HttpClientHandler())
                    {
                        #if prod
                        var certificate = new System.Security.Cryptography.X509Certificates.X509Certificate2(CERT_FILE_PATH, PassHash);
                        clientHandler.ClientCertificates.Add(certificate);
                        clientHandler.ClientCertificateOptions = ClientCertificateOption.Manual;
                        #endif

                        using (HttpClient client = new HttpClient(clientHandler))
                        {
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            using (HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, BASE_URL))
                            {
                                client.DefaultRequestHeaders.ExpectContinue = false;
                                message += " Request URL : " + BASE_URL + Environment.NewLine;
                                req.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                string dataAsJson = System.Text.Json.JsonSerializer.Serialize(RequestData);
                                message += " Request DATA IN AS JSON : " + dataAsJson + Environment.NewLine;
                                var content = new StringContent(dataAsJson, Encoding.UTF8, "application/json");
                                req.Content = content;
                                req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                HttpResponseMessage responsse = client.SendAsync(req).Result;
                                return responsse;
                            }
                        }
                    }
                }
                else
                {
                    return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.ExpectationFailed, ReasonPhrase = "Exception : Problem while creating data for this client" };
                }

            }
            catch (Exception ex)
            {
                string exceptionMessage = "Exception Message : " + ex.Message;
                if (ex.InnerException != null)
                {
                    exceptionMessage += " , Inner Exception : " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                        exceptionMessage += " , Inner Exception Details : " + ex.InnerException.InnerException;
                }
                return new HttpResponseMessage { StatusCode = System.Net.HttpStatusCode.ExpectationFailed, Content = new StringContent(exceptionMessage) };
            }
        }
        public static string SendGidaRequestWithCertPath(string CLIENT_ID, string IBAN_ACCTNO, string UNIQUE_REF, string URL, string CERT_FILE_PATH, string PassHash)
        {
            HttpResponseMessage response = SendRequestGCAR_Validation_WithCertFilePath(CLIENT_ID, IBAN_ACCTNO, UNIQUE_REF, URL, CERT_FILE_PATH, PassHash);
            int StatusCode = (int)response.StatusCode;
            //string StatusDesc = response.StatusCode.ToString();            

            int attempCount = 0;
            while (StatusCode != 200 && attempCount <= 10)
            {
                response = SendRequestGCAR_Validation_WithCertFilePath(CLIENT_ID, IBAN_ACCTNO, UNIQUE_REF, URL, CERT_FILE_PATH, PassHash);
                StatusCode = (int)response.StatusCode;
                attempCount += 1;
            }
            if (StatusCode == 200)
            {
                if (response.Content != null)
                {
                    var ResponseData = response.Content.ReadAsStringAsync().Result;
                    if (ResponseData != null && ResponseData != "")
                    {
                        dynamic responsedataAsObject = System.Web.Helpers.Json.Decode(ResponseData);
                        if (responsedataAsObject != null)
                        {
                            if (responsedataAsObject.VALIDATION_STAT != null)
                            {
                                string STATUS = responsedataAsObject.VALIDATION_STAT;
                                return "VALIDATION_STAT = " + STATUS;
                            }
                            else
                                return "Error while reading VALIDATION_STAT from GCAR response";
                        }
                        else
                            return "Error while Decoding JSON from GCAR - " + ResponseData.ToString();
                    }
                    else
                        return "Error while reading response Content";
                }
                else
                    return "Request Was Successfull, but no content found in the response.";
            }                    
            else
            {
                string returningmessage = "GCAR Request Failed" + Environment.NewLine;//"Request Details : " + Environment.NewLine;
                if (response.Content != null)
                {
                    var ResponseData = response.Content.ReadAsStringAsync().Result;
                    returningmessage += " Response Code : " + StatusCode + " Response Data : " + ResponseData + Environment.NewLine;
                }
                return returningmessage;
            }
        }
       
        #endregion

        #region Generate S-PRC Number
        public static string GenerateSPRCNumber()
        {
            int y = DateTime.Now.Year;
            int m = DateTime.Now.Month;
            int d = DateTime.Now.Day;
            string PRCNumber = "";
            int daysCount = 0;
            for (int i = 1; i <= m; i++)
            {
                string getDate = "";
                if (i > 9)
                    getDate = i + "/" + (d) + "/" + y;
                else
                    getDate = "0" + i + "/" + (d) + "/" + y;
                DateTime gDate = DateTime.Now;

                if (i == 2)
                {
                    string getDateX = "0" + i + "/" + 28 + "/" + y;
                    gDate = Convert.ToDateTime(getDateX);
                }
                else
                {
                    if (d == 31 && (i == 4 || i == 6 || i == 9 || i == 11))
                    {
                        getDate = "0" + i + "/" + 30 + "/" + y;
                    }
                    gDate = Convert.ToDateTime(getDate);
                }

                int DaysInMonth = 0;

                if (i != m)
                {
                    DaysInMonth = DateTime.DaysInMonth(gDate.Year, gDate.Month);
                }
                else
                {
                    DateTime getCurrentDate = DateTime.Now;
                    DaysInMonth = Convert.ToDateTime(getCurrentDate).Day;
                }
                daysCount = daysCount + DaysInMonth;
            }
            int getdtRowCount = 0;
            string TestsubStringx = "000";
            int TestlengValx = Convert.ToInt32(Convert.ToString(daysCount).Length);
            string TestgetValdaysCount = TestsubStringx.Substring(0, (3 - TestlengValx)) + daysCount;
            string CheckPRCNumber = "CITI-SPRC-" + DateTime.Now.ToString("yyyy") + "-" + TestgetValdaysCount;
            FXDIGEntities context = new FXDIGEntities();
            PRC_REQUEST_LOG Entity = context.PRC_REQUEST_LOG.Where(x => x.PRL_UNIQUE_NO.Contains(CheckPRCNumber)).OrderByDescending(x => x.PRL_UNIQUE_NO).FirstOrDefault();
            if (Entity != null)
            {
                string getMaxValue = Entity.PRL_UNIQUE_NO;
                if (!string.IsNullOrEmpty(getMaxValue))
                {
                    string getVals = getMaxValue.Substring(18, 6);

                    int getMaxVal = Convert.ToInt32(getVals);

                    getdtRowCount = getMaxVal + 1;
                }
                else
                {
                    getdtRowCount = 1;
                }
            }
            else
            {
                getdtRowCount = 1;
            }
            string subStringx = "000";
            int lengValx = Convert.ToInt32(Convert.ToString(daysCount).Length);
            string getValdaysCount = subStringx.Substring(0, (3 - lengValx)) + daysCount;
            int lengVal = Convert.ToInt32(Convert.ToString(getdtRowCount).Length);
            string subString = "000000";
            string getVal = subString.Substring(0, (6 - lengVal));
            PRCNumber = "CITI-SPRC-" + DateTime.Now.ToString("yyyy") + "-" + getValdaysCount + getVal + getdtRowCount;
            return PRCNumber;
        }
        #endregion

        #region Generate Log Request Number
        public static string GenerateRequestNo()
        {
            FXDIGEntities context = new FXDIGEntities();

            int getdtRowCount = 0;
            string CheckPRCNumber = "REQ-" + DateTime.Now.ToString("ddMMyyyy") + "-";
            PRC_REQUEST_LOG Entity = context.PRC_REQUEST_LOG.Where(x => x.PRL_REQUEST_NO.Contains(CheckPRCNumber)).OrderByDescending(x => x.PRL_REQUEST_NO).FirstOrDefault();
            if (Entity != null)
            {
                string getMaxValue = Entity.PRL_REQUEST_NO;
                if (!string.IsNullOrEmpty(getMaxValue))
                {
                    string getVals = getMaxValue.Substring(13, 6);

                    int getMaxVal = Convert.ToInt32(getVals);

                    getdtRowCount = getMaxVal + 1;
                }
                else
                {
                    getdtRowCount = 1;
                }
            }
            else
            {
                getdtRowCount = 1;
            }
            CheckPRCNumber += string.Format("{0:000000}", getdtRowCount);
            return CheckPRCNumber;
        }
        #endregion

        #region Log Every Exception
        public static string LogException(string controllername, string functionName, string userId, Exception excep)
        {
            try
            {
                string path = System.Web.Hosting.HostingEnvironment.MapPath("~") + "FXDIG_CLIENT_logs\\";
                string filename = DateTime.Now.Date.Day + "_" + DateTime.Now.Date.Month + "_" + DateTime.Now.Date.Year + ".txt";
                bool bool_icorelogs = false; bool bool_icorelogs_rps_logs = false;
                string complete_path = path + filename;
                if (System.IO.Directory.Exists(path))
                {
                    bool_icorelogs = true;
                }
                else
                {
                    System.IO.DirectoryInfo dr = System.IO.Directory.CreateDirectory(path);
                    bool_icorelogs = true;
                }

                if (bool_icorelogs)
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(complete_path, System.IO.FileMode.Append, System.IO.FileAccess.Write))
                    {
                        using (System.IO.StreamWriter writer = new System.IO.StreamWriter(fs))
                        {
                            writer.WriteLine("---------------------------------------------------------------");
                            writer.WriteLine("Time of logging: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());
                            writer.WriteLine("Class/Form/Report Name: " + controllername);
                            writer.WriteLine("Function Name: " + functionName);
                            writer.WriteLine("User ID: " + userId);
                            if (excep.InnerException != null)
                            {
                                writer.WriteLine("Inner Exception: " + excep.InnerException);
                                if (excep.InnerException.Message != null)
                                    writer.WriteLine("      Inner Exception Message : " + excep.InnerException.Message);

                                if (excep.InnerException.InnerException != null)
                                {
                                    writer.WriteLine("Inner Inner Exception: " + excep.InnerException.InnerException);
                                    if (excep.InnerException.InnerException.Message != null)
                                        writer.WriteLine("      Inner Inner Exception Message : " + excep.InnerException.InnerException.Message);
                                }
                            }
                            writer.WriteLine("Exception Description:");
                            writer.WriteLine(excep.Message);
                            writer.WriteLine("Exception Stack Trace:");
                            writer.WriteLine(excep.StackTrace);
                            writer.WriteLine("--------------------------------------------------------------- ");
                            writer.Close();
                        }
                    }
                }


                return "Some error occured, please contact software administrator.";

            }
            catch (Exception ex)
            {
                string message = "Exception Occured " + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                }
                return message;
            }
        }
        #endregion

    }
}