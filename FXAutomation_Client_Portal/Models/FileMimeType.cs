﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;

namespace FXAutomation_Client_Portal.Models
{
    public class FileMimeType
    {
        [DllImport(@"urlmon.dll", CharSet = CharSet.Auto)]
        public extern static System.UInt64 FindMimeFromData(System.UInt64 pBC,
            [MarshalAs(UnmanagedType.LPStr)] System.String pwzUrl,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pBuffer,
            System.UInt64 cbSize, [MarshalAs(UnmanagedType.LPStr)] System.String pwzMimeProposed,
            System.UInt64 dwMimeFlags,
            out System.UInt64 ppwzMimeOut,
            System.UInt64 dwReserverd);
    }
}