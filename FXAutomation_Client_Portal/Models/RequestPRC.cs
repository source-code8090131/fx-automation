﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FXAutomation_Client_Portal.Models
{
    public class RequestPRC
    {
        // PRC Fields
        [Required(ErrorMessage = "IBAN Number is required !!")]
        public string IBAN_NO { get; set; }
        [Required(ErrorMessage = "From Date is required !!")]
        public DateTime? FROM_DATE { get; set; }
        [Required(ErrorMessage = "To Date is required !!")]
        public DateTime? TO_DATE { get; set; }
        public string PaymentType { get; set; }
        public string TRANS_REF { get; set; }
        public string PRC_REF { get; set; }
        // E-PRC Fields
        public bool ePRC { get; set; }
        public bool sPRC { get; set; }
        [Required(ErrorMessage = "e-PRC/s-PRC Unique ID is required !!")]
        public string UNIQUE_ID_NO { get; set; }
        [Required(ErrorMessage = "Date of Issuance is required !!")]
        public DateTime? DATE_OF_ISSUANCE { get; set; }
        public string AMOUNT_IN_PKR { get; set; }
        public string VERIFICAION_AGENCY { get; set; }
        public string DEPARTMENT_FIELD_FORMATION { get; set; }
        [Required(ErrorMessage = "Name Of Verifying Officer is required !!")]
        public string VERIFYING_OFFICER { get; set; }
        [Required(ErrorMessage = "CNIC Number is required !!")]
        [RegularExpression("^[0-9]{5}-[0-9]{7}-[0-9]$", ErrorMessage = "CNIC No must follow the XXXXX-XXXXXXX-X format!")]
        public string CNIC_NO { get; set; }
        public string PURPOSE_OF_VERIFICATION { get; set; }
        public string EMAIL_ADDRESS { get; set; }
    }
}