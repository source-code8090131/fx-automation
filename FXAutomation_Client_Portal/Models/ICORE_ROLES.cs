//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FXAutomation_Client_Portal.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ICORE_ROLES
    {
        public int R_ID { get; set; }
        public string R_NAME { get; set; }
        public string R_DESCRIPTION { get; set; }
        public string R_STATUS { get; set; }
        public string R_MAKER_ID { get; set; }
        public string R_CHECKER_ID { get; set; }
        public Nullable<System.DateTime> R_DATA_ENTRY_DATETIME { get; set; }
        public Nullable<System.DateTime> R_DATA_EDIT_DATETIME { get; set; }
        public Nullable<bool> R_ISAUTH { get; set; }
    }
}
