﻿//#region Custom Layout Js
$(function () {
    GetEPFPortalFrameSrc();
    GetClientHomeUrl();
    //GetLogoutUrl();
    $("#example1").DataTable({
        "responsive": true, "lengthChange": true, "autoWidth": false, "order": false, "lengthMenu": [[5, 50, 500, 5000, -1], [5, 50, 500, 5000, "All"]],
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    })
    if ($('[name="ePRC"]').is(':checked')) {
        $("#sPRC").prop("checked", false);
        $("#astuniqueno").show();
        $("#astamount").show();
        $("#AMOUNT_IN_PKR").prop("disabled", false);
    }
    else if ($('[name="sPRC"]').is(':checked')) {
        $("#ePRC").prop("checked", false);
        $("#astuniqueno").show();
        $("#astamount").hide();
        $("#AMOUNT_IN_PKR").prop("disabled", true);
    }
});
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});
$(function () {
    bsCustomFileInput.init();
});
//#endregion

//#region Get Client Home Url
function GetClientHomeUrl() {
    $.ajax({
        url: "/FXDIG/fxdig/GetURLForHomeRedirect",
        type: "GET",
        processData: false,
        contentType: false,
        success: function (r) {
            if (r != null || r != "") {
                $('.GetHomeUrl').attr('href', r);
            }
            else {
                alert("No data found in the system setup.");
            }
        }
    });
}
//#endregion

//#region Get EPF Portal Frame Src
function GetEPFPortalFrameSrc() {
    $.ajax({
        url: "/FXDIG/fxdig/GetEPFPortalFrameSrc",
        type: "GET",
        processData: false,
        contentType: false,
        success: function (r) {
            if (r != null || r != "") {
                $('#iframeClientPortal').prop('src', r);
            }
            else {
                alert("No data found in the system setup.");
            }
        }
    });

}
//#endregion

$('body').click(function () {
    var iframe = document.getElementById('iframeClientPortal');
    var iframeDoc = iframe.contentDocument;
    if (iframeDoc != null) {
        if (iframeDoc.readyState == 'complete') {
            ExtendPortalSession();
        }
    }
});



//#region Extend Portal Session
function ExtendPortalSession() {
    var portalURL = "";
    $.ajax({
        url: "/FXDIG/fxdig/GetEPFPortalURI",
        type: "GET",
        success: function (r) {
            if (r != null || r != "") {
                portalURL = r;
                var ifrm = document.getElementById("iframeClientPortal");
                var data = new Object();
                data.extendServerSession = true;
                data.extendClientSession = true;
                ifrm.contentWindow.postMessage(JSON.stringify({ name: 'portal.session.extendSSO', body: data }), portalURL);
            }
            else {
                alert("No data found in the System Setup.");
            }
        }
    });
}
//#endregion


//#region Logout Button
$("[name='Logout_btn']").click(function (button) {
    window.location.href = "/FXDIG/fxdig/Logout";
});
//#endregion

//#region CustomMessage
$(function () {
    if ($("#CustomMessagePrc").length > 0) {
        if ($("#CustomMessagePrc").val() != null && $("#CustomMessagePrc").val() != "") {
            Swal.fire({
                icon: 'warning',
                showCloseButton: true,
                title: '',
                confirmButtonText:
                    '<a href="/FXDIG/PRC" class="text-white"> OK </a>',
                confirmButtonAriaLabel: ' OK ',
                text: $("#CustomMessagePrc").val()
            })
        }
    }
    if ($("#CustomMessageePrc").length > 0) {
        if ($("#CustomMessageePrc").val() != null && $("#CustomMessageePrc").val() != "") {
            Swal.fire({
                icon: 'warning',
                showCloseButton: true,
                title: '',
                confirmButtonText:
                    '<a href="/FXDIG/ePRC" class="text-white"> OK </a>',
                confirmButtonAriaLabel: ' OK ',
                text: $("#CustomMessageePrc").val()
            })
        }
    }
});
//#endregion

$("[name='ePRC']").click(function (button) {
    $("#sPRC").prop("checked", false);
    $("#astuniqueno").show();
    $("#astamount").show();
    $("#AMOUNT_IN_PKR").prop("disabled", false);
});
$("[name='sPRC']").click(function (button) {
    $("#ePRC").prop("checked", false);
    $("#astuniqueno").show();
    $("#astamount").hide();
    $("#AMOUNT_IN_PKR").prop("disabled", true);
});