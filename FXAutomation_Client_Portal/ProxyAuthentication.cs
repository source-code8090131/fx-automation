﻿using FXAutomation_Client_Portal.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;

namespace FXAutomation_Client_Portal
{
    public class ProxyAuthentication
    {
        private Dictionary<string, X509Certificate> certificateCache = new Dictionary<string, X509Certificate>();

        /**
         * Given an X509 cert name (sigkey), verify that the digital signature (base64signatureString)
         * is produced by the signer associated with the cert and that the supplied
         * userid and signTime were signed.
         */
        public Boolean VerifySignedData(string sigkey, string userid, string signTime, string base64signatureString)
        {
            return VerifySignedData(GetCertificate(sigkey), userid, signTime, base64signatureString);
        }

        private X509Certificate GetCertificate(string sigkey)
        {
            X509Certificate result = certificateCache.Count > 0 ? certificateCache[sigkey] : null;
            if (result == null)
            {
                using (FXDIGEntities context = new FXDIGEntities())
                {
                    ICORE_SYSTEM_SETUP SystemData = new ICORE_SYSTEM_SETUP();
                    SystemData = context.ICORE_SYSTEM_SETUP.Where(m => m.SS_ID == 1).FirstOrDefault();
                    string url = SystemData.SS_PROXY_SIGNIN_URL;
                    result = ReadCertificate(url);
                    if (result != null)
                    {
                        certificateCache.Add(sigkey, result);
                    }
                }
            }
            return result;
        }

        /**
         * Read X509Certificate from given URL
         */
        private static X509Certificate ReadCertificate(string url)
        {
            X509CertificateParser certParser = new X509CertificateParser();
            Stream contentStream = GetContentStream(url);
            X509Certificate cert = certParser.ReadCertificate(contentStream);
            contentStream.Close();
            return cert;
        }

        private static Boolean VerifySignedData(X509Certificate x509CertFromPem, string userid, string signTime, string base64signatureString)
        {
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();

            byte[] signatureBytes = System.Convert.FromBase64String(base64signatureString);
            ISigner sig = SignerUtilities.GetSigner("SHA256withRSA");
            sig.Init(false, x509CertFromPem.GetPublicKey());
            sig.BlockUpdate(encoding.GetBytes(userid), 0, encoding.GetByteCount(userid));
            sig.BlockUpdate(encoding.GetBytes(signTime), 0, encoding.GetByteCount(signTime));
            bool valid = sig.VerifySignature(signatureBytes);
            return valid;
        }

        private static Stream GetContentStream(string uri)
        {
            Stream inStream = null;
            if (uri.StartsWith("file:///"))
            {
                string filename = uri.Substring("file:///".Length);
                inStream = new FileStream(filename, FileMode.Open);
            }
            else
            {
                // prepare the web page we will be asking for
                HttpWebRequest request = (HttpWebRequest)
                    WebRequest.Create(uri);

                // execute the request
                HttpWebResponse response = (HttpWebResponse)
                    request.GetResponse();

                // we will read data via the response stream
                inStream = response.GetResponseStream();
            }
            return inStream;
        }



        public bool VerifyData(string userid, string sig, string sigkey, string signtime)
        {
            X509Certificate x509Cert = GetCertificate(sigkey);
            if (x509Cert != null)
            {
                return VerifySignedData(x509Cert, userid, signtime, sig);
            }
            else
                return false;
        }

        public void Main()
        {

            const string userid = "mb27369";
            const string signtime = "1325621976094";
            const string sig = "MCwCFAr6tI5j3gqWxx88/zeaFZg8cwELAhRd0rl41TiWI5pED7AiDHNMzL0euQ==";
            const string sigkey = "20111205-DEV-ProxySigning";

            /**
             * Produce the certificate URL
             */
            X509Certificate x509Cert = GetCertificate(sigkey);

            Console.WriteLine("X509 Cert:  " + x509Cert.ToString());


            Console.WriteLine("Verifies: " +
              VerifySignedData(x509Cert, userid, signtime, sig));
            Console.WriteLine("Verifies (Expect False): " +
              VerifySignedData(x509Cert, "dfsafda", signtime, sig));
            Console.ReadLine();
        }

    }
}