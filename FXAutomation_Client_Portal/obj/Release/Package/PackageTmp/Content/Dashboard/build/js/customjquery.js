//#region Custom Date Picker
//$(function () {
//    $(".CustomDatePicker").datepicker();
//});
//#endregion
$(function () {

    //#region ICORE REGION Start
    GetRegionGrid();
    $("#Reg_CaseTitleId").change(function (event) {
        GetRegionGrid();
    });
    function GetRegionGrid() {
        //debugger;
        var Ajax_Id = $('#Reg_CaseTitleId').val();
        if (Ajax_Id != 0) {
            $.ajax({
                url: "",
                type: "Get",
                dataType: "html",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                }
            });
        }
    }
    //#endregion
});
$(function () {

    $("#CaseType_Id").attr("disabled", false);
    $("#CaseTitle_Id").attr("disabled", true);
    $("#FormDocumentBody").hide();
    $("#FormDocumentBodyDraft").hide();
    $("#FormSubmission").attr("disabled", true);
    $("#FormSubmissionDraft").attr("disabled", true);

    if ($("#CaseFormSubmissionView").length == 1) {
        //#region Dynamic Drop Down And View Case Submission Form Start
        function GetDropDownFields(DEPART_ID, REQ_URL, DROPDOWN_ID, VALUE_ID, VALUE_NAME) {
            var TargetID = DEPART_ID;
            $.ajax
                ({
                    url: REQ_URL + TargetID,
                    type: 'POST',
                    datatype: 'application/json',
                    contentType: 'application/json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (result) {
                        $(DROPDOWN_ID).html("");
                        switch (DROPDOWN_ID) {
                            case '#Categ_DeptId':
                                $(DROPDOWN_ID).append($('<option></option>').val(0).html('--Select Category--'));
                                $("#Categ_DeptId").attr("disabled", false);
                                //$("#CaseType_Id").attr("disabled", true);
                                $("#CaseTitle_Id").attr("disabled", true);
                                break;
                            case '#CaseType_Id':
                                $(DROPDOWN_ID).append($('<option></option>').val(0).html('--Select Case Type--'));
                                $("#CaseType_Id").attr("disabled", false);
                                $("#CaseTitle_Id").attr("disabled", true);
                                break;
                            case '#CaseTitle_Id':
                                $(DROPDOWN_ID).append($('<option></option>').val(0).html('--Select Case Title--'));
                                $("#CaseTitle_Id").attr("disabled", false);
                                break;
                            default:
                            // code block
                        }
                        $.each($.parseJSON(result), function (i, Data) {
                            $(DROPDOWN_ID).append($('<option></option>').val(Data[VALUE_ID]).html(Data[VALUE_NAME]));
                        });
                    },
                    error: function () {
                        //alert("Something went wrong..");
                    },
                });
        }
        //#endregion
        //#region Detail of Dynamic Dropdown for Submission Form

        function GetCategoryWiseCaseTypes() {
            var CategoryID = $("#Categ_DeptId").val();
            var URL = '/FXDIG/CaseSubmission_Form/GetCaseTypeMenu?TargetID=';
            var DRP_ID = "#CaseType_Id";
            //alert('Category ' + CategoryID);
            if (CategoryID != 0) {
                $("#FormDocumentBody").hide();
                $("#FormDocumentBodyDraft").hide();
                var ID = 'CaseType_Id';
                var NAME = 'CaseType_Name';
                GetDropDownFields(
                    DEPART_ID = CategoryID,
                    REQ_URL = URL,
                    DROPDOWN_ID = DRP_ID,
                    VALUE_ID = ID,
                    VALUE_NAME = NAME
                );
            }
        }

        function GetCaseTypeWiseCaseTitles() {
            var CaseTypeId = $("#CaseType_Id").val();
            var URL = '/FXDIG/CaseSubmission_Form/GetCaseTitleMenu?TargetID=';
            var DRP_ID = "#CaseTitle_Id";
            //alert('Case Type Id ' + CaseTypeId);
            if (CaseTypeId != 0) {
                var ID = 'CaseTitle_Id';
                var NAME = 'CaseTitle_Name';
                GetDropDownFields(
                    DEPART_ID = CaseTypeId,
                    REQ_URL = URL,
                    DROPDOWN_ID = DRP_ID,
                    VALUE_ID = ID,
                    VALUE_NAME = NAME
                );

                $.ajax({
                    url: "/FXDIG/CaseSubmission_Form/GetFormForSubmission?CaseType_ID=" + CaseTypeId,
                    type: "Get",
                    dataType: "html",
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (data) {
                        var FormId = data;
                        $("#FormDocumentBody").hide();
                        $("#FormDocumentBodyDraft").hide();
                        $("#CaseSubmittedFormList").hide();
                        //$("#CaseForm_PartialView").load("/FXDIG/CaseSubmission_Form/CaseFormView?FormId=" + FormId);
                        //$("#CaseDocument_PartialView").load("/FXDIG/CaseSubmission_Form/Case_Document_View?CaseType_ID=" + CaseTypeId);
                        //$("#CaseSubmittedFormList").load("/FXDIG/CaseSubmission_Form/SubmittedCaseForm?FormId=" + FormId);
                    }
                });
            }
        }
        function GetCaseFieldsNDocumentOnNewPage(FormId, CaseTypeId, CaseTitle_ID) {
            alert("Form ID " + FormId + " Case Type ID " + CaseTypeId + " Case Title ID " + CaseTitle_ID);
        }
        function GetCaseTitles() {
            var CaseTitle_ID = $("#CaseTitle_Id").val();
            $("#FormDocumentBody").hide();
            $("#FormDocumentBodyDraft").hide();
            //alert('Case Type Id ' + CaseTypeId);
            if (CaseTitle_ID != 0) {

                $.ajax({
                    url: "/FXDIG/CaseSubmission_Form/GetFormForSubmission?CaseTitle_ID=" + CaseTitle_ID,
                    type: "Get",
                    dataType: "html",
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (data) {
                        var FormId = data;
                        if (FormId != 0) {
                            $("#FormDocumentBody").show();
                            $("#CaseForm_PartialView").load("/FXDIG/CaseSubmission_Form/CaseFormView?FormId=" + FormId);
                            $("#CaseDocument_PartialView").load("/FXDIG/CaseSubmission_Form/Case_Document_View?CaseTitle_ID=" + CaseTitle_ID);
                            //$("#CaseSubmittedFormList").load("/FXDIG/CaseSubmission_Form/SubmittedCaseForm?FormId=" + FormId);
                        }
                        else {
                            $("#FormDocumentBody").hide();
                            $("#CaseForm_PartialView").html();
                            $("#CaseDocument_PartialView").html();
                            $("#CaseSubmittedFormList").html();
                        }
                    }
                });

            }
            else {
                $("#FormDocumentBody").hide();
                $("#CaseForm_PartialView").html();
                $("#CaseDocument_PartialView").html();
                $("#CaseSubmittedFormList").html();
            }
        }

        $("#Dept_Id").change(function (event) {
            GetDeptWiseCategories();
        });

        $("#Categ_DeptId").change(function (event) {
            GetCategoryWiseCaseTypes();
        });

        $("#CaseType_Id").change(function (event) {
            GetCaseTypeWiseCaseTitles();
        });
        //$("#CaseTitle_Id").change(function (event) {
        //  GetCaseTitles();
        //});
        $("#ProceedToNewPage").click(function () {
            GetCaseTitles();
        });
        //#region Submit Button of Case Form Submission
        $("#FormSubmission").click(function (event) {
            $('#loading').show();
            $("#FormSubmission").attr("disabled", true);
            var CaseTitleId = $("#CaseTitle_Id").val();
            var CommentByClient = $("#CommentForClientBox").val();
            if (CaseTitleId != 0) {
                $.ajax({
                    url: "/FXDIG/CaseSubmission_Form/GetFormForSubmission?CaseTitle_ID=" + CaseTitleId,
                    type: "POST",
                    dataType: "html",
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (data) {
                        var FormId = data;
                        $.ajax({
                            url: "/FXDIG/CaseSubmission_Form/GetCaseFiledsData?FormId=" + FormId,
                            type: "POST",
                            dataType: "json",
                            beforeSend: function () {
                                $('#loading').show();
                            },
                            complete: function () {
                                $('#loading').hide();
                            },
                            success: function (result) {
                                var frmData = new FormData();
                                $.each($.parseJSON(result), function (i, ResultedData) {
                                    var ElementId = "#ctrlfrmsub" + ResultedData.CaseForm_Field_Name.toString().replace(" ", "").substring(0, 3) + ResultedData.CaseForm_Field_Id;
                                    var ElementValue = $(ElementId).val();
                                    frmData.append(ElementId, ElementValue);
                                });
                                $.ajax({
                                    url: "/FXDIG/CaseSubmission_Form/GetCaseDocumentsData/",
                                    data: { CaseTitleId: CaseTitleId },
                                    type: "POST",
                                    dataType: "json",
                                    beforeSend: function () {
                                        $('#loading').show();
                                    },
                                    complete: function () {
                                        $('#loading').hide();
                                    },
                                    success: function (result) {
                                        var PostRequestToSubmit = "true";
                                        $('#CaseDocument_PartialView').each(function () {
                                            $.each($.parseJSON(result), function (i, ResultedData) {
                                                var DocumentId = "#ctrldocsub" + ResultedData.Case_Doc_Name.toString().replace(" ", "").substring(0, 3) + ResultedData.Case_Doc_Id;
                                                var fileUpload = $(DocumentId).get(0);
                                                var files = fileUpload.files;
                                                for (var i = 0; i < files.length; i++) {
                                                    if (files[i].size > 0) {
                                                        frmData.append(DocumentId, files[i]);
                                                    }
                                                    else {
                                                        PostRequestToSubmit = "false";
                                                        alert("Invalid file # " + files[i].name + ".");
                                                        setTimeout(function () {
                                                            $('#FormSubmission').prop('disabled', false);
                                                        }, 3000);
                                                    }
                                                    //alert(files[i].name);
                                                }
                                            });
                                        });
                                        if (PostRequestToSubmit == "true") {
                                            $.ajax({
                                                url: "/FXDIG/CaseSubmission_Form/SubmitCaseFormFields?FormId=" + FormId + "&CommentByClient=" + CommentByClient,
                                                data: frmData,
                                                type: "POST",
                                                processData: false,
                                                contentType: false,
                                                dataType: "json",
                                                beforeSend: function () {
                                                    $('#loading').show();
                                                },
                                                complete: function () {
                                                    $('#loading').hide();
                                                },
                                                success: function (r) {
                                                    if (r == "") {
                                                        alert("Form Submitted Successfully And Email Sent Successfully.");
                                                        window.location.href = '/FXDIG/SubmittedFormStatus/SubmittedCaseForm';
                                                        setTimeout(function () {
                                                            $('#FormSubmission').prop('disabled', false);
                                                        }, 3000);
                                                    }
                                                    else if (r == "Unable to send the email.") {
                                                        alert(r);
                                                        window.location.href = '/FXDIG/SubmittedFormStatus/SubmittedCaseForm';
                                                        setTimeout(function () {
                                                            $('#FormSubmission').prop('disabled', false);
                                                        }, 3000);
                                                    }
                                                    else {
                                                        alert(r);
                                                        $('#loading').hide();
                                                        setTimeout(function () {
                                                            $('#FormSubmission').prop('disabled', false);
                                                        }, 3000);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            }
            $('#loading').hide();
        });
        //#endregion
    }
});
$("#AcknowledgeCheckBox").click(function () {
    $("#FormSubmission").attr("disabled", true);
    $("#FormSubmissionDraft").attr("disabled", true);
    $("#AcknowledgeCheckBox").prop('checked', false);
    $("#modal-default").modal();
    $("#modal-default").show();

    $('#BtnAgree').click(function () {
        if (this.id == 'BtnAgree') {
            $("#FormSubmission").attr("disabled", false);
            $("#FormSubmissionDraft").attr("disabled", false);
            $("#AcknowledgeCheckBox").prop('checked', true);
            $('#modal-default').modal('hide');
        }
    });
})

$("#CloseClientChatAgaintCase").click(function () {
   location.reload();
})

//#region Case Form Draft Start
$(function () {
    //#region Proceed Button Draft
    $("#ProceedToNewPageDraft").click(function () {
        var CaseTitle_ID = $("#CaseTitle_Id").val();
        $("#FormDocumentBody").hide();
        $("#FormDocumentBodyDraft").hide();
        //alert('Case Type Id ' + CaseTypeId);
        if (CaseTitle_ID != 0) {

            $.ajax({
                url: "/FXDIG/Case_Submission_Form_Draft/GetFormForSubmissionDraft?CaseTitle_ID=" + CaseTitle_ID,
                type: "Get",
                dataType: "html",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    var FormId = data;
                    if (FormId != 0) {
                        $("#FormDocumentBodyDraft").show();
                        $("#CaseForm_PartialViewDraft").load("/FXDIG/Case_Submission_Form_Draft/CaseFormViewDraft?FormId=" + FormId);
                        $("#CaseDocument_PartialViewDraft").load("/FXDIG/Case_Submission_Form_Draft/Case_Document_ViewDraft?CaseTitle_ID=" + CaseTitle_ID);
                    }
                    else {
                        $("#FormDocumentBodyDraft").hide();
                        $("#CaseForm_PartialViewDraft").html();
                        $("#CaseDocument_PartialViewDraft").html();
                    }
                }
            });

        }
        else {
            $("#FormDocumentBodyDraft").hide();
            $("#CaseForm_PartialViewDraft").html();
            $("#CaseDocument_PartialViewDraft").html();
        }
    });
    //#endregion
    //#region Fields Insert Draft
    $("#FormSubmissionDraft").click(function (event) {
        var CaseTitleId = $("#CaseTitle_Id").val();
        if (CaseTitleId != 0) {
            $.ajax({
                url: "/FXDIG/Case_Submission_Form_Draft/GetFormForSubmissionDraft?CaseTitle_ID=" + CaseTitleId,
                type: "Get",
                dataType: "html",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (data) {
                    var FormId = data;
                    $.ajax({
                        url: "/FXDIG/Case_Submission_Form_Draft/GetCaseFiledsData?FormId=" + FormId,
                        type: "Get",
                        dataType: "json",
                        beforeSend: function () {
                            $('#loading').show();
                        },
                        complete: function () {
                            $('#loading').hide();
                        },
                        success: function (result) {
                            var frmData = new FormData();
                            $.each($.parseJSON(result), function (i, ResultedData) {
                                var ElementId = "#ctrlfrmsub" + ResultedData.CaseForm_Field_Name.toString().replace(" ", "").substring(0, 3) + ResultedData.CaseForm_Field_Id;
                                var ElementValue = $(ElementId).val();
                                frmData.append(ElementId, ElementValue);
                            });
                            $.ajax({
                                url: "/FXDIG/Case_Submission_Form_Draft/GetCaseDocumentsData/",
                                data: { CaseTitleId: CaseTitleId },
                                type: "Get",
                                dataType: "json",
                                beforeSend: function () {
                                    $('#loading').show();
                                },
                                complete: function () {
                                    $('#loading').hide();
                                },
                                success: function (result) {
                                    var PostRequestToSubmit = "true";
                                    $('#CaseDocument_PartialView').each(function () {
                                        $.each($.parseJSON(result), function (i, ResultedData) {
                                            var DocumentId = "#ctrldocsub" + ResultedData.Case_Doc_Name.toString().replace(" ", "").substring(0, 3) + ResultedData.Case_Doc_Id;
                                            //alert(DocumentId);
                                            var fileUpload = $(DocumentId).get(0);
                                            var files = fileUpload.files;
                                            for (var i = 0; i < files.length; i++) {
                                                if (files[i].size > 0) {
                                                    frmData.append(DocumentId, files[i]);
                                                }
                                                else {
                                                    PostRequestToSubmit = "false";
                                                    alert("Invalid file # " + files[i].name + ".");
                                                    setTimeout(function () {
                                                        $('#FormSubmissionDraft').prop('disabled', false);
                                                    }, 3000);
                                                }
                                                //alert(files[i].name);
                                            }
                                        });
                                    });
                                    if (PostRequestToSubmit == "true") {
                                        $.ajax({
                                            url: "/FXDIG/Case_Submission_Form_Draft/SubmitCaseFormFields?FormId=" + FormId,
                                            data: frmData,
                                            type: "POST",
                                            processData: false,
                                            contentType: false,
                                            dataType: "json",
                                            beforeSend: function () {
                                                $('#loading').show();
                                            },
                                            complete: function () {
                                                $('#loading').hide();
                                            },
                                            success: function (r) {
                                                if (r == "") {
                                                    alert("Form Saved Successfully.");
                                                    window.location.href = '/FXDIG/Case_Submission_Form_Draft/SubmittedCaseFormDarft';
                                                    setTimeout(function () {
                                                        $("#FormSubmissionDraft").prop('disabled', false);
                                                    }, 3000);
                                                }
                                                else {
                                                    alert(r);
                                                    $('#loading').hide();
                                                    setTimeout(function () {
                                                        $("#FormSubmissionDraft").prop('disabled', false);
                                                    }, 3000);
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    });
                }
            });
        }
    });
    //#endregion
})

//#endregion

$(document).ready(function () {

    //#region View Submitted Case Draft
    $("[name='ViewSubmitted_Form']").click(function (button) {
        var value = $(this).attr("value").replace('sp_', '');
        var SCFM_ID = value.substr(0, value.indexOf(' '));
        //alert(SCFM_ID);
        var SCFM_CASE_FORM_ID = value.substr(value.indexOf(' ') + 2);
        //alert(SCFM_CASE_FORM_ID);
        $.ajax({
            url: "/FXDIG/CaseSubmission_Form/ViewSubmittedCaseForm?SCFM_ID=" + SCFM_ID,
            type: "POST",
            data: { SCFM_ID: SCFM_ID },
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (Data) {
                $("#SubmittedCaseFormFields").load("/FXDIG/CaseSubmission_Form/ViewSubmittedCaseForm?SCFM_ID=" + SCFM_ID);
                $.ajax({
                    url: "/FXDIG/CaseSubmission_Form/ViewSubmittedDocument?SCFM_ID=" + SCFM_ID,
                    type: "POST",
                    data: { SCFM_ID: SCFM_ID },
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (Data) {
                        $("#SubmittedCaseDocument").load("/FXDIG/CaseSubmission_Form/ViewSubmittedDocument?SCFM_ID=" + SCFM_ID);
                    },
                    error: function (error) {
                        alert(error);
                    }
                });
            },
            error: function (error) {
                alert(error);
            }
        });
    });
    //#endregion

    //#region View Submitted Case Draft
    $("[name='ViewSubmittedForm_Draft']").click(function (button) {
        var value = $(this).attr("value").replace('sp_', '');
        var SCFMD_ID = value.substr(0, value.indexOf(' '));
        //alert("SCFMD_ID = " + SCFMD_ID);
        var SCFMD_CASE_FORM_ID = value.substr(value.indexOf(' ') + 2);
        //alert("SCFMD_CASE_FORM_ID = " + SCFMD_CASE_FORM_ID);
        $.ajax({
            url: "/FXDIG/Case_Submission_Form_Draft/ViewSubmittedCaseFormDraft?SCFMD_ID=" + SCFMD_ID,
            type: "POST",
            data: { SCFMD_ID: SCFMD_ID },
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (Data) {
                $("#SubmittedCaseFormFieldsDraft").load("/FXDIG/Case_Submission_Form_Draft/ViewSubmittedCaseFormDraft?SCFMD_ID=" + SCFMD_ID);
                $.ajax({
                    url: "/FXDIG/Case_Submission_Form_Draft/ViewSubmittedDocumentDraft?SCFMD_ID=" + SCFMD_ID,
                    type: "POST",
                    data: { SCFMD_ID: SCFMD_ID },
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (Data) {
                        $("#SubmittedCaseDocumentDraft").load("/FXDIG/Case_Submission_Form_Draft/ViewSubmittedDocumentDraft?SCFMD_ID=" + SCFMD_ID);
                    },
                    error: function (error) {
                        alert(error);
                    }
                });
            },
            error: function (error) {
                alert(error);
            }
        });
    });
    //#endregion

    //#region Edit Save Form
    $("[name='EditSavedDraft']").click(function (button) {
        var FormID = $(this).attr("value");
        $("[name='EditSavedDraft']").attr("disabled", true);
        $("[name='SubmitDraftToCase']").attr("disabled", true);
        if ($("#EditSavedDraft").length == 1) {
            $.ajax({
                url: "/FXDIG/Case_Submission_Form_Draft/GetSavedFormFields?FormID=" + FormID,
                type: "Get",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (Result) {
                    var frmData = new FormData();
                    $.each($.parseJSON(Result), function (i, ResultedData) {
                        var ElementId = "#ctrlfrmsub" + ResultedData.CaseForm_Field_Name.toString().replace(" ", "").substring(0, 3) + ResultedData.SCFDD_FIELD_ID;
                        var ElementValue = $(ElementId).val();
                        frmData.append(ElementId, ElementValue);
                    });
                    $.ajax({
                        url: "/FXDIG/Case_Submission_Form_Draft/GetSavedFormDocuments?ForeignID=" + FormID,
                        type: "Get",
                        dataType: "json",
                        beforeSend: function () {
                            $('#loading').show();
                        },
                        complete: function () {
                            $('#loading').hide();
                        },
                        success: function (Result) {
                            var PostRequestToSubmit = "true";
                            $('#SavedDocument').each(function () {
                                $.each($.parseJSON(Result), function (i, ResultedData) {
                                    var DocumentId = "#ctrldocsub" + ResultedData.Case_Doc_Name.toString().replace(" ", "").substring(0, 3) + ResultedData.SCDD_DOCUMENT_ID;
                                    //alert(DocumentId);
                                    var fileUpload = $(DocumentId).get(0);
                                    var files = fileUpload.files;
                                    for (var i = 0; i < files.length; i++) {
                                        if (files[i].size > 0) {
                                            frmData.append(DocumentId, files[i]);
                                        }
                                        else {
                                            PostRequestToSubmit = "false";
                                            alert("Invalid file # " + files[i].name + ".");
                                            setTimeout(function () {
                                                $("[name='EditSavedDraft']").prop('disabled', false);
                                                $("[name='SubmitDraftToCase']").attr("disabled", false);
                                            }, 3000);
                                        }
                                        //alert(files[i].name);
                                    }
                                });
                            });
                            if (PostRequestToSubmit == "true")
                            {
                                $.ajax({
                                    url: "/FXDIG/Case_Submission_Form_Draft/EditSavedFieldsDraft?FormId=" + FormID,
                                    data: frmData,
                                    type: "POST",
                                    processData: false,
                                    contentType: false,
                                    dataType: "json",
                                    beforeSend: function () {
                                        $('#loading').show();
                                    },
                                    complete: function () {
                                        $('#loading').hide();
                                    },
                                    success: function (r) {
                                        if (r == "") {
                                            alert("Form Updated Successfully.");
                                            window.location.href = '/FXDIG/Case_Submission_Form_Draft/SubmittedCaseFormDarft';
                                            setTimeout(function () {
                                                $("[name='EditSavedDraft']").prop('disabled', false);
                                                $("[name='SubmitDraftToCase']").attr("disabled", false);
                                            }, 3000);
                                        }
                                        else {
                                            alert(r);
                                            $('#loading').hide();
                                            setTimeout(function () {
                                                $("[name='EditSavedDraft']").prop('disabled', false);
                                                $("[name='SubmitDraftToCase']").attr("disabled", false);
                                            }, 3000);
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    });
    //#endregion

    //#region Submit Draft Case To Actual Case
    $("[name='SubmitDraftToCase']").click(function (button) {
        var FormID = $(this).attr("value");
        $('#loading').show();
        $("[name='EditSavedDraft']").prop('disabled', true);
        $("[name='SubmitDraftToCase']").attr("disabled", true);
        $.ajax({
            url: "/FXDIG/Case_Submission_Form_Draft/GetSavedFormFields?FormID=" + FormID,
            type: "Get",
            dataType: "json",
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (Result) {
                var frmData = new FormData();
                $.each($.parseJSON(Result), function (i, ResultedData) {
                    var ElementId = "#ctrlfrmsub" + ResultedData.CaseForm_Field_Name.toString().replace(" ", "").substring(0, 3) + ResultedData.SCFDD_FIELD_ID;
                    var ElementValue = $(ElementId).val();
                    frmData.append(ElementId, ElementValue);
                });
                $.ajax({
                    url: "/FXDIG/Case_Submission_Form_Draft/GetSavedFormDocuments?ForeignID=" + FormID,
                    type: "Get",
                    dataType: "json",
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: function (Result) {
                        var PostRequestToSubmit = "true";
                        $('#SavedDocument').each(function () {
                            $.each($.parseJSON(Result), function (i, ResultedData) {
                                var DocumentId = "#ctrldocsub" + ResultedData.Case_Doc_Name.toString().replace(" ", "").substring(0, 3) + ResultedData.SCDD_DOCUMENT_ID;
                                //alert(DocumentId);
                                var fileUpload = $(DocumentId).get(0);
                                var files = fileUpload.files;
                                for (var i = 0; i < files.length; i++) {
                                    if (files[i].size > 0) {
                                        frmData.append(DocumentId, files[i]);
                                    }
                                    else {
                                        PostRequestToSubmit = "false";
                                        alert("Invalid file # " + files[i].name + ".");
                                        setTimeout(function () {
                                            $("[name='EditSavedDraft']").prop('disabled', false);
                                            $("[name='SubmitDraftToCase']").prop('disabled', false);
                                        }, 3000);
                                    }
                                    //alert(files[i].name);
                                }
                            });
                        });
                        if (PostRequestToSubmit == "true")
                        {
                            $.ajax({
                                url: "/FXDIG/Case_Submission_Form_Draft/SubmitDraftToCase?FormId=" + FormID,
                                data: frmData,
                                type: "POST",
                                processData: false,
                                contentType: false,
                                dataType: "json",
                                beforeSend: function () {
                                    $('#loading').show();
                                },
                                complete: function () {
                                    $('#loading').hide();
                                },
                                success: function (r) {
                                    if (r == "") {
                                        alert("Form Submitted Successfully And Email Sent Successfully.");
                                        window.location.href = '/FXDIG/SubmittedFormStatus/SubmittedCaseForm';
                                        setTimeout(function () {
                                            $("[name='SubmitDraftToCase']").prop('disabled', false);
                                            $("[name='EditSavedDraft']").prop('disabled', false);
                                        }, 3000);
                                    }
                                    else if (r == "Unable to send the email.") {
                                        alert(r);
                                        window.location.href = '/FXDIG/SubmittedFormStatus/SubmittedCaseForm';
                                        setTimeout(function () {
                                            $("[name='SubmitDraftToCase']").prop('disabled', false);
                                            $("[name='EditSavedDraft']").prop('disabled', false);
                                        }, 3000);
                                    }
                                    else {
                                        alert(r);
                                        $('#loading').hide();
                                        setTimeout(function () {
                                            $("[name='EditSavedDraft']").prop('disabled', false);
                                            $("[name='SubmitDraftToCase']").prop('disabled', false);
                                        }, 3000);
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
        $('#loading').hide();
    });
    //#endregion

    //#region Edit Discrepent Cases
    $("[name='EditSubmittedForm']").click(function (button) {
        var FormID = $(this).attr("value");
        var CommentByClient = $("#CommentForClientBoxOnEditCase").val();
        if ($("#EditSubmitted").length == 1) {
            $.ajax({
                url: "/FXDIG/CaseSubmission_Form/GetSubmittedFormFields?FormID=" + FormID,
                type: "Get",
                dataType: "json",
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: function (Result) {
                    var frmData = new FormData();
                    $.each($.parseJSON(Result), function (i, ResultedData) {
                        var ElementId = "#ctrlfrmsub" + ResultedData.CaseForm_Field_Name.toString().replace(" ", "").substring(0, 3) + ResultedData.SCFD_FIELD_ID;
                        var ElementValue = $(ElementId).val();
                        frmData.append(ElementId, ElementValue);
                    });
                    $.ajax({
                        url: "/FXDIG/CaseSubmission_Form/GetSubmittedFormDocuments?ForeignID=" + FormID,
                        type: "Get",
                        dataType: "json",
                        beforeSend: function () {
                            $('#loading').show();
                        },
                        complete: function () {
                            $('#loading').hide();
                        },
                        success: function (Result) {
                            var PostRequestToSubmit = "true";
                            $('#SavedDocument').each(function () {
                                $.each($.parseJSON(Result), function (i, ResultedData) {
                                    var DocumentId = "#ctrldocsub" + ResultedData.Case_Doc_Name.toString().replace(" ", "").substring(0, 3) + ResultedData.SCD_DOCUMENT_ID;
                                    //alert(DocumentId);
                                    var fileUpload = $(DocumentId).get(0);
                                    var files = fileUpload.files;
                                    for (var i = 0; i < files.length; i++) {
                                        if (files[i].size > 0) {
                                            frmData.append(DocumentId, files[i]);
                                        }
                                        else {
                                            PostRequestToSubmit = "false";
                                            alert("Invalid file # " + files[i].name + ".");
                                        }
                                        //alert(files[i].name);
                                    }
                                });
                            });
                            if (PostRequestToSubmit == "true") {
                                $.ajax({
                                    url: "/FXDIG/CaseSubmission_Form/EditSubmittedForm?FormId=" + FormID + "&CommentByClient=" + CommentByClient,
                                    data: frmData,
                                    type: "POST",
                                    processData: false,
                                    contentType: false,
                                    dataType: "json",
                                    beforeSend: function () {
                                        $('#loading').show();
                                    },
                                    complete: function () {
                                        $('#loading').hide();
                                    },
                                    success: function (r) {
                                        if (r == "") {
                                            alert("Form Submitted Successfully And Email Sent Successfully.");
                                            window.location.href = '/FXDIG/SubmittedFormStatus/SubmittedCaseForm';
                                        }
                                        else if (r == "Unable to send the email.") {
                                            alert(r);
                                            window.location.href = '/FXDIG/SubmittedFormStatus/SubmittedCaseForm';
                                        }
                                        else {
                                            alert(r);
                                            $('#loading').hide();
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    });
    //#endregion

});

//#region Open Chat popup
$(document).on("click", "[name='OpenChatAgainstDocument']", function (event) {
    var DocID = $(this).attr("value");
   $("#LoadChatWithClientsHere").load("/FXDIG/CaseSubmission_Form/ViewChatWithClient?DocElementId=" + DocID);
});
//#endregion

//#region Add Comments to database
$(document).on("click", "[name='GetCommendFromSendButton']", function (event) {
    var Comments = $("#CommentsData").val();
    var DocumentID = $("#DocumentID").val();
     $.ajax({
        url: "/FXDIG/CaseSubmission_Form/AddDocumentComments",
        type: "POST",
        data: { DocElementId: DocumentID, comment: Comments },
        beforeSend: function () {
            $('#loading').show();
        },
        complete: function () {
            $('#loading').hide();
        },
        success: function (Data) {
            if(Data == "Comment Added Successfully")
            {
                $("#LoadChatWithClientsHere").load("/FXDIG/CaseSubmission_Form/ViewChatWithClient?DocElementId=" + DocumentID);
                var Comments = $("#CommentsData").val('');
            }
            else
            {
                alert(Data);
            }
        },
        error: function (error) {
            alert(error);
        }
    });
});
//#endregion

//#region Open Chat popup for Master Case
$(document).on("click", "[name='ViewPreviousCommentsOfMaster']", function (event) {
    var CaseNo = $(this).attr("value");
   $("#LoadPreviousMasterCaseComments").load("/FXDIG/SubmittedFormStatus/LoadPreviousMasterCaseComments?CaseNo=" + CaseNo);
});
//#endregion

//#region Add Comments to database
$(document).on("click", "[name='MasterCaseCommentFromSendButton']", function (event) {
    var Comments = $("#CommentsDataForMasterCase").val();
    var CaseNo = $("#GetCaseNoForComment").val();
     $.ajax({
        url: "/FXDIG/SubmittedFormStatus/AddCommentToMasterLog",
        type: "POST",
        data: { CaseNo: CaseNo, comment: Comments },
        beforeSend: function () {
            $('#loading').show();
        },
        complete: function () {
            $('#loading').hide();
        },
        success: function (Data) {
            if(Data == "Comment Added Successfully")
            {
                $("#LoadPreviousMasterCaseComments").load("/FXDIG/SubmittedFormStatus/LoadPreviousMasterCaseComments?CaseNo=" + CaseNo);
                var Comments = $("#CommentsDataForMasterCase").val('');
            }
            else
            {
                alert(Data);
            }
        },
        error: function (error) {
            alert(error);
        }
    });
});
//#endregion