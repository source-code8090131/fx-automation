﻿$(function () {
    GetUrls();
    GetPRCUrls();
});

function GetUrls(){
    $.ajax({
        url: "/FXDIG/ePRC/GetePRCUrls",
        type: "POST",
        processData: false,
        contentType: false,
        success: function (Data) {
            $('.CPass').attr('href', Data.ePRCCP);
            $('.Logout').attr('href', Data.ePRCLogout);
        }
    });
}
function GetPRCUrls() {
    $.ajax({
        url: "/FXDIG/PRC/GetPRCUrls",
        type: "POST",
        processData: false,
        contentType: false,
        success: function (Data) {
            $('.PRCCPass').attr('href', Data.PRCCP);
            $('.PRCLogout').attr('href', Data.PRCLogout);
        }
    });
}