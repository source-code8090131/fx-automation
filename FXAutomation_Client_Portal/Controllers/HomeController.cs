﻿using FXAutomation_Client_Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FXAutomation_Client_Portal.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
    public class HomeController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();

        [CustomRequestFilter]
        // GET: Home
        public ActionResult Index()
        {
            if (Session["USER_ID"] != null)
            {
                string InsertedBy = Session["USER_PARTY_ID"].ToString();
                ViewBag.SubmittedCase = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_PARTY_ID == InsertedBy).Count();
                ViewBag.SubmittedCaseDraft = context.ICORE_SUBMIT_CASE_FORM_MASTER_DRAFT.Where(m => m.SCFMD_PARTY_ID == InsertedBy).Count();
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "fxdig");
            }
        }
    }
}