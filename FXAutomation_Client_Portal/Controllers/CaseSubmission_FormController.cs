﻿using FXAutomation_Client_Portal;
using FXAutomation_Client_Portal.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
//using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FXAutomation.Controllers
{
    [CustomRequestFilter]
    public class CaseSubmission_FormController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: CaseSubmission_Form

        public FileContentResult ViewFile(int ID = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (ID != 0)
                {
                    ICORE_SUBMIT_CASE_DOCUMENT_VIEW GetFile = context.ICORE_SUBMIT_CASE_DOCUMENT_VIEW.Where(m => m.SCD_ID == ID).FirstOrDefault();
                    string FileAddress = GetFile.SCD_DOCUMENT_VALUE;
                    string FileToOpen = Path.GetFileName(FileAddress);
                    string filePath = FileAddress;  //HttpContext.Server.MapPath(@"~/Uploads/" + FileToOpen);
                    if (System.IO.File.Exists(filePath))
                    {
                        byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
                        string mimeType = "application/pdf";
                        string FileNameDisplay = GetFile.Case_Doc_Name + "." + GetFile.Format_extension;
                        Response.AppendHeader("Content-Disposition", "inline; filename=" + FileNameDisplay);
                        return File(fileBytes, mimeType);
                    }
                    else
                    {
                        string localPath = "";
                        ICORE_SYSTEM_SETUP SystemData = context.ICORE_SYSTEM_SETUP.Where(m => m.SS_ID == 1).FirstOrDefault();
                        if (SystemData != null)
                        {
                            string FilePath = "";
                            string FileDomain = SystemData.SS_DOMAIN_FOR_CLIENT;
                            FilePath = FileDomain + "Uploads/" + FileToOpen;
                            if (System.IO.File.Exists(FilePath))
                            {
                                localPath = new Uri(FilePath).LocalPath;
                            }
                            else
                            {
                                FileDomain = SystemData.SS_DOMAIN_FOR_WEB;
                                FilePath = FileDomain + "Uploads/" + FileToOpen;
                                localPath = new Uri(FilePath).LocalPath;
                            }
                            // Check Here if System Have Permissions To View File on Network
                        }
                        else
                        {
                            string message = "";
                            message = "No data found in the system setup.";
                            return CreateLogFile(message);
                        }
                        try
                        {
                            System.IO.File.OpenRead(localPath);
                        }
                        catch (Exception ex)
                        {
                            string message = "";
                            message = "File : " + FileToOpen + " Exception : " + ex.Message;
                            return CreateLogFile(message);
                        }
                        if (System.IO.File.Exists(localPath))
                        {
                            byte[] fileBytes = System.IO.File.ReadAllBytes(localPath);
                            string mimeType = "application/pdf";
                            string FileNameDisplay = GetFile.Case_Doc_Name + "." + GetFile.Format_extension;
                            Response.AppendHeader("Content-Disposition", "inline; filename=" + FileNameDisplay);
                            return File(fileBytes, mimeType);
                        }
                        else
                        {
                            string message = "";
                            message = "File : " + FileToOpen + " not found in the given path." + Environment.NewLine + "please check permissions.";
                            return CreateLogFile(message);
                        }
                    }
                }
                else
                    return null;
            }
            return null;
        }
        public FileContentResult CreateLogFile(string Message)
        {
            var byteArray = System.Text.Encoding.ASCII.GetBytes(Message);
            var stream = new System.IO.MemoryStream(byteArray);
            var logfile = new FileContentResult(stream.ToArray(), "text/plain");
            return logfile;
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Submission_Form()
        {
            string message = "";
            try
            {
                if (Session["USER_ID"] != null)
                {
                    //List<SelectListItem> Department = new SelectList(context.ICORE_DEPARTMENTS.Where(m => m.Dept_Status == true).ToList(), "Dept_Id", "Dept_Name", 0).ToList();
                    //Department.Insert(0, (new SelectListItem { Text = "--Select Department--", Value = "0" }));
                    //ViewBag.Dept_Id = Department;
                    //List<SelectListItem> Category = new SelectList(context.ICORE_CATEGORIES.Where(m => m.Categ_Status == true).ToList(), "Categ_Id", "Categ_Name", 0).ToList();
                    //Category.Insert(0, (new SelectListItem { Text = "--Select Category--", Value = "0" }));
                    //ViewBag.Categ_DeptId = Category;
                    List<SelectListItem> items = new SelectList(context.ICORE_CASE_TYPES.Where(m => m.CaseType_Status == true).ToList(), "CaseType_Id", "CaseType_Name", 0).ToList();
                    items.Insert(0, (new SelectListItem { Text = "--Select Case Type--", Value = "0" }));
                    ViewBag.CaseType_Id = items;
                    List<SelectListItem> item = new SelectList(context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Status == true).ToList(), "CaseTitle_Id", "CaseTitle_Name", 0).ToList();
                    item.Insert(0, (new SelectListItem { Text = "--Select Case Title--", Value = "0" }));
                    ViewBag.CaseTitle_Id = item;
                    return View();
                }
                else
                {
                    return RedirectToAction("Logout", "fxdig");
                }
            }
            catch (Exception ex)
            {
                message = DAL.LogException("CaseSubmission_Form", "Submission_Form", Session["USER_ID"].ToString(), ex);
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public ActionResult Submission_Form(ICORE_CASE_SUBMISSION_FORM_VIEW dbtable_view)
        {
            string message = "";
            try
            {
                if (Session["USER_ID"] != null)
                {
                    //List<SelectListItem> Department = new SelectList(context.ICORE_DEPARTMENTS.Where(m => m.Dept_Status == true).ToList(), "Dept_Id", "Dept_Name", 0).ToList();
                    //Department.Insert(0, (new SelectListItem { Text = "--Select Department--", Value = "0" }));
                    //ViewBag.Dept_Id = Department;
                    //List<SelectListItem> Category = new SelectList(context.ICORE_CATEGORIES.Where(m => m.Categ_Status == true).ToList(), "Categ_Id", "Categ_Name", 0).ToList();
                    //Category.Insert(0, (new SelectListItem { Text = "--Select Category--", Value = "0" }));
                    //ViewBag.Categ_DeptId = Category;
                    List<SelectListItem> items = new SelectList(context.ICORE_CASE_TYPES.Where(m => m.CaseType_Status == true).ToList(), "CaseType_Id", "CaseType_Name", 0).ToList();
                    items.Insert(0, (new SelectListItem { Text = "--Select Case Type--", Value = "0" }));
                    ViewBag.CaseType_Id = items;
                    List<SelectListItem> item = new SelectList(context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Status == true).ToList(), "CaseTitle_Id", "CaseTitle_Name", 0).ToList();
                    item.Insert(0, (new SelectListItem { Text = "--Select Case Title--", Value = "0" }));
                    ViewBag.CaseTitle_Id = item;
                    return View();
                }
                else
                {
                    return RedirectToAction("Logout", "fxdig");
                }
            }
            catch (Exception ex)
            {
                message = DAL.LogException("CaseSubmission_Form", "Submission_Form", Session["USER_ID"].ToString(), ex);
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }

        #region Get Case Form Fields And Document
        public ActionResult CaseFormView(decimal? FormId)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();
                List<ICORE_CASE_FORMS_DETAILS_VIEW> ViewData = context.ICORE_CASE_FORMS_DETAILS_VIEW.Where(m => m.CaseForm_FormId == FormId && m.CaseForm_Filed_IsAD == false && m.CaseForm_Field_IsAuth == true).OrderBy(m => m.CaseForm_Data_ORDER_BY).ToList();
                ICORE_CASE_FORMS_DETAILS_VIEW Data = context.ICORE_CASE_FORMS_DETAILS_VIEW.Where(m => m.CaseForm_FormId == FormId && m.CaseForm_Filed_IsAD == false && m.CaseForm_Field_IsAuth == true).FirstOrDefault();
                ICORE_CLIENT_PORTFOLIO ClientInfo = context.ICORE_CLIENT_PORTFOLIO.Where(x => x.CP_EMAIL == SessionUser).FirstOrDefault();
                foreach (ICORE_CASE_FORMS_DETAILS_VIEW item in ViewData)
                {
                    if (item.CaseForm_Field_Name == "Applicant's Name" || item.CaseForm_Field_Name == "Applicant's Name" || item.CaseForm_Field_Name == "Applicant Name")
                    {
                        item.FieldDefaultValue = ClientInfo.CP_COMPANY_NAME;
                    }
                    else if (item.CaseForm_Field_Name == "NTN/CNIC")
                    {
                        item.FieldDefaultValue = ClientInfo.CP_NTN;
                    }
                    else if (item.CaseForm_Field_Name == "NTN")
                    {
                        item.FieldDefaultValue = ClientInfo.CP_NTN;
                    }
                    else if (item.CaseForm_Field_Name == "Address" || item.CaseForm_Field_Name == "Applicant Address")
                    {
                        item.FieldDefaultValue = ClientInfo.CP_ADDRESS;
                    }
                    else if (item.CaseForm_Field_Name == "Email ID" || item.CaseForm_Field_Name == "Applicant Email ID")
                    {
                        item.FieldDefaultValue = ClientInfo.CP_EMAIL;
                    }
                    else if (item.CaseForm_Field_Name == "Applicant Brief Profile" || item.CaseForm_Field_Name == "Applicant Profile" || item.CaseForm_Field_Name == "Brief Profile" || item.CaseForm_Field_Name == "Company Brief Profile" || item.CaseForm_Field_Name == "Company Profile")
                    {
                        item.FieldDefaultValue = ClientInfo.CP_BRIEF_PROFILE;
                    }

                }
                int NewFormId = Convert.ToInt32(context.ICORE_SUBMIT_CASE_FORM_MASTER.Max(m => (decimal?)m.SCFM_ID)) + 1;
                ViewBag.txtFormIdToSubmitForm = NewFormId;
                return PartialView(ViewData);
            }
            else
            {
                return RedirectToAction("Logout", "fxdig");
            }

        }
        public PartialViewResult Case_Document_View(decimal? CaseTitle_ID)
        {
            List<ICORE_CASE_DOCUMENTS_VIEW> data = new List<ICORE_CASE_DOCUMENTS_VIEW>();
            if (CaseTitle_ID != null)
            {
                data = context.ICORE_CASE_DOCUMENTS_VIEW.Where(model => model.Case_Doc_CaseTitleId == CaseTitle_ID && model.Case_Doc_IsAD == false && model.Case_Doc_IsAuth == true).OrderBy(m => m.CASE_Doc_Data_ORDER_BY).ToList();
            }
            return PartialView(data);
        }
        public int GetFormForSubmission(decimal? CaseTitle_ID)
        {
            string message = "";
            try
            {
                if (CaseTitle_ID != null && CaseTitle_ID != 0)
                {
                    ICORE_CASE_FORMS_MASTER formmaster = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_CaseTitle_Id == CaseTitle_ID).OrderByDescending(m => m.CaseForm_Id).FirstOrDefault();
                    if (formmaster != null)
                    {
                        ICORE_CASE_DOCUMENTS_VIEW data;
                        data = context.ICORE_CASE_DOCUMENTS_VIEW.Where(model => model.Case_Doc_CaseTitleId == CaseTitle_ID).FirstOrDefault();
                        return Convert.ToInt32(formmaster.CaseForm_Id);
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                message = DAL.LogException("CaseSubmission_Form", "GetFormForSubmission", Session["USER_ID"].ToString(), ex);
            }
            return 0;
        }
        #endregion

        #region Submit Case Form Fields And Document
        [HttpPost]
        public JsonResult SubmitCaseFormFields(decimal? FormId, string CommentByClient)
        {
            string message = "";
            string InsertedBy = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();
            try
            {
                ICORE_CASE_FORMS_MASTER MasterEntity = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == FormId).FirstOrDefault();
                List<ICORE_CASE_FORMS_DETAILS> DetailEntity = context.ICORE_CASE_FORMS_DETAILS.Where(m => m.CaseForm_FormId == FormId && m.CaseForm_Field_IsAuth == true && m.CaseForm_Field_Status == true).ToList();
                foreach (ICORE_CASE_FORMS_DETAILS item in DetailEntity)
                {
                    var ElementId = "#ctrlfrmsub" + item.CaseForm_Field_Name.ToString().Replace(" ", "").Substring(0, 3) + item.CaseForm_Field_Id;
                    string FieldsData = Request.Form[ElementId];
                    if (FieldsData != "" || FieldsData != null)
                    {
                        if (item.CaseForm_Field_IsRequired && item.CaseForm_Filed_IsAD == false)
                        {

                            if (FieldsData == null || FieldsData == "")
                            {
                                message = item.CaseForm_Field_Name + " field is required !!";
                                return Json(message);
                            }
                            if (item.CaseForm_DataTypeId != 6)
                            {
                                if (FieldsData.Length < item.CaseForm_Data_MIN_LENGTH)
                                {
                                    message = "The value you provided does not reach the minimum length limit for " + item.CaseForm_Field_Name + " field : minimum length limit is : " + item.CaseForm_Data_MIN_LENGTH;
                                    return Json(message);
                                }
                                if (FieldsData.Length > item.CaseForm_Data_MAX_LENGTH)
                                {
                                    message = "Maximum length limit exceeds for " + item.CaseForm_Field_Name + " field maximum length limit is : " + item.CaseForm_Data_MAX_LENGTH;
                                    return Json(message);
                                }
                            }
                        }
                    }
                }
                List<ICORE_CASE_DOCUMENTS> DocumentEntity = context.ICORE_CASE_DOCUMENTS.Where(m => m.Case_Doc_CaseTitleId == MasterEntity.CaseForm_CaseTitle_Id && m.Case_Doc_IsAuth == true && m.Case_Doc_Status == true).ToList();
                foreach (ICORE_CASE_DOCUMENTS DocItem in DocumentEntity)
                {
                    ICORE_CASE_DOCUMENTS_FORMAT DocumentFormat = context.ICORE_CASE_DOCUMENTS_FORMAT.Where(m => m.Format_id == DocItem.Case_Doc_Format_id).FirstOrDefault();
                    var DocumentId = "#ctrldocsub" + DocItem.Case_Doc_Name.ToString().Replace(" ", "").Substring(0, 3) + DocItem.Case_Doc_Id;
                    HttpPostedFileBase DocumentData = Request.Files[DocumentId];
                    if (DocItem.Case_Doc_IsRequired && DocItem.Case_Doc_IsAD == false)
                    {
                        if (DocumentData != null)
                        {
                            string FileExtension = Path.GetExtension(DocumentData.FileName);
                            string FileExten = FileExtension.Replace(".", "").ToString();
                            string Filename = Path.GetFileNameWithoutExtension(DocumentData.FileName);
                            byte[] document = new byte[DocumentData.ContentLength];
                            DocumentData.InputStream.Read(document, 0, DocumentData.ContentLength);
                            //System.UInt32 mimetype;
                            //FileMimeType.FindMimeFromData(0, null, document, 256, null, 0, out mimetype, 0);
                            //System.IntPtr mimeTypePtr = new IntPtr(mimetype);
                            //string mime = Marshal.PtrToStringUni(mimeTypePtr);
                            //Marshal.FreeCoTaskMem(mimeTypePtr);
                            //if (FileExten.ToLower() == "pdf")
                            //{
                            //    if (mime != "application/pdf")
                            //    {
                            //        message = "Invalid file : " + Filename + Environment.NewLine + "Invalid PDF file provided.";
                            //        return Json(message);
                            //    }
                            //}
                            if (Filename != DocItem.Case_Doc_Name)
                            {
                                string RequiredExten = Path.GetExtension(DocumentData.FileName);
                                message = "Invalid document name" + Environment.NewLine + "Required document name is " + DocItem.Case_Doc_Name + RequiredExten;
                                return Json(message);
                            }
                            if (DocumentData.FileName != "" || DocumentData.FileName != null)
                            {
                                if (DocumentData.FileName == null)
                                {
                                    message = "Invalid File Provided in Document : " + DocItem.Case_Doc_Name;
                                    return Json(message);
                                }
                                if (DocumentFormat.Format_extension.ToLower() == FileExten.ToLower())
                                {
                                    if (DocumentData.ContentLength > (5 * 1024 * 1024))
                                    {
                                        message = "Maximum file size is 5mb." + Environment.NewLine + "Selected file " + DocumentData.FileName + " is greater than 5mb";
                                        return Json(message);
                                    }
                                }
                                else
                                {
                                    message = "Invalid format Provided for the file " + DocumentData.FileName + Environment.NewLine + " Required format is " + DocumentFormat.Format_Name;
                                    return Json(message);
                                }
                            }
                        }
                        else
                        {
                            message = DocItem.Case_Doc_Name + " is required !!";
                            return Json(message);
                        }
                    }
                    else
                    {
                        if (DocumentData != null && DocItem.Case_Doc_IsAD == false)
                        {
                            string FileExtension = Path.GetExtension(DocumentData.FileName);
                            string FileExten = FileExtension.Replace(".", "").ToString();
                            if (DocumentFormat.Format_extension.ToLower() == FileExten.ToLower())
                            {
                                if (DocumentData.ContentLength > (5 * 1024 * 1024))
                                {
                                    message = "Maximum file size is 5mb." + Environment.NewLine + "Selected file " + DocumentData.FileName + " is greater than 5mb";
                                    return Json(message);
                                }
                            }
                            else
                            {
                                message = "Invalid format Provided for the file " + DocumentData.FileName + Environment.NewLine + " Required format is " + DocumentFormat.Format_Name;
                                return Json(message);
                            }
                        }
                    }

                }
                if (message == "")
                {
                    ICORE_SUBMIT_CASE_FORM_MASTER SubmitMasterEntity = new ICORE_SUBMIT_CASE_FORM_MASTER();
                    int MasterCaseForm_ID = 0;
                    if (SubmitMasterEntity.SCFM_ID == 0)
                    {
                        DateTime AdditionDatetime = DateTime.Now;
                        AdditionDatetime = AdditionDatetime.AddHours(5);
                        ICORE_CASE_FORMS_MASTER FormDetails = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == FormId).FirstOrDefault();
                        ICORE_CASE_TITLES Title = context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Id == FormDetails.CaseForm_CaseTitle_Id).FirstOrDefault();
                        ICORE_CASE_TYPES Types = context.ICORE_CASE_TYPES.Where(m => m.CaseType_Id == Title.CaseTitle_CaseTypeId).FirstOrDefault();
                        ICORE_CATEGORIES Categ = context.ICORE_CATEGORIES.Where(m => m.Categ_Id == Types.CaseType_CategoryId).FirstOrDefault();
                        ICORE_DEPARTMENTS Dept = context.ICORE_DEPARTMENTS.Where(m => m.Dept_Id == Categ.Categ_DeptId).FirstOrDefault();
                        int MAXID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_FORM_MASTER.Max(m => (decimal?)m.SCFM_ID)) + 1;
                        MasterCaseForm_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_FORM_MASTER.Max(m => (decimal?)m.SCFM_ID)) + 1;
                        SubmitMasterEntity.SCFM_ID = MasterCaseForm_ID;
                        SubmitMasterEntity.SCFM_CASE_FORM_ID = Convert.ToInt32(FormId);
                        SubmitMasterEntity.SCFM_CASE_NO = GenerateCaseNumber(Dept.Dept_Code.Trim(), Categ.Categ_Code.Trim(), MAXID);
                        SubmitMasterEntity.SCFM_STATUS = "1";
                        SubmitMasterEntity.SCFM_STATUS_DATE = AdditionDatetime;
                        SubmitMasterEntity.SCFM_INSERTED_BY = InsertedBy;
                        SubmitMasterEntity.SCFM_ENTRY_DATETIME = AdditionDatetime;
                        SubmitMasterEntity.SCFM_MAKER_ID = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();
                        SubmitMasterEntity.SCFM_CHECKER_ID = null;
                        SubmitMasterEntity.SCFM_QUEUE = "Ops Queue";
                        SubmitMasterEntity.SCFM_PARTY_ID = Session["USER_PARTY_ID"].ToString();
                        SubmitMasterEntity.SCFM_COMMENT = CommentByClient;
                        context.ICORE_SUBMIT_CASE_FORM_MASTER.Add(SubmitMasterEntity);
                        int MasterRowCount = context.SaveChanges();
                        if (MasterRowCount > 0)
                        {
                            MasterRowCount = 0;
                            foreach (ICORE_CASE_FORMS_DETAILS item in DetailEntity)
                            {
                                ICORE_SUBMIT_CASE_FORM_DETAIL FieldsEntity = new ICORE_SUBMIT_CASE_FORM_DETAIL();
                                var ElementId = "#ctrlfrmsub" + item.CaseForm_Field_Name.ToString().Replace(" ", "").Substring(0, 3) + item.CaseForm_Field_Id;
                                string FieldsData = Request.Form[ElementId];
                                string GetFieldID = ElementId.Remove(0, 14);
                                int FieldID = Convert.ToInt32(GetFieldID);
                                if (FieldsEntity.SCFD_ID == 0)
                                {
                                    FieldsEntity.SCFD_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_FORM_DETAIL.Max(m => (decimal?)m.SCFD_ID)) + 1;
                                    FieldsEntity.SCFD_SCFM_ID = MasterCaseForm_ID;
                                    FieldsEntity.SCFD_FORM_ID = Convert.ToInt32(FormId);
                                    FieldsEntity.SCFD_FIELD_ID = FieldID;
                                    FieldsEntity.SCFD_FIELD_VALUE = FieldsData;
                                    FieldsEntity.SCFD_ENTRY_DATETIME = DateTime.Now;
                                    FieldsEntity.SCFD_MAKER_ID = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();
                                    FieldsEntity.SCFD_CHECKER_ID = "0";
                                }
                                context.ICORE_SUBMIT_CASE_FORM_DETAIL.Add(FieldsEntity);
                                MasterRowCount += context.SaveChanges();
                            }
                            if (MasterRowCount > 0)
                            {
                                MasterRowCount = 0;
                                foreach (ICORE_CASE_DOCUMENTS DocItem in DocumentEntity)
                                {
                                    ICORE_CASE_DOCUMENTS_FORMAT DocumentFormat = context.ICORE_CASE_DOCUMENTS_FORMAT.Where(m => m.Format_id == DocItem.Case_Doc_Format_id).FirstOrDefault();
                                    var DocumentId = "#ctrldocsub" + DocItem.Case_Doc_Name.ToString().Replace(" ", "").Substring(0, 3) + DocItem.Case_Doc_Id;
                                    HttpPostedFileBase DocumentData = Request.Files[DocumentId];
                                    if (DocumentData != null)
                                    {
                                        string GetDocID = DocumentId.Remove(0, 14);
                                        int DocId = Convert.ToInt32(GetDocID);
                                        ICORE_SUBMIT_CASE_DOCUMENT DocEntity = context.ICORE_SUBMIT_CASE_DOCUMENT.Where(m => m.SCD_DOCUMENT_ID == DocId && m.SCD_FORM_ID == MasterCaseForm_ID).FirstOrDefault();
                                        if (DocEntity == null)
                                        {
                                            ICORE_SUBMIT_CASE_DOCUMENT InsertEntity = new ICORE_SUBMIT_CASE_DOCUMENT();
                                            //Insert Data to entity Start
                                            InsertEntity.SCD_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_DOCUMENT.Max(m => (int?)m.SCD_ID)) + 1;
                                            InsertEntity.SCD_DOCUMENT_ID = DocId;
                                            InsertEntity.SCD_FORM_ID = MasterCaseForm_ID;
                                            InsertEntity.SCD_ENTRY_DATETIME = DateTime.Now;
                                            InsertEntity.SCD_MAKER_ID = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();
                                            InsertEntity.SCD_INSERTED_BY = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();
                                            InsertEntity.SCD_CHECKER_ID = "0";
                                            string newFileName = "";
                                            string path = HttpContext.Server.MapPath(@"~/Uploads/");

                                            bool exists = System.IO.Directory.Exists(path);

                                            if (!exists)
                                                System.IO.Directory.CreateDirectory(path);

                                            string extension = Path.GetExtension(DocumentData.FileName);
                                            newFileName = "C-" + String.Format("{0:000000}", MasterCaseForm_ID) + "-D-" + String.Format("{0:000000}", DocId) + "-" + DateTime.Now.ToString("ddMMyyyyhhmmss") + extension;
                                            string filePath = Path.Combine(path, newFileName);
                                            InsertEntity.SCD_DOCUMENT_VALUE = filePath;
                                            DocumentData.SaveAs(filePath);
                                            context.ICORE_SUBMIT_CASE_DOCUMENT.Add(InsertEntity);
                                        }
                                        else
                                        {
                                            message = "Document already exist";
                                        }
                                        MasterRowCount = context.SaveChanges();
                                    }
                                    else
                                    {
                                        if (DocItem.Case_Doc_IsAD == true)
                                        {
                                            string GetDocID = DocumentId.Remove(0, 14);
                                            int DocId = Convert.ToInt32(GetDocID);
                                            ICORE_SUBMIT_CASE_DOCUMENT DocEntity = context.ICORE_SUBMIT_CASE_DOCUMENT.Where(m => m.SCD_DOCUMENT_ID == DocId && m.SCD_FORM_ID == MasterCaseForm_ID).FirstOrDefault();
                                            if (DocEntity == null)
                                            {
                                                ICORE_SUBMIT_CASE_DOCUMENT UpdateEntity = new ICORE_SUBMIT_CASE_DOCUMENT();
                                                UpdateEntity.SCD_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_DOCUMENT.Max(m => (int?)m.SCD_ID)) + 1;
                                                UpdateEntity.SCD_DOCUMENT_ID = DocId;
                                                UpdateEntity.SCD_FORM_ID = MasterCaseForm_ID;
                                                UpdateEntity.SCD_EDIT_DATETIME = DateTime.Now;
                                                UpdateEntity.SCD_MAKER_ID = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();
                                                UpdateEntity.SCD_CHECKER_ID = "0";
                                                UpdateEntity.SCD_DOCUMENT_VALUE = null;
                                                context.ICORE_SUBMIT_CASE_DOCUMENT.Add(UpdateEntity);
                                            }
                                            MasterRowCount = context.SaveChanges();
                                        }
                                    }  
                                }
                                if (MasterRowCount > 0)
                                {
                                    #region Ops Log
                                    string Form_Amount = "";
                                    string Form_Currency = "";
                                    string Form_MFormNo = "";
                                    ICORE_OPS_LOG CaseLog = new ICORE_OPS_LOG();
                                    ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW getAmount = context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW.Where(m => m.CaseForm_Field_Name.Contains("Amount") && m.SCFD_SCFM_ID == SubmitMasterEntity.SCFM_ID).FirstOrDefault();
                                    if (getAmount != null)
                                        Form_Amount = getAmount.SCFD_FIELD_VALUE;
                                    ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW getCurrency = context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW.Where(m => m.CaseForm_Field_Name.Contains("Currency") && m.SCFD_SCFM_ID == SubmitMasterEntity.SCFM_ID).FirstOrDefault();
                                    if (getCurrency != null)
                                        Form_Currency = getCurrency.SCFD_FIELD_VALUE;
                                    ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW getFormNo = context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW.Where(m => m.CaseForm_Field_Name.Contains("Form M No") && m.SCFD_SCFM_ID == SubmitMasterEntity.SCFM_ID).FirstOrDefault();
                                    if (getFormNo != null)
                                        Form_MFormNo = getFormNo.SCFD_FIELD_VALUE;

                                    CaseLog.OL_ID = Convert.ToInt32(context.ICORE_OPS_LOG.Max(m => (int?)m.OL_ID)) + 1;
                                    CaseLog.OL_CASE_NUMBER = SubmitMasterEntity.SCFM_CASE_NO+ "-1";
                                    CaseLog.OL_SUBMITTED_DATE = SubmitMasterEntity.SCFM_ENTRY_DATETIME;
                                    CaseLog.OL_DEPARTMENT = Dept.Dept_Name;
                                    CaseLog.OL_CATEGORY = Categ.Categ_Name;
                                    CaseLog.OL_CASE_TYPE = Types.CaseType_Name;
                                    CaseLog.OL_CASE_TITLE = Title.CaseTitle_Name;
                                    CaseLog.OL_CASE_TYPE = Types.CaseType_Name;
                                    CaseLog.OL_CASE_FORM = FormDetails.CaseForm_Name;
                                    CaseLog.OL_REGION = "";
                                    CaseLog.OL_AGEING = 0;
                                    CaseLog.OL_UPDATE_BY = "Customer";
                                    CaseLog.OL_CUSTOMER_EMAIL = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();
                                    //Get Status Name from Status ID
                                    int SubmitStatus = Convert.ToInt32(SubmitMasterEntity.SCFM_STATUS);
                                    ICORE_SUBMIT_CASE_STATUS Case_Status = context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID == SubmitStatus).FirstOrDefault();
                                    ///End
                                    CaseLog.OL_CASE_STATUS = Case_Status.SCFS_NAME;
                                    CaseLog.OL_LOG_STATUS = Case_Status.SCFS_DESCRIPTION;
                                    CaseLog.OL_STATUS_DATE = SubmitMasterEntity.SCFM_STATUS_DATE;
                                    CaseLog.OL_CUSTOMER = Session["USER_NAME"] == null ? "" : Session["USER_NAME"].ToString();
                                    CaseLog.OL_AMOUNT = Form_Amount;
                                    CaseLog.OL_CURRENCY = Form_Currency;
                                    CaseLog.OL_USER_ID = Session["USER_PARTY_ID"] == null ? "" : Session["USER_PARTY_ID"].ToString();
                                    CaseLog.OL_M_FORM_NUMBER = Form_MFormNo;
                                    CaseLog.OL_USER_ROLE = "Customer";
                                    CaseLog.OL_ENTRY_DATETIME = DateTime.Now;
                                    context.ICORE_OPS_LOG.Add(CaseLog);
                                    MasterRowCount = context.SaveChanges();
                                    #endregion

                                    #region Log Master Case Comments
                                    if (CommentByClient != null && CommentByClient != "")
                                    {
                                        ICORE_SUBMIT_CASE_COMMENTS log = new ICORE_SUBMIT_CASE_COMMENTS();
                                        log.SCC_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_COMMENTS.Max(m => (int?)m.SCC_ID)) + 1;
                                        log.SCC_CASE_NO = SubmitMasterEntity.SCFM_CASE_NO;
                                        log.SCC_COMMENT = SubmitMasterEntity.SCFM_COMMENT;
                                        log.SCC_ADDED_BY = Session["USER_ID"].ToString();
                                        log.SCC_ADDED_DATE_TIME = DateTime.Now;
                                        log.SCC_IS_AD = false;
                                        context.ICORE_SUBMIT_CASE_COMMENTS.Add(log);
                                        int rowCount = context.SaveChanges();
                                    }
                                    #endregion

                                    if (MasterRowCount > 0)
                                    {
                                        MasterRowCount = 0;
                                        ICORE_CASE_FORMS_MASTER Master = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == SubmitMasterEntity.SCFM_CASE_FORM_ID).FirstOrDefault();
                                        int SubmitStatusID = Convert.ToInt32(SubmitMasterEntity.SCFM_STATUS);
                                        ICORE_SUBMIT_CASE_STATUS CaseStatus = context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID == SubmitStatusID).FirstOrDefault();
                                        string LoggedInUser = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();
                                        string LoggedInUserName = Session["USER_NAME"] == null ? "" : Session["USER_NAME"].ToString();
                                        string DateNTime = Convert.ToDateTime(SubmitMasterEntity.SCFM_ENTRY_DATETIME).ToString();
                                        #region Get Email CC
                                        string CCUser = "";

                                        int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                                        if (ROlEIdsWithCaseTitles.Count() > 0)
                                        {
                                            List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == 12 && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                            if (checkfunction != null)
                                            {
                                                foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                                {
                                                    ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                                    if (getRoleFromFunction != null)
                                                    {
                                                        List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                                        if (GetRolesDetail.Count() > 0)
                                                        {
                                                            foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                            {
                                                                ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                                if (GetUser != null)
                                                                {
                                                                    if (!CCUser.Contains(GetUser.LOG_EMAIL))
                                                                        CCUser += GetUser.LOG_EMAIL + ",";
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (ROlEIdsWithCaseTitles.Count() > 0)
                                            CCUser = CCUser.Remove(CCUser.Length - 1);
                                        #endregion
                                        ICORE_EMAIL_CONFIGRATION EmailConfigration = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_IS_AUTH == true && m.EC_STATUS == true).FirstOrDefault();
                                        if (EmailConfigration != null)
                                        {
                                            string ClientToEmail = "";
                                            #region Get All Emails on from current party id
                                            List<ICORE_CLIENT_PORTFOLIO> GetClientEmailsFromPartyId = context.ICORE_CLIENT_PORTFOLIO.Where(m => m.CP_PARTY_ID == SubmitMasterEntity.SCFM_PARTY_ID && m.CP_STATUS == "Active").ToList();
                                            if (GetClientEmailsFromPartyId.Count() > 0)
                                            {
                                                foreach (ICORE_CLIENT_PORTFOLIO item in GetClientEmailsFromPartyId)
                                                {
                                                    if (ClientToEmail != "")
                                                    {
                                                        if (!ClientToEmail.Contains(item.CP_EMAIL))
                                                        {
                                                            ClientToEmail += "," + item.CP_EMAIL;
                                                        }
                                                    }
                                                    else
                                                        ClientToEmail += item.CP_EMAIL;
                                                }
                                                if (ClientToEmail.EndsWith(","))
                                                    ClientToEmail = ClientToEmail.Remove(ClientToEmail.Length, -1);
                                            }
                                            #endregion

                                            using (MailMessage mail = new MailMessage())
                                            {
                                                string FromEmail = EmailConfigration.EC_CREDENTIAL_ID;
                                                string HostName = EmailConfigration.EC_SEREVER_HOST;
                                                int EmailPort = Convert.ToInt32(EmailConfigration.EC_EMAIL_PORT);
                                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                                NetworkCredentials.UserName = FromEmail;
                                                ////#if dev
                                                //NetworkCredentials.Password = "Testuser0592";
                                                ////#if dev
                                                mail.From = new MailAddress(FromEmail);
                                                if (ClientToEmail != "")
                                                {
                                                    if (ClientToEmail.Contains(","))
                                                    {
                                                        string[] TO_Email = ClientToEmail.Split(',');
                                                        foreach (string MultiEmail in TO_Email)
                                                        {
                                                            mail.To.Add(MultiEmail);
                                                        }
                                                    }
                                                    else
                                                        mail.To.Add(ClientToEmail);
                                                }
                                                else
                                                    mail.To.Add(LoggedInUser);
                                                if (CCUser != "")
                                                {
                                                    string[] CC_Email = CCUser.Split(',');
                                                    foreach (string MultiEmail in CC_Email)
                                                    {
                                                        mail.CC.Add(MultiEmail);
                                                    }
                                                }
                                                mail.Subject = "FX Digitalization (Case No : " + SubmitMasterEntity.SCFM_CASE_NO + ").";
                                                if(CommentByClient != null && CommentByClient != "")
                                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:40px;text-align:center'>Foreign Exchange Case Submission</p><p style='font-family:Candara;font-size:15px'>Dear Valued Client,</p><p style='font-family:Candara;font-size:15px'>We acknowledge that you have submitted a Case (No. <span style = 'font-family:Arial'><b> " + SubmitMasterEntity.SCFM_CASE_NO + "</b></span>) for Foreign Exchange transaction.<br />  </p><p style='font-family:Candara;font-size:15px'>Below are the Case details,</p><p style='font-family:Candara;font-size:15px'>Case No# : <span style='font-family:Arial'><b>" + SubmitMasterEntity.SCFM_CASE_NO + "</b></span></p><p style='font-family:Candara;font-size:15px'>Case Status : <b style='font-size:18px'>" + CaseStatus.SCFS_NAME + " -- " + CaseStatus.SCFS_DESCRIPTION + "</b></p><p style='font-family:Candara;font-size:15px'>Submitted Date & Time : <span style='font-family:Arial'><b>" + DateNTime + "</b></span></p><p style='font-family:Candara;font-size:15px'>Comments : <span style='font-family:Arial'><b>" + CommentByClient + "</b></span></p><p style='font-family:Candara;font-size:15px'> <b>This is a system generated email, please do reply all. In case of queries please call 111-777-777 for respective your service contact. </b></p></div></div></div><div class='col-md-2'></div></div>";
                                                else
                                                    mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:40px;text-align:center'>Foreign Exchange Case Submission</p><p style='font-family:Candara;font-size:15px'>Dear Valued Client,</p><p style='font-family:Candara;font-size:15px'>We acknowledge that you have submitted a Case (No. <span style = 'font-family:Arial'><b> " + SubmitMasterEntity.SCFM_CASE_NO + "</b></span>) for Foreign Exchange transaction.<br />  </p><p style='font-family:Candara;font-size:15px'>Below are the Case details,</p><p style='font-family:Candara;font-size:15px'>Case No# : <span style='font-family:Arial'><b>" + SubmitMasterEntity.SCFM_CASE_NO + "</b></span></p><p style='font-family:Candara;font-size:15px'>Case Status : <b style='font-size:18px'>" + CaseStatus.SCFS_NAME + " -- " + CaseStatus.SCFS_DESCRIPTION + "</b></p><p style='font-family:Candara;font-size:15px'>Submitted Date & Time : <span style='font-family:Arial'><b>" + DateNTime + "</b></span></p><p style='font-family:Candara;font-size:15px'> <b>This is a system generated email, please do reply all. In case of queries please call 111-777-777 for respective your service contact. </b></p></div></div></div><div class='col-md-2'></div></div>";
                                                mail.IsBodyHtml = true;

                                                try
                                                {
                                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                                    {
                                                        smtp.Credentials = NetworkCredentials;
                                                        //smtp.EnableSsl = true;
                                                        smtp.Send(mail);
                                                    }
                                                    message = "";
                                                }
                                                catch (Exception ex)
                                                {
                                                    DAL.LogException("CaseSubmission_Form", "SubmitCaseFormFields", Session["USER_ID"].ToString(), ex);
                                                    message = "Unable to send the email.";
                                                }
                                                return Json(message);
                                            }
                                        }
                                        else
                                        {
                                            message = "No Email setup found in the system.";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                message = "Exception Occur " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                        else
                        {
                            message = "Exception Occur " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = DAL.LogException("CaseSubmission_Form", "SubmitCaseFormFields", Session["USER_ID"].ToString(), ex);
            }
            return Json(message);
        }
        public string GenerateCaseNumber(string DeptCode, string CATEG,int MAXID)
        {
            string CaseNo = "";
            CaseNo = "CBN-" + DeptCode + "-" + CATEG + "-SBP-" + String.Format("{0:000000}", MAXID);
            return CaseNo;
        }
        #endregion

        #region View Submitted Case Form Fields And Document
        public PartialViewResult ViewChatWithClient(string DocElementId)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                int DocId = 0;
                bool IsValid = int.TryParse(DocElementId, out DocId);
                ICORE_SUBMIT_CASE_DOCUMENT_VIEW GetDocumentDetail = new ICORE_SUBMIT_CASE_DOCUMENT_VIEW();
                if (DocElementId != null)
                {
                    GetDocumentDetail = context.ICORE_SUBMIT_CASE_DOCUMENT_VIEW.Where(m => m.SCD_ID == DocId).FirstOrDefault();
                    if (GetDocumentDetail != null)
                    {
                        ICORE_SUBMIT_CASE_FORM_MASTER CaseFormMaster = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == GetDocumentDetail.SCD_FORM_ID).FirstOrDefault();
                        if (CaseFormMaster != null)
                        {
                            List<ICORE_COMMENT_DETAIL_LOG> ViewData = context.ICORE_COMMENT_DETAIL_LOG.Where(m => m.CDL_CASE_NO == CaseFormMaster.SCFM_CASE_NO && m.CDL_DOCUMENT_NAME == GetDocumentDetail.Case_Doc_Name).OrderByDescending(m => m.CDL_ID).ToList();
                            ViewBag.DocumentID = GetDocumentDetail.SCD_ID;
                            return PartialView(ViewData);
                        }
                        else
                            return PartialView("Problem while fetching data");
                    }
                    else
                        return PartialView("Problem while fetching data");
                }
                else
                    return PartialView("Problem while fetching data");
            }
            else
            {
                return PartialView();
            }
        }
        [HttpPost]
        public ActionResult AddDocumentComments(string DocElementId, string comment)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                string message = "";
                int DocId = 0;
                bool IsValid = int.TryParse(DocElementId, out DocId);
                if (IsValid)
                {
                    if (comment != null && comment != "" && comment.Length <= 999)
                    {
                        ICORE_SUBMIT_CASE_DOCUMENT UpdateEntity = context.ICORE_SUBMIT_CASE_DOCUMENT.Where(m => m.SCD_ID == DocId).FirstOrDefault();
                        if (UpdateEntity != null)
                        {
                            #region Add Comments To Comments Log
                            ICORE_COMMENT_DETAIL_LOG LogComment = new ICORE_COMMENT_DETAIL_LOG();
                            ICORE_CASE_DOCUMENTS Doc = context.ICORE_CASE_DOCUMENTS.Where(m => m.Case_Doc_Id == UpdateEntity.SCD_DOCUMENT_ID).FirstOrDefault();
                            ICORE_SUBMIT_CASE_FORM_MASTER Master = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == UpdateEntity.SCD_FORM_ID).FirstOrDefault();
                            if (Master != null)
                            {
                                LogComment.CDL_ID = Convert.ToInt32(context.ICORE_COMMENT_DETAIL_LOG.Max(m => (int?)m.CDL_ID)) + 1;
                                LogComment.CDL_FORM_ID = Master.SCFM_CASE_FORM_ID;
                                LogComment.CDL_CASE_NO = Master.SCFM_CASE_NO;
                                LogComment.CDL_CLIENT_NAME = Master.SCFM_INSERTED_BY;
                                LogComment.CDL_DOCUMENT_NAME = Doc.Case_Doc_Name;
                                LogComment.CDL_COMMENTS = comment;
                                LogComment.CDL_ADDED_BY = Session["USER_ID"].ToString();
                                LogComment.CDL_ENTRY_DATETIME = DateTime.Now;
                                LogComment.CDL_IS_AD = false;
                                context.ICORE_COMMENT_DETAIL_LOG.Add(LogComment);
                                int RowCount = context.SaveChanges();
                                if (RowCount > 0)
                                {
                                    message = "Comment Added Successfully";
                                    return Json(message);
                                }
                            }
                            #endregion
                        }
                        else
                            return Json("Problem while updating Documents");
                    }
                    else
                    {
                        if (comment == "" || comment == null)
                            message = "Enter comment for the document";
                        else
                            message = "The maximum length for comment is 999";
                        return Json(message);
                    }
                }
                else
                    message = "Invalid Document id Provided";
                return Json(message);
            }
            else
                return Json("Invalid request");
        }
        public PartialViewResult ViewSubmittedCaseForm(decimal? SCFM_ID)
        {
            List<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW> data = new List<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW>();
            if (SCFM_ID != 0)
            {
                data = context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW.Where(m => m.SCFD_SCFM_ID == SCFM_ID && m.CaseForm_Filed_IsAD == false).OrderBy(m => m.CaseForm_Data_ORDER_BY).ToList();
            }
            return PartialView(data);
        }
        public PartialViewResult ViewSubmittedDocument(decimal? SCFM_ID)
        {
            string EnteredBy = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();
            List<ICORE_SUBMIT_CASE_DOCUMENT_VIEW> ViewData = new List<ICORE_SUBMIT_CASE_DOCUMENT_VIEW>();
            if (SCFM_ID != null)
            {
                ViewData = context.ICORE_SUBMIT_CASE_DOCUMENT_VIEW.Where(m => m.SCD_FORM_ID == SCFM_ID && m.SCD_INSERTED_BY == EnteredBy && m.Case_Doc_IsAD == false).OrderBy(m => m.CASE_Doc_Data_ORDER_BY).ToList();
            }
            return PartialView(ViewData);
        }
        public PartialViewResult SubmittedCaseForm(decimal? FormId)
        {
            List<ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW> MasterData = new List<ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW>();
            if (FormId != 0)
            {
                MasterData = context.ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW.Where(m => m.SCFM_CASE_FORM_ID == FormId).ToList();
            }
            return PartialView(MasterData);
        }
        public ActionResult GetCaseDocumentData(decimal? CaseType_ID, decimal? CaseTitle_ID)
        {
            List<ICORE_CASE_DOCUMENTS> FieldsName = new List<ICORE_CASE_DOCUMENTS>();
            if (CaseType_ID != 0)
            {
                int CatID = Convert.ToInt32(CaseType_ID);
                FieldsName = (context.ICORE_CASE_DOCUMENTS.Where(x => x.Case_Doc_CaseType_Id == CatID)).ToList();
            }
            else if (CaseTitle_ID != 0)
            {
                int CatID = Convert.ToInt32(CaseType_ID);
                FieldsName = (context.ICORE_CASE_DOCUMENTS.Where(x => x.Case_Doc_CaseType_Id == CatID)).ToList();
            }
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(FieldsName);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetCaseFiledsData(decimal? FormId)
        {
            List<ICORE_CASE_FORMS_DETAILS> FieldsName = new List<ICORE_CASE_FORMS_DETAILS>();
            int CatID = Convert.ToInt32(FormId);
            FieldsName = (context.ICORE_CASE_FORMS_DETAILS.Where(x => x.CaseForm_FormId == CatID && x.CaseForm_Filed_IsAD == false && x.CaseForm_Field_IsAuth == true && x.CaseForm_Field_Status == true)).ToList();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(FieldsName);
            return Json(result, JsonRequestBehavior.AllowGet);
            //return PartialView(context.ICORE_CASE_FORMS_DETAILS_VIEW.Where(m => m.CaseForm_FormId == FormId).ToList());
        }
        [HttpPost]
        public JsonResult GetCaseDocumentsData(decimal? CaseTitleId)
        {
            List<ICORE_CASE_DOCUMENTS> DocumentsName = new List<ICORE_CASE_DOCUMENTS>();
            int DocID = Convert.ToInt32(CaseTitleId);
            DocumentsName = (context.ICORE_CASE_DOCUMENTS.Where(x => x.Case_Doc_CaseTitleId == DocID && x.Case_Doc_IsAD == false && x.Case_Doc_IsAuth == true && x.Case_Doc_Status == true)).ToList();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(DocumentsName);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get Dynamic Dropdown Values
        public ActionResult GetCategoryMenu(string TargetID)
        {
            string message = "";
            try
            {
                List<ICORE_CATEGORIES> drp_menu = new List<ICORE_CATEGORIES>();
                int CatID = Convert.ToInt32(TargetID);
                drp_menu = (context.ICORE_CATEGORIES.Where(x => x.Categ_DeptId == CatID)).ToList();
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                string result = javaScriptSerializer.Serialize(drp_menu);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                message = DAL.LogException("CaseSubmission_Form", "GetCategoryMenu", Session["USER_ID"].ToString(), ex);
            }
            finally
            {
                ViewBag.message = message;
            }
            return null;
        }
        public ActionResult GetCaseTypeMenu(string TargetID)
        {
            string message = "";
            try
            {
                List<ICORE_CASE_TYPES> drp_menu = new List<ICORE_CASE_TYPES>();
                int CatID = Convert.ToInt32(TargetID);
                drp_menu = (context.ICORE_CASE_TYPES.Where(x => x.CaseType_CategoryId == CatID && x.CaseType_IsAuth == true && x.CaseType_Status == true)).ToList();
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                string result = javaScriptSerializer.Serialize(drp_menu);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                message = DAL.LogException("CaseSubmission_Form", "GetCaseTypeMenu", Session["USER_ID"].ToString(), ex);
            }
            finally
            {
                ViewBag.message = message;
            }
            return null;
        }
        public ActionResult GetCaseTitleMenu(string TargetID)
        {
            string message = "";
            try
            {
                List<ICORE_CASE_TITLES> drp_menu = new List<ICORE_CASE_TITLES>();
                int CatID = Convert.ToInt32(TargetID);
                drp_menu = (context.ICORE_CASE_TITLES.Where(x => x.CaseTitle_CaseTypeId == CatID && x.CaseTitle_IsAuth == true && x.CaseTitle_Status == true)).ToList();
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                string result = javaScriptSerializer.Serialize(drp_menu);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                message = DAL.LogException("CaseSubmission_Form", "GetCaseTitleMenu", Session["USER_ID"].ToString(), ex);
            }
            finally
            {
                ViewBag.message = message;
            }
            return null;
        }
        #endregion

        #region Edit Submitted Form
        public JsonResult GetSubmittedFormFields(int FormID)
        {
            List<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW> DetailEntity = new List<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW>();
            int FormFieldsID = Convert.ToInt32(FormID);
            DetailEntity = (context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW.Where(x => x.SCFD_SCFM_ID == FormFieldsID && x.CaseForm_Filed_IsAD == false)).ToList();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string Result = javaScriptSerializer.Serialize(DetailEntity);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSubmittedFormDocuments(int ForeignID)
        {
            List<ICORE_SUBMIT_CASE_DOCUMENT_VIEW> DetailEntity = new List<ICORE_SUBMIT_CASE_DOCUMENT_VIEW>();
            int FormFieldsID = Convert.ToInt32(ForeignID);
            DetailEntity = (context.ICORE_SUBMIT_CASE_DOCUMENT_VIEW.Where(x => x.SCD_FORM_ID == FormFieldsID && x.Case_Doc_IsAD == false)).ToList();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string Result = javaScriptSerializer.Serialize(DetailEntity);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult EditSubmittedForm(int Form_Id)
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                try
                {
                    EditSubmittedForm Entity = new EditSubmittedForm();
                    Entity.Master = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == Form_Id).FirstOrDefault();
                    Entity.FieldList = context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW.Where(m => m.SCFD_SCFM_ID == Form_Id && m.CaseForm_Filed_IsAD == false).ToList();
                    Entity.DocumentList = context.ICORE_SUBMIT_CASE_DOCUMENT_VIEW.Where(m => m.SCD_FORM_ID == Form_Id && m.Case_Doc_IsAD == false).ToList();
                    if (Entity != null)
                    {
                        return View(Entity);
                    }
                    else
                    {
                        message = "Error while fetching records on selected Id";
                    }
                }
                catch (Exception ex)
                {
                    message = DAL.LogException("CaseSubmission_Form", "EditSubmittedForm", Session["USER_ID"].ToString(), ex);
                }
                finally
                {
                    ViewBag.message = message;
                }
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "fxdig");
            }
        }
        [HttpPost]
        public ActionResult EditSubmittedForm(decimal? FormId, string CommentByClient)
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                try
                {
                    int RowCount = 0;
                    int FormID = 0;
                    FormID = Convert.ToInt32(FormId);

                    ICORE_SUBMIT_CASE_FORM_MASTER SubmitMaster = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_ID == FormId).FirstOrDefault();
                    ICORE_CASE_FORMS_MASTER FormDetails = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == SubmitMaster.SCFM_CASE_FORM_ID).FirstOrDefault();
                    ICORE_CASE_TITLES Title = context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Id == FormDetails.CaseForm_CaseTitle_Id).FirstOrDefault();
                    ICORE_CASE_FORMS_MASTER MasterEntity = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == SubmitMaster.SCFM_CASE_FORM_ID).FirstOrDefault();
                    List<ICORE_CASE_FORMS_DETAILS> DetailEntity = context.ICORE_CASE_FORMS_DETAILS.Where(m => m.CaseForm_FormId == SubmitMaster.SCFM_CASE_FORM_ID && m.CaseForm_Filed_IsAD == false).ToList();
                    foreach (ICORE_CASE_FORMS_DETAILS item in DetailEntity)
                    {
                        var ElementId = "#ctrlfrmsub" + item.CaseForm_Field_Name.ToString().Replace(" ", "").Substring(0, 3) + item.CaseForm_Field_Id;
                        string FieldsData = Request.Form[ElementId];
                        if (FieldsData != "" || FieldsData != null)
                        {
                            if (item.CaseForm_Field_IsRequired && item.CaseForm_Filed_IsAD == false)
                            {
                                if (FieldsData == null)
                                {
                                    message = "Invalid Data Provided in Field : " + item.CaseForm_Field_Name;
                                    return Json(message);
                                }
                                if (item.CaseForm_DataTypeId != 6)
                                {
                                    if (FieldsData.Length < item.CaseForm_Data_MIN_LENGTH)
                                    {
                                        message = "The value you provided does not reach the minimum length limit for " + item.CaseForm_Field_Name + " field : minimum length limit is : " + item.CaseForm_Data_MIN_LENGTH;
                                        return Json(message);
                                    }
                                    if (FieldsData.Length > item.CaseForm_Data_MAX_LENGTH)
                                    {
                                        message = "Maximum length limit exceeds for " + item.CaseForm_Field_Name + " field maximum length limit is : " + item.CaseForm_Data_MAX_LENGTH;
                                        return Json(message);
                                    }
                                }
                            }
                        }
                    }
                    List<ICORE_CASE_DOCUMENTS> DocumentEntity = context.ICORE_CASE_DOCUMENTS.Where(m => m.Case_Doc_CaseTitleId == MasterEntity.CaseForm_CaseTitle_Id && m.Case_Doc_IsAD == false).ToList();
                    foreach (ICORE_CASE_DOCUMENTS DocItem in DocumentEntity)
                    {
                        ICORE_CASE_DOCUMENTS_FORMAT DocumentFormat = context.ICORE_CASE_DOCUMENTS_FORMAT.Where(m => m.Format_id == DocItem.Case_Doc_Format_id).FirstOrDefault();
                        var DocumentId = "#ctrldocsub" + DocItem.Case_Doc_Name.ToString().Replace(" ", "").Substring(0, 3) + DocItem.Case_Doc_Id;
                        HttpPostedFileBase DocumentData = Request.Files[DocumentId];
                        if (DocItem.Case_Doc_IsRequired && DocItem.Case_Doc_IsAD == false)
                        {
                            if (DocumentData != null)
                            {
                                string FileExtension = Path.GetExtension(DocumentData.FileName);
                                string FileExten = FileExtension.Replace(".", "").ToString();
                                byte[] document = new byte[DocumentData.ContentLength];
                                DocumentData.InputStream.Read(document, 0, DocumentData.ContentLength);
                                //System.UInt32 mimetype;
                                //FileMimeType.FindMimeFromData(0, null, document, 256, null, 0, out mimetype, 0);
                                //System.IntPtr mimeTypePtr = new IntPtr(mimetype);
                                //string mime = Marshal.PtrToStringUni(mimeTypePtr);
                                //Marshal.FreeCoTaskMem(mimeTypePtr);
                                //if (FileExten.ToLower() == "pdf")
                                //{
                                //    if (mime != "application/pdf")
                                //    {
                                //        message = "Invalid file : " + DocumentData.FileName + Environment.NewLine + "Invalid PDF file provided.";
                                //        return Json(message);
                                //    }
                                //}
                                if (DocumentData.FileName != "" || DocumentData.FileName != null)
                                {
                                    string FileName = Path.GetFileNameWithoutExtension(DocumentData.FileName);
                                    if (FileName != DocItem.Case_Doc_Name)
                                    {
                                        string RequiredExten = Path.GetExtension(DocumentData.FileName);
                                        message = "Invalid document provided for field : " + DocItem.Case_Doc_Name + Environment.NewLine + "Required document name is " + DocItem.Case_Doc_Name + RequiredExten;
                                        return Json(message);
                                    }
                                    if (DocumentData.FileName == null)
                                    {
                                        message = "Invalid File Provided in Document : " + DocItem.Case_Doc_Name;
                                        return Json(message);
                                    }
                                    if (DocumentFormat.Format_extension.ToLower() == FileExten.ToLower())
                                    {
                                        if (DocumentData.ContentLength > (5 * 1024 * 1024))
                                        {
                                            message = "Maximum file size is 5mb." + Environment.NewLine + "Selected file " + DocumentData.FileName + " is greater than 5mb";
                                            return Json(message);
                                        }
                                    }
                                    else
                                    {
                                        message = "Invalid format Provided for the file " + DocumentData.FileName + Environment.NewLine + " Required format is " + DocumentFormat.Format_Name;
                                    }
                                }
                            }
                            else
                            {
                                message = DocItem.Case_Doc_Name + " is Required ";
                                return Json(message);
                            }
                        }
                        else
                        {
                            if (DocumentData != null && DocItem.Case_Doc_IsAD == false)
                            {
                                string FileExtension = Path.GetExtension(DocumentData.FileName);
                                string FileExten = FileExtension.Replace(".", "").ToString();
                                if (DocumentFormat.Format_extension.ToLower() == FileExten.ToLower())
                                {
                                    if (DocumentData.ContentLength > (5 * 1024 * 1024))
                                    {
                                        message = "Maximum file size is 5mb." + Environment.NewLine + "Selected file " + DocumentData.FileName + " is greater than 5mb";
                                        return Json(message);
                                    }
                                }
                                else
                                {
                                    message = "Invalid format Provided for the file " + DocumentData.FileName + Environment.NewLine + " Required format is " + DocumentFormat.Format_Name;
                                }
                            }
                        }
                    }

                    if (SubmitMaster != null)
                    {
                        DateTime AdditionDatetime = DateTime.Now;
                        AdditionDatetime = AdditionDatetime.AddHours(5);
                        ICORE_SUBMIT_CASE_FORM_MASTER EntityToUpdate = new ICORE_SUBMIT_CASE_FORM_MASTER();
                        EntityToUpdate = SubmitMaster;
                        EntityToUpdate.SCFM_ENTRY_DATETIME = AdditionDatetime;
                        EntityToUpdate.SCFM_STATUS_DATE = AdditionDatetime;
                        EntityToUpdate.SCFM_EDIT_DATETIME = null;
                        EntityToUpdate.SCFM_COMMENT = CommentByClient;
                        EntityToUpdate.SCFM_STATUS = "1";
                        EntityToUpdate.SCFM_QUEUE = "Ops Queue";
                        context.Entry(EntityToUpdate).State = EntityState.Modified;
                        RowCount = context.SaveChanges();
                        if (RowCount > 0)
                        {
                            List<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW> DetailData = context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW.Where(m => m.SCFD_SCFM_ID == FormId).ToList();
                            if (DetailData.Count > 0)
                            {
                                foreach (ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW item in DetailData)
                                {
                                    ICORE_SUBMIT_CASE_FORM_DETAIL UpdateEntity = context.ICORE_SUBMIT_CASE_FORM_DETAIL.Where(m => m.SCFD_ID == item.SCFD_ID).FirstOrDefault();
                                    if (UpdateEntity != null)
                                    {
                                        var ElementId = "#ctrlfrmsub" + item.CaseForm_Field_Name.ToString().Replace(" ", "").Substring(0, 3) + item.SCFD_FIELD_ID;
                                        string FieldsData = Request.Form[ElementId];
                                        string GetFieldID = ElementId.Remove(0, 14);
                                        int FieldID = Convert.ToInt32(GetFieldID);
                                        UpdateEntity.SCFD_FIELD_VALUE = FieldsData;
                                        UpdateEntity.SCFD_EDIT_DATETIME = DateTime.Now;
                                    }
                                    else
                                    {
                                        message = "Error While fetching previous records";
                                    }
                                    context.Entry(UpdateEntity).State = EntityState.Modified;
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    RowCount = 0;
                                    List<ICORE_SUBMIT_CASE_DOCUMENT_VIEW> ListOfDocument = context.ICORE_SUBMIT_CASE_DOCUMENT_VIEW.Where(m => m.SCD_FORM_ID == FormId).ToList();
                                    if (ListOfDocument.Count > 0)
                                    {
                                        foreach (ICORE_SUBMIT_CASE_DOCUMENT_VIEW item in ListOfDocument)
                                        {
                                            ICORE_SUBMIT_CASE_DOCUMENT UpdateDocument = context.ICORE_SUBMIT_CASE_DOCUMENT.Where(m => m.SCD_ID == item.SCD_ID).FirstOrDefault();
                                            if (UpdateDocument != null)
                                            {
                                                var DocumentId = "#ctrldocsub" + item.Case_Doc_Name.ToString().Replace(" ", "").Substring(0, 3) + item.SCD_DOCUMENT_ID;
                                                HttpPostedFileBase DocumentData = Request.Files[DocumentId];
                                                if (DocumentData != null && item.Case_Doc_IsAD == false)
                                                {
                                                    string GetDocID = DocumentId.Remove(0, 14);
                                                    int DocId = Convert.ToInt32(GetDocID);
                                                    string newFileName = "";
                                                    string path = HttpContext.Server.MapPath(@"~/Uploads/");
                                                    bool exists = System.IO.Directory.Exists(path);
                                                    if (!exists)
                                                        System.IO.Directory.CreateDirectory(path);
                                                    string extension = Path.GetExtension(DocumentData.FileName);
                                                    newFileName = "C-" + String.Format("{0:000000}", MasterEntity.CaseForm_Id) + "-D-" + String.Format("{0:000000}", DocId) + "-" + DateTime.Now.ToString("ddMMyyyyhhmmss") + extension;
                                                    string filePath = Path.Combine(path, newFileName);
                                                    if (System.IO.File.Exists(UpdateDocument.SCD_DOCUMENT_VALUE))
                                                    {
                                                        System.IO.File.Delete(UpdateDocument.SCD_DOCUMENT_VALUE);
                                                    }
                                                    else
                                                    {
                                                        ICORE_SYSTEM_SETUP SystemData = context.ICORE_SYSTEM_SETUP.Where(m => m.SS_ID == 1).FirstOrDefault();
                                                        if (SystemData != null)
                                                        {
                                                            string FilePath = "";
                                                            string FileName = Path.GetFileName(UpdateDocument.SCD_DOCUMENT_VALUE);
                                                            string FileDomain = SystemData.SS_DOMAIN_FOR_CLIENT;
                                                            FilePath = FileDomain + "Uploads/" + FileName;
                                                            string localPath = new Uri(FilePath).LocalPath;
                                                            // Check Here if System Have Permissions To View File on Network
                                                            if (System.IO.File.Exists(localPath))
                                                            {
                                                                System.IO.File.Delete(localPath);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            return Json("No data found in the system setup.");
                                                        }
                                                    }
                                                    UpdateDocument.SCD_DOCUMENT_VALUE = filePath;
                                                    UpdateDocument.SCD_EDIT_DATETIME = DateTime.Now;
                                                    DocumentData.SaveAs(filePath);
                                                    context.Entry(UpdateDocument).State = System.Data.Entity.EntityState.Modified;
                                                }
                                                else
                                                {
                                                    if (item.Case_Doc_IsAD == true)
                                                    {
                                                        string GetDocID = DocumentId.Remove(0, 14);
                                                        int DocId = Convert.ToInt32(GetDocID);
                                                        UpdateDocument.SCD_DOCUMENT_VALUE = null;
                                                        UpdateDocument.SCD_EDIT_DATETIME = DateTime.Now;
                                                        context.Entry(UpdateDocument).State = System.Data.Entity.EntityState.Modified;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                message = "Error While fetching previous records";
                                            }
                                            RowCount += context.SaveChanges();
                                        }
                                        if (RowCount > 0)
                                        {
                                            RowCount = 0;
                                            ICORE_CASE_FORMS_MASTER Master = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == SubmitMaster.SCFM_CASE_FORM_ID).FirstOrDefault();
                                            int SubmitStatusID = Convert.ToInt32(SubmitMaster.SCFM_STATUS);
                                            ICORE_SUBMIT_CASE_STATUS CaseStatus = context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID == SubmitStatusID).FirstOrDefault();
                                            string LoggedInUser = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();
                                            string LoggedInUserName = Session["USER_NAME"] == null ? "" : Session["USER_NAME"].ToString();
                                            string DateNTime = Convert.ToDateTime(SubmitMaster.SCFM_ENTRY_DATETIME).ToString();
                                            #region Get Email CC
                                            string CCUser = "";
                                            int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                                            if (ROlEIdsWithCaseTitles.Count() > 0)
                                            {
                                                List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == 12 && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                                if (checkfunction != null)
                                                {
                                                    foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                                    {
                                                        ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                                        if (getRoleFromFunction != null)
                                                        {
                                                            List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                                            if (GetRolesDetail.Count() > 0)
                                                            {
                                                                foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                                {
                                                                    ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                                    if (GetUser != null)
                                                                    {
                                                                        if (!CCUser.Contains(GetUser.LOG_EMAIL))
                                                                            CCUser += GetUser.LOG_EMAIL + ",";
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (ROlEIdsWithCaseTitles.Count() > 0)
                                                CCUser = CCUser.Remove(CCUser.Length - 1);
                                            #endregion

                                            #region Log Master Case Comments
                                            if (CommentByClient != null && CommentByClient != "")
                                            {
                                                ICORE_SUBMIT_CASE_COMMENTS log = new ICORE_SUBMIT_CASE_COMMENTS();
                                                log.SCC_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_COMMENTS.Max(m => (int?)m.SCC_ID)) + 1;
                                                log.SCC_CASE_NO = EntityToUpdate.SCFM_CASE_NO;
                                                log.SCC_COMMENT = EntityToUpdate.SCFM_COMMENT;
                                                log.SCC_ADDED_BY = Session["USER_ID"].ToString();
                                                log.SCC_ADDED_DATE_TIME = DateTime.Now;
                                                log.SCC_IS_AD = false;
                                                context.ICORE_SUBMIT_CASE_COMMENTS.Add(log);
                                                int rowCount = context.SaveChanges();
                                            }
                                            #endregion

                                            ICORE_EMAIL_CONFIGRATION EmailConfigration = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_IS_AUTH == true && m.EC_STATUS == true).FirstOrDefault();
                                            if (EmailConfigration != null)
                                            {
                                                string ClientToEmail = "";
                                                #region Get All Emails on from current party id
                                                string PartyID = Session["USER_PARTY_ID"].ToString();
                                                List<ICORE_CLIENT_PORTFOLIO> GetClientEmailsFromPartyId = context.ICORE_CLIENT_PORTFOLIO.Where(m => m.CP_PARTY_ID == PartyID && m.CP_STATUS == "Active").ToList();
                                                if (GetClientEmailsFromPartyId.Count() > 0)
                                                {
                                                    foreach (ICORE_CLIENT_PORTFOLIO item in GetClientEmailsFromPartyId)
                                                    {
                                                        if (ClientToEmail != "")
                                                        {
                                                            if (!ClientToEmail.Contains(item.CP_EMAIL))
                                                            {
                                                                ClientToEmail += "," + item.CP_EMAIL;
                                                            }
                                                        }
                                                        else
                                                            ClientToEmail += item.CP_EMAIL;
                                                    }
                                                    if (ClientToEmail.EndsWith(","))
                                                        ClientToEmail = ClientToEmail.Remove(ClientToEmail.Length, -1);
                                                }
                                                #endregion

                                                using (MailMessage mail = new MailMessage())
                                                {
                                                    string FromEmail = EmailConfigration.EC_CREDENTIAL_ID;
                                                    string HostName = EmailConfigration.EC_SEREVER_HOST;
                                                    int EmailPort = Convert.ToInt32(EmailConfigration.EC_EMAIL_PORT);
                                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                                    NetworkCredentials.UserName = FromEmail;
                                                    ////#if dev
                                                    //NetworkCredentials.Password = "Testuser0592";
                                                    ////#if dev
                                                    mail.From = new MailAddress(FromEmail);
                                                    if (ClientToEmail != "")
                                                    {
                                                        if (ClientToEmail.Contains(","))
                                                        {
                                                            string[] TO_Email = ClientToEmail.Split(',');
                                                            foreach (string MultiEmail in TO_Email)
                                                            {
                                                                mail.To.Add(MultiEmail);
                                                            }
                                                        }
                                                        else
                                                            mail.To.Add(ClientToEmail);
                                                    }
                                                    else
                                                        mail.To.Add(LoggedInUser);
                                                    if (CCUser != "")
                                                    {
                                                        string[] CC_Email = CCUser.Split(',');
                                                        foreach (string MultiEmail in CC_Email)
                                                        {
                                                            mail.CC.Add(MultiEmail);
                                                        }
                                                    }
                                                    mail.Subject = "FX Digitalization (Case No : " + SubmitMaster.SCFM_CASE_NO + ").";
                                                    if (CommentByClient != null && CommentByClient != "")
                                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:40px;text-align:center'>Foreign Exchange Case Submission</p><p style='font-family:Candara;font-size:15px'>Dear Valued Client,</p><p style='font-family:Candara;font-size:15px'>We acknowledge that you have submitted a case (No. " + SubmitMaster.SCFM_CASE_NO + ") for Foreign Exchange transaction.<br />  </p><p style='font-family:Candara;font-size:15px'>Below are the Case details,</p><p style='font-family:Candara;font-size:15px'>Case No# : <span style='font-family:Arial'><b>" + SubmitMaster.SCFM_CASE_NO + "</b></span></p><p style='font-family:Candara;font-size:15px'>Case Status : <b style='font-size:18px'>" + CaseStatus.SCFS_NAME + " -- " + CaseStatus.SCFS_DESCRIPTION + "</b></p><p style='font-family:Candara;font-size:15px'>Submitted Date & Time : <span style='font-family:Arial'><b>" + DateNTime + "</b></span></p><p style='font-family:Candara;font-size:15px'>Comments : <span style='font-family:Arial'><b>" + CommentByClient + "</b></span></p><p style='font-family:Candara;font-size:15px'> <b>This is a system generated email, please do reply all. In case of queries please call 111-777-777 for respective your service contact. </b></p></div></div></div><div class='col-md-2'></div></div>";
                                                    else
                                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:40px;text-align:center'>Foreign Exchange Case Submission</p><p style='font-family:Candara;font-size:15px'>Dear Valued Client,</p><p style='font-family:Candara;font-size:15px'>We acknowledge that you have submitted a case (No. " + SubmitMaster.SCFM_CASE_NO + ") for Foreign Exchange transaction.<br />  </p><p style='font-family:Candara;font-size:15px'>Below are the Case details,</p><p style='font-family:Candara;font-size:15px'>Case No# : <span style='font-family:Arial'><b>" + SubmitMaster.SCFM_CASE_NO + "</b></span></p><p style='font-family:Candara;font-size:15px'>Case Status : <b style='font-size:18px'>" + CaseStatus.SCFS_NAME + " -- " + CaseStatus.SCFS_DESCRIPTION + "</b></p><p style='font-family:Candara;font-size:15px'>Submitted Date & Time : <span style='font-family:Arial'><b>" + DateNTime + "</b></span></p><p style='font-family:Candara;font-size:15px'> <b>This is a system generated email, please do reply all. In case of queries please call 111-777-777 for respective your service contact. </b></p></div></div></div><div class='col-md-2'></div></div>";
                                                    mail.IsBodyHtml = true;

                                                    try
                                                    {
                                                        using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                                        {
                                                            smtp.Credentials = NetworkCredentials;
                                                            //smtp.EnableSsl = true;
                                                            smtp.Send(mail);
                                                        }
                                                        message = "";
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        DAL.LogException("CaseSubmission_Form", "EditSubmittedForm", Session["USER_ID"].ToString(), ex);
                                                        return Json("Unable to send the email.");
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                return Json("No Email setup found in the system.");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        message = "Error While fetching previous records";
                                    }
                                }
                                else
                                {
                                    message = "Exception Occur " + Environment.NewLine + "Please Contact to Administrator";
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    message = DAL.LogException("CaseSubmission_Form", "EditSubmittedForm", Session["USER_ID"].ToString(), ex);
                }
                finally
                {
                    ViewBag.message = message;
                }
                return Json(message);
            }
            else
            {
                return RedirectToAction("Logout", "fxdig");
            }
        }
        #endregion
        public PartialViewResult GetCaseFieldsNDocumentOnNewPage()
        {
            return PartialView();
        }
    }
}