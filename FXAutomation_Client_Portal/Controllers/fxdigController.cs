﻿using FXAutomation_Client_Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace FXAutomation_Client_Portal.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
    public class fxdigController : Controller
    {  
        FXDIGEntities context = new FXDIGEntities();

        [CustomRequestFilter]
        // GET: fxdig
        public ActionResult Index()
        {
            if (Session["USER_ID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "fxdig");
            }
        }

        public ActionResult Error()
        {
            ViewBag.Heading = Session["ERRORH"].ToString();
            ViewBag.message = Session["ERRORM"].ToString();
            return View();
        }

        public ActionResult Logout()
        {
            Request.Headers.Remove("x-citiportal-LoginID");
            Request.Headers.Remove("x-citiportal-authtime");
            Request.Headers.Remove("x-citiportal-emailaddr");
            Request.Headers.Remove("x-citiportal-clientpartyid");
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            FormsAuthentication.SignOut();
            Session.Clear();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ExpiresAbsolute = DateTime.UtcNow.AddDays(-1d);
            Response.Expires = -1500;
            Response.CacheControl = "no-Cache";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            return View();
        }


        public string GetEPFPortalFrameSrc()
        {
            ICORE_SYSTEM_SETUP SystemData = context.ICORE_SYSTEM_SETUP.Where(m => m.SS_ID == 1).FirstOrDefault();
            if (SystemData != null)
            {
                return SystemData.SS_EPF_PORTAL_FRAME_SOURCE;
            }
            else
            {
                return null;
            }
        }
        public string GetEPFPortalURI()
        {
            ICORE_SYSTEM_SETUP SystemData = context.ICORE_SYSTEM_SETUP.Where(m => m.SS_ID == 1).FirstOrDefault();
            if (SystemData != null)
            {
                string Domain = SystemData.SS_EPF_PORTAL_URI;
                return Domain;
            }
            else
            {
                return null;
            }
        }
        public string GetURLForHomeRedirect()
        {
            ICORE_SYSTEM_SETUP SystemData = context.ICORE_SYSTEM_SETUP.Where(m => m.SS_ID == 1).FirstOrDefault();
            if (SystemData != null)
            {
                return SystemData.SS_URL_FOR_HOME_REDIRECT;
            }
            else
            {
                return null;
            }
        }
    }
}