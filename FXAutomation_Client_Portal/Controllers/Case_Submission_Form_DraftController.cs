﻿using FXAutomation_Client_Portal.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FXAutomation_Client_Portal.Controllers
{
    [CustomRequestFilter]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
    public class Case_Submission_Form_DraftController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: Case_Submission_Form_Draft
        
        public FileContentResult ViewFile(int ID = 0)
        {
            if (Session["USER_ID"] != null)
            {
                if (ID != 0)
                {
                    ICORE_SUBMIT_CASE_DOCUMENT_DRAFT_VIEW GetFile = context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT_VIEW.Where(m => m.SCDD_ID == ID).FirstOrDefault();
                    string FileAddress = GetFile.SCDD_DOCUMENT_VALUE;
                    string FileToOpen = Path.GetFileName(FileAddress);
                    string filePath = FileAddress;  //HttpContext.Server.MapPath(@"~/Uploads/" + FileToOpen);
                    if (System.IO.File.Exists(filePath))
                    {
                        byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
                        string mimeType = "application/pdf";
                        string FileNameDisplay = GetFile.Case_Doc_Name + "." + GetFile.Format_extension;
                        Response.AppendHeader("Content-Disposition", "inline; filename=" + FileNameDisplay);
                        return File(fileBytes, mimeType);
                    }
                    else
                    {
                        string localPath = "";
                        ICORE_SYSTEM_SETUP SystemData = context.ICORE_SYSTEM_SETUP.Where(m => m.SS_ID == 1).FirstOrDefault();
                        if (SystemData != null)
                        {
                            string FilePath = "";
                            string FileDomain = SystemData.SS_DOMAIN_FOR_CLIENT;
                            FilePath = FileDomain + "Draft_Uploads/" + FileToOpen;
                            localPath = new Uri(FilePath).LocalPath;
                            // Check Here if System Have Permissions To View File on Network
                        }
                        else
                        {
                            string message = "";
                            message = "No data found in the system setup.";
                            return CreateLogFile(message);
                        }
                        try
                        {
                            System.IO.File.OpenRead(localPath);
                        }
                        catch (Exception ex)
                        {
                            string message = "";
                            message = "File : " + FileToOpen + " Exception : " + ex.Message;
                            return CreateLogFile(message);
                        }
                        if (System.IO.File.Exists(localPath))
                        {
                            byte[] fileBytes = System.IO.File.ReadAllBytes(localPath);
                            string mimeType = "application/pdf";
                            string FileNameDisplay = GetFile.Case_Doc_Name + "." + GetFile.Format_extension;
                            Response.AppendHeader("Content-Disposition", "inline; filename=" + FileNameDisplay);
                            return File(fileBytes, mimeType);
                        }
                        else
                        {
                            string message = "";
                            message = "File : " + FileToOpen + " not found in the given path : " + localPath + Environment.NewLine + "please check permissions.";
                            return CreateLogFile(message);
                        }
                    }
                }
                else
                    return null;
            }
            return null;
        }
        public FileContentResult CreateLogFile(string Message)
        {
            var byteArray = System.Text.Encoding.ASCII.GetBytes(Message);
            var stream = new System.IO.MemoryStream(byteArray);
            var logfile = new FileContentResult(stream.ToArray(), "text/plain");
            return logfile;
        }
        public ActionResult Submission_Form_Draft()
        {
            string message = "";
            try
            {
                if (Session["USER_ID"] != null)
                {
                    List<SelectListItem> Department = new SelectList(context.ICORE_DEPARTMENTS.Where(m => m.Dept_Status == true).ToList(), "Dept_Id", "Dept_Name", 0).ToList();
                    Department.Insert(0, (new SelectListItem { Text = "--Select Department--", Value = "0" }));
                    ViewBag.Dept_Id = Department;
                    List<SelectListItem> Category = new SelectList(context.ICORE_CATEGORIES.Where(m => m.Categ_Status == true).ToList(), "Categ_Id", "Categ_Name", 0).ToList();
                    Category.Insert(0, (new SelectListItem { Text = "--Select Category--", Value = "0" }));
                    ViewBag.Categ_DeptId = Category;
                    List<SelectListItem> items = new SelectList(context.ICORE_CASE_TYPES.Where(m => m.CaseType_Status == true).ToList(), "CaseType_Id", "CaseType_Name", 0).ToList();
                    items.Insert(0, (new SelectListItem { Text = "--Select Case Type--", Value = "0" }));
                    ViewBag.CaseType_Id = items;
                    List<SelectListItem> item = new SelectList(context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Status == true).ToList(), "CaseTitle_Id", "CaseTitle_Name", 0).ToList();
                    item.Insert(0, (new SelectListItem { Text = "--Select Case Title--", Value = "0" }));
                    ViewBag.CaseTitle_Id = item;
                    return View();
                }
                else
                {
                    return RedirectToAction("Logout", "fxdig");
                }
            }
            catch (Exception ex)
            {
                message = DAL.LogException("Case_Submission_Form_Draft", "Submission_Form_Draft", Session["USER_ID"].ToString(), ex);
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }
        [HttpPost]
        public ActionResult Submission_Form_Draft(ICORE_CASE_SUBMISSION_FORM_VIEW dbtable_view)
        {
            string message = "";
            try
            {
                if (Session["USER_ID"] != null)
                {
                    List<SelectListItem> Department = new SelectList(context.ICORE_DEPARTMENTS.Where(m => m.Dept_Status == true).ToList(), "Dept_Id", "Dept_Name", 0).ToList();
                    Department.Insert(0, (new SelectListItem { Text = "--Select Department--", Value = "0" }));
                    ViewBag.Dept_Id = Department;
                    List<SelectListItem> Category = new SelectList(context.ICORE_CATEGORIES.Where(m => m.Categ_Status == true).ToList(), "Categ_Id", "Categ_Name", 0).ToList();
                    Category.Insert(0, (new SelectListItem { Text = "--Select Category--", Value = "0" }));
                    ViewBag.Categ_DeptId = Category;
                    List<SelectListItem> items = new SelectList(context.ICORE_CASE_TYPES.Where(m => m.CaseType_Status == true).ToList(), "CaseType_Id", "CaseType_Name", 0).ToList();
                    items.Insert(0, (new SelectListItem { Text = "--Select Case Type--", Value = "0" }));
                    ViewBag.CaseType_Id = items;
                    List<SelectListItem> item = new SelectList(context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Status == true).ToList(), "CaseTitle_Id", "CaseTitle_Name", 0).ToList();
                    item.Insert(0, (new SelectListItem { Text = "--Select Case Title--", Value = "0" }));
                    ViewBag.CaseTitle_Id = item;
                    return View();
                }
                else
                {
                    return RedirectToAction("Logout", "fxdig");
                }
            }
            catch (Exception ex)
            {
                message = DAL.LogException("Case_Submission_Form_Draft", "Submission_Form_Draft", Session["USER_ID"].ToString(), ex);
            }
            finally
            {
                ViewBag.message = message;
            }
            return View();
        }

        #region Get Case Form Fields And Document
        public ActionResult CaseFormViewDraft(decimal? FormId)
        {
            if (Session["USER_ID"] != null)
            {
                string SessionUser = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();;
                List<ICORE_CASE_FORMS_DETAILS_VIEW> ViewData = context.ICORE_CASE_FORMS_DETAILS_VIEW.Where(m => m.CaseForm_FormId == FormId && m.CaseForm_Filed_IsAD == false).OrderBy(m => m.CaseForm_Data_ORDER_BY).ToList();
                ICORE_CASE_FORMS_DETAILS_VIEW Data = context.ICORE_CASE_FORMS_DETAILS_VIEW.Where(m => m.CaseForm_FormId == FormId).FirstOrDefault();
                ICORE_CLIENT_PORTFOLIO ClientInfo = context.ICORE_CLIENT_PORTFOLIO.Where(x => x.CP_EMAIL == SessionUser).FirstOrDefault();
                foreach (ICORE_CASE_FORMS_DETAILS_VIEW item in ViewData)
                {
                    if (item.CaseForm_Field_Name == "Applicant's Name" || item.CaseForm_Field_Name == "Applicant's Name" || item.CaseForm_Field_Name == "Applicant Name")
                    {
                        item.FieldDefaultValue = ClientInfo.CP_COMPANY_NAME;
                    }
                    else if (item.CaseForm_Field_Name == "NTN/CNIC")
                    {
                        item.FieldDefaultValue = ClientInfo.CP_NTN;
                    }
                    else if (item.CaseForm_Field_Name == "NTN")
                    {
                        item.FieldDefaultValue = ClientInfo.CP_NTN;
                    }
                    else if (item.CaseForm_Field_Name == "Address" || item.CaseForm_Field_Name == "Applicant Address")
                    {
                        item.FieldDefaultValue = ClientInfo.CP_ADDRESS;
                    }
                    else if (item.CaseForm_Field_Name == "Email ID" || item.CaseForm_Field_Name == "Applicant Email ID")
                    {
                        item.FieldDefaultValue = ClientInfo.CP_EMAIL;
                    }
                    else if (item.CaseForm_Field_Name == "Applicant Brief Profile" || item.CaseForm_Field_Name == "Applicant Profile" || item.CaseForm_Field_Name == "Brief Profile" || item.CaseForm_Field_Name == "Company Brief Profile" || item.CaseForm_Field_Name == "Company Profile")
                    {
                        item.FieldDefaultValue = ClientInfo.CP_BRIEF_PROFILE;
                    }
                }
                int NewFormId = Convert.ToInt32(context.ICORE_SUBMIT_CASE_FORM_MASTER_DRAFT.Max(m => (decimal?)m.SCFMD_ID)) + 1;
                ViewBag.txtFormIdToSubmitForm = NewFormId;
                return PartialView(ViewData);
            }
            else
            {
                return RedirectToAction("Logout", "fxdig");
            }

        }
        public PartialViewResult Case_Document_ViewDraft(decimal? CaseTitle_ID)
        {
            List<ICORE_CASE_DOCUMENTS_VIEW> data = new List<ICORE_CASE_DOCUMENTS_VIEW>();
            if (CaseTitle_ID != null)
            {
                data = context.ICORE_CASE_DOCUMENTS_VIEW.Where(model => model.Case_Doc_CaseTitleId == CaseTitle_ID && model.Case_Doc_IsAD == false).OrderBy(m => m.CASE_Doc_Data_ORDER_BY).ToList();
            }
            return PartialView(data);
        }
        public int GetFormForSubmissionDraft(decimal? CaseTitle_ID)
        {
            string message = "";
            try
            {
                if (CaseTitle_ID != null && CaseTitle_ID != 0)
                {
                    ICORE_CASE_FORMS_MASTER formmaster = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_CaseTitle_Id == CaseTitle_ID).OrderByDescending(m => m.CaseForm_Id).FirstOrDefault();
                    if (formmaster != null)
                    {
                        ICORE_CASE_DOCUMENTS_VIEW data;
                        data = context.ICORE_CASE_DOCUMENTS_VIEW.Where(model => model.Case_Doc_CaseTitleId == CaseTitle_ID).FirstOrDefault();
                        return Convert.ToInt32(formmaster.CaseForm_Id);
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                message = DAL.LogException("Case_Submission_Form_Draft", "GetFormForSubmissionDraft", Session["USER_ID"].ToString(), ex);
            }
            return 0;
        }
        #endregion

        #region Submit Case Form Fields And Document
        [HttpPost]
        public ActionResult SubmitCaseFormFields(decimal? FormId)
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                string InsertedBy = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();;
                try
                {
                    ICORE_CASE_FORMS_MASTER MasterEntity = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == FormId).FirstOrDefault();
                    List<ICORE_CASE_FORMS_DETAILS> DetailEntity = context.ICORE_CASE_FORMS_DETAILS.Where(m => m.CaseForm_FormId == FormId && m.CaseForm_Field_IsAuth == true && m.CaseForm_Field_Status == true).ToList();
                    //foreach (ICORE_CASE_FORMS_DETAILS item in DetailEntity)
                    //{
                    //    var ElementId = "#ctrlfrmsub" + item.CaseForm_Field_Name.ToString().Replace(" ", "").Substring(0, 3) + item.CaseForm_Field_Id;
                    //    string FieldsData = Request.Form[ElementId];
                    //    if (FieldsData != "" || FieldsData != null)
                    //    {
                    //        if (item.CaseForm_Field_IsRequired && item.CaseForm_Filed_IsAD == false)
                    //        {

                    //            if (FieldsData == null)
                    //            {
                    //                message = "Invalid Data Provided in Field : " + FieldsData;
                    //                return Json(message);
                    //            }
                    //            if (item.CaseForm_DataTypeId != 6)
                    //            {
                    //                if (FieldsData.Length < item.CaseForm_Data_MIN_LENGTH)
                    //                {
                    //                    message = "The value you provided does not reach the minimum length limit for " + item.CaseForm_Field_Name + " field : minimum length limit is : " + item.CaseForm_Data_MIN_LENGTH;
                    //                    return Json(message);
                    //                }
                    //                if (FieldsData.Length > item.CaseForm_Data_MAX_LENGTH)
                    //                {
                    //                    message = "Maximum length limit exceeds for " + item.CaseForm_Field_Name + " field maximum length limit is : " + item.CaseForm_Data_MAX_LENGTH;
                    //                    return Json(message);
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    List<ICORE_CASE_DOCUMENTS> DocumentEntity = context.ICORE_CASE_DOCUMENTS.Where(m => m.Case_Doc_CaseTitleId == MasterEntity.CaseForm_CaseTitle_Id).ToList();
                    foreach (ICORE_CASE_DOCUMENTS DocItem in DocumentEntity)
                    {
                        ICORE_CASE_DOCUMENTS_FORMAT DocumentFormat = context.ICORE_CASE_DOCUMENTS_FORMAT.Where(m => m.Format_id == DocItem.Case_Doc_Format_id).FirstOrDefault();
                        var DocumentId = "#ctrldocsub" + DocItem.Case_Doc_Name.ToString().Replace(" ", "").Substring(0, 3) + DocItem.Case_Doc_Id;
                        HttpPostedFileBase DocumentData = Request.Files[DocumentId];
                        if (DocumentData != null)
                        {
                            string FileExtension = Path.GetExtension(DocumentData.FileName);
                            string FileExten = FileExtension.Replace(".", "").ToString();
                            byte[] document = new byte[DocumentData.ContentLength];
                            DocumentData.InputStream.Read(document, 0, DocumentData.ContentLength);
                            //System.UInt32 mimetype;
                            //FileMimeType.FindMimeFromData(0, null, document, 256, null, 0, out mimetype, 0);
                            //System.IntPtr mimeTypePtr = new IntPtr(mimetype);
                            //string mime = Marshal.PtrToStringUni(mimeTypePtr);
                            //Marshal.FreeCoTaskMem(mimeTypePtr);
                            //if (FileExten.ToLower() == "pdf")
                            //{
                            //    if (mime != "application/pdf")
                            //    {
                            //        message = "Invalid file : " + DocumentData.FileName + Environment.NewLine + "Invalid PDF file provided.";
                            //        return Json(message);
                            //    }
                            //}
                            string FileName = Path.GetFileNameWithoutExtension(DocumentData.FileName);
                            if (FileName != DocItem.Case_Doc_Name)
                            {
                                string RequiredExten = Path.GetExtension(DocumentData.FileName);
                                message = "Invalid document name" + Environment.NewLine + "Required document name is " + DocItem.Case_Doc_Name + RequiredExten;
                                return Json(message);
                            }
                        }
                    }
                    if (message == "")
                    {
                        ICORE_SUBMIT_CASE_FORM_MASTER_DRAFT SubmitMasterEntity = new ICORE_SUBMIT_CASE_FORM_MASTER_DRAFT();
                        int MasterCaseForm_ID = 0;
                        if (SubmitMasterEntity.SCFMD_ID == 0)
                        {
                            ICORE_CASE_FORMS_MASTER FormDetails = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == FormId).FirstOrDefault();
                            ICORE_CASE_TITLES Title = context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Id == FormDetails.CaseForm_CaseTitle_Id).FirstOrDefault();
                            ICORE_CASE_TYPES Types = context.ICORE_CASE_TYPES.Where(m => m.CaseType_Id == Title.CaseTitle_CaseTypeId).FirstOrDefault();
                            ICORE_CATEGORIES Categ = context.ICORE_CATEGORIES.Where(m => m.Categ_Id == Types.CaseType_CategoryId).FirstOrDefault();
                            ICORE_DEPARTMENTS Dept = context.ICORE_DEPARTMENTS.Where(m => m.Dept_Id == Categ.Categ_DeptId).FirstOrDefault();
                            int MAXID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_FORM_MASTER_DRAFT.Max(m => (decimal?)m.SCFMD_ID)) + 1;
                            MasterCaseForm_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_FORM_MASTER_DRAFT.Max(m => (decimal?)m.SCFMD_ID)) + 1;
                            SubmitMasterEntity.SCFMD_ID = MasterCaseForm_ID;
                            SubmitMasterEntity.SCFMD_CASE_FORM_ID = Convert.ToInt32(FormId);
                            SubmitMasterEntity.SCFMD_CASE_NO = GenerateCaseNumber(Dept.Dept_Code.Trim(), Categ.Categ_Code.Trim(), MAXID);
                            SubmitMasterEntity.SCFMD_STATUS = "1";
                            SubmitMasterEntity.SCFMD_STATUS_DATE = DateTime.Now;
                            SubmitMasterEntity.SCFMD_INSERTED_BY = InsertedBy;
                            SubmitMasterEntity.SCFMD_ENTRY_DATETIME = DateTime.Now;
                            SubmitMasterEntity.SCFMD_MAKER_ID = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();;
                            SubmitMasterEntity.SCFMD_CHECKER_ID = null;
                            SubmitMasterEntity.SCFMD_PARTY_ID = Session["USER_PARTY_ID"].ToString();

                            context.ICORE_SUBMIT_CASE_FORM_MASTER_DRAFT.Add(SubmitMasterEntity);
                            int MasterRowCount = context.SaveChanges();
                            if (MasterRowCount > 0)
                            {
                                MasterRowCount = 0;
                                foreach (ICORE_CASE_FORMS_DETAILS item in DetailEntity)
                                {
                                    ICORE_SUBMIT_CASE_FORM_DETAIL_DRAFT FieldsEntity = new ICORE_SUBMIT_CASE_FORM_DETAIL_DRAFT();
                                    var ElementId = "#ctrlfrmsub" + item.CaseForm_Field_Name.ToString().Replace(" ", "").Substring(0, 3) + item.CaseForm_Field_Id;
                                    string FieldsData = Request.Form[ElementId];
                                    string GetFieldID = ElementId.Remove(0, 14);
                                    int FieldID = Convert.ToInt32(GetFieldID);
                                    if (FieldsEntity.SCFDD_ID == 0)
                                    {
                                        FieldsEntity.SCFDD_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_FORM_DETAIL_DRAFT.Max(m => (decimal?)m.SCFDD_ID)) + 1;
                                        FieldsEntity.SCFDD_SCFM_ID = MasterCaseForm_ID;
                                        FieldsEntity.SCFDD_FORM_ID = Convert.ToInt32(FormId);
                                        FieldsEntity.SCFDD_FIELD_ID = FieldID;
                                        FieldsEntity.SCFDD_FIELD_VALUE = FieldsData;
                                        FieldsEntity.SCFDD_ENTRY_DATETIME = DateTime.Now;
                                        FieldsEntity.SCFDD_MAKER_ID = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();;
                                        FieldsEntity.SCFDD_CHECKER_ID = "0";
                                    }
                                    context.ICORE_SUBMIT_CASE_FORM_DETAIL_DRAFT.Add(FieldsEntity);
                                    MasterRowCount += context.SaveChanges();
                                }
                                if (MasterRowCount > 0)
                                {
                                    MasterRowCount = 0;
                                    foreach (ICORE_CASE_DOCUMENTS DocItem in DocumentEntity)
                                    {
                                        ICORE_CASE_DOCUMENTS_FORMAT DocumentFormat = context.ICORE_CASE_DOCUMENTS_FORMAT.Where(m => m.Format_id == DocItem.Case_Doc_Format_id).FirstOrDefault();
                                        var DocumentId = "#ctrldocsub" + DocItem.Case_Doc_Name.ToString().Replace(" ", "").Substring(0, 3) + DocItem.Case_Doc_Id;
                                        HttpPostedFileBase DocumentData = Request.Files[DocumentId];
                                        if (DocumentData != null && DocItem.Case_Doc_IsAD == false)
                                        {
                                            string GetDocID = DocumentId.Remove(0, 14);
                                            int DocId = Convert.ToInt32(GetDocID);
                                            ICORE_SUBMIT_CASE_DOCUMENT_DRAFT DocEntity = context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT.Where(m => m.SCDD_DOCUMENT_ID == DocId && m.SCDD_FORM_ID == MasterCaseForm_ID).FirstOrDefault();
                                            if (DocEntity == null)
                                            {
                                                ICORE_SUBMIT_CASE_DOCUMENT_DRAFT InsertEntity = new ICORE_SUBMIT_CASE_DOCUMENT_DRAFT();
                                                //Insert Data to entity Start
                                                InsertEntity.SCDD_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT.Max(m => (int?)m.SCDD_ID)) + 1;
                                                InsertEntity.SCDD_DOCUMENT_ID = DocId;
                                                InsertEntity.SCDD_FORM_ID = MasterCaseForm_ID;
                                                InsertEntity.SCDD_ENTRY_DATETIME = DateTime.Now;
                                                InsertEntity.SCDD_MAKER_ID = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();;
                                                InsertEntity.SCDD_INSERTED_BY = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();;
                                                InsertEntity.SCDD_CHECKER_ID = "0";
                                                string newFileName = "";
                                                string path = HttpContext.Server.MapPath(@"~/Draft_Uploads/");

                                                bool exists = System.IO.Directory.Exists(path);

                                                if (!exists)
                                                    System.IO.Directory.CreateDirectory(path);

                                                string extension = Path.GetExtension(DocumentData.FileName);
                                                newFileName = "C-" + String.Format("{0:000000}", MasterCaseForm_ID) + "-D-" + String.Format("{0:000000}", DocId) + "-" + DateTime.Now.ToString("ddMMyyyyhhmmss") + extension;
                                                string filePath = Path.Combine(path, newFileName);
                                                InsertEntity.SCDD_DOCUMENT_VALUE = filePath;
                                                DocumentData.SaveAs(filePath);
                                                context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT.Add(InsertEntity);
                                            }
                                            else
                                            {
                                                message = "Document already exist";
                                            }
                                            MasterRowCount = context.SaveChanges();
                                        }
                                        else
                                        {
                                            if (DocItem.Case_Doc_IsAD == true)
                                            {
                                                string GetDocID = DocumentId.Remove(0, 14);
                                                int DocId = Convert.ToInt32(GetDocID);
                                                ICORE_SUBMIT_CASE_DOCUMENT_DRAFT DocEntity = context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT.Where(m => m.SCDD_DOCUMENT_ID == DocId && m.SCDD_FORM_ID == MasterCaseForm_ID).FirstOrDefault();
                                                if (DocEntity == null)
                                                {
                                                    ICORE_SUBMIT_CASE_DOCUMENT_DRAFT InsertEntity = new ICORE_SUBMIT_CASE_DOCUMENT_DRAFT();
                                                    //Insert Data to entity Start
                                                    InsertEntity.SCDD_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT.Max(m => (int?)m.SCDD_ID)) + 1;
                                                    InsertEntity.SCDD_DOCUMENT_ID = DocId;
                                                    InsertEntity.SCDD_FORM_ID = MasterCaseForm_ID;
                                                    InsertEntity.SCDD_ENTRY_DATETIME = DateTime.Now;
                                                    InsertEntity.SCDD_MAKER_ID = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();;
                                                    InsertEntity.SCDD_INSERTED_BY = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();;
                                                    InsertEntity.SCDD_CHECKER_ID = "0";
                                                    InsertEntity.SCDD_DOCUMENT_VALUE = null;
                                                    context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT.Add(InsertEntity);
                                                }
                                                MasterRowCount = context.SaveChanges();
                                            }
                                            else
                                            {
                                                if (DocumentData == null)
                                                {
                                                    string GetDocID = DocumentId.Remove(0, 14);
                                                    int DocId = Convert.ToInt32(GetDocID);
                                                    ICORE_SUBMIT_CASE_DOCUMENT_DRAFT DocEntity = context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT.Where(m => m.SCDD_DOCUMENT_ID == DocId && m.SCDD_FORM_ID == MasterCaseForm_ID).FirstOrDefault();
                                                    if (DocEntity == null)
                                                    {
                                                        ICORE_SUBMIT_CASE_DOCUMENT_DRAFT InsertEntity = new ICORE_SUBMIT_CASE_DOCUMENT_DRAFT();
                                                        //Insert Data to entity Start
                                                        InsertEntity.SCDD_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT.Max(m => (int?)m.SCDD_ID)) + 1;
                                                        InsertEntity.SCDD_DOCUMENT_ID = DocId;
                                                        InsertEntity.SCDD_FORM_ID = MasterCaseForm_ID;
                                                        InsertEntity.SCDD_ENTRY_DATETIME = DateTime.Now;
                                                        InsertEntity.SCDD_MAKER_ID = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();;
                                                        InsertEntity.SCDD_INSERTED_BY = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();;
                                                        InsertEntity.SCDD_CHECKER_ID = "0";
                                                        InsertEntity.SCDD_DOCUMENT_VALUE = null;
                                                        context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT.Add(InsertEntity);
                                                    }
                                                    MasterRowCount = context.SaveChanges();
                                                }
                                            }
                                        }
                                    }
                                    if (MasterRowCount > 0)
                                    {
                                        message = "";
                                        return Json(message);
                                    }
                                }
                                else
                                {
                                    message = "Exception Occur " + Environment.NewLine + "Please Contact to Administrator";
                                }
                            }
                            else
                            {
                                message = "Exception Occur " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    message = DAL.LogException("Case_Submission_Form_Draft", "SubmitCaseFormFields", Session["USER_ID"].ToString(), ex);
                }
                return Json(message);
            }
            else
            {
                return RedirectToAction("Logout", "fxdig");
            }
        }
        public string GenerateCaseNumber(string DeptCode, string CATEG, int MAXID)
        {
            string CaseNo = "";
            CaseNo = "DRAFT-" + DeptCode + "-" + CATEG + "-" + String.Format("{0:000000}", MAXID);
            return CaseNo;
        }
        public string GenerateCaseNumberMaster(string DeptCode, string CATEG, int MAXID)
        {
            string CaseNo = "";
            CaseNo = "CBN-" + DeptCode + "-" + CATEG + "-SBP-" + String.Format("{0:000000}", MAXID);
            return CaseNo;
        }
        #endregion

        #region View Submitted Case Form Fields And Document
        public PartialViewResult ViewSubmittedCaseFormDraft(decimal? SCFMD_ID)
        {
            List<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_DRAFT_VIEW> data = new List<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_DRAFT_VIEW>();
            if (SCFMD_ID != 0)
            {
                data = context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_DRAFT_VIEW.Where(m => m.SCFDD_SCFM_ID == SCFMD_ID && m.CaseForm_Filed_IsAD == false).OrderBy(m => m.CaseForm_Data_ORDER_BY).ToList();
            }
            return PartialView(data);
        }
        public PartialViewResult ViewSubmittedDocumentDraft(decimal? SCFMD_ID)
        {
            string EnteredBy = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();;
            List<ICORE_SUBMIT_CASE_DOCUMENT_DRAFT_VIEW> ViewData = new List<ICORE_SUBMIT_CASE_DOCUMENT_DRAFT_VIEW>();
            if (SCFMD_ID != null)
            {
                ViewData = context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT_VIEW.Where(m => m.SCDD_FORM_ID == SCFMD_ID && m.SCDD_INSERTED_BY == EnteredBy && m.Case_Doc_IsAD == false).OrderBy(m => m.CASE_Doc_Data_ORDER_BY).ToList();
            }
            return PartialView(ViewData);
        }
        public PartialViewResult SubmittedCaseFormDraft(decimal? FormId)
        {
            List<ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_DRAFT_VIEW> MasterData = new List<ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_DRAFT_VIEW>();
            if (FormId != 0)
            {
                MasterData = context.ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_DRAFT_VIEW.Where(m => m.SCFMD_CASE_FORM_ID == FormId).ToList();
            }
            return PartialView(MasterData);
        }
        public ActionResult GetCaseDocumentData(decimal? CaseType_ID, decimal? CaseTitle_ID)
        {
            List<ICORE_CASE_DOCUMENTS> FieldsName = new List<ICORE_CASE_DOCUMENTS>();
            if (CaseType_ID != 0)
            {
                int CatID = Convert.ToInt32(CaseType_ID);
                FieldsName = (context.ICORE_CASE_DOCUMENTS.Where(x => x.Case_Doc_CaseType_Id == CatID)).ToList();
            }
            else if (CaseTitle_ID != 0)
            {
                int CatID = Convert.ToInt32(CaseType_ID);
                FieldsName = (context.ICORE_CASE_DOCUMENTS.Where(x => x.Case_Doc_CaseType_Id == CatID)).ToList();
            }
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(FieldsName);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCaseFiledsData(decimal? FormId)
        {
            List<ICORE_CASE_FORMS_DETAILS> FieldsName = new List<ICORE_CASE_FORMS_DETAILS>();
            int CatID = Convert.ToInt32(FormId);
            FieldsName = (context.ICORE_CASE_FORMS_DETAILS.Where(x => x.CaseForm_FormId == CatID && x.CaseForm_Filed_IsAD == false && x.CaseForm_Field_Status == true && x.CaseForm_Field_IsAuth == true)).ToList();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(FieldsName);
            return Json(result, JsonRequestBehavior.AllowGet);
            //return PartialView(context.ICORE_CASE_FORMS_DETAILS_VIEW.Where(m => m.CaseForm_FormId == FormId).ToList());
        }
        public ActionResult GetCaseDocumentsData(decimal? CaseTitleId)
        {
            List<ICORE_CASE_DOCUMENTS> DocumentsName = new List<ICORE_CASE_DOCUMENTS>();
            int DocID = Convert.ToInt32(CaseTitleId);
            DocumentsName = (context.ICORE_CASE_DOCUMENTS.Where(x => x.Case_Doc_CaseTitleId == DocID && x.Case_Doc_IsAD == false && x.Case_Doc_Status == true && x.Case_Doc_IsAuth == true)).ToList();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(DocumentsName);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SubmittedCaseFormDarft()
        {
            if (Session["USER_ID"] != null)
            {
                //string UserEmail = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();
                string UserPartyId = Session["USER_PARTY_ID"] == null ? "" : Session["USER_PARTY_ID"].ToString();
                var PartialData = context.ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_DRAFT_VIEW.Where(m => m.SCFMD_PARTY_ID == UserPartyId).OrderByDescending(m => m.SCFMD_ID).ToList();
                return View(PartialData);
            }
            else
            {
                return RedirectToAction("Logout", "fxdig");
            }
        }
        public PartialViewResult ViewSubmittedCaseOnModalDraft()
        {
            string UserEmail = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();;
            var PartialData = context.ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_DRAFT_VIEW.Where(m => m.SCFMD_INSERTED_BY == UserEmail).ToList();
            return PartialView(PartialData);
        }
        public JsonResult SubmitDraftToCase(decimal? FormId)
        {
            string message = "";
            try
            {
                if (FormId != 0)
                {
                    ICORE_SUBMIT_CASE_FORM_MASTER_DRAFT DraftMaster = context.ICORE_SUBMIT_CASE_FORM_MASTER_DRAFT.Where(m => m.SCFMD_ID == FormId).FirstOrDefault();
                    if (DraftMaster != null)
                    {
                        ICORE_CASE_FORMS_MASTER MasterEntity = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == DraftMaster.SCFMD_CASE_FORM_ID).FirstOrDefault();
                        List<ICORE_CASE_DOCUMENTS> DocumentEntity = context.ICORE_CASE_DOCUMENTS.Where(m => m.Case_Doc_CaseTitleId == MasterEntity.CaseForm_CaseTitle_Id && m.Case_Doc_IsAD == false).ToList();
                        foreach (ICORE_CASE_DOCUMENTS DocItem in DocumentEntity)
                        {
                            ICORE_CASE_DOCUMENTS_FORMAT DocumentFormat = context.ICORE_CASE_DOCUMENTS_FORMAT.Where(m => m.Format_id == DocItem.Case_Doc_Format_id).FirstOrDefault();
                            var DocumentId = "#ctrldocsub" + DocItem.Case_Doc_Name.ToString().Replace(" ", "").Substring(0, 3) + DocItem.Case_Doc_Id;
                            HttpPostedFileBase DocumentData = Request.Files[DocumentId];
                            if (DocumentData != null)
                            {
                                string FileName = Path.GetFileNameWithoutExtension(DocumentData.FileName);
                                string FileExtension = Path.GetExtension(DocumentData.FileName);
                                string FileExten = FileExtension.Replace(".", "").ToString();
                                byte[] document = new byte[DocumentData.ContentLength];
                                DocumentData.InputStream.Read(document, 0, DocumentData.ContentLength);
                                //System.UInt32 mimetype;
                                //FileMimeType.FindMimeFromData(0, null, document, 256, null, 0, out mimetype, 0);
                                //System.IntPtr mimeTypePtr = new IntPtr(mimetype);
                                //string mime = Marshal.PtrToStringUni(mimeTypePtr);
                                //Marshal.FreeCoTaskMem(mimeTypePtr);
                                //if (FileExten.ToLower() == "pdf")
                                //{
                                //    if (mime != "application/pdf")
                                //    {
                                //        message = "Invalid file : " + DocumentData.FileName + Environment.NewLine + "Invalid PDF file provided.";
                                //        return Json(message);
                                //    }
                                //}
                                if (FileName != DocItem.Case_Doc_Name)
                                {
                                    string RequiredExten = Path.GetExtension(DocumentData.FileName);
                                    message = "Invalid document provided for field : " + DocItem.Case_Doc_Name + Environment.NewLine + "Required document name is " + DocItem.Case_Doc_Name + RequiredExten;
                                    return Json(message);
                                }
                            }
                        }

                        List<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_DRAFT_VIEW> DetailData = context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_DRAFT_VIEW.Where(m => m.SCFDD_SCFM_ID == FormId).ToList();
                        int RowCount = 0;
                        if (DetailData.Count > 0)
                        {
                            foreach (ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_DRAFT_VIEW item in DetailData)
                            {
                                ICORE_SUBMIT_CASE_FORM_DETAIL_DRAFT UpdateEntity = context.ICORE_SUBMIT_CASE_FORM_DETAIL_DRAFT.Where(m => m.SCFDD_ID == item.SCFDD_ID).FirstOrDefault();
                                if (UpdateEntity != null)
                                {
                                    var ElementId = "#ctrlfrmsub" + item.CaseForm_Field_Name.ToString().Replace(" ", "").Substring(0, 3) + item.SCFDD_FIELD_ID;
                                    string FieldsData = Request.Form[ElementId];
                                    string GetFieldID = ElementId.Remove(0, 14);
                                    int FieldID = Convert.ToInt32(GetFieldID);
                                    UpdateEntity.SCFDD_FIELD_VALUE = FieldsData;
                                    UpdateEntity.SCFDD_EDIT_DATETIME = DateTime.Now;
                                }
                                else
                                {
                                    message = "Error While fetching previous records";
                                }
                                context.Entry(UpdateEntity).State = EntityState.Modified;
                                RowCount += context.SaveChanges();
                            }
                            if (RowCount > 0)
                            {
                                RowCount = 0;
                                List<ICORE_SUBMIT_CASE_DOCUMENT_DRAFT_VIEW> ListOfDocument = context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT_VIEW.Where(m => m.SCDD_FORM_ID == FormId).ToList();
                                if (ListOfDocument.Count > 0)
                                {
                                    foreach (ICORE_SUBMIT_CASE_DOCUMENT_DRAFT_VIEW item in ListOfDocument)
                                    {
                                        ICORE_SUBMIT_CASE_DOCUMENT_DRAFT UpdateDocument = context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT.Where(m => m.SCDD_ID == item.SCDD_ID).FirstOrDefault();
                                        if (UpdateDocument != null)
                                        {
                                            var DocumentId = "#ctrldocsub" + item.Case_Doc_Name.ToString().Replace(" ", "").Substring(0, 3) + item.SCDD_DOCUMENT_ID;
                                            HttpPostedFileBase DocumentData = Request.Files[DocumentId];
                                            if (DocumentData != null && item.Case_Doc_IsAD == false)
                                            {
                                                string GetDocID = DocumentId.Remove(0, 14);
                                                int DocId = Convert.ToInt32(GetDocID);
                                                string newFileName = "";
                                                string path = HttpContext.Server.MapPath(@"~/Draft_Uploads/");
                                                bool exists = System.IO.Directory.Exists(path);
                                                if (!exists)
                                                    System.IO.Directory.CreateDirectory(path);
                                                string extension = Path.GetExtension(DocumentData.FileName);
                                                newFileName = "C-" + String.Format("{0:000000}", MasterEntity.CaseForm_Id) + "-D-" + String.Format("{0:000000}", DocId) + "-" + DateTime.Now.ToString("ddMMyyyyhhmmss") + extension;
                                                string filePath = Path.Combine(path, newFileName);
                                                if (System.IO.File.Exists(UpdateDocument.SCDD_DOCUMENT_VALUE))
                                                {
                                                    System.IO.File.Delete(UpdateDocument.SCDD_DOCUMENT_VALUE);
                                                }
                                                else
                                                {
                                                    ICORE_SYSTEM_SETUP SystemData = context.ICORE_SYSTEM_SETUP.Where(m => m.SS_ID == 1).FirstOrDefault();
                                                    if (SystemData != null)
                                                    {
                                                        string FilePath = "";
                                                        string FileName = Path.GetFileName(UpdateDocument.SCDD_DOCUMENT_VALUE);
                                                        string FileDomain = SystemData.SS_DOMAIN_FOR_CLIENT;
                                                        FilePath = FileDomain + "Draft_Uploads/" + FileName;
                                                        string localPath = new Uri(FilePath).LocalPath;
                                                        // Check Here if System Have Permissions To View File on Network
                                                        if (System.IO.File.Exists(localPath))
                                                        {
                                                            System.IO.File.Delete(localPath);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        return Json("No data found in the system setup.");
                                                    }
                                                }
                                                UpdateDocument.SCDD_DOCUMENT_VALUE = filePath;
                                                UpdateDocument.SCDD_EDIT_DATETIME = DateTime.Now;
                                                DocumentData.SaveAs(filePath);
                                                context.Entry(UpdateDocument).State = System.Data.Entity.EntityState.Modified;
                                            }
                                            else
                                            {
                                                if (item.Case_Doc_IsAD == true)
                                                {
                                                    string GetDocID = DocumentId.Remove(0, 14);
                                                    int DocId = Convert.ToInt32(GetDocID);
                                                    UpdateDocument.SCDD_DOCUMENT_VALUE = null;
                                                    UpdateDocument.SCDD_EDIT_DATETIME = DateTime.Now;
                                                    context.Entry(UpdateDocument).State = System.Data.Entity.EntityState.Modified;
                                                }
                                                else
                                                {
                                                    if (DocumentData == null)
                                                    {
                                                        string GetDocID = DocumentId.Remove(0, 14);
                                                        int DocId = Convert.ToInt32(GetDocID);
                                                        if (UpdateDocument.SCDD_DOCUMENT_VALUE != null && UpdateDocument.SCDD_DOCUMENT_VALUE != null && DocumentData == null)
                                                            UpdateDocument.SCDD_DOCUMENT_VALUE = item.SCDD_DOCUMENT_VALUE;
                                                        else
                                                            UpdateDocument.SCDD_DOCUMENT_VALUE = null;
                                                        UpdateDocument.SCDD_EDIT_DATETIME = DateTime.Now;
                                                        context.Entry(UpdateDocument).State = System.Data.Entity.EntityState.Modified;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            message = "Error While fetching previous records";
                                        }
                                        RowCount += context.SaveChanges();
                                    }
                                    if (RowCount > 0)
                                    {
                                        if (DraftMaster != null)
                                        {
                                            List<ICORE_SUBMIT_CASE_FORM_DETAIL_DRAFT> DraftDetails = context.ICORE_SUBMIT_CASE_FORM_DETAIL_DRAFT.Where(m => m.SCFDD_SCFM_ID == DraftMaster.SCFMD_ID && m.SCFDD_MAKER_ID == DraftMaster.SCFMD_INSERTED_BY).ToList();
                                            if (DraftDetails.Count() > 0)
                                            {
                                                foreach (ICORE_SUBMIT_CASE_FORM_DETAIL_DRAFT DraftFields in DraftDetails)
                                                {
                                                    ICORE_CASE_FORMS_DETAILS MasterFormDetail = context.ICORE_CASE_FORMS_DETAILS.Where(m => m.CaseForm_Field_Id == DraftFields.SCFDD_FIELD_ID && m.CaseForm_Field_Status == true && m.CaseForm_Field_IsAuth == true).FirstOrDefault();
                                                    if (MasterFormDetail.CaseForm_Field_IsRequired == true && MasterFormDetail.CaseForm_Filed_IsAD == false)
                                                    {
                                                        if (DraftFields.SCFDD_FIELD_VALUE == "" || DraftFields.SCFDD_FIELD_VALUE == null)
                                                        {
                                                            return Json(MasterFormDetail.CaseForm_Field_Name + " : field is required !!");
                                                        }
                                                        if (MasterFormDetail.CaseForm_DataTypeId != 6)
                                                        {
                                                            if (DraftFields.SCFDD_FIELD_VALUE.Length < MasterFormDetail.CaseForm_Data_MIN_LENGTH)
                                                            {
                                                                return Json("The value you provided does not reach the minimum length limit for " + MasterFormDetail.CaseForm_Field_Name + " field : minimum length limit is : " + MasterFormDetail.CaseForm_Data_MIN_LENGTH);
                                                            }
                                                            if (DraftFields.SCFDD_FIELD_VALUE.Length > MasterFormDetail.CaseForm_Data_MAX_LENGTH)
                                                            {
                                                                return Json("Maximum length limit exceeds for " + MasterFormDetail.CaseForm_Field_Name + " field maximum length limit is : " + MasterFormDetail.CaseForm_Data_MAX_LENGTH);
                                                            }
                                                        }
                                                    }
                                                }

                                                List<ICORE_SUBMIT_CASE_DOCUMENT_DRAFT> DraftDocs = context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT.Where(m => m.SCDD_FORM_ID == DraftMaster.SCFMD_ID && m.SCDD_INSERTED_BY == DraftMaster.SCFMD_INSERTED_BY).ToList();
                                                if (DraftDocs.Count > 0)
                                                {
                                                    foreach (ICORE_SUBMIT_CASE_DOCUMENT_DRAFT DraftDocuments in DraftDocs)
                                                    {
                                                        ICORE_CASE_DOCUMENTS OrignalDoc = context.ICORE_CASE_DOCUMENTS.Where(m => m.Case_Doc_Id == DraftDocuments.SCDD_DOCUMENT_ID).FirstOrDefault();
                                                        if (OrignalDoc.Case_Doc_IsRequired == true && OrignalDoc.Case_Doc_IsAD == false)
                                                        {
                                                            if (DraftDocuments.SCDD_DOCUMENT_VALUE == "" || DraftDocuments.SCDD_DOCUMENT_VALUE == null)
                                                            {
                                                                return Json("Field : " + OrignalDoc.Case_Doc_Name + " is required !!");
                                                            }
                                                        }
                                                    }
                                                    DateTime AdditionDatetime = DateTime.Now;
                                                    AdditionDatetime = AdditionDatetime.AddHours(5);
                                                    ICORE_SUBMIT_CASE_FORM_MASTER SubmitMasterEntity = new ICORE_SUBMIT_CASE_FORM_MASTER();
                                                    ICORE_CASE_FORMS_MASTER FormDetails = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == DraftMaster.SCFMD_CASE_FORM_ID).FirstOrDefault();
                                                    ICORE_CASE_TITLES Title = context.ICORE_CASE_TITLES.Where(m => m.CaseTitle_Id == FormDetails.CaseForm_CaseTitle_Id).FirstOrDefault();
                                                    ICORE_CASE_TYPES Types = context.ICORE_CASE_TYPES.Where(m => m.CaseType_Id == Title.CaseTitle_CaseTypeId).FirstOrDefault();
                                                    ICORE_CATEGORIES Categ = context.ICORE_CATEGORIES.Where(m => m.Categ_Id == Types.CaseType_CategoryId).FirstOrDefault();
                                                    ICORE_DEPARTMENTS Dept = context.ICORE_DEPARTMENTS.Where(m => m.Dept_Id == Categ.Categ_DeptId).FirstOrDefault();
                                                    int MasterCaseForm_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_FORM_MASTER.Max(m => (decimal?)m.SCFM_ID)) + 1;
                                                    SubmitMasterEntity.SCFM_ID = MasterCaseForm_ID;
                                                    SubmitMasterEntity.SCFM_CASE_FORM_ID = DraftMaster.SCFMD_CASE_FORM_ID;
                                                    SubmitMasterEntity.SCFM_CASE_NO = GenerateCaseNumberMaster(Dept.Dept_Code.Trim(), Categ.Categ_Code.Trim(), MasterCaseForm_ID);
                                                    SubmitMasterEntity.SCFM_STATUS = "1";
                                                    SubmitMasterEntity.SCFM_STATUS_DATE = AdditionDatetime;
                                                    SubmitMasterEntity.SCFM_INSERTED_BY = DraftMaster.SCFMD_INSERTED_BY;
                                                    SubmitMasterEntity.SCFM_ENTRY_DATETIME = AdditionDatetime;
                                                    SubmitMasterEntity.SCFM_MAKER_ID = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();
                                                    SubmitMasterEntity.SCFM_CHECKER_ID = null;
                                                    SubmitMasterEntity.SCFM_QUEUE = "Ops Queue";
                                                    SubmitMasterEntity.SCFM_PARTY_ID = Session["USER_PARTY_ID"].ToString();
                                                    context.ICORE_SUBMIT_CASE_FORM_MASTER.Add(SubmitMasterEntity);
                                                    int MasterRowCount = context.SaveChanges();
                                                    if (MasterRowCount > 0)
                                                    {
                                                        RowCount = 0;
                                                        foreach (ICORE_SUBMIT_CASE_FORM_DETAIL_DRAFT item in DraftDetails)
                                                        {
                                                            ICORE_SUBMIT_CASE_FORM_DETAIL DataEntity = new ICORE_SUBMIT_CASE_FORM_DETAIL();
                                                            DataEntity.SCFD_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_FORM_DETAIL.Max(m => (decimal?)m.SCFD_ID)) + 1;
                                                            DataEntity.SCFD_SCFM_ID = MasterCaseForm_ID;
                                                            DataEntity.SCFD_FORM_ID = item.SCFDD_FORM_ID;
                                                            DataEntity.SCFD_FIELD_ID = item.SCFDD_FIELD_ID;
                                                            DataEntity.SCFD_FIELD_VALUE = item.SCFDD_FIELD_VALUE;
                                                            DataEntity.SCFD_ENTRY_DATETIME = DateTime.Now;
                                                            DataEntity.SCFD_MAKER_ID = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();;
                                                            DataEntity.SCFD_CHECKER_ID = "0";
                                                            context.ICORE_SUBMIT_CASE_FORM_DETAIL.Add(DataEntity);
                                                            RowCount += context.SaveChanges();
                                                        }
                                                        if (RowCount > 0)
                                                        {
                                                            RowCount = 0;
                                                            foreach (ICORE_SUBMIT_CASE_DOCUMENT_DRAFT EachDocDraft in DraftDocs)
                                                            {
                                                                ICORE_SUBMIT_CASE_DOCUMENT DataEntity = new ICORE_SUBMIT_CASE_DOCUMENT();
                                                                DataEntity.SCD_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_DOCUMENT.Max(m => (int?)m.SCD_ID)) + 1;
                                                                DataEntity.SCD_DOCUMENT_ID = EachDocDraft.SCDD_DOCUMENT_ID;
                                                                DataEntity.SCD_FORM_ID = MasterCaseForm_ID;
                                                                DataEntity.SCD_ENTRY_DATETIME = DateTime.Now;
                                                                DataEntity.SCD_MAKER_ID = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();;
                                                                DataEntity.SCD_INSERTED_BY = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();;
                                                                DataEntity.SCD_CHECKER_ID = "0";
                                                                if (EachDocDraft.SCDD_DOCUMENT_VALUE != null)
                                                                {
                                                                    string Newpath = EachDocDraft.SCDD_DOCUMENT_VALUE.Replace("Draft_Uploads", "Uploads");
                                                                    DataEntity.SCD_DOCUMENT_VALUE = Newpath;
                                                                    string FileName = Path.GetFileName(EachDocDraft.SCDD_DOCUMENT_VALUE);
                                                                    string OldPath = HttpContext.Server.MapPath(@"~/Draft_Uploads/" + FileName);
                                                                    if (System.IO.File.Exists(OldPath))
                                                                    {
                                                                        System.IO.File.Move(OldPath, Newpath);
                                                                        context.ICORE_SUBMIT_CASE_DOCUMENT.Add(DataEntity);
                                                                        RowCount += context.SaveChanges();
                                                                    }
                                                                    else
                                                                    {
                                                                        ICORE_SYSTEM_SETUP SystemData = context.ICORE_SYSTEM_SETUP.Where(m => m.SS_ID == 1).FirstOrDefault();
                                                                        if (SystemData != null)
                                                                        {
                                                                            string FilePath = "";
                                                                            string FileDomain = SystemData.SS_DOMAIN_FOR_CLIENT;
                                                                            FilePath = FileDomain + "Draft_Uploads/" + FileName;
                                                                            string localPath = new Uri(FilePath).LocalPath;
                                                                            // Check Here if System Have Permissions To View File on Network
                                                                            if (System.IO.File.Exists(localPath))
                                                                            {
                                                                                System.IO.File.Move(localPath, Newpath);
                                                                                context.ICORE_SUBMIT_CASE_DOCUMENT.Add(DataEntity);
                                                                                RowCount += context.SaveChanges();
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            return Json("No data found in the system setup.");
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    DataEntity.SCD_DOCUMENT_VALUE = null;
                                                                    context.ICORE_SUBMIT_CASE_DOCUMENT.Add(DataEntity);
                                                                    RowCount += context.SaveChanges();
                                                                }
                                                            }
                                                            if (RowCount > 0)
                                                            {
                                                                RowCount = 0;
                                                                ICORE_SUBMIT_CASE_FORM_MASTER_DRAFT masterData = context.ICORE_SUBMIT_CASE_FORM_MASTER_DRAFT.Where(m => m.SCFMD_ID == DraftMaster.SCFMD_ID).FirstOrDefault();
                                                                if (masterData != null)
                                                                {
                                                                    context.ICORE_SUBMIT_CASE_FORM_MASTER_DRAFT.Remove(masterData);
                                                                    RowCount += context.SaveChanges();
                                                                    List<ICORE_SUBMIT_CASE_FORM_DETAIL_DRAFT> detailData = context.ICORE_SUBMIT_CASE_FORM_DETAIL_DRAFT.Where(m => m.SCFDD_SCFM_ID == masterData.SCFMD_ID).ToList();
                                                                    if (detailData.Count > 0)
                                                                    {
                                                                        foreach (ICORE_SUBMIT_CASE_FORM_DETAIL_DRAFT DetailsData in detailData)
                                                                        {
                                                                            context.ICORE_SUBMIT_CASE_FORM_DETAIL_DRAFT.Remove(DetailsData);
                                                                            RowCount += context.SaveChanges();
                                                                        }
                                                                    }
                                                                }
                                                                if (RowCount > 0)
                                                                {
                                                                    RowCount = 0;
                                                                    if (DraftDocs.Count > 0)
                                                                    {
                                                                        foreach (ICORE_SUBMIT_CASE_DOCUMENT_DRAFT DocData in DraftDocs)
                                                                        {
                                                                            context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT.Remove(DocData);
                                                                            string FileName = Path.GetFileName(DocData.SCDD_DOCUMENT_VALUE);
                                                                            string OldPath = HttpContext.Server.MapPath(@"~/Draft_Uploads/" + FileName);
                                                                            if (System.IO.File.Exists(OldPath))
                                                                            {
                                                                                System.IO.File.Delete(OldPath);
                                                                            }
                                                                            RowCount += context.SaveChanges();
                                                                        }
                                                                        if (RowCount > 0)
                                                                        {
                                                                            #region Ops Log
                                                                            string Form_Amount = "";
                                                                            string Form_Currency = "";
                                                                            string Form_MFormNo = "";
                                                                            ICORE_OPS_LOG CaseLog = new ICORE_OPS_LOG();
                                                                            ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW getAmount = context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW.Where(m => m.CaseForm_Field_Name.Contains("Amount") && m.SCFD_SCFM_ID == SubmitMasterEntity.SCFM_ID).FirstOrDefault();
                                                                            if (getAmount != null)
                                                                                Form_Amount = getAmount.SCFD_FIELD_VALUE;
                                                                            ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW getCurrency = context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW.Where(m => m.CaseForm_Field_Name.Contains("Currency") && m.SCFD_SCFM_ID == SubmitMasterEntity.SCFM_ID).FirstOrDefault();
                                                                            if (getCurrency != null)
                                                                                Form_Currency = getCurrency.SCFD_FIELD_VALUE;
                                                                            ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW getFormNo = context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_VIEW.Where(m => m.CaseForm_Field_Name.Contains("Form M No") && m.SCFD_SCFM_ID == SubmitMasterEntity.SCFM_ID).FirstOrDefault();
                                                                            if (getFormNo != null)
                                                                                Form_MFormNo = getFormNo.SCFD_FIELD_VALUE;

                                                                            CaseLog.OL_ID = Convert.ToInt32(context.ICORE_OPS_LOG.Max(m => (int?)m.OL_ID)) + 1;
                                                                            CaseLog.OL_CASE_NUMBER = SubmitMasterEntity.SCFM_CASE_NO + "-1";
                                                                            CaseLog.OL_SUBMITTED_DATE = SubmitMasterEntity.SCFM_ENTRY_DATETIME;
                                                                            CaseLog.OL_DEPARTMENT = Dept.Dept_Name;
                                                                            CaseLog.OL_CATEGORY = Categ.Categ_Name;
                                                                            CaseLog.OL_CASE_TYPE = Types.CaseType_Name;
                                                                            CaseLog.OL_CASE_TITLE = Title.CaseTitle_Name;
                                                                            CaseLog.OL_CASE_TYPE = Types.CaseType_Name;
                                                                            CaseLog.OL_CASE_FORM = FormDetails.CaseForm_Name;
                                                                            CaseLog.OL_REGION = "";
                                                                            CaseLog.OL_AGEING = 0;
                                                                            CaseLog.OL_UPDATE_BY = "Customer";
                                                                            CaseLog.OL_CUSTOMER_EMAIL = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();;
                                                                            //Get Status Name from Status ID
                                                                            int SubmitStatus = Convert.ToInt32(SubmitMasterEntity.SCFM_STATUS);
                                                                            ICORE_SUBMIT_CASE_STATUS Case_Status = context.ICORE_SUBMIT_CASE_STATUS.Where(m => m.SCFS_ID == SubmitStatus).FirstOrDefault();
                                                                            ///End
                                                                            CaseLog.OL_CASE_STATUS = Case_Status.SCFS_NAME;
                                                                            CaseLog.OL_LOG_STATUS = Case_Status.SCFS_DESCRIPTION;
                                                                            CaseLog.OL_STATUS_DATE = DraftMaster.SCFMD_STATUS_DATE;
                                                                            CaseLog.OL_CUSTOMER = Session["USER_NAME"] == null ? "" : Session["USER_NAME"].ToString();
                                                                            CaseLog.OL_AMOUNT = Form_Amount;
                                                                            CaseLog.OL_CURRENCY = Form_Currency;
                                                                            CaseLog.OL_USER_ID = Session["USER_PARTY_ID"] == null ? "" : Session["USER_PARTY_ID"].ToString();
                                                                            CaseLog.OL_M_FORM_NUMBER = Form_MFormNo;
                                                                            CaseLog.OL_USER_ROLE = "Customer";
                                                                            CaseLog.OL_ENTRY_DATETIME = DateTime.Now;
                                                                            context.ICORE_OPS_LOG.Add(CaseLog);
                                                                            RowCount = context.SaveChanges();
                                                                            #endregion
                                                                            if (RowCount > 0)
                                                                            {
                                                                                ICORE_CASE_FORMS_MASTER Master = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == SubmitMasterEntity.SCFM_CASE_FORM_ID).FirstOrDefault();
                                                                                string LoggedInUser = Session["USER_ID"] == null ? "" : Session["USER_ID"].ToString();;
                                                                                string LoggedInUserName = Session["USER_NAME"] == null ? "" : Session["USER_NAME"].ToString();
                                                                                string DateNTime = Convert.ToDateTime(SubmitMasterEntity.SCFM_ENTRY_DATETIME).ToString();
                                                                                #region Get Email CC
                                                                                string CCUser = "";

                                                                                int?[] ROlEIdsWithCaseTitles = context.ICORE_ASSIGN_CASE_TITLES.Where(m => m.ACT_TITLE_ID == Title.CaseTitle_Id && m.ACT_STATUS == true).Select(m => m.ACT_ROLE_ID).ToArray();
                                                                                if (ROlEIdsWithCaseTitles.Count() > 0)
                                                                                {
                                                                                    List<ICORE_ASSIGN_FUNCTIONS> checkfunction = context.ICORE_ASSIGN_FUNCTIONS.Where(m => ROlEIdsWithCaseTitles.Contains(m.AF_ROLE_ID) && m.AF_FUNCTION_ID == 12 && m.AF_STATUS == true && m.AF_ROLE_ID != 1).ToList();
                                                                                    if (checkfunction != null)
                                                                                    {
                                                                                        foreach (ICORE_ASSIGN_FUNCTIONS item in checkfunction)
                                                                                        {
                                                                                            ICORE_ROLES getRoleFromFunction = context.ICORE_ROLES.Where(m => m.R_ID == item.AF_ROLE_ID && m.R_ISAUTH == true && m.R_STATUS == "true").FirstOrDefault();
                                                                                            if (getRoleFromFunction != null)
                                                                                            {
                                                                                                List<ICORE_ASSIGN_LOGIN_RIGHTS> GetRolesDetail = context.ICORE_ASSIGN_LOGIN_RIGHTS.Where(m => m.ALR_ROLE_ID == getRoleFromFunction.R_ID && m.ALR_STATUS == true).ToList();
                                                                                                if (GetRolesDetail.Count() > 0)
                                                                                                {
                                                                                                    foreach (ICORE_ASSIGN_LOGIN_RIGHTS items in GetRolesDetail)
                                                                                                    {
                                                                                                        ICORE_LOGIN GetUser = context.ICORE_LOGIN.Where(m => m.LOG_USER_ID == items.ALR_USER_ID && m.LOG_ISAUTH == true && m.LOG_USER_STATUS == "Active").FirstOrDefault();
                                                                                                        if (GetUser != null)
                                                                                                        {
                                                                                                            if (!CCUser.Contains(GetUser.LOG_EMAIL))
                                                                                                                CCUser += GetUser.LOG_EMAIL + ",";
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                                if (ROlEIdsWithCaseTitles.Count() > 0)
                                                                                    CCUser = CCUser.Remove(CCUser.Length - 1);
                                                                                #endregion

                                                                                ICORE_EMAIL_CONFIGRATION EmailConfigration = context.ICORE_EMAIL_CONFIGRATION.Where(m => m.EC_IS_AUTH == true && m.EC_STATUS == true).FirstOrDefault();
                                                                                if (EmailConfigration != null)
                                                                                {
                                                                                    string ClientToEmail = "";
                                                                                    #region Get All Emails on from current party id
                                                                                    string PartyID = Session["USER_PARTY_ID"].ToString();
                                                                                    List<ICORE_CLIENT_PORTFOLIO> GetClientEmailsFromPartyId = context.ICORE_CLIENT_PORTFOLIO.Where(m => m.CP_PARTY_ID == PartyID && m.CP_STATUS == "Active").ToList();
                                                                                    if (GetClientEmailsFromPartyId.Count() > 0)
                                                                                    {
                                                                                        foreach (ICORE_CLIENT_PORTFOLIO item in GetClientEmailsFromPartyId)
                                                                                        {
                                                                                            if (ClientToEmail != "")
                                                                                            {
                                                                                                if (!ClientToEmail.Contains(item.CP_EMAIL))
                                                                                                {
                                                                                                    ClientToEmail += "," + item.CP_EMAIL;
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                                ClientToEmail += item.CP_EMAIL;
                                                                                        }
                                                                                        if (ClientToEmail.EndsWith(","))
                                                                                            ClientToEmail = ClientToEmail.Remove(ClientToEmail.Length, -1);
                                                                                    }
                                                                                    #endregion

                                                                                    using (MailMessage mail = new MailMessage())
                                                                                    {
                                                                                        string FromEmail = EmailConfigration.EC_CREDENTIAL_ID;
                                                                                        string HostName = EmailConfigration.EC_SEREVER_HOST;
                                                                                        int EmailPort = Convert.ToInt32(EmailConfigration.EC_EMAIL_PORT);
                                                                                        NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                                                                        NetworkCredentials.UserName = FromEmail;
                                                                                        ////#if dev
                                                                                        //NetworkCredentials.Password = "Test@@123";
                                                                                        ////#if dev
                                                                                        mail.From = new MailAddress(FromEmail);
                                                                                        if (ClientToEmail != "")
                                                                                        {
                                                                                            if (ClientToEmail.Contains(","))
                                                                                            {
                                                                                                string[] TO_Email = ClientToEmail.Split(',');
                                                                                                foreach (string MultiEmail in TO_Email)
                                                                                                {
                                                                                                    mail.To.Add(MultiEmail);
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                                mail.To.Add(ClientToEmail);
                                                                                        }
                                                                                        else
                                                                                            mail.To.Add(LoggedInUser);
                                                                                        if (CCUser != "")
                                                                                        {
                                                                                            string[] CC_Email = CCUser.Split(',');
                                                                                            foreach (string MultiEmail in CC_Email)
                                                                                            {
                                                                                                mail.CC.Add(MultiEmail);
                                                                                            }
                                                                                        }
                                                                                        mail.Subject = "FX Digitalization (Case No : " + SubmitMasterEntity.SCFM_CASE_NO + ").";
                                                                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Candara;font-size:40px;text-align:center'>Foreign Exchange Case Submission</p><p style='font-family:Candara;font-size:15px'>Dear Valued Client,</p><p style='font-family:Candara;font-size:15px'>We acknowledge that you have submitted a Case (No. <span style = 'font-family:Arial'><b> " + SubmitMasterEntity.SCFM_CASE_NO + "</b></span>) for Foreign Exchange transaction.<br />  </p><p style='font-family:Candara;font-size:15px'>Below are the Case details,</p><p style='font-family:Candara;font-size:15px'>Case No# : <span style='font-family:Arial'><b>" + SubmitMasterEntity.SCFM_CASE_NO + "</b></span></p><p style='font-family:Candara;font-size:15px'>Case Status : <b style='font-size:18px'>" + Case_Status.SCFS_NAME + " -- " + Case_Status.SCFS_DESCRIPTION + "</b></p><p style='font-family:Candara;font-size:15px'>Submitted Date & Time : <span style='font-family:Arial'><b>" + DateNTime + "</b></span></p><p style='font-family:Candara;font-size:15px'> <b>This is a system generated email, please do reply all. In case of queries please call 111-777-777 for respective your service contact. </b></p></div></div></div><div class='col-md-2'></div></div>";
                                                                                        mail.IsBodyHtml = true;
                                                                                        try
                                                                                        {
                                                                                            using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                                                                            {
                                                                                                smtp.Credentials = NetworkCredentials;
                                                                                                //smtp.EnableSsl = true;
                                                                                                smtp.Send(mail);
                                                                                            }
                                                                                            return Json("");
                                                                                        }
                                                                                        catch (Exception ex)
                                                                                        {
                                                                                            DAL.LogException("Case_Submission_Form_Draft", "SubmitDraftToCase", Session["USER_ID"].ToString(), ex);
                                                                                            return Json("Unable to send the email.");
                                                                                        }
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    return Json("No Email setup found in the system.");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        return Json("Error While Feching Document");
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    return Json("Error Occured While Removing Draft From The System");
                                                                }
                                                            }
                                                            else
                                                            {
                                                                return Json("Error While Saving Documents For The Case");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            return Json("Error While Saving Document Fields Data");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        return Json("Error While Saving Master Entry For The Case");
                                                    }
                                                }
                                                else
                                                {
                                                    return Json("No Document Information Found For the Given Draft No : " + DraftMaster.SCFMD_CASE_NO);
                                                }
                                            }
                                            else
                                            {
                                                return Json("No Fields Information Found in Drafts for the following Draft No : " + DraftMaster.SCFMD_CASE_NO);
                                            }
                                        }
                                        else
                                        {
                                            return Json("Error While Fetching Draft on the Given Draft No : " + DraftMaster.SCFMD_CASE_NO);
                                        }
                                    }
                                }
                                else
                                {
                                    message = "Error While fetching previous records";
                                }
                            }
                            else
                            {
                                message = "Exception Occur " + Environment.NewLine + "Please Contact to Administrator";
                            }
                        }
                    }
                    else
                        return Json("No related Case found in the drafts.");
                }
                else
                {
                    return Json("InValid Case No Provided");
                }
            }
            catch (Exception ex)
            {
                message = DAL.LogException("Case_Submission_Form_Draft", "SubmitDraftToCase", Session["USER_ID"].ToString(), ex);
            }
            return Json(message);
        }
        #endregion

        #region Edit Saved Draft
        public JsonResult GetSavedFormFields(int FormID)
        {
            List<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_DRAFT_VIEW> DetailEntity = new List<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_DRAFT_VIEW>();
            int FormFieldsID = Convert.ToInt32(FormID);
            DetailEntity = (context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_DRAFT_VIEW.Where(x => x.SCFDD_SCFM_ID == FormFieldsID && x.CaseForm_Filed_IsAD == false)).ToList();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string Result = javaScriptSerializer.Serialize(DetailEntity);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSavedFormDocuments(int ForeignID)
        {
            List<ICORE_SUBMIT_CASE_DOCUMENT_DRAFT_VIEW> DetailEntity = new List<ICORE_SUBMIT_CASE_DOCUMENT_DRAFT_VIEW>();
            int FormFieldsID = Convert.ToInt32(ForeignID);
            DetailEntity = (context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT_VIEW.Where(x => x.SCDD_FORM_ID == FormFieldsID && x.Case_Doc_IsAD == false)).ToList();
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string Result = javaScriptSerializer.Serialize(DetailEntity);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult EditSavedFieldsDraft(int Form_Id)
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                try
                {
                    EditSavedForm Entity = new EditSavedForm();
                    Entity.Master = context.ICORE_SUBMIT_CASE_FORM_MASTER_DRAFT.Where(m => m.SCFMD_ID == Form_Id).FirstOrDefault();
                    Entity.FieldList = context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_DRAFT_VIEW.Where(m => m.SCFDD_SCFM_ID == Form_Id && m.CaseForm_Filed_IsAD == false).ToList();
                    Entity.DocumentList = context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT_VIEW.Where(m => m.SCDD_FORM_ID == Form_Id && m.Case_Doc_IsAD == false).ToList();
                    if (Entity != null)
                    {
                        return View(Entity);
                    }
                    else
                    {
                        message = "Error while fetching records on selected Id";
                    }
                }
                catch (Exception ex)
                {
                    message = DAL.LogException("Case_Submission_Form_Draft", "EditSavedFieldsDraft", Session["USER_ID"].ToString(), ex);
                }
                finally
                {
                    ViewBag.message = message;
                }
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "fxdig");
            }
        }
        [HttpPost]
        public ActionResult EditSavedFieldsDraft(decimal? FormId)
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                try
                {
                    ICORE_SUBMIT_CASE_FORM_MASTER_DRAFT DraftMaster = context.ICORE_SUBMIT_CASE_FORM_MASTER_DRAFT.Where(m => m.SCFMD_ID == FormId).FirstOrDefault();

                    ICORE_CASE_FORMS_MASTER MasterEntity = context.ICORE_CASE_FORMS_MASTER.Where(m => m.CaseForm_Id == DraftMaster.SCFMD_CASE_FORM_ID).FirstOrDefault();
                    List<ICORE_CASE_DOCUMENTS> DocumentEntity = context.ICORE_CASE_DOCUMENTS.Where(m => m.Case_Doc_CaseTitleId == MasterEntity.CaseForm_CaseTitle_Id && m.Case_Doc_IsAD == false).ToList();
                    foreach (ICORE_CASE_DOCUMENTS DocItem in DocumentEntity)
                    {
                        ICORE_CASE_DOCUMENTS_FORMAT DocumentFormat = context.ICORE_CASE_DOCUMENTS_FORMAT.Where(m => m.Format_id == DocItem.Case_Doc_Format_id).FirstOrDefault();
                        var DocumentId = "#ctrldocsub" + DocItem.Case_Doc_Name.ToString().Replace(" ", "").Substring(0, 3) + DocItem.Case_Doc_Id;
                        HttpPostedFileBase DocumentData = Request.Files[DocumentId];
                        if (DocumentData != null)
                        {
                            string FileName = Path.GetFileNameWithoutExtension(DocumentData.FileName);
                            string FileExtension = Path.GetExtension(DocumentData.FileName);
                            string FileExten = FileExtension.Replace(".", "").ToString();
                            byte[] document = new byte[DocumentData.ContentLength];
                            DocumentData.InputStream.Read(document, 0, DocumentData.ContentLength);
                            //System.UInt32 mimetype;
                            //FileMimeType.FindMimeFromData(0, null, document, 256, null, 0, out mimetype, 0);
                            //System.IntPtr mimeTypePtr = new IntPtr(mimetype);
                            //string mime = Marshal.PtrToStringUni(mimeTypePtr);
                            //Marshal.FreeCoTaskMem(mimeTypePtr);
                            //if (FileExten.ToLower() == "pdf")
                            //{
                            //    if (mime != "application/pdf")
                            //    {
                            //        message = "Invalid file : " + DocumentData.FileName + Environment.NewLine + "Invalid PDF file provided.";
                            //        return Json(message);
                            //    }
                            //}
                            if (FileName != DocItem.Case_Doc_Name)
                            {
                                string RequiredExten = Path.GetExtension(DocumentData.FileName);
                                message = "Invalid document provided for field : " + DocItem.Case_Doc_Name + Environment.NewLine + "Required document name is " + DocItem.Case_Doc_Name + RequiredExten;
                                return Json(message);
                            }
                        }
                    }

                    List<ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_DRAFT_VIEW> DetailData = context.ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_DRAFT_VIEW.Where(m => m.SCFDD_SCFM_ID == FormId).ToList();
                    int RowCount = 0;
                    if (DetailData.Count > 0)
                    {
                        foreach (ICORE_SUBMIT_CASE_FORM_FIELD_DETAIL_DRAFT_VIEW item in DetailData)
                        {
                            ICORE_SUBMIT_CASE_FORM_DETAIL_DRAFT UpdateEntity = context.ICORE_SUBMIT_CASE_FORM_DETAIL_DRAFT.Where(m => m.SCFDD_ID == item.SCFDD_ID).FirstOrDefault();
                            if (UpdateEntity != null)
                            {
                                var ElementId = "#ctrlfrmsub" + item.CaseForm_Field_Name.ToString().Replace(" ", "").Substring(0, 3) + item.SCFDD_FIELD_ID;
                                string FieldsData = Request.Form[ElementId];
                                string GetFieldID = ElementId.Remove(0, 14);
                                int FieldID = Convert.ToInt32(GetFieldID);
                                UpdateEntity.SCFDD_FIELD_VALUE = FieldsData;
                                UpdateEntity.SCFDD_EDIT_DATETIME = DateTime.Now;
                            }
                            else
                            {
                                message = "Error While fetching previous records";
                            }
                            context.Entry(UpdateEntity).State = EntityState.Modified;
                            RowCount += context.SaveChanges();
                        }
                        if (RowCount > 0)
                        {
                            RowCount = 0;
                            List<ICORE_SUBMIT_CASE_DOCUMENT_DRAFT_VIEW> ListOfDocument = context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT_VIEW.Where(m => m.SCDD_FORM_ID == FormId).ToList();
                            if (ListOfDocument.Count > 0)
                            {
                                foreach (ICORE_SUBMIT_CASE_DOCUMENT_DRAFT_VIEW item in ListOfDocument)
                                {
                                    ICORE_SUBMIT_CASE_DOCUMENT_DRAFT UpdateDocument = context.ICORE_SUBMIT_CASE_DOCUMENT_DRAFT.Where(m => m.SCDD_ID == item.SCDD_ID).FirstOrDefault();
                                    if (UpdateDocument != null)
                                    {
                                        var DocumentId = "#ctrldocsub" + item.Case_Doc_Name.ToString().Replace(" ", "").Substring(0, 3) + item.SCDD_DOCUMENT_ID;
                                        HttpPostedFileBase DocumentData = Request.Files[DocumentId];
                                        if (DocumentData != null && item.Case_Doc_IsAD == false)
                                        {
                                            string GetDocID = DocumentId.Remove(0, 14);
                                            int DocId = Convert.ToInt32(GetDocID);
                                            string newFileName = "";
                                            string path = HttpContext.Server.MapPath(@"~/Draft_Uploads/");
                                            bool exists = System.IO.Directory.Exists(path);
                                            if (!exists)
                                                System.IO.Directory.CreateDirectory(path);
                                            string extension = Path.GetExtension(DocumentData.FileName);
                                            newFileName = "C-" + String.Format("{0:000000}", MasterEntity.CaseForm_Id) + "-D-" + String.Format("{0:000000}", DocId) + "-" + DateTime.Now.ToString("ddMMyyyyhhmmss") + extension;
                                            string filePath = Path.Combine(path, newFileName);
                                            if (System.IO.File.Exists(UpdateDocument.SCDD_DOCUMENT_VALUE))
                                            {
                                                System.IO.File.Delete(UpdateDocument.SCDD_DOCUMENT_VALUE);
                                            }
                                            else
                                            {
                                                ICORE_SYSTEM_SETUP SystemData = context.ICORE_SYSTEM_SETUP.Where(m => m.SS_ID == 1).FirstOrDefault();
                                                if (SystemData != null)
                                                {
                                                    string FilePath = "";
                                                    string FileName = Path.GetFileName(UpdateDocument.SCDD_DOCUMENT_VALUE);
                                                    string FileDomain = SystemData.SS_DOMAIN_FOR_CLIENT;
                                                    FilePath = FileDomain + "Draft_Uploads/" + FileName;
                                                    string localPath = new Uri(FilePath).LocalPath;
                                                    // Check Here if System Have Permissions To View File on Network
                                                    if (System.IO.File.Exists(localPath))
                                                    {
                                                        System.IO.File.Delete(localPath);
                                                    }
                                                }
                                                else
                                                {
                                                    return Json("No data found in the system setup.");
                                                }
                                            }
                                            UpdateDocument.SCDD_DOCUMENT_VALUE = filePath;
                                            UpdateDocument.SCDD_EDIT_DATETIME = DateTime.Now;
                                            DocumentData.SaveAs(filePath);
                                            context.Entry(UpdateDocument).State = System.Data.Entity.EntityState.Modified;
                                        }
                                        else
                                        {
                                            if (item.Case_Doc_IsAD == true)
                                            {
                                                string GetDocID = DocumentId.Remove(0, 14);
                                                int DocId = Convert.ToInt32(GetDocID);
                                                UpdateDocument.SCDD_DOCUMENT_VALUE = null;
                                                UpdateDocument.SCDD_EDIT_DATETIME = DateTime.Now;
                                                context.Entry(UpdateDocument).State = System.Data.Entity.EntityState.Modified;
                                            }
                                            else
                                            {
                                                //if (DocumentData == null)
                                                //{
                                                //    string GetDocID = DocumentId.Remove(0, 14);
                                                //    int DocId = Convert.ToInt32(GetDocID);
                                                //    UpdateDocument.SCDD_DOCUMENT_VALUE = null;
                                                //    UpdateDocument.SCDD_EDIT_DATETIME = DateTime.Now;
                                                //    context.Entry(UpdateDocument).State = System.Data.Entity.EntityState.Modified;
                                                //}
                                            }
                                        }
                                    }
                                    else
                                    {
                                        message = "Error While fetching previous records";
                                    }
                                    RowCount += context.SaveChanges();
                                }
                                if (RowCount > 0)
                                {
                                    message = "";
                                }
                            }
                            else
                            {
                                message = "Error While fetching previous records";
                            }
                        }
                        else
                        {
                            message = "Exception Occur " + Environment.NewLine + "Please Contact to Administrator";
                        }
                    }
                }
                catch (Exception ex)
                {
                    message = DAL.LogException("Case_Submission_Form_Draft", "EditSavedFieldsDraft", Session["USER_ID"].ToString(), ex);
                }
                finally
                {
                    ViewBag.message = message;
                }
                return Json(message);
            }
            else
            {
                return RedirectToAction("Logout", "fxdig");
            }
        }
        #endregion
    }
}