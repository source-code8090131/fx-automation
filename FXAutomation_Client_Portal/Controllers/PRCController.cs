﻿#define prod
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using FXAutomation_Client_Portal.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace FXAutomation_Client_Portal.Controllers
{
    public class PRCController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();

        [CustomRequestFilterPRC]
        // GET: PRC
        public ActionResult Index()
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                RequestPRC Entity = new RequestPRC();
                try
                {
                    Entity.FROM_DATE = null;
                    Entity.TO_DATE = null;
                }
                catch (Exception ex)
                {
                    message = DAL.LogException("PRC", "Index", Session["USER_ID"].ToString(), ex);
                }
                finally
                {
                    List<SelectListItem> PaymentType = new SelectList(context.PRC_PAYMENT_TYPE.ToList(), "PPT_NAME", "PPT_NAME", 0).ToList();
                    PaymentType.Insert(0, (new SelectListItem { Text = "--Select Payment Type--", Value = "0" }));
                    ViewBag.PaymentType = PaymentType;
                }
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "fxdig");
            }
        }
        [HttpPost]
        public ActionResult Index(RequestPRC Entity)
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                int RequestLogID = 0;
                List<int> RequestLogIDs = new List<int>();
                try
                {
                    if (Entity.FROM_DATE != null && Entity.TO_DATE != null && Entity.IBAN_NO != null)
                    {
                        string fromdate = Convert.ToDateTime(Entity.FROM_DATE, System.Globalization.CultureInfo.CurrentCulture).ToString("dd MMMM yyyy");
                        string todate = Convert.ToDateTime(Entity.TO_DATE, System.Globalization.CultureInfo.CurrentCulture).ToString("dd MMMM yyyy");
                        PRC_EMAIL_CONFIGRATION CheckEmail = context.PRC_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true && m.EC_IS_AUTH == true).FirstOrDefault();
                        if (CheckEmail != null)
                        {
                            string PARTY_ID = Session["USER_PARTY_ID"] == null ? "" : Session["USER_PARTY_ID"].ToString();
#if prod
                            PRC_SYSYTEM_SETUP SystemConfig = context.PRC_SYSYTEM_SETUP.Where(m => m.PSS_ISAUTH == true && m.PSS_GCAR_API_URL != null && m.PSS_GCAR_CERT_HASH != null).FirstOrDefault();
                            if (SystemConfig != null)
                            {
                                string CertificateDirectory = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "ClientCertificates\\GCAR");
                                if (Directory.Exists(CertificateDirectory))
                                {
                                    string firstFileName = Directory.GetFiles(CertificateDirectory).FirstOrDefault();
                                    if (firstFileName != null)
                                    {
                                        if (firstFileName.Contains(".pfx"))
                                        {
                                            string CertPath = Path.Combine(CertificateDirectory, firstFileName);
                                            if (System.IO.File.Exists(CertPath))
                                            {
                                                string UNIQUE_REF = Guid.NewGuid().ToString();
                                                if (UNIQUE_REF.Length >= 10)
                                                {
                                                    if (PARTY_ID.Length > 5)
                                                    {                                                       
                                                        message = DAL.SendGidaRequestWithCertPath(PARTY_ID, Entity.IBAN_NO, UNIQUE_REF, SystemConfig.PSS_GCAR_API_URL, CertPath, DAL.Decrypt(SystemConfig.PSS_GCAR_CERT_HASH));
                                                        if (message == "VALIDATION_STAT = ACK")
                                                        {
#endif
                                                          string ClientEmail = Session["USER_ID"].ToString();
                                                            if (ClientEmail.Length > 4)
                                                            {
                                                                /// Add This Client To Database
                                                                int RowCount = 0;
                                                                #region Adding Client to Database from Request header
                                                                PRC_CLIENT_PORTFOLIO newClient = context.PRC_CLIENT_PORTFOLIO.Where(m => m.PCP_IBAN == Entity.IBAN_NO && m.PCP_PARTY_ID == PARTY_ID && m.PCP_EMAIL == ClientEmail).FirstOrDefault();
                                                                if (newClient == null)
                                                                {
                                                                    newClient = new PRC_CLIENT_PORTFOLIO();
                                                                    newClient.PCP_ID = Convert.ToInt32(context.PRC_CLIENT_PORTFOLIO.Max(m => (decimal?)m.PCP_ID)) + 1;
                                                                    newClient.PCP_PARTY_ID = PARTY_ID;
                                                                    newClient.PCP_IBAN = Entity.IBAN_NO;
                                                                    newClient.PCP_EMAIL = ClientEmail;
                                                                    newClient.PCP_NAME = GetClientNameFromEmail(ClientEmail);
                                                                    //// Get Previous Notification Email
                                                                    PRC_CLIENT_PORTFOLIO PrevNotiFEmails = context.PRC_CLIENT_PORTFOLIO.Where(m => m.PCP_IBAN == Entity.IBAN_NO && m.PCP_PARTY_ID == PARTY_ID).FirstOrDefault();
                                                                    if (PrevNotiFEmails != null)
                                                                        newClient.PCP_NOTIFICATION_EMAIL = PrevNotiFEmails.PCP_NOTIFICATION_EMAIL;
                                                                    newClient.PCP_MAKER_ID = "CitiDirect";
                                                                    newClient.PCP_CHECKER_ID = "CitiDirect";
                                                                    newClient.PCP_ISAUTH = true;
                                                                    newClient.PCP_STATUS = "Active";
                                                                    newClient.PCP_SENT_AUTO_EMAILING = "Enable";
                                                                    context.PRC_CLIENT_PORTFOLIO.Add(newClient);
                                                                    RowCount = context.SaveChanges();
                                                                }
                                                                else
                                                                    RowCount = 1;
                                                                #endregion
                                                                if (RowCount > 0)
                                                                {
                                                                    ReportDocument rd = new ReportDocument();
                                                                    PRC_CLIENT_PORTFOLIO GetClientClient = context.PRC_CLIENT_PORTFOLIO.Where(m => m.PCP_IBAN == Entity.IBAN_NO && m.PCP_PARTY_ID == PARTY_ID && m.PCP_EMAIL == ClientEmail).FirstOrDefault();
                                                                    if (GetClientClient != null)
                                                                    {
                                                                        if (Entity.ePRC == true)
                                                                        {
                                                                            var PRCData = context.PRC_SEND_EMAIL_WITH_PRC(Entity.FROM_DATE, Entity.TO_DATE, Entity.IBAN_NO, Entity.PRC_REF, Entity.TRANS_REF).ToList();
                                                                            if (PRCData.Count() > 0)
                                                                            {
                                                                                PRCData = PRCData.Where(m => m.GPD_MAKER_ID != Session["USER_ID"].ToString() || m.GPD_ISAUTH == true).ToList();
                                                                                if (PRCData.Count > 0)
                                                                                {
                                                                                    if (PRCData.Count <= 200)
                                                                                    {
                                                                                        using (MailMessage mail = new MailMessage())
                                                                                        {
                                                                                            string ToEmail = GetClientClient.PCP_EMAIL;
                                                                                            string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                                                                            string HostName = CheckEmail.EC_SEREVER_HOST;
                                                                                            int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                                                                            NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                                                                            NetworkCredentials.UserName = FromEmail;
                                                                                            //////#if dev
                                                                                            //NetworkCredentials.Password = "Test@@123";
                                                                                            mail.From = new MailAddress(FromEmail);
                                                                                            mail.To.Add(new MailAddress(ToEmail));
                                                                                            if (GetClientClient.PCP_NOTIFICATION_EMAIL != null && GetClientClient.PCP_NOTIFICATION_EMAIL != "")
                                                                                            {
                                                                                                string[] CC_Email = GetClientClient.PCP_NOTIFICATION_EMAIL.Split(',');
                                                                                                foreach (string MultiEmail in CC_Email)
                                                                                                {
                                                                                                    mail.CC.Add(MultiEmail);
                                                                                                }
                                                                                            }
                                                                                            if (Entity.ePRC)
                                                                                            {
                                                                                                mail.Subject = "(Secure) PRC Report " + fromdate + " To " + todate;
                                                                                                mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Dear Client,</p><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Please find attached here with the PRC (Proceed Realization Certificate) for transactions covering the period of " + fromdate + " with respect to Account number " + Entity.IBAN_NO + ".<br />  </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>For any queries, please reach out to below contacts:<br /> </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>Email: citiservice.pakistan@citi.com <br /> </p><p style='font-family:Arial;font-size:13px'>Contact No: 021-111-777-777 </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'><b>Note</b>: This is a system generated email, Please don't reply. <br /> </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;' ><b> Disclaimer </b>: This email is confidential, may be legally privileged, and is for the intended recipient only.Access, disclosure, copying, distribution, or reliance on any of it by anyone else is prohibited and may be a criminal offence.Please delete if obtained in error and email confirmation to the sender.<br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                                                                            }
                                                                                            string LogRequestNo = DAL.GenerateRequestNo();
                                                                                            foreach (var PRC in PRCData)
                                                                                            {
                                                                                                #region Log PRC Request
                                                                                                PRC_REQUEST_LOG Request = new PRC_REQUEST_LOG();
                                                                                                Request.PRL_ID = Convert.ToInt32(context.PRC_REQUEST_LOG.Max(m => (decimal?)m.PRL_ID)) + 1;
                                                                                                RequestLogIDs.Add(Request.PRL_ID);
                                                                                                Request.PRL_CLIENT_EMAIL = GetClientClient.PCP_EMAIL;
                                                                                                Request.PRL_FROM_DATE = Entity.FROM_DATE;
                                                                                                Request.PRL_TO_DATE = Entity.TO_DATE;
                                                                                                Request.PRL_IS_SUCCESS = false;
                                                                                                Request.PRL_REQUEST_DATE = DateTime.Now;
                                                                                                Request.PRL_IBAN_NO = Entity.IBAN_NO;
                                                                                                Request.PRL_REQUEST_TYPE = "ePRC";
                                                                                                Request.PRL_UNIQUE_NO = PRC.GPD_PRC_NUMBER;
                                                                                                Request.PRL_REQUEST_NO = LogRequestNo;
                                                                                                Request.PRL_ISSUANCE_DATE = PRC.GPD_UPLOAD_DATE;
                                                                                                context.PRC_REQUEST_LOG.Add(Request);
                                                                                                context.SaveChanges();
                                                                                                #endregion

                                                                                                string FileName = "";
                                                                                                var PRCReportView = context.PRC_GOV_PRC_DETAIL_VIEW.Where(m => m.GPD_ID == PRC.GPD_ID).FirstOrDefault();
                                                                                                if (Entity.ePRC)
                                                                                                {
                                                                                                    FileName = "EPRC_" + PRC.GPD_BANKREFNO.Replace(" ", "") + "_" + DateTime.Now.ToString("yyyyMMdd");
                                                                                                    rd.Load(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "CRReport\\e-PRC.rpt"));
                                                                                                    rd.DataSourceConnections.Clear();
                                                                                                    rd.SetDataSource(new[] { PRCReportView });
                                                                                                }
                                                                                                if (Entity.PaymentType != "0")
                                                                                                {
                                                                                                    if (Entity.ePRC)
                                                                                                        rd.SetParameterValue("PaymentCode", Entity.PaymentType);
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    if (Entity.ePRC)
                                                                                                        rd.SetParameterValue("PaymentCode", "J/O-3");
                                                                                                }
                                                                                                mail.Attachments.Add(new Attachment(rd.ExportToStream(ExportFormatType.PortableDocFormat), FileName + ".pdf"));
                                                                                            }
                                                                                            mail.IsBodyHtml = true;
                                                                                            using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                                                                            {
                                                                                                smtp.Credentials = NetworkCredentials;
                                                                                                //if dev
                                                                                                //smtp.EnableSsl = true;
                                                                                                smtp.Send(mail);
                                                                                            }
                                                                                            message = "Email has been sent successfully.";
                                                                                            foreach (int LogRequestIds in RequestLogIDs)
                                                                                            {
                                                                                                PRC_REQUEST_LOG Update = context.PRC_REQUEST_LOG.Where(m => m.PRL_ID == LogRequestIds).FirstOrDefault();
                                                                                                if (Update != null)
                                                                                                {
                                                                                                    Update.PRL_IS_SUCCESS = true;
                                                                                                    Update.PRL_RESPONSE = message;
                                                                                                    context.Entry(Update).State = System.Data.Entity.EntityState.Modified;
                                                                                                    context.SaveChanges();
                                                                                                }
                                                                                                else
                                                                                                    message = "Problem while updating response in logs on : " + LogRequestIds + " .";
                                                                                            }


                                                                                            return View();
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                        message = "Requested data exceeds the maximum limit. Please reduce your request date range.";
                                                                                }
                                                                                else
                                                                                    message = "No PRC Found on the selectd Start Date : " + fromdate + " End Date : " + todate;
                                                                            }
                                                                            else
                                                                                message = "No PRC Found on the selectd Start Date : " + fromdate + " End Date : " + todate;
                                                                        }
                                                                        else if (Entity.sPRC == true)
                                                                        {
                                                                            var PRCData = context.PRC_SEND_EMAIL_WITH_PRC(Entity.FROM_DATE, Entity.TO_DATE, Entity.IBAN_NO, Entity.PRC_REF, Entity.TRANS_REF).FirstOrDefault();
                                                                            if (PRCData != null)
                                                                            {
                                                                                #region Log PRC Request
                                                                                PRC_REQUEST_LOG Request = new PRC_REQUEST_LOG();
                                                                                Request.PRL_ID = Convert.ToInt32(context.PRC_REQUEST_LOG.Max(m => (decimal?)m.PRL_ID)) + 1;
                                                                                RequestLogID = Request.PRL_ID;
                                                                                Request.PRL_CLIENT_EMAIL = GetClientClient.PCP_EMAIL;
                                                                                Request.PRL_FROM_DATE = Entity.FROM_DATE;
                                                                                Request.PRL_TO_DATE = Entity.TO_DATE;
                                                                                Request.PRL_IS_SUCCESS = false;
                                                                                Request.PRL_REQUEST_DATE = DateTime.Now;
                                                                                Request.PRL_IBAN_NO = Entity.IBAN_NO;
                                                                                Request.PRL_REQUEST_TYPE = "sPRC";
                                                                                Request.PRL_UNIQUE_NO = DAL.GenerateSPRCNumber();
                                                                                Request.PRL_REQUEST_NO = DAL.GenerateRequestNo();
                                                                                Request.PRL_ISSUANCE_DATE = Request.PRL_REQUEST_DATE;
                                                                                context.PRC_REQUEST_LOG.Add(Request);
                                                                                context.SaveChanges();
                                                                                #endregion

                                                                                using (MailMessage mail = new MailMessage())
                                                                                {
                                                                                    string ToEmail = GetClientClient.PCP_EMAIL;
                                                                                    string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                                                                    string HostName = CheckEmail.EC_SEREVER_HOST;
                                                                                    int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                                                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                                                                    NetworkCredentials.UserName = FromEmail;
                                                                                    //////#if dev
                                                                                    //NetworkCredentials.Password = "Test@@123";
                                                                                    mail.From = new MailAddress(FromEmail);
                                                                                    mail.To.Add(new MailAddress(ToEmail));
                                                                                    if (GetClientClient.PCP_NOTIFICATION_EMAIL != null && GetClientClient.PCP_NOTIFICATION_EMAIL != "")
                                                                                    {
                                                                                        string[] CC_Email = GetClientClient.PCP_NOTIFICATION_EMAIL.Split(',');
                                                                                        foreach (string MultiEmail in CC_Email)
                                                                                        {
                                                                                            mail.CC.Add(MultiEmail);
                                                                                        }
                                                                                    }
                                                                                    if (Entity.sPRC)
                                                                                    {
                                                                                        mail.Subject = "(Secure) S-PRC Report " + fromdate + " To " + todate;
                                                                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Dear Client,</p><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Please find attached herewith the S-PRC (Statement of Proceeds Realization Certificates) for transactions covering the period of " + fromdate + " with respect to Account number " + Entity.IBAN_NO + ".<br />  </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>For any queries, please reach out to below contacts:<br /> </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>Email: citiservice.pakistan@citi.com <br /> </p><p style='font-family:Arial;font-size:13px'>Contact No: 021-111-777-777 </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'><b>Note</b>: This is a system generated email, Please don't reply. <br /> </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;' ><b> Disclaimer </b>: This email is confidential, may be legally privileged, and is for the intended recipient only.Access, disclosure, copying, distribution, or reliance on any of it by anyone else is prohibited and may be a criminal offence.Please delete if obtained in error and email confirmation to the sender.<br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                                                                    }

                                                                                    string FileName = "";
                                                                                    var PRCReportView = context.PRC_GOV_PRC_DETAIL_VIEW.Where(m => m.GPD_ID == PRCData.GPD_ID).FirstOrDefault();
                                                                                    if (Entity.sPRC)
                                                                                    {
                                                                                        FileName = "SPRC_Summary_" + DateTime.Now.ToString("yyyyMMdd");
                                                                                        var PRCSubReportSource = context.PRC_GOV_PRC_DETAIL_SUB_REPORT_PROC(Entity.FROM_DATE, Entity.TO_DATE, Entity.IBAN_NO).ToList();
                                                                                        if (PRCSubReportSource.Count() > 0)
                                                                                            PRCSubReportSource = PRCSubReportSource.Where(m => m.GPD_MAKER_ID != Session["USER_ID"].ToString() || m.GPD_ISAUTH == true).ToList();

                                                                                        rd.Load(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "CRReport\\S-PRC.rpt"));
                                                                                        rd.DataSourceConnections.Clear();
                                                                                        rd.SetDataSource(new[] { PRCReportView });
                                                                                        rd.Subreports[0].DataSourceConnections.Clear();
                                                                                        rd.Subreports[0].SetDataSource(PRCSubReportSource);
                                                                                    }
                                                                                    if (Entity.PaymentType != "0")
                                                                                    {
                                                                                        if (Entity.sPRC)
                                                                                        {
                                                                                            rd.SetParameterValue("PaymentCode", Entity.PaymentType);
                                                                                            rd.SetParameterValue("S-PRCNumber", Request.PRL_UNIQUE_NO);
                                                                                            rd.SetParameterValue("RequestIssuanceDate", DateTime.Now);
                                                                                            rd.SetParameterValue("FromDateParm", Entity.FROM_DATE);
                                                                                            rd.SetParameterValue("ToDateParm", Entity.TO_DATE);
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if (Entity.sPRC)
                                                                                        {
                                                                                            rd.SetParameterValue("PaymentCode", "J/O-3");
                                                                                            rd.SetParameterValue("S-PRCNumber", Request.PRL_UNIQUE_NO);
                                                                                            rd.SetParameterValue("RequestIssuanceDate", DateTime.Now);
                                                                                            rd.SetParameterValue("FromDateParm", Entity.FROM_DATE);
                                                                                            rd.SetParameterValue("ToDateParm", Entity.TO_DATE);
                                                                                        }
                                                                                    }
                                                                                    mail.Attachments.Add(new Attachment(rd.ExportToStream(ExportFormatType.PortableDocFormat), FileName + ".pdf"));

                                                                                    mail.IsBodyHtml = true;
                                                                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                                                                    {
                                                                                        smtp.Credentials = NetworkCredentials;
                                                                                        //if dev
                                                                                        //smtp.EnableSsl = true;
                                                                                        smtp.Send(mail);
                                                                                    }
                                                                                    message = "Email has been sent successfully.";
                                                                                    PRC_REQUEST_LOG Update = context.PRC_REQUEST_LOG.Where(m => m.PRL_ID == Request.PRL_ID).FirstOrDefault();
                                                                                    if (Update != null)
                                                                                    {
                                                                                        Update.PRL_IS_SUCCESS = true;
                                                                                        Update.PRL_RESPONSE = message;
                                                                                        context.Entry(Update).State = System.Data.Entity.EntityState.Modified;
                                                                                        context.SaveChanges();
                                                                                    }
                                                                                    else
                                                                                        message = "Problem while updating response in logs";
                                                                                    return View();
                                                                                }

                                                                            }
                                                                            else
                                                                                message = "No PRC Found on the selectd Start Date : " + fromdate + " End Date : " + todate;
                                                                        }
                                                                        else
                                                                        {
                                                                            message = "Please select atleast one option from ePRC or SPRC";
                                                                            rd.Close();
                                                                            rd.Dispose();
                                                                            return View();
                                                                        }
                                                                    }
                                                                    else
                                                                        message = "No Client found on the given Account Number : " + Entity.IBAN_NO;
                                                                }
                                                                else
                                                                    message = "GCAR Request was success, problem occurred while updating client information to database.";
                                                                /// Add This Client To Database
                                                            }
                                                            else
                                                                message = "GCAR Request was success but problem while fetching email address for the new client";                                                          
#if prod
                                                        }
                                                        else
                                                        {
                                                            message = "Customer Account Validation Failed. Hence email will not be sent.";
                                                        }
                                                    }
                                                    else
                                                        message = "Problem Occurred while fetching party Id for Client validation";
                                                }
                                                else
                                                {
                                                    message = "Error While Generating new Unique number";
                                                }
                                            }
                                            else
                                            {
                                                message = "unable to find file @ " + CertPath;
                                            }
                                        }
                                        else
                                        {
                                            message = "cant find a pfx file under client certificates folder";
                                        }
                                    }
                                    else
                                    {
                                        message = "no file exist in the client certificates";
                                    }
                                }
                                else
                                {
                                    message = "Client certificates directory does not exist on the server";
                                }
                            }
                            else
                            {
                                message = "Error occured while fetching system settings, Please contact support desk.";
                            }
#endif
                        }
                        else
                            message = "No Email Configration found." + Environment.NewLine;
                    }
                    else
                    {
                        if (Entity.FROM_DATE == null)
                            message = "Select From Date to continue";
                        else if (Entity.TO_DATE == null)
                            message = "Select To Date to continue";
                        else if (Entity.IBAN_NO == null)
                            message = "Enter Account Number to continue";
                        else
                            message = "From Date and To Date are required";
                    }
                    //message = "Oops something went wrong!!";
                }
                catch (Exception ex)
                {
                    message = DAL.LogException("PRC", "Index", Session["USER_ID"].ToString(), ex) + Environment.NewLine;
                    if (ex.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.Message + Environment.NewLine;
                        if (ex.InnerException.InnerException != null)
                            message += "Inner Exception Details : " + ex.InnerException.InnerException + Environment.NewLine;
                    }
                    if (Entity.sPRC)
                    {
                        if (RequestLogID != 0)
                        {
                            PRC_REQUEST_LOG Update = context.PRC_REQUEST_LOG.Where(m => m.PRL_ID == RequestLogID).FirstOrDefault();
                            if (Update != null)
                            {
                                Update.PRL_RESPONSE = message;
                                context.Entry(Update).State = System.Data.Entity.EntityState.Modified;
                                context.SaveChanges();
                            }
                            else
                                message += "Problem while updating response in logs";
                        }
                    }
                    else if (Entity.ePRC)
                    {
                        if (RequestLogIDs.Count > 0)
                        {
                            foreach (int LogRequestId in RequestLogIDs)
                            {
                                PRC_REQUEST_LOG Update = context.PRC_REQUEST_LOG.Where(m => m.PRL_ID == LogRequestId).FirstOrDefault();
                                if (Update != null)
                                {
                                    Update.PRL_IS_SUCCESS = false;
                                    Update.PRL_RESPONSE = message;
                                    context.Entry(Update).State = System.Data.Entity.EntityState.Modified;
                                    context.SaveChanges();
                                }
                                else
                                    message = "Problem while updating response in logs on : " + LogRequestId + " ."; 
                            }
                        }
                    }
                }
                finally
                {
                    List<SelectListItem> PaymentType = new SelectList(context.PRC_PAYMENT_TYPE.ToList(), "PPT_NAME", "PPT_NAME", 0).ToList();
                    PaymentType.Insert(0, (new SelectListItem { Text = "--Select Payment Type--", Value = "0" }));
                    ViewBag.PaymentType = PaymentType;
                    ViewBag.message = message;
                }
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "fxdig");
            }
        }
        [HttpPost]
        public JsonResult GetPRCUrls()
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                PRC_SYSYTEM_SETUP Entity = context.PRC_SYSYTEM_SETUP.Where(m => m.PSS_ISAUTH == true).FirstOrDefault();
                if (Entity != null)
                {
                    string ChangePassword = Entity.PSS_CHANGE_PASSWORD_CITI_DIRECT;
                    string Logout = Entity.PSS_LOGOUT_CITI_DIRECT;
                    var coverletters = new
                    {
                        PRCCP = ChangePassword,
                        PRCLogout = Logout
                    };
                    return Json(coverletters, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json("");
            }
            else
            {
                return Json("Invalid Request");
            }
        }

        public string GetClientNameFromEmail(string Email = "")
        {
            string Client_Name = "";
            string UserName = (Email).Split('@')[0];
            Client_Name = System.Text.RegularExpressions.Regex.Replace(UserName, @"[^a-zA-Z]+", " ");
            return Client_Name;
        }


    }
}