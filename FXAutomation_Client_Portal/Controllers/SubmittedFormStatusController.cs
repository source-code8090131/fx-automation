﻿using FXAutomation_Client_Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FXAutomation_Client_Portal.Controllers
{
    [CustomRequestFilter]
    public class SubmittedFormStatusController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();
        // GET: SubmittedFormStatus
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult SubmittedCaseForm()
        {
            if (Session["USER_ID"] != null)
            {
                //string UserEmail = Session["USER_ID"].ToString();
                string UserPartyId = Session["USER_PARTY_ID"].ToString();
                var PartialData = context.ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW.Where(m => m.SCFM_PARTY_ID == UserPartyId).OrderByDescending(m => m.SCFM_ID).ToList();
                return View(PartialData);
            }
            else
            {
                return RedirectToAction("Logout", "fxdig");
            }
        }
        [OutputCache(Duration = 10, VaryByParam = "*")]
        public PartialViewResult ViewSubmittedCaseOnModal()
        {
            if (Session["USER_ID"] != null)
            {

                string UserEmail = Session["USER_ID"].ToString();
                string Party_ID = Session["USER_PARTY_ID"].ToString();
                ICORE_CLIENT_PORTFOLIO GetClientData = context.ICORE_CLIENT_PORTFOLIO.Where(m => m.CP_EMAIL == UserEmail && m.CP_PARTY_ID == Party_ID).FirstOrDefault();
                if (GetClientData != null)
                {
                    ViewBag.CP_CONTACT = GetClientData.CP_CONTACT == null ? "" : GetClientData.CP_CONTACT;
                    ViewBag.CP_NAME = GetClientData.CP_NAME == null ? "" : GetClientData.CP_NAME;
                }
                var PartialData = context.ICORE_SUBMIT_CASE_FORM_FIELD_MASTER_VIEW.Where(m => m.SCFM_INSERTED_BY == UserEmail).OrderByDescending(m => m.SCFM_ID).ToList();
                return PartialView(PartialData);
            }
            else
            {
                return PartialView();
            }
        }
        public PartialViewResult LoadPreviousMasterCaseComments(string CaseNo)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                List<ICORE_SUBMIT_CASE_COMMENTS> ViewData = context.ICORE_SUBMIT_CASE_COMMENTS.Where(m => m.SCC_CASE_NO == CaseNo).OrderByDescending(m => m.SCC_ID).ToList();
                return PartialView(ViewData);
            }
            else
            {
                return PartialView();
            }
        }
        [HttpPost]
        public ActionResult AddCommentToMasterLog(string CaseNo, string comment)
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                string message = "";
                if (comment != null && comment != "" && comment.Length <= 999)
                {
                    ICORE_SUBMIT_CASE_COMMENTS log = new ICORE_SUBMIT_CASE_COMMENTS();
                    log.SCC_ID = Convert.ToInt32(context.ICORE_SUBMIT_CASE_COMMENTS.Max(m => (int?)m.SCC_ID)) + 1;
                    log.SCC_CASE_NO = CaseNo;
                    log.SCC_COMMENT = comment;
                    log.SCC_ADDED_BY = Session["USER_ID"].ToString();
                    log.SCC_ADDED_DATE_TIME = DateTime.Now;
                    log.SCC_IS_AD = false;
                    context.ICORE_SUBMIT_CASE_COMMENTS.Add(log);
                    int rowCount = context.SaveChanges();
                    if (rowCount > 0)
                    {
                        ICORE_SUBMIT_CASE_FORM_MASTER Master = new ICORE_SUBMIT_CASE_FORM_MASTER();
                        Master = context.ICORE_SUBMIT_CASE_FORM_MASTER.Where(m => m.SCFM_CASE_NO == CaseNo).FirstOrDefault();
                        if (Master != null)
                        {
                            Master.SCFM_COMMENT = comment;
                            context.Entry(Master).State = System.Data.Entity.EntityState.Modified;
                            rowCount = context.SaveChanges();
                            if (rowCount > 0)
                                return Json("Comment Added Successfully");
                            else
                                return Json("Problem while processing");
                        }
                        else
                            return Json("Unable to fetech form details against Case No: " + CaseNo);
                    }
                    else
                        return Json("Problem while processing");
                }
                else
                {
                    if (comment == "" || comment == null)
                        message = "Enter comment against the Case No:" + CaseNo;
                    else
                        message = "The maximum length for comment is 999";
                    return Json(message);
                }
            }
            else
                return Json("Invalid request");
        }
    }
}