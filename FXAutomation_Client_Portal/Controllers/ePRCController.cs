﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using FXAutomation_Client_Portal.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace FXAutomation_Client_Portal.Controllers
{
    public class ePRCController : Controller
    {
        FXDIGEntities context = new FXDIGEntities();

        [CustomRequestFilterEPRC]
        // GET: ePRC
        public ActionResult Index()
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                try
                {
                    RequestPRC Entity = new RequestPRC();
                    Entity.ePRC = true;
                    Entity.DATE_OF_ISSUANCE= null;
                    Entity.EMAIL_ADDRESS = Session["USER_ID"].ToString();
                    return View(Entity);
                }
                catch (Exception ex)
                {
                    message = DAL.LogException("ePRC", "Index", Session["USER_ID"].ToString(), ex);
                }
                finally
                {
                    List<SelectListItem> Agency = new SelectList(context.PRC_VERIFYING_AGENCY.Where(m => m.PVA_STATUS == true && m.PVA_ISAUTH == true).ToList(), "PVA_NAME", "PVA_NAME", 0).ToList();
                    Agency.Insert(0, (new SelectListItem { Text = "--Select Verifying Agency--", Value = "0" }));
                    ViewBag.VERIFICAION_AGENCY = Agency;
                    List<SelectListItem> Purpose = new SelectList(context.PRC_PURPOSE_OF_VERIFICATION.Where(m => m.PPV_STATUS == true && m.PPV_ISAUTH == true).ToList(), "PPV_NAME", "PPV_NAME", 0).ToList();
                    Purpose.Insert(0, (new SelectListItem { Text = "--Select Verification Purpose--", Value = "0" }));
                    ViewBag.PURPOSE_OF_VERIFICATION = Purpose;
                    List<SelectListItem> Department = new SelectList(context.PRC_DEPARTMENT.Where(m => m.PD_STATUS == true && m.PD_ISAUTH == true).ToList(), "PD_NAME", "PD_NAME", 0).ToList();
                    Department.Insert(0, (new SelectListItem { Text = "--Select Department--", Value = "0" }));
                    ViewBag.DEPARTMENT_FIELD_FORMATION = Department;
                    ViewBag.message = message;
                }
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "fxdig");
            }
        }
        [HttpPost]
        public ActionResult Index(RequestPRC Entity)
        {
            if (Session["USER_ID"] != null)
            {
                string message = "";
                int RequestLogID = 0;
                try
                {
                    if (Entity.UNIQUE_ID_NO == null)
                    {
                        message = "Enter e-PRC/s-PRC Unique ID Nubmer to continue";
                        return View();
                    }
                    if (Entity.ePRC == true)
                    {
                        if (Entity.AMOUNT_IN_PKR == null)
                        {
                            message = "Amount (in PKR) is required";
                            return View();
                        }
                    }
                    if (Entity.DATE_OF_ISSUANCE != null && Entity.VERIFICAION_AGENCY != "0" && Entity.DEPARTMENT_FIELD_FORMATION != "0" && Entity.VERIFYING_OFFICER != null && Entity.CNIC_NO != null && Entity.PURPOSE_OF_VERIFICATION != "0")
                    {
                        string IssuanceDate = "";
                        if (Entity.DATE_OF_ISSUANCE != null)
                            IssuanceDate = Convert.ToDateTime(Entity.DATE_OF_ISSUANCE, System.Globalization.CultureInfo.CurrentCulture).ToString("dd MMMM yyyy");
                        PRC_EMAIL_CONFIGRATION CheckEmail = context.PRC_EMAIL_CONFIGRATION.Where(m => m.EC_STATUS == true && m.EC_IS_AUTH == true).FirstOrDefault();
                        if (CheckEmail != null)
                        {
                            ReportDocument rd = new ReportDocument();
                            PRC_AGENT_PORTOLIO GetAgentDetail = context.PRC_AGENT_PORTOLIO.Where(m => m.PAP_CNIC_NO == Entity.CNIC_NO && m.PAP_EMAIL_ADDRESS == Entity.EMAIL_ADDRESS).FirstOrDefault();
                            if (GetAgentDetail != null)
                            {
                                if (Entity.ePRC)
                                {
                                    PRC_REQUEST_LOG CheckLOgCitiDirect = context.PRC_REQUEST_LOG.Where(m => m.PRL_UNIQUE_NO == Entity.UNIQUE_ID_NO && System.Data.Entity.DbFunctions.TruncateTime(m.PRL_ISSUANCE_DATE) == System.Data.Entity.DbFunctions.TruncateTime(Entity.DATE_OF_ISSUANCE)).FirstOrDefault();
                                    if (CheckLOgCitiDirect != null)
                                    {
                                        List<PRC_SEND_EMAIL_WITH_ePRC_sPRC_Result> PRCData = new List<PRC_SEND_EMAIL_WITH_ePRC_sPRC_Result>();
                                        PRCData = context.PRC_SEND_EMAIL_WITH_ePRC_sPRC(CheckLOgCitiDirect.PRL_FROM_DATE, CheckLOgCitiDirect.PRL_TO_DATE, CheckLOgCitiDirect.PRL_UNIQUE_NO,Entity.AMOUNT_IN_PKR, CheckLOgCitiDirect.PRL_IBAN_NO, "ePRC").ToList();
                                        if (PRCData.Count > 0)
                                        {
                                            PRCData = PRCData.Where(m => m.GPD_MAKER_ID != Session["USER_ID"].ToString() || m.GPD_ISAUTH == true).ToList();
                                            if (PRCData.Count <= 200)
                                            {
                                                #region Log e-PRC Request
                                                PRC_EPRC_REQUEST_LOG Request = new PRC_EPRC_REQUEST_LOG();
                                                Request.PERL_ID = Convert.ToInt32(context.PRC_EPRC_REQUEST_LOG.Max(m => (decimal?)m.PERL_ID)) + 1;
                                                RequestLogID = Request.PERL_ID;
                                                Request.PERL_UNIQUE_NO = Entity.UNIQUE_ID_NO;
                                                Request.PERL_DATE_OF_ISSUE = Entity.DATE_OF_ISSUANCE;
                                                Request.PERL_AMOUNT = Entity.AMOUNT_IN_PKR;
                                                Request.PERL_AGENCY_NAME = Entity.VERIFICAION_AGENCY;
                                                Request.PERL_DEPARTMENT_NAME = Entity.DEPARTMENT_FIELD_FORMATION;
                                                Request.PERL_OFFICER_NAME = Entity.VERIFYING_OFFICER;
                                                Request.PERL_CNIC_NO = Entity.CNIC_NO;
                                                Request.PERL_VERIFICATION_PURPOSE = Entity.PURPOSE_OF_VERIFICATION;
                                                Request.PERL_EMAIL_ADDRESS = Entity.EMAIL_ADDRESS;
                                                Request.PERL_CNIC_NO = Entity.CNIC_NO;
                                                Request.PERL_REQUEST_TYPE = "ePRC";
                                                Request.PERL_REQUESTED_DATE = DateTime.Now;
                                                Request.PERL_IS_SUCCESS = false;
                                                context.PRC_EPRC_REQUEST_LOG.Add(Request);
                                                context.SaveChanges();
                                                #endregion

                                                using (MailMessage mail = new MailMessage())
                                                {
                                                    string ToEmail = GetAgentDetail.PAP_EMAIL_ADDRESS;
                                                    string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                                    string HostName = CheckEmail.EC_SEREVER_HOST;
                                                    int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                                    NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                                    NetworkCredentials.UserName = FromEmail;
                                                    //////#if dev
                                                    //NetworkCredentials.Password = "Test@@123";
                                                    mail.From = new MailAddress(FromEmail);
                                                    mail.To.Add(new MailAddress(ToEmail));
                                                    if (Entity.ePRC)
                                                    {
                                                        mail.Subject = "(Secure) PRC Verification - " + Entity.UNIQUE_ID_NO;
                                                        List<PRC_EPRC_REQUEST_LOG> CheckPrevReq = context.PRC_EPRC_REQUEST_LOG.Where(m => m.PERL_UNIQUE_NO == Entity.UNIQUE_ID_NO && m.PERL_REQUEST_TYPE == "ePRC").ToList();
                                                        if (CheckPrevReq.Count() > 1)
                                                        {
                                                            PRC_EPRC_REQUEST_LOG PrevReq = context.PRC_EPRC_REQUEST_LOG.Where(m => m.PERL_UNIQUE_NO == Entity.UNIQUE_ID_NO && m.PERL_REQUEST_TYPE == "ePRC").OrderByDescending(m => m.PERL_ID).Skip(1).Take(1).FirstOrDefault();
                                                            string LongRequestedDate = Convert.ToDateTime(PrevReq.PERL_REQUESTED_DATE).ToLongDateString();
                                                            mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Dear User,</p><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Please find attached here with the PRC (Proceed Realization Certificate) for transactions covering the period of " + IssuanceDate + " with respect to e-PRC Number " + Entity.UNIQUE_ID_NO + ".<br />  </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>The PRC " + PrevReq.PERL_UNIQUE_NO + " was last fetched on " + LongRequestedDate + " , From " + PrevReq.PERL_DEPARTMENT_NAME + " by " + PrevReq.PERL_OFFICER_NAME + ".<br /> </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>For any queries, please reach out to below contacts:<br /> </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>Email: citiservice.pakistan@citi.com <br /> </p><p style='font-family:Arial;font-size:13px'>Contact No: 021-111-777-777 </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'><b>Note</b>: This is a system generated email, Please don't reply. <br /> </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;' ><b> Disclaimer </b>: This email is confidential, may be legally privileged, and is for the intended recipient only.Access, disclosure, copying, distribution, or reliance on any of it by anyone else is prohibited and may be a criminal offence.Please delete if obtained in error and email confirmation to the sender.<br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                                        }
                                                        else
                                                            mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Dear User,</p><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Please find attached here with the PRC (Proceed Realization Certificate) for transactions covering the period of " + IssuanceDate + " with respect to e-PRC Number " + Entity.UNIQUE_ID_NO + ".<br />  </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>For any queries, please reach out to below contacts:<br /> </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>Email: citiservice.pakistan@citi.com <br /> </p><p style='font-family:Arial;font-size:13px'>Contact No: 021-111-777-777 </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'><b>Note</b>: This is a system generated email, Please don't reply. <br /> </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;' ><b> Disclaimer </b>: This email is confidential, may be legally privileged, and is for the intended recipient only.Access, disclosure, copying, distribution, or reliance on any of it by anyone else is prohibited and may be a criminal offence.Please delete if obtained in error and email confirmation to the sender.<br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                                    }
                                                    foreach (var PRC in PRCData)
                                                    {
                                                        string FileName = "EPRC_" + PRC.GPD_BANKREFNO.Replace(" ", "") + "_" + DateTime.Now.ToString("yyyyMMdd");
                                                        var PRCReportView = context.PRC_GOV_PRC_DETAIL_VIEW.Where(m => m.GPD_ID == PRC.GPD_ID).FirstOrDefault();
                                                        if (Entity.ePRC)
                                                        {
                                                            rd.Load(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "CRReport\\e-PRC.rpt"));
                                                            rd.DataSourceConnections.Clear();
                                                            rd.SetDataSource(new[] { PRCReportView });
                                                            rd.SetParameterValue("PaymentCode", "J/O-3");
                                                        }
                                                        mail.Attachments.Add(new Attachment(rd.ExportToStream(ExportFormatType.PortableDocFormat), FileName + ".pdf"));
                                                    }
                                                    mail.IsBodyHtml = true;
                                                    using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                                    {
                                                        smtp.Credentials = NetworkCredentials;
                                                        //if dev
                                                        //smtp.EnableSsl = true;
                                                        smtp.Send(mail);
                                                    }
                                                    message += "Email has been sent successfully.";
                                                    PRC_EPRC_REQUEST_LOG Update = context.PRC_EPRC_REQUEST_LOG.Where(m => m.PERL_ID == Request.PERL_ID).FirstOrDefault();
                                                    Update.PERL_IS_SUCCESS = true;
                                                    Update.PERL_RESPONSE = message;
                                                    context.Entry(Update).State = System.Data.Entity.EntityState.Modified;
                                                    context.SaveChanges();
                                                    rd.Close();
                                                    rd.Dispose();
                                                    return View();
                                                }
                                            }
                                            else
                                                message = "Requested data exceeds the maximum limit. Please reduce your request date range.";
                                        }
                                        else
                                        {
                                            message = "No record found on the provided data";
                                            rd.Close();
                                            rd.Dispose();
                                            return View();
                                        }
                                    }
                                    else
                                    {
                                        message = "no request found in the system from client with the unique ePRC # " + Entity.UNIQUE_ID_NO + " and issuance date " + Convert.ToDateTime(Entity.DATE_OF_ISSUANCE, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " .";
                                        rd.Close();
                                        rd.Dispose();
                                        return View();
                                    }
                                }
                                else if (Entity.sPRC)
                                {
                                    PRC_REQUEST_LOG CheckLOgCitiDirect = context.PRC_REQUEST_LOG.Where(m => m.PRL_UNIQUE_NO == Entity.UNIQUE_ID_NO && System.Data.Entity.DbFunctions.TruncateTime(m.PRL_ISSUANCE_DATE)  == System.Data.Entity.DbFunctions.TruncateTime(Entity.DATE_OF_ISSUANCE)).FirstOrDefault();
                                    if (CheckLOgCitiDirect != null)
                                    {
                                        PRC_SEND_EMAIL_WITH_ePRC_sPRC_Result PRCData = context.PRC_SEND_EMAIL_WITH_ePRC_sPRC(CheckLOgCitiDirect.PRL_FROM_DATE, CheckLOgCitiDirect.PRL_TO_DATE, Entity.UNIQUE_ID_NO, Entity.AMOUNT_IN_PKR, CheckLOgCitiDirect.PRL_IBAN_NO, "sPRC").FirstOrDefault();
                                        if (PRCData != null)
                                        {
                                            #region Log e-PRC Request
                                            PRC_EPRC_REQUEST_LOG Request = new PRC_EPRC_REQUEST_LOG();
                                            Request.PERL_ID = Convert.ToInt32(context.PRC_EPRC_REQUEST_LOG.Max(m => (decimal?)m.PERL_ID)) + 1;
                                            RequestLogID = Request.PERL_ID;
                                            Request.PERL_UNIQUE_NO = PRCData.GPD_PRC_NUMBER;
                                            Request.PERL_DATE_OF_ISSUE = Entity.DATE_OF_ISSUANCE;
                                            Request.PERL_AMOUNT = Entity.AMOUNT_IN_PKR;
                                            Request.PERL_AGENCY_NAME = Entity.VERIFICAION_AGENCY;
                                            Request.PERL_DEPARTMENT_NAME = Entity.DEPARTMENT_FIELD_FORMATION;
                                            Request.PERL_OFFICER_NAME = Entity.VERIFYING_OFFICER;
                                            Request.PERL_CNIC_NO = Entity.CNIC_NO;
                                            Request.PERL_VERIFICATION_PURPOSE = Entity.PURPOSE_OF_VERIFICATION;
                                            Request.PERL_EMAIL_ADDRESS = Entity.EMAIL_ADDRESS;
                                            Request.PERL_CNIC_NO = Entity.CNIC_NO;
                                            Request.PERL_REQUEST_TYPE = "sPRC";
                                            Request.PERL_REQUESTED_DATE = DateTime.Now;
                                            Request.PERL_IS_SUCCESS = false;
                                            context.PRC_EPRC_REQUEST_LOG.Add(Request);
                                            context.SaveChanges();
                                            #endregion

                                            using (MailMessage mail = new MailMessage())
                                            {
                                                string ToEmail = GetAgentDetail.PAP_EMAIL_ADDRESS;
                                                string FromEmail = CheckEmail.EC_CREDENTIAL_ID;
                                                string HostName = CheckEmail.EC_SEREVER_HOST;
                                                int EmailPort = Convert.ToInt32(CheckEmail.EC_EMAIL_PORT);
                                                NetworkCredential NetworkCredentials = new System.Net.NetworkCredential();
                                                NetworkCredentials.UserName = FromEmail;
                                                //////#if dev
                                                //NetworkCredentials.Password = "Test@@123";
                                                mail.From = new MailAddress(FromEmail);
                                                mail.To.Add(new MailAddress(ToEmail));
                                                if (Entity.sPRC)
                                                {
                                                    mail.Subject = "(Secure) PRC Certificate Request - " + IssuanceDate;
                                                    List<PRC_EPRC_REQUEST_LOG> CheckPrevReq = context.PRC_EPRC_REQUEST_LOG.Where(m => m.PERL_UNIQUE_NO == Entity.UNIQUE_ID_NO && m.PERL_REQUEST_TYPE == "sPRC").ToList();
                                                    if (CheckPrevReq.Count() > 1)
                                                    {
                                                        PRC_EPRC_REQUEST_LOG PrevReq = context.PRC_EPRC_REQUEST_LOG.Where(m => m.PERL_UNIQUE_NO == Entity.UNIQUE_ID_NO && m.PERL_REQUEST_TYPE == "sPRC").OrderByDescending(m => m.PERL_ID).Skip(1).Take(1).FirstOrDefault();
                                                        string LongRequestedDate = Convert.ToDateTime(PrevReq.PERL_REQUESTED_DATE).ToLongDateString();
                                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Dear User,</p><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Please find attached herewith the s-PRC (Statement of Proceeds Realization Certificates) for transactions covering the period of " + IssuanceDate + " with respect to s-PRC Number " + Entity.UNIQUE_ID_NO + ".<br />  </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>The s-PRC " + PrevReq.PERL_UNIQUE_NO + " was last fetched on " + LongRequestedDate + " , From " + PrevReq.PERL_DEPARTMENT_NAME + " by " + PrevReq.PERL_OFFICER_NAME + ".<br /> </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>For any queries, please reach out to below contacts:<br /> </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>Email: citiservice.pakistan@citi.com <br /> </p><p style='font-family:Arial;font-size:13px'>Contact No: 021-111-777-777 </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'><b>Note</b>: This is a system generated email, Please don't reply. <br /> </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;' ><b> Disclaimer </b>: This email is confidential, may be legally privileged, and is for the intended recipient only.Access, disclosure, copying, distribution, or reliance on any of it by anyone else is prohibited and may be a criminal offence.Please delete if obtained in error and email confirmation to the sender.<br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                                    }
                                                    else
                                                        mail.Body = "<div class='row' style='background-color:#E9ECEF;'><div class='col-md-2'></div><div class='col-md-8'><div class='container' style='background-color:#FFFFFF'><div class='container-fluid'><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Dear User,</p><p style='font-family:Bahnschrift;font-weight:lighter;font-size:15px'>Please find attached herewith the s-PRC (Statement of Proceeds Realization Certificates) for transactions covering the period of " + IssuanceDate + " with respect to s-PRC Number " + Entity.UNIQUE_ID_NO + ".<br />  </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>For any queries, please reach out to below contacts:<br /> </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'>Email: citiservice.pakistan@citi.com <br /> </p><p style='font-family:Arial;font-size:13px'>Contact No: 021-111-777-777 </p>	<p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;'><b>Note</b>: This is a system generated email, Please don't reply. <br /> </p><p style='font-family:Bahnschrift;font-size:15px;font-weight:lighter;' ><b> Disclaimer </b>: This email is confidential, may be legally privileged, and is for the intended recipient only.Access, disclosure, copying, distribution, or reliance on any of it by anyone else is prohibited and may be a criminal offence.Please delete if obtained in error and email confirmation to the sender.<br /> </p></div></div></div><div class='col-md-2'></div></div>";
                                                }

                                                string FileName = "SPRC_Summary_" + DateTime.Now.ToString("yyyyMMdd");
                                                var PRCReportView = context.PRC_GOV_PRC_DETAIL_VIEW.Where(m => m.GPD_ID == PRCData.GPD_ID).FirstOrDefault();
                                                if (Entity.sPRC)
                                                {
                                                    var PRCSubReportSource = context.PRC_GOV_PRC_DETAIL_SUB_REPORT_PROC(CheckLOgCitiDirect.PRL_FROM_DATE, CheckLOgCitiDirect.PRL_TO_DATE,CheckLOgCitiDirect.PRL_IBAN_NO).ToList();
                                                    if (PRCSubReportSource.Count() > 0)
                                                        PRCSubReportSource = PRCSubReportSource.Where(m => m.GPD_MAKER_ID != Session["USER_ID"].ToString() || m.GPD_ISAUTH == true).ToList();
                                                    rd.Load(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "CRReport\\S-PRC.rpt"));
                                                    rd.DataSourceConnections.Clear();
                                                    rd.SetDataSource(new[] { PRCReportView });
                                                    rd.Subreports[0].DataSourceConnections.Clear();
                                                    rd.Subreports[0].SetDataSource(PRCSubReportSource);
                                                    rd.SetParameterValue("PaymentCode", "J/O-3");
                                                    rd.SetParameterValue("S-PRCNumber", CheckLOgCitiDirect.PRL_UNIQUE_NO);
                                                    rd.SetParameterValue("RequestIssuanceDate", CheckLOgCitiDirect.PRL_REQUEST_DATE);
                                                    rd.SetParameterValue("FromDateParm", CheckLOgCitiDirect.PRL_FROM_DATE);
                                                    rd.SetParameterValue("ToDateParm", CheckLOgCitiDirect.PRL_TO_DATE);
                                                }
                                                mail.Attachments.Add(new Attachment(rd.ExportToStream(ExportFormatType.PortableDocFormat), FileName + ".pdf"));
                                                mail.IsBodyHtml = true;
                                                using (SmtpClient smtp = new SmtpClient(HostName, EmailPort))
                                                {
                                                    smtp.Credentials = NetworkCredentials;
                                                    //if dev
                                                    //smtp.EnableSsl = true;
                                                    smtp.Send(mail);
                                                }
                                                message += "Email has been sent successfully.";
                                                PRC_EPRC_REQUEST_LOG Update = context.PRC_EPRC_REQUEST_LOG.Where(m => m.PERL_ID == Request.PERL_ID).FirstOrDefault();
                                                Update.PERL_IS_SUCCESS = true;
                                                Update.PERL_RESPONSE = message;
                                                context.Entry(Update).State = System.Data.Entity.EntityState.Modified;
                                                context.SaveChanges();
                                                rd.Close();
                                                rd.Dispose();
                                                return View();
                                            }
                                        }
                                        else
                                        {
                                            message = "No record found on the provided data";
                                            rd.Close();
                                            rd.Dispose();
                                            return View();
                                        }
                                            
                                    }
                                    else
                                    {
                                        message = "no request found in the system from client with the unique sPRC # " + Entity.UNIQUE_ID_NO + " and issuance date " + Convert.ToDateTime(Entity.DATE_OF_ISSUANCE, System.Globalization.CultureInfo.CurrentCulture).ToString("dd-MM-yyyy") + " .";
                                        rd.Close();
                                        rd.Dispose();
                                        return View();
                                    }
                                }
                                else
                                {
                                    message = "Please select atleast one option from ePRC or SPRC";
                                    rd.Close();
                                    rd.Dispose();
                                    return View();
                                }
                                rd.Close();
                                rd.Dispose();
                            }
                            else
                                message = "No Agent found against CNIC No : " + Entity.CNIC_NO + " in the system";
                        }
                        else
                            message = "No Email Configration found.";
                    }
                    else
                    {
                        if (Entity.DATE_OF_ISSUANCE == null)
                            message = "Select Date of Issuance to continue";
                        else if (Entity.VERIFICAION_AGENCY == "0")
                            message = "Select Verifying Agency to continue";
                        else if (Entity.DEPARTMENT_FIELD_FORMATION == "0")
                            message = "Select Department/Field Formation to continue";
                        else if (Entity.VERIFYING_OFFICER == null)
                            message = "Enter Name Of Verifying Officer to continue";
                        else if (Entity.CNIC_NO == null)
                            message = "Enter CNIC to continue";
                        else if (Entity.PURPOSE_OF_VERIFICATION == "0")
                            message = "Select Purpose of Verification to continue";
                        
                    }
                }
                catch (Exception ex)
                {
                    message = DAL.LogException("ePRC", "Index", Session["USER_ID"].ToString(), ex) + Environment.NewLine;
                    if (ex.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.Message + Environment.NewLine;
                        if (ex.InnerException.InnerException != null)
                            message += "Inner Exception Details : " + ex.InnerException.InnerException + Environment.NewLine;
                    }
                    if (RequestLogID != 0)
                    {
                        PRC_EPRC_REQUEST_LOG Update = context.PRC_EPRC_REQUEST_LOG.Where(m => m.PERL_ID == RequestLogID).FirstOrDefault();
                        Update.PERL_RESPONSE = message;
                        context.Entry(Update).State = System.Data.Entity.EntityState.Modified;
                        context.SaveChanges();
                    }
                }
                finally
                {
                    List<SelectListItem> Agency = new SelectList(context.PRC_VERIFYING_AGENCY.Where(m => m.PVA_STATUS == true && m.PVA_ISAUTH == true).ToList(), "PVA_NAME", "PVA_NAME", 0).ToList();
                    Agency.Insert(0, (new SelectListItem { Text = "--Select Verifying Agency--", Value = "0" }));
                    ViewBag.VERIFICAION_AGENCY = Agency;
                    List<SelectListItem> Purpose = new SelectList(context.PRC_PURPOSE_OF_VERIFICATION.Where(m => m.PPV_STATUS == true && m.PPV_ISAUTH == true).ToList(), "PPV_NAME", "PPV_NAME", 0).ToList();
                    Purpose.Insert(0, (new SelectListItem { Text = "--Select Verification Purpose--", Value = "0" }));
                    ViewBag.PURPOSE_OF_VERIFICATION = Purpose;
                    List<SelectListItem> Department = new SelectList(context.PRC_DEPARTMENT.Where(m => m.PD_STATUS == true && m.PD_ISAUTH == true).ToList(), "PD_NAME", "PD_NAME", 0).ToList();
                    Department.Insert(0, (new SelectListItem { Text = "--Select Department--", Value = "0" }));
                    ViewBag.DEPARTMENT_FIELD_FORMATION = Department;
                    ViewBag.message = message;
                }
                return View();
            }
            else
            {
                return RedirectToAction("Logout", "fxdig");
            }
        }
        [HttpPost]
        public JsonResult GetePRCUrls()
        {
            if (Session["USER_ID"] != null && Request.IsAjaxRequest())
            {
                PRC_SYSYTEM_SETUP Entity = context.PRC_SYSYTEM_SETUP.Where(m => m.PSS_ISAUTH == true).FirstOrDefault();
                if (Entity != null)
                {
                    string ChangePassword = Entity.PSS_CHANGE_PASSWORD_CITI_VELOCITY;
                    string Logout = Entity.PSS_LOGOUT_CITI_VELOCITY;
                    var coverletters = new
                    {
                        ePRCCP = ChangePassword,
                        ePRCLogout = Logout
                    };
                    return Json(coverletters, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json("");
            }
            else
            {
                return Json("Invalid Request");
            }
        }
    }
}