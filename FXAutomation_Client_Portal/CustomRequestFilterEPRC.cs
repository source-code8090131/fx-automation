﻿//#define DEV
#define PROD
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FXAutomation_Client_Portal
{
    public class CustomRequestFilterEPRC : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
#if DEV
            filterContext.HttpContext.Session["USER_ID"] = "laraib.shah.g69@gmail.com";
            filterContext.HttpContext.Session["USER_NAME"] = "Laraib Shah";
            filterContext.HttpContext.Session["USER_PARTY_ID"] = "82014465";
            base.OnActionExecuting(filterContext);
            return;
#endif

#if PROD
            try
            {
                System.Collections.Specialized.NameValueCollection headers = filterContext.HttpContext.Request.Headers;
                string userid = "";
                string sigtime = "";
                string sig = "";
                string sigkey = "";
                if (headers["x-citiportal-LoginID"] != null)
                    userid = headers["x-citiportal-LoginID"].ToString();
                else
                {
                    filterContext.HttpContext.Session["ERRORH"] = "Insuficient Headers Or Invalid Request";
                    filterContext.HttpContext.Session["ERRORM"] = "Please try to Login Again from citidirect portal, " + Environment.NewLine + "Details : No header exist in the request for LoginID";
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "fxdig",
                        action = "Error"
                    }));
                    Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
                    base.OnActionExecuting(filterContext);
                    return;
                }
                if (headers["x-citiportal-sigkey"] != null)
                    sigkey = headers["x-citiportal-sigkey"].ToString();
                else
                {
                    filterContext.HttpContext.Session["ERRORH"] = "Insuficient Headers Or Invalid Request";
                    filterContext.HttpContext.Session["ERRORM"] = "Please try to Login Again from citidirect portal, " + Environment.NewLine + "Details : No header exist in the request for sig key";
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "fxdig",
                        action = "Error"
                    }));
                    Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
                    base.OnActionExecuting(filterContext);
                    return;
                }
                if (headers["x-citiportal-authtime"] != null)
                    sigtime = headers["x-citiportal-authtime"].ToString();
                else
                {
                    filterContext.HttpContext.Session["ERRORH"] = "Insuficient Headers Or Invalid Request";
                    filterContext.HttpContext.Session["ERRORM"] = "Please try to Login Again from citidirect portal, " + Environment.NewLine + "Details : No header exist in the request for sig time";
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "fxdig",
                        action = "Error"
                    }));
                    Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
                    base.OnActionExecuting(filterContext);
                    return;
                }
                if (headers["x-citiportal-sig"] != null)
                    sig = headers["x-citiportal-sig"].ToString();
                else
                {
                    filterContext.HttpContext.Session["ERRORH"] = "Insuficient Headers Or Invalid Request";
                    filterContext.HttpContext.Session["ERRORM"] = "Please try to Login Again from citidirect portal, " + Environment.NewLine + "Details : No header exist in the request for signature";
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "fxdig",
                        action = "Error"
                    }));
                    Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
                    base.OnActionExecuting(filterContext);
                    return;
                }

                ProxyAuthentication proxy = new ProxyAuthentication();
                if (proxy.VerifyData(userid, sig, sigkey, sigtime))
                {
                    if (headers["x-citiportal-emailaddr"] != null)
                    {
                        string email = headers["x-citiportal-emailaddr"].ToString();
                        //if (headers["x-citiportal-clientpartyid"] != null)
                        //{
                        //string partyId = headers["x-citiportal-clientpartyid"].ToString();
                        Models.FXDIGEntities context = new Models.FXDIGEntities();
                        Models.PRC_AGENT_PORTOLIO CheckAgentEmail = context.PRC_AGENT_PORTOLIO.Where(m => m.PAP_EMAIL_ADDRESS == email).FirstOrDefault();
                        if (CheckAgentEmail != null)
                        {
                            filterContext.HttpContext.Session["USER_ID"] = CheckAgentEmail.PAP_EMAIL_ADDRESS;
                            filterContext.HttpContext.Session["USER_NAME"] = CheckAgentEmail.PAP_NAME_OF_OFFICER;
                            Log(filterContext, "Success");
                        }
                        else
                        {
                            Models.PRC_AGENT_PORTOLIO UpdateEntity = new Models.PRC_AGENT_PORTOLIO();
                            UpdateEntity.PAP_ID = Convert.ToInt32(context.PRC_AGENT_PORTOLIO.Max(m => (int?)m.PAP_ID)) + 1;
                            UpdateEntity.PAP_EMAIL_ADDRESS = email;
                            UpdateEntity.PAP_NAME_OF_OFFICER = GetClientNameFromEmail(email);
                            UpdateEntity.PAP_ENTRY_DATETIME = DateTime.Now;
                            UpdateEntity.PAP_STATUS = true;
                            UpdateEntity.PAP_ISAUTH = true;
                            context.PRC_AGENT_PORTOLIO.Add(UpdateEntity);
                            int CheckResult = context.SaveChanges();
                            if (CheckResult > 0)
                            {
                                filterContext.HttpContext.Session["USER_ID"] = UpdateEntity.PAP_EMAIL_ADDRESS;
                                filterContext.HttpContext.Session["USER_NAME"] = UpdateEntity.PAP_NAME_OF_OFFICER;
                                Log(filterContext, "Success");
                            }
                            else
                            {
                                filterContext.HttpContext.Session["ERRORH"] = "Exception Occured";
                                filterContext.HttpContext.Session["ERRORM"] = "Please try to Login Again from citidirect portal, " + Environment.NewLine + "Details :  Unable to update client email";
                                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                                {
                                    controller = "fxdig",
                                    action = "Error"
                                }));
                                Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
                                base.OnActionExecuting(filterContext);
                                return;
                            }
                        }
                        //}
                        //else
                        //{
                        //    filterContext.HttpContext.Session["ERRORH"] = "Insuficient Headers Or Invalid Request";
                        //    filterContext.HttpContext.Session["ERRORM"] = "Please try to Login Again from citidirect portal, " + Environment.NewLine + "Details : No header exist in the request for partyId";
                        //    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                        //    {
                        //        controller = "fxdig",
                        //        action = "Error"
                        //    }));
                        //    Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
                        //    base.OnActionExecuting(filterContext);
                        //    return;
                        //}
                    }
                    else
                    {
                        filterContext.HttpContext.Session["ERRORH"] = "Insuficient Headers Or Invalid Request";
                        filterContext.HttpContext.Session["ERRORM"] = "Please try to Login Again from citidirect portal, " + Environment.NewLine + "Details : No header exist in the request for email";
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                        {
                            controller = "fxdig",
                            action = "Error"
                        }));
                        Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
                        base.OnActionExecuting(filterContext);
                        return;
                    }
                }
                else
                {
                    filterContext.HttpContext.Session["ERRORH"] = "Unable to Verify Signature";
                    filterContext.HttpContext.Session["ERRORM"] = "Please try to Login Again from citidirect portal, " + Environment.NewLine + "Details : The Proxy Authentication Method VerifySignedData returned false";
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "fxdig",
                        action = "Error"
                    }));
                    Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
                    base.OnActionExecuting(filterContext);
                    return;
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                filterContext.HttpContext.Session["ERRORH"] = "Exception Occured";
                filterContext.HttpContext.Session["ERRORM"] = message;
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "fxdig",
                    action = "Error"
                }));
                Log(filterContext, filterContext.HttpContext.Session["ERRORM"].ToString());
            }
#endif
        }

        public string GetClientNameFromEmail(string Email = "")
        {
            string Client_Name = "";
            string UserName = (Email).Split('@')[0];
            Client_Name = Regex.Replace(UserName, @"[^a-zA-Z]+", " ");
            return Client_Name;
        }

        public void Log(ActionExecutingContext requestcontext, string message)
        {
            Models.FXDIGEntities cont = new Models.FXDIGEntities();
            Models.PRC_CLIENT_REQUESTS_LOG log = new Models.PRC_CLIENT_REQUESTS_LOG();
            log.PCRL_DATETIME = DateTime.Now;
            string RequestRaw = "Request : " + Environment.NewLine;
            RequestRaw += "Request Method : " + requestcontext.HttpContext.Request.HttpMethod + ", RequestUri : " + requestcontext.HttpContext.Request.Url.ToString() + Environment.NewLine;
            string headers = "";
            foreach (string key in requestcontext.HttpContext.Request.Headers)
                headers += key + "=" + requestcontext.HttpContext.Request.Headers[key] + Environment.NewLine;
            RequestRaw += "Request Headers : { " + Environment.NewLine + headers + " }" + Environment.NewLine;
            string Resheaders = "";
            foreach (string key in requestcontext.HttpContext.Response.Headers)
                Resheaders += key + "=" + requestcontext.HttpContext.Response.Headers[key] + Environment.NewLine;
            RequestRaw += "Response Headers : { " + Environment.NewLine + Resheaders + " }" + Environment.NewLine;
            log.PCRL_REQUEST_RECIEVED = RequestRaw;
            log.PCRL_SYSTEM_RESPONSE = message;
            if (message == "Success")
                log.PCRL_STATUS = true;
            else
                log.PCRL_STATUS = false;
            cont.PRC_CLIENT_REQUESTS_LOG.Add(log);
            cont.SaveChanges();
        }

    }
}