﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;

namespace FXDIGService
{
    class DAL
    {
        public DAL()
        {

        }

        public SqlConnection GetSqlConnection()
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                string SERVER_PROP_NAME = ConfigurationManager.AppSettings["SERVER_PROP_NAME"];
                string SERVER_PROP_VALUE = ConfigurationManager.AppSettings["SERVER_PROP_VALUE"];
                string DATABASE_PROP_NAME = ConfigurationManager.AppSettings["DATABASE_PROP_NAME"];
                string DATABASE_PROP_VALUE = ConfigurationManager.AppSettings["DATABASE_PROP_VALUE"];
                conn.ConnectionString = SERVER_PROP_NAME + "=" + SERVER_PROP_VALUE + ";" + DATABASE_PROP_NAME + "=" + DATABASE_PROP_VALUE + ";" + "Trusted_Connection=True;MultipleActiveResultSets=true";
            }
            catch (Exception ex)
            {
                string Message = "";
                if (ex is SqlException)
                {
                    SqlException exx = (SqlException)ex;
                    for (int i = 0; i < exx.Errors.Count; i++)
                    {
                        Message += "Index #" + i + "\n" +
                            "Message: " + exx.Errors[i].Message + Environment.NewLine +
                            "LineNumber: " + exx.Errors[i].LineNumber + Environment.NewLine +
                            "Source: " + exx.Errors[i].Source + Environment.NewLine +
                            "Procedure: " + exx.Errors[i].Procedure + Environment.NewLine;
                    }
                    WriteToFile("Error On Connection : " + Message, false, EventLogEntryType.Information);
                }
                else
                {
                    WriteToFile("Error On Connection : " + ex.ToString(), false, EventLogEntryType.Information);
                }
            }

            return conn;
        }
        public void WriteToFile(string Message, bool WriteTolEvent, EventLogEntryType type)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\" + "FXDIGService" + "Log_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }

        public bool ValidateSqlConnection()
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                System.Threading.Thread.Sleep(5000);
                conn.Close();
                return true;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return false;
            }
            finally
            {
                if (message != "")
                {
                    WriteToFile(message, false, EventLogEntryType.Error);
                }
            }

        }

        public DataTable GetAllActiveUsers()
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                SqlCommand cmd = new SqlCommand("Select * from ICORE_LOGIN WHERE LOG_USER_STATUS = 'Active' AND LOG_USER_ID <> 'Admin123' AND LOG_ISAUTH = 1", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return new DataTable();
            }
            finally
            {
                if (message != "")
                {
                    WriteToFile(message, false, EventLogEntryType.Error);
                }
            }
        }


        public DataTable GetAllActiveUsersPRC()
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                SqlCommand cmd = new SqlCommand("Select * from PRC_LOGIN WHERE PLOG_USER_STATUS = 'Active' AND PLOG_USER_ID <> 'Admin123' AND PLOG_ISAUTH = 1", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return new DataTable();
            }
            finally
            {
                if (message != "")
                {
                    WriteToFile(message, false, EventLogEntryType.Error);
                }
            }
        }

        public List<int?> GetAssignedRoleIds(string USERID)
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                SqlCommand cmd = new SqlCommand("Select ALR_ROLE_ID from ICORE_ASSIGN_LOGIN_RIGHTS WHERE ALR_USER_ID = '" + USERID + "' and ALR_STATUS = 'true'", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                List<int?> Ids = new List<int?>();
                foreach (DataRow row in dt.Rows)
                {
                    int Id = Convert.ToInt32(row[0]);
                    Ids.Add(Id);
                }
                return Ids;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return new List<int?>();
            }
            finally
            {
                if (message != "")
                {
                    WriteToFile(message, false, EventLogEntryType.Error);
                }
            }
        }


        public List<int?> GetAssignedRoleIdsPRC(string USERID)
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT PALR_ROLE_ID from PRC_ASSIGN_LOGIN_RIGHTS WHERE PALR_USER_ID = '" + USERID + "' and PALR_STATUS = 'true'", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                List<int?> Ids = new List<int?>();
                foreach (DataRow row in dt.Rows)
                {
                    int Id = Convert.ToInt32(row[0]);
                    Ids.Add(Id);
                }
                return Ids;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return new List<int?>();
            }
            finally
            {
                if (message != "")
                {
                    WriteToFile(message, false, EventLogEntryType.Error);
                }
            }
        }


        public DataTable GetRoleInfoByRoleId(int RoleId)
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                SqlCommand cmd = new SqlCommand("Select * from ICORE_ROLES WHERE R_ID = '" + RoleId + "' AND R_ID <> 1", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return new DataTable();
            }
            finally
            {
                if (message != "")
                {
                    WriteToFile(message, false, EventLogEntryType.Error);
                }
            }
        }

        public DataTable GetRoleInfoByRoleIdPRC(int RoleId)
        {
            string message = "";
            try
            {
                SqlConnection conn = GetSqlConnection();
                conn.Open();
                SqlCommand cmd = new SqlCommand("Select * from PRC_ROLES WHERE PR_ID = '" + RoleId + "' AND PR_ID <> 2", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd.CommandText, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                return dt;
            }
            catch (Exception ex)
            {
                message = DateTime.Now.ToString() + Environment.NewLine;
                message += "Exception Occured on onStart" + ex.Message + Environment.NewLine;
                if (ex.InnerException != null)
                {
                    message += "Inner Exception : " + ex.InnerException.Message.ToString() + Environment.NewLine;
                    if (ex.InnerException.InnerException != null)
                    {
                        message += "Inner Exception : " + ex.InnerException.InnerException.Message.ToString() + Environment.NewLine;
                    }
                }
                return new DataTable();
            }
            finally
            {
                if (message != "")
                {
                    WriteToFile(message, false, EventLogEntryType.Error);
                }
            }
        }

    }
}
